// source: v1/order_private/user_access_private.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
var v1_order_model_user_access_pb = require('../../v1/order/model_user_access_pb.js');
goog.object.extend(proto, v1_order_model_user_access_pb);
goog.exportSymbol('proto.fcp.order.v1.order_private.SaveUserAccessRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_private.SaveUserAccessResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_private.SaveUserAccessRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_private.SaveUserAccessRequest.displayName = 'proto.fcp.order.v1.order_private.SaveUserAccessRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_private.SaveUserAccessResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_private.SaveUserAccessResponse.displayName = 'proto.fcp.order.v1.order_private.SaveUserAccessResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_private.SaveUserAccessRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_private.SaveUserAccessRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && v1_order_model_user_access_pb.UserAccess.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_private.SaveUserAccessRequest}
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_private.SaveUserAccessRequest;
  return proto.fcp.order.v1.order_private.SaveUserAccessRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_private.SaveUserAccessRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_private.SaveUserAccessRequest}
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_model_user_access_pb.UserAccess;
      reader.readMessage(value,v1_order_model_user_access_pb.UserAccess.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_private.SaveUserAccessRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_private.SaveUserAccessRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_order_model_user_access_pb.UserAccess.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.order.v1.order.UserAccess item = 1;
 * @return {?proto.fcp.order.v1.order.UserAccess}
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.order.v1.order.UserAccess} */ (
    jspb.Message.getWrapperField(this, v1_order_model_user_access_pb.UserAccess, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.UserAccess|undefined} value
 * @return {!proto.fcp.order.v1.order_private.SaveUserAccessRequest} returns this
*/
proto.fcp.order.v1.order_private.SaveUserAccessRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order_private.SaveUserAccessRequest} returns this
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order_private.SaveUserAccessRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_private.SaveUserAccessResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_private.SaveUserAccessResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_private.SaveUserAccessResponse}
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_private.SaveUserAccessResponse;
  return proto.fcp.order.v1.order_private.SaveUserAccessResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_private.SaveUserAccessResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_private.SaveUserAccessResponse}
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_private.SaveUserAccessResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_private.SaveUserAccessResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.SaveUserAccessResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


goog.object.extend(exports, proto.fcp.order.v1.order_private);
