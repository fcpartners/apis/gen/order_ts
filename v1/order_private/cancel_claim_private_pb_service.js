// package: fcp.order.v1.order_private
// file: v1/order_private/cancel_claim_private.proto

var v1_order_private_cancel_claim_private_pb = require("../../v1/order_private/cancel_claim_private_pb");
var v1_order_model_cancel_claim_pb = require("../../v1/order/model_cancel_claim_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var CancelClaimService = (function () {
  function CancelClaimService() {}
  CancelClaimService.serviceName = "fcp.order.v1.order_private.CancelClaimService";
  return CancelClaimService;
}());

CancelClaimService.GetCancelClaim = {
  methodName: "GetCancelClaim",
  service: CancelClaimService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_cancel_claim_pb.GetCancelClaimRequest,
  responseType: v1_order_model_cancel_claim_pb.GetCancelClaimResponse
};

CancelClaimService.ListCancelClaim = {
  methodName: "ListCancelClaim",
  service: CancelClaimService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_cancel_claim_pb.ListCancelClaimRequest,
  responseType: v1_order_model_cancel_claim_pb.ListCancelClaimResponse
};

CancelClaimService.RejectMultiCancelClaim = {
  methodName: "RejectMultiCancelClaim",
  service: CancelClaimService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimRequest,
  responseType: v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimResponse
};

CancelClaimService.DoneMultiCancelClaim = {
  methodName: "DoneMultiCancelClaim",
  service: CancelClaimService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimRequest,
  responseType: v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimResponse
};

exports.CancelClaimService = CancelClaimService;

function CancelClaimServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

CancelClaimServiceClient.prototype.getCancelClaim = function getCancelClaim(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelClaimService.GetCancelClaim, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CancelClaimServiceClient.prototype.listCancelClaim = function listCancelClaim(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelClaimService.ListCancelClaim, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CancelClaimServiceClient.prototype.rejectMultiCancelClaim = function rejectMultiCancelClaim(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelClaimService.RejectMultiCancelClaim, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CancelClaimServiceClient.prototype.doneMultiCancelClaim = function doneMultiCancelClaim(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelClaimService.DoneMultiCancelClaim, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.CancelClaimServiceClient = CancelClaimServiceClient;

