// source: v1/order_private/offer_private.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_common_pb = require('../../v1/order/common_pb.js');
goog.object.extend(proto, v1_order_common_pb);
var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
var v1_order_model_offer_pb = require('../../v1/order/model_offer_pb.js');
goog.object.extend(proto, v1_order_model_offer_pb);
