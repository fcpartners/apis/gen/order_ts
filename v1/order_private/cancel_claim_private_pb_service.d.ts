// package: fcp.order.v1.order_private
// file: v1/order_private/cancel_claim_private.proto

import * as v1_order_private_cancel_claim_private_pb from "../../v1/order_private/cancel_claim_private_pb";
import * as v1_order_model_cancel_claim_pb from "../../v1/order/model_cancel_claim_pb";
import {grpc} from "@improbable-eng/grpc-web";

type CancelClaimServiceGetCancelClaim = {
  readonly methodName: string;
  readonly service: typeof CancelClaimService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_cancel_claim_pb.GetCancelClaimRequest;
  readonly responseType: typeof v1_order_model_cancel_claim_pb.GetCancelClaimResponse;
};

type CancelClaimServiceListCancelClaim = {
  readonly methodName: string;
  readonly service: typeof CancelClaimService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_cancel_claim_pb.ListCancelClaimRequest;
  readonly responseType: typeof v1_order_model_cancel_claim_pb.ListCancelClaimResponse;
};

type CancelClaimServiceRejectMultiCancelClaim = {
  readonly methodName: string;
  readonly service: typeof CancelClaimService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimRequest;
  readonly responseType: typeof v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimResponse;
};

type CancelClaimServiceDoneMultiCancelClaim = {
  readonly methodName: string;
  readonly service: typeof CancelClaimService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimRequest;
  readonly responseType: typeof v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimResponse;
};

export class CancelClaimService {
  static readonly serviceName: string;
  static readonly GetCancelClaim: CancelClaimServiceGetCancelClaim;
  static readonly ListCancelClaim: CancelClaimServiceListCancelClaim;
  static readonly RejectMultiCancelClaim: CancelClaimServiceRejectMultiCancelClaim;
  static readonly DoneMultiCancelClaim: CancelClaimServiceDoneMultiCancelClaim;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class CancelClaimServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getCancelClaim(
    requestMessage: v1_order_model_cancel_claim_pb.GetCancelClaimRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_claim_pb.GetCancelClaimResponse|null) => void
  ): UnaryResponse;
  getCancelClaim(
    requestMessage: v1_order_model_cancel_claim_pb.GetCancelClaimRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_claim_pb.GetCancelClaimResponse|null) => void
  ): UnaryResponse;
  listCancelClaim(
    requestMessage: v1_order_model_cancel_claim_pb.ListCancelClaimRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_claim_pb.ListCancelClaimResponse|null) => void
  ): UnaryResponse;
  listCancelClaim(
    requestMessage: v1_order_model_cancel_claim_pb.ListCancelClaimRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_claim_pb.ListCancelClaimResponse|null) => void
  ): UnaryResponse;
  rejectMultiCancelClaim(
    requestMessage: v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimResponse|null) => void
  ): UnaryResponse;
  rejectMultiCancelClaim(
    requestMessage: v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_private_cancel_claim_private_pb.RejectMultiCancelClaimResponse|null) => void
  ): UnaryResponse;
  doneMultiCancelClaim(
    requestMessage: v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimResponse|null) => void
  ): UnaryResponse;
  doneMultiCancelClaim(
    requestMessage: v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_private_cancel_claim_private_pb.DoneMultiCancelClaimResponse|null) => void
  ): UnaryResponse;
}

