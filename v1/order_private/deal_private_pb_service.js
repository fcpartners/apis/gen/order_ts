// package: fcp.order.v1.order_private
// file: v1/order_private/deal_private.proto

var v1_order_private_deal_private_pb = require("../../v1/order_private/deal_private_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_deal_pb = require("../../v1/order/model_deal_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var DealService = (function () {
  function DealService() {}
  DealService.serviceName = "fcp.order.v1.order_private.DealService";
  return DealService;
}());

DealService.ListDealProcess = {
  methodName: "ListDealProcess",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealProcessType = {
  methodName: "ListDealProcessType",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealDeliveryCondition = {
  methodName: "ListDealDeliveryCondition",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealPaymentCondition = {
  methodName: "ListDealPaymentCondition",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealCurrency = {
  methodName: "ListDealCurrency",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealCurrencyRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealCategory = {
  methodName: "ListDealCategory",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealBrand = {
  methodName: "ListDealBrand",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealProduct = {
  methodName: "ListDealProduct",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealQuantityType = {
  methodName: "ListDealQuantityType",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

DealService.ListDealNames = {
  methodName: "ListDealNames",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealNamesRequest,
  responseType: v1_order_model_deal_pb.ListDealNamesResponse
};

DealService.GetDeal = {
  methodName: "GetDeal",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.GetDealRequest,
  responseType: v1_order_model_deal_pb.GetDealResponse
};

DealService.ListDeal = {
  methodName: "ListDeal",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealRequest,
  responseType: v1_order_model_deal_pb.ListDealResponse
};

DealService.ListDealWrapped = {
  methodName: "ListDealWrapped",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealWrappedRequest,
  responseType: v1_order_model_deal_pb.ListDealWrappedResponse
};

DealService.ListDealAgent = {
  methodName: "ListDealAgent",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealAgentRequest,
  responseType: v1_order_model_deal_pb.ListDealAgentResponse
};

DealService.GetDealFile = {
  methodName: "GetDealFile",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.GetDealFileRequest,
  responseType: v1_order_model_deal_pb.GetDealFileResponse
};

DealService.ListDealFile = {
  methodName: "ListDealFile",
  service: DealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_deal_pb.ListDealFileRequest,
  responseType: v1_order_model_deal_pb.ListDealFileResponse
};

exports.DealService = DealService;

function DealServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

DealServiceClient.prototype.listDealProcess = function listDealProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealProcessType = function listDealProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealDeliveryCondition = function listDealDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealPaymentCondition = function listDealPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealCurrency = function listDealCurrency(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealCurrency, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealCategory = function listDealCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealBrand = function listDealBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealProduct = function listDealProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealQuantityType = function listDealQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealNames = function listDealNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.getDeal = function getDeal(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.GetDeal, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDeal = function listDeal(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDeal, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealWrapped = function listDealWrapped(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealWrapped, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealAgent = function listDealAgent(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealAgent, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.getDealFile = function getDealFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.GetDealFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DealServiceClient.prototype.listDealFile = function listDealFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DealService.ListDealFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.DealServiceClient = DealServiceClient;

