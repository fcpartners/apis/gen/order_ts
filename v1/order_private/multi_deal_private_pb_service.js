// package: fcp.order.v1.order_private
// file: v1/order_private/multi_deal_private.proto

var v1_order_private_multi_deal_private_pb = require("../../v1/order_private/multi_deal_private_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_multi_deal_pb = require("../../v1/order/model_multi_deal_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MultiDealService = (function () {
  function MultiDealService() {}
  MultiDealService.serviceName = "fcp.order.v1.order_private.MultiDealService";
  return MultiDealService;
}());

MultiDealService.ListMultiDealProcessType = {
  methodName: "ListMultiDealProcessType",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.ListMultiDealProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiDealService.GetMultiDeal = {
  methodName: "GetMultiDeal",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.GetMultiDealRequest,
  responseType: v1_order_model_multi_deal_pb.GetMultiDealResponse
};

MultiDealService.ListMultiDeal = {
  methodName: "ListMultiDeal",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.ListMultiDealRequest,
  responseType: v1_order_model_multi_deal_pb.ListMultiDealResponse
};

MultiDealService.GetMultiDealFile = {
  methodName: "GetMultiDealFile",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.GetMultiDealFileRequest,
  responseType: v1_order_model_multi_deal_pb.GetMultiDealFileResponse
};

MultiDealService.ListMultiDealFile = {
  methodName: "ListMultiDealFile",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.ListMultiDealFileRequest,
  responseType: v1_order_model_multi_deal_pb.ListMultiDealFileResponse
};

MultiDealService.GetMultiDealContact = {
  methodName: "GetMultiDealContact",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.GetMultiDealContactRequest,
  responseType: v1_order_model_multi_deal_pb.GetMultiDealContactResponse
};

MultiDealService.ListMultiDealContact = {
  methodName: "ListMultiDealContact",
  service: MultiDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_deal_pb.ListMultiDealContactRequest,
  responseType: v1_order_model_multi_deal_pb.ListMultiDealContactResponse
};

exports.MultiDealService = MultiDealService;

function MultiDealServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MultiDealServiceClient.prototype.listMultiDealProcessType = function listMultiDealProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.ListMultiDealProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiDealServiceClient.prototype.getMultiDeal = function getMultiDeal(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.GetMultiDeal, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiDealServiceClient.prototype.listMultiDeal = function listMultiDeal(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.ListMultiDeal, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiDealServiceClient.prototype.getMultiDealFile = function getMultiDealFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.GetMultiDealFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiDealServiceClient.prototype.listMultiDealFile = function listMultiDealFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.ListMultiDealFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiDealServiceClient.prototype.getMultiDealContact = function getMultiDealContact(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.GetMultiDealContact, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiDealServiceClient.prototype.listMultiDealContact = function listMultiDealContact(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiDealService.ListMultiDealContact, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MultiDealServiceClient = MultiDealServiceClient;

