// package: fcp.order.v1.order_private
// file: v1/order_private/deal_private.proto

import * as v1_order_private_deal_private_pb from "../../v1/order_private/deal_private_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_deal_pb from "../../v1/order/model_deal_pb";
import {grpc} from "@improbable-eng/grpc-web";

type DealServiceListDealProcess = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealProcessType = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealCurrency = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealCurrencyRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealCategory = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealBrand = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealProduct = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealQuantityType = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type DealServiceListDealNames = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealNamesRequest;
  readonly responseType: typeof v1_order_model_deal_pb.ListDealNamesResponse;
};

type DealServiceGetDeal = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.GetDealRequest;
  readonly responseType: typeof v1_order_model_deal_pb.GetDealResponse;
};

type DealServiceListDeal = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealRequest;
  readonly responseType: typeof v1_order_model_deal_pb.ListDealResponse;
};

type DealServiceListDealWrapped = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealWrappedRequest;
  readonly responseType: typeof v1_order_model_deal_pb.ListDealWrappedResponse;
};

type DealServiceListDealAgent = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealAgentRequest;
  readonly responseType: typeof v1_order_model_deal_pb.ListDealAgentResponse;
};

type DealServiceGetDealFile = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.GetDealFileRequest;
  readonly responseType: typeof v1_order_model_deal_pb.GetDealFileResponse;
};

type DealServiceListDealFile = {
  readonly methodName: string;
  readonly service: typeof DealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_deal_pb.ListDealFileRequest;
  readonly responseType: typeof v1_order_model_deal_pb.ListDealFileResponse;
};

export class DealService {
  static readonly serviceName: string;
  static readonly ListDealProcess: DealServiceListDealProcess;
  static readonly ListDealProcessType: DealServiceListDealProcessType;
  static readonly ListDealDeliveryCondition: DealServiceListDealDeliveryCondition;
  static readonly ListDealPaymentCondition: DealServiceListDealPaymentCondition;
  static readonly ListDealCurrency: DealServiceListDealCurrency;
  static readonly ListDealCategory: DealServiceListDealCategory;
  static readonly ListDealBrand: DealServiceListDealBrand;
  static readonly ListDealProduct: DealServiceListDealProduct;
  static readonly ListDealQuantityType: DealServiceListDealQuantityType;
  static readonly ListDealNames: DealServiceListDealNames;
  static readonly GetDeal: DealServiceGetDeal;
  static readonly ListDeal: DealServiceListDeal;
  static readonly ListDealWrapped: DealServiceListDealWrapped;
  static readonly ListDealAgent: DealServiceListDealAgent;
  static readonly GetDealFile: DealServiceGetDealFile;
  static readonly ListDealFile: DealServiceListDealFile;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class DealServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listDealProcess(
    requestMessage: v1_order_model_deal_pb.ListDealProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealProcess(
    requestMessage: v1_order_model_deal_pb.ListDealProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealProcessType(
    requestMessage: v1_order_model_deal_pb.ListDealProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealProcessType(
    requestMessage: v1_order_model_deal_pb.ListDealProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealDeliveryCondition(
    requestMessage: v1_order_model_deal_pb.ListDealDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealDeliveryCondition(
    requestMessage: v1_order_model_deal_pb.ListDealDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealPaymentCondition(
    requestMessage: v1_order_model_deal_pb.ListDealPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealPaymentCondition(
    requestMessage: v1_order_model_deal_pb.ListDealPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealCurrency(
    requestMessage: v1_order_model_deal_pb.ListDealCurrencyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealCurrency(
    requestMessage: v1_order_model_deal_pb.ListDealCurrencyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealCategory(
    requestMessage: v1_order_model_deal_pb.ListDealCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealCategory(
    requestMessage: v1_order_model_deal_pb.ListDealCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealBrand(
    requestMessage: v1_order_model_deal_pb.ListDealBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealBrand(
    requestMessage: v1_order_model_deal_pb.ListDealBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealProduct(
    requestMessage: v1_order_model_deal_pb.ListDealProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealProduct(
    requestMessage: v1_order_model_deal_pb.ListDealProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealQuantityType(
    requestMessage: v1_order_model_deal_pb.ListDealQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealQuantityType(
    requestMessage: v1_order_model_deal_pb.ListDealQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listDealNames(
    requestMessage: v1_order_model_deal_pb.ListDealNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealNamesResponse|null) => void
  ): UnaryResponse;
  listDealNames(
    requestMessage: v1_order_model_deal_pb.ListDealNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealNamesResponse|null) => void
  ): UnaryResponse;
  getDeal(
    requestMessage: v1_order_model_deal_pb.GetDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.GetDealResponse|null) => void
  ): UnaryResponse;
  getDeal(
    requestMessage: v1_order_model_deal_pb.GetDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.GetDealResponse|null) => void
  ): UnaryResponse;
  listDeal(
    requestMessage: v1_order_model_deal_pb.ListDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealResponse|null) => void
  ): UnaryResponse;
  listDeal(
    requestMessage: v1_order_model_deal_pb.ListDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealResponse|null) => void
  ): UnaryResponse;
  listDealWrapped(
    requestMessage: v1_order_model_deal_pb.ListDealWrappedRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealWrappedResponse|null) => void
  ): UnaryResponse;
  listDealWrapped(
    requestMessage: v1_order_model_deal_pb.ListDealWrappedRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealWrappedResponse|null) => void
  ): UnaryResponse;
  listDealAgent(
    requestMessage: v1_order_model_deal_pb.ListDealAgentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealAgentResponse|null) => void
  ): UnaryResponse;
  listDealAgent(
    requestMessage: v1_order_model_deal_pb.ListDealAgentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealAgentResponse|null) => void
  ): UnaryResponse;
  getDealFile(
    requestMessage: v1_order_model_deal_pb.GetDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.GetDealFileResponse|null) => void
  ): UnaryResponse;
  getDealFile(
    requestMessage: v1_order_model_deal_pb.GetDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.GetDealFileResponse|null) => void
  ): UnaryResponse;
  listDealFile(
    requestMessage: v1_order_model_deal_pb.ListDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealFileResponse|null) => void
  ): UnaryResponse;
  listDealFile(
    requestMessage: v1_order_model_deal_pb.ListDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_deal_pb.ListDealFileResponse|null) => void
  ): UnaryResponse;
}

