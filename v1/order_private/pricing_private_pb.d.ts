// package: fcp.order.v1.order_private
// file: v1/order_private/pricing_private.proto

import * as jspb from "google-protobuf";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_pricing_pb from "../../v1/order/model_pricing_pb";

export class UpdatePriceListRequest extends jspb.Message {
  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getStatus(): v1_order_model_pricing_pb.Pricing.StatusMap[keyof v1_order_model_pricing_pb.Pricing.StatusMap];
  setStatus(value: v1_order_model_pricing_pb.Pricing.StatusMap[keyof v1_order_model_pricing_pb.Pricing.StatusMap]): void;

  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdatePriceListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdatePriceListRequest): UpdatePriceListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdatePriceListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdatePriceListRequest;
  static deserializeBinaryFromReader(message: UpdatePriceListRequest, reader: jspb.BinaryReader): UpdatePriceListRequest;
}

export namespace UpdatePriceListRequest {
  export type AsObject = {
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    status: v1_order_model_pricing_pb.Pricing.StatusMap[keyof v1_order_model_pricing_pb.Pricing.StatusMap],
    userId: string,
  }
}

export class UpdatePriceListResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdatePriceListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdatePriceListResponse): UpdatePriceListResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdatePriceListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdatePriceListResponse;
  static deserializeBinaryFromReader(message: UpdatePriceListResponse, reader: jspb.BinaryReader): UpdatePriceListResponse;
}

export namespace UpdatePriceListResponse {
  export type AsObject = {
  }
}

export class DeletePriceListRequest extends jspb.Message {
  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePriceListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePriceListRequest): DeletePriceListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePriceListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePriceListRequest;
  static deserializeBinaryFromReader(message: DeletePriceListRequest, reader: jspb.BinaryReader): DeletePriceListRequest;
}

export namespace DeletePriceListRequest {
  export type AsObject = {
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    userId: string,
  }
}

export class DeletePriceListResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePriceListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePriceListResponse): DeletePriceListResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePriceListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePriceListResponse;
  static deserializeBinaryFromReader(message: DeletePriceListResponse, reader: jspb.BinaryReader): DeletePriceListResponse;
}

export namespace DeletePriceListResponse {
  export type AsObject = {
  }
}

