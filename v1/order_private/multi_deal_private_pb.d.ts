// package: fcp.order.v1.order_private
// file: v1/order_private/multi_deal_private.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_multi_deal_pb from "../../v1/order/model_multi_deal_pb";

