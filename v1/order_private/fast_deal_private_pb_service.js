// package: fcp.order.v1.order_private
// file: v1/order_private/fast_deal_private.proto

var v1_order_private_fast_deal_private_pb = require("../../v1/order_private/fast_deal_private_pb");
var v1_order_model_fast_deal_pb = require("../../v1/order/model_fast_deal_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var FastDealService = (function () {
  function FastDealService() {}
  FastDealService.serviceName = "fcp.order.v1.order_private.FastDealService";
  return FastDealService;
}());

FastDealService.ListFastDealProcess = {
  methodName: "ListFastDealProcess",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealProcessType = {
  methodName: "ListFastDealProcessType",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealProductGroup = {
  methodName: "ListFastDealProductGroup",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealProductGroupRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealBrand = {
  methodName: "ListFastDealBrand",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealProduct = {
  methodName: "ListFastDealProduct",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealCurrency = {
  methodName: "ListFastDealCurrency",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealCurrencyRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealDeliveryCondition = {
  methodName: "ListFastDealDeliveryCondition",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealPaymentCondition = {
  methodName: "ListFastDealPaymentCondition",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealLogisticsOperator = {
  methodName: "ListFastDealLogisticsOperator",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealLogisticsOperatorRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealState = {
  methodName: "ListFastDealState",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealStateRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealCity = {
  methodName: "ListFastDealCity",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealCityRequest,
  responseType: v1_order_enum_pb.DictResponse
};

FastDealService.ListFastDealNames = {
  methodName: "ListFastDealNames",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealNamesRequest,
  responseType: v1_order_model_fast_deal_pb.ListFastDealNamesResponse
};

FastDealService.GetFastDeal = {
  methodName: "GetFastDeal",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.GetFastDealRequest,
  responseType: v1_order_model_fast_deal_pb.GetFastDealResponse
};

FastDealService.ListFastDeal = {
  methodName: "ListFastDeal",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealRequest,
  responseType: v1_order_model_fast_deal_pb.ListFastDealResponse
};

FastDealService.ListFastDealItemAgent = {
  methodName: "ListFastDealItemAgent",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealItemAgentRequest,
  responseType: v1_order_model_fast_deal_pb.ListFastDealItemAgentResponse
};

FastDealService.GetFastDealFile = {
  methodName: "GetFastDealFile",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.GetFastDealFileRequest,
  responseType: v1_order_model_fast_deal_pb.GetFastDealFileResponse
};

FastDealService.ListFastDealFile = {
  methodName: "ListFastDealFile",
  service: FastDealService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_fast_deal_pb.ListFastDealFileRequest,
  responseType: v1_order_model_fast_deal_pb.ListFastDealFileResponse
};

exports.FastDealService = FastDealService;

function FastDealServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

FastDealServiceClient.prototype.listFastDealProcess = function listFastDealProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealProcessType = function listFastDealProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealProductGroup = function listFastDealProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealBrand = function listFastDealBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealProduct = function listFastDealProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealCurrency = function listFastDealCurrency(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealCurrency, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealDeliveryCondition = function listFastDealDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealPaymentCondition = function listFastDealPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealLogisticsOperator = function listFastDealLogisticsOperator(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealLogisticsOperator, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealState = function listFastDealState(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealState, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealCity = function listFastDealCity(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealCity, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealNames = function listFastDealNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.getFastDeal = function getFastDeal(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.GetFastDeal, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDeal = function listFastDeal(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDeal, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealItemAgent = function listFastDealItemAgent(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealItemAgent, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.getFastDealFile = function getFastDealFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.GetFastDealFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

FastDealServiceClient.prototype.listFastDealFile = function listFastDealFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(FastDealService.ListFastDealFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.FastDealServiceClient = FastDealServiceClient;

