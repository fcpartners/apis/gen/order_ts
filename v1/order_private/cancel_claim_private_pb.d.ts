// package: fcp.order.v1.order_private
// file: v1/order_private/cancel_claim_private.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_model_cancel_claim_pb from "../../v1/order/model_cancel_claim_pb";

export class RejectMultiCancelClaimRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_cancel_claim_pb.CancelClaim>;
  setItemsList(value: Array<v1_order_model_cancel_claim_pb.CancelClaim>): void;
  addItems(value?: v1_order_model_cancel_claim_pb.CancelClaim, index?: number): v1_order_model_cancel_claim_pb.CancelClaim;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiCancelClaimRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiCancelClaimRequest): RejectMultiCancelClaimRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiCancelClaimRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiCancelClaimRequest;
  static deserializeBinaryFromReader(message: RejectMultiCancelClaimRequest, reader: jspb.BinaryReader): RejectMultiCancelClaimRequest;
}

export namespace RejectMultiCancelClaimRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_cancel_claim_pb.CancelClaim.AsObject>,
  }
}

export class RejectMultiCancelClaimResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiCancelClaimResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiCancelClaimResponse): RejectMultiCancelClaimResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiCancelClaimResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiCancelClaimResponse;
  static deserializeBinaryFromReader(message: RejectMultiCancelClaimResponse, reader: jspb.BinaryReader): RejectMultiCancelClaimResponse;
}

export namespace RejectMultiCancelClaimResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

export class DoneMultiCancelClaimRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_cancel_claim_pb.CancelClaim>;
  setItemsList(value: Array<v1_order_model_cancel_claim_pb.CancelClaim>): void;
  addItems(value?: v1_order_model_cancel_claim_pb.CancelClaim, index?: number): v1_order_model_cancel_claim_pb.CancelClaim;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiCancelClaimRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiCancelClaimRequest): DoneMultiCancelClaimRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiCancelClaimRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiCancelClaimRequest;
  static deserializeBinaryFromReader(message: DoneMultiCancelClaimRequest, reader: jspb.BinaryReader): DoneMultiCancelClaimRequest;
}

export namespace DoneMultiCancelClaimRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_cancel_claim_pb.CancelClaim.AsObject>,
  }
}

export class DoneMultiCancelClaimResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiCancelClaimResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiCancelClaimResponse): DoneMultiCancelClaimResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiCancelClaimResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiCancelClaimResponse;
  static deserializeBinaryFromReader(message: DoneMultiCancelClaimResponse, reader: jspb.BinaryReader): DoneMultiCancelClaimResponse;
}

export namespace DoneMultiCancelClaimResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

