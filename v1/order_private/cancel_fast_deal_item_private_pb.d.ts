// package: fcp.order.v1.order_private
// file: v1/order_private/cancel_fast_deal_item_private.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_model_cancel_fast_deal_item_pb from "../../v1/order/model_cancel_fast_deal_item_pb";

export class RejectMultiCancelFastDealItemRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem>;
  setItemsList(value: Array<v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem>): void;
  addItems(value?: v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem, index?: number): v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiCancelFastDealItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiCancelFastDealItemRequest): RejectMultiCancelFastDealItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiCancelFastDealItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiCancelFastDealItemRequest;
  static deserializeBinaryFromReader(message: RejectMultiCancelFastDealItemRequest, reader: jspb.BinaryReader): RejectMultiCancelFastDealItemRequest;
}

export namespace RejectMultiCancelFastDealItemRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem.AsObject>,
  }
}

export class RejectMultiCancelFastDealItemResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiCancelFastDealItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiCancelFastDealItemResponse): RejectMultiCancelFastDealItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiCancelFastDealItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiCancelFastDealItemResponse;
  static deserializeBinaryFromReader(message: RejectMultiCancelFastDealItemResponse, reader: jspb.BinaryReader): RejectMultiCancelFastDealItemResponse;
}

export namespace RejectMultiCancelFastDealItemResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

export class DoneMultiCancelFastDealItemRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem>;
  setItemsList(value: Array<v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem>): void;
  addItems(value?: v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem, index?: number): v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiCancelFastDealItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiCancelFastDealItemRequest): DoneMultiCancelFastDealItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiCancelFastDealItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiCancelFastDealItemRequest;
  static deserializeBinaryFromReader(message: DoneMultiCancelFastDealItemRequest, reader: jspb.BinaryReader): DoneMultiCancelFastDealItemRequest;
}

export namespace DoneMultiCancelFastDealItemRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_cancel_fast_deal_item_pb.CancelFastDealItem.AsObject>,
  }
}

export class DoneMultiCancelFastDealItemResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiCancelFastDealItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiCancelFastDealItemResponse): DoneMultiCancelFastDealItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiCancelFastDealItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiCancelFastDealItemResponse;
  static deserializeBinaryFromReader(message: DoneMultiCancelFastDealItemResponse, reader: jspb.BinaryReader): DoneMultiCancelFastDealItemResponse;
}

export namespace DoneMultiCancelFastDealItemResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

