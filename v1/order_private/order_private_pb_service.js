// package: fcp.order.v1.order_private
// file: v1/order_private/order_private.proto

var v1_order_private_order_private_pb = require("../../v1/order_private/order_private_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_order_pb = require("../../v1/order/model_order_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var OrderService = (function () {
  function OrderService() {}
  OrderService.serviceName = "fcp.order.v1.order_private.OrderService";
  return OrderService;
}());

OrderService.ListOrderProcess = {
  methodName: "ListOrderProcess",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderProcessType = {
  methodName: "ListOrderProcessType",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderDeliveryCondition = {
  methodName: "ListOrderDeliveryCondition",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderPaymentCondition = {
  methodName: "ListOrderPaymentCondition",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderCategory = {
  methodName: "ListOrderCategory",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderBrand = {
  methodName: "ListOrderBrand",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderProduct = {
  methodName: "ListOrderProduct",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderQuantityType = {
  methodName: "ListOrderQuantityType",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OrderService.ListOrderNames = {
  methodName: "ListOrderNames",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderNamesRequest,
  responseType: v1_order_model_order_pb.ListOrderNamesResponse
};

OrderService.GetOrder = {
  methodName: "GetOrder",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.GetOrderRequest,
  responseType: v1_order_model_order_pb.GetOrderResponse
};

OrderService.ListOrder = {
  methodName: "ListOrder",
  service: OrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_order_pb.ListOrderRequest,
  responseType: v1_order_model_order_pb.ListOrderResponse
};

exports.OrderService = OrderService;

function OrderServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

OrderServiceClient.prototype.listOrderProcess = function listOrderProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderProcessType = function listOrderProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderDeliveryCondition = function listOrderDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderPaymentCondition = function listOrderPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderCategory = function listOrderCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderBrand = function listOrderBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderProduct = function listOrderProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderQuantityType = function listOrderQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrderNames = function listOrderNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrderNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.getOrder = function getOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.GetOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OrderServiceClient.prototype.listOrder = function listOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OrderService.ListOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.OrderServiceClient = OrderServiceClient;

