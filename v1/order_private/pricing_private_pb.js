// source: v1/order_private/pricing_private.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
var v1_order_model_pricing_pb = require('../../v1/order/model_pricing_pb.js');
goog.object.extend(proto, v1_order_model_pricing_pb);
goog.exportSymbol('proto.fcp.order.v1.order_private.DeletePriceListRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_private.DeletePriceListResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_private.UpdatePriceListRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_private.UpdatePriceListResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_private.UpdatePriceListRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_private.UpdatePriceListRequest.displayName = 'proto.fcp.order.v1.order_private.UpdatePriceListRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_private.UpdatePriceListResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_private.UpdatePriceListResponse.displayName = 'proto.fcp.order.v1.order_private.UpdatePriceListResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_private.DeletePriceListRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_private.DeletePriceListRequest.displayName = 'proto.fcp.order.v1.order_private.DeletePriceListRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_private.DeletePriceListResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_private.DeletePriceListResponse.displayName = 'proto.fcp.order.v1.order_private.DeletePriceListResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_private.UpdatePriceListRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    process: jspb.Message.getFieldWithDefault(msg, 2, 0),
    type: jspb.Message.getFieldWithDefault(msg, 3, 0),
    status: jspb.Message.getFieldWithDefault(msg, 6, 0),
    userId: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListRequest}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_private.UpdatePriceListRequest;
  return proto.fcp.order.v1.order_private.UpdatePriceListRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListRequest}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = /** @type {!proto.fcp.order.v1.order.ProductProcess} */ (reader.readEnum());
      msg.setProcess(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.order.v1.order.ProductType} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 6:
      var value = /** @type {!proto.fcp.order.v1.order.Pricing.Status} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_private.UpdatePriceListRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getProcess();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      6,
      f
    );
  }
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * optional fcp.order.v1.order.ProductProcess process = 2;
 * @return {!proto.fcp.order.v1.order.ProductProcess}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.getProcess = function() {
  return /** @type {!proto.fcp.order.v1.order.ProductProcess} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.ProductProcess} value
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.setProcess = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional fcp.order.v1.order.ProductType type = 3;
 * @return {!proto.fcp.order.v1.order.ProductType}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.getType = function() {
  return /** @type {!proto.fcp.order.v1.order.ProductType} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.ProductType} value
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional fcp.order.v1.order.Pricing.Status status = 6;
 * @return {!proto.fcp.order.v1.order.Pricing.Status}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.getStatus = function() {
  return /** @type {!proto.fcp.order.v1.order.Pricing.Status} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.Pricing.Status} value
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 6, value);
};


/**
 * optional string user_id = 5;
 * @return {string}
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.UpdatePriceListRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_private.UpdatePriceListResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_private.UpdatePriceListResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListResponse}
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_private.UpdatePriceListResponse;
  return proto.fcp.order.v1.order_private.UpdatePriceListResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_private.UpdatePriceListResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_private.UpdatePriceListResponse}
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_private.UpdatePriceListResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_private.UpdatePriceListResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.UpdatePriceListResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_private.DeletePriceListRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_private.DeletePriceListRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    process: jspb.Message.getFieldWithDefault(msg, 1, 0),
    type: jspb.Message.getFieldWithDefault(msg, 2, 0),
    userId: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListRequest}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_private.DeletePriceListRequest;
  return proto.fcp.order.v1.order_private.DeletePriceListRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_private.DeletePriceListRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListRequest}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.order.v1.order.ProductProcess} */ (reader.readEnum());
      msg.setProcess(value);
      break;
    case 2:
      var value = /** @type {!proto.fcp.order.v1.order.ProductType} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_private.DeletePriceListRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_private.DeletePriceListRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getProcess();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional fcp.order.v1.order.ProductProcess process = 1;
 * @return {!proto.fcp.order.v1.order.ProductProcess}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.getProcess = function() {
  return /** @type {!proto.fcp.order.v1.order.ProductProcess} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.ProductProcess} value
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.setProcess = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional fcp.order.v1.order.ProductType type = 2;
 * @return {!proto.fcp.order.v1.order.ProductType}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.getType = function() {
  return /** @type {!proto.fcp.order.v1.order.ProductType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.ProductType} value
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional string user_id = 4;
 * @return {string}
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListRequest} returns this
 */
proto.fcp.order.v1.order_private.DeletePriceListRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_private.DeletePriceListResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_private.DeletePriceListResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListResponse}
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_private.DeletePriceListResponse;
  return proto.fcp.order.v1.order_private.DeletePriceListResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_private.DeletePriceListResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_private.DeletePriceListResponse}
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_private.DeletePriceListResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_private.DeletePriceListResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_private.DeletePriceListResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


goog.object.extend(exports, proto.fcp.order.v1.order_private);
