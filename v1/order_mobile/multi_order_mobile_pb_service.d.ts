// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/multi_order_mobile.proto

import * as v1_order_mobile_multi_order_mobile_pb from "../../v1/order_mobile/multi_order_mobile_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_multi_order_pb from "../../v1/order/model_multi_order_pb";
import * as v1_order_model_supplier_info_pb from "../../v1/order/model_supplier_info_pb";
import {grpc} from "@improbable-eng/grpc-web";

type MultiOrderServiceListMultiOrderProcess = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderProcessType = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderCategory = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderBrand = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderProduct = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderQuantityType = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiOrderServiceListMultiOrderNames = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderNamesRequest;
  readonly responseType: typeof v1_order_model_multi_order_pb.ListMultiOrderNamesResponse;
};

type MultiOrderServiceGetMultiOrder = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.GetMultiOrderRequest;
  readonly responseType: typeof v1_order_model_multi_order_pb.GetMultiOrderResponse;
};

type MultiOrderServiceListMultiOrder = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_order_pb.ListMultiOrderRequest;
  readonly responseType: typeof v1_order_model_multi_order_pb.ListMultiOrderResponse;
};

type MultiOrderServiceSaveMultiOrder = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderRequest;
  readonly responseType: typeof v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderResponse;
};

type MultiOrderServiceDoneMultiOrder = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderRequest;
  readonly responseType: typeof v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderResponse;
};

type MultiOrderServiceRejectMultiOrder = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderRequest;
  readonly responseType: typeof v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderResponse;
};

type MultiOrderServiceListSupplierInfo = {
  readonly methodName: string;
  readonly service: typeof MultiOrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_supplier_info_pb.ListSupplierInfoRequest;
  readonly responseType: typeof v1_order_model_supplier_info_pb.ListSupplierInfoResponse;
};

export class MultiOrderService {
  static readonly serviceName: string;
  static readonly ListMultiOrderProcess: MultiOrderServiceListMultiOrderProcess;
  static readonly ListMultiOrderProcessType: MultiOrderServiceListMultiOrderProcessType;
  static readonly ListMultiOrderDeliveryCondition: MultiOrderServiceListMultiOrderDeliveryCondition;
  static readonly ListMultiOrderPaymentCondition: MultiOrderServiceListMultiOrderPaymentCondition;
  static readonly ListMultiOrderCategory: MultiOrderServiceListMultiOrderCategory;
  static readonly ListMultiOrderBrand: MultiOrderServiceListMultiOrderBrand;
  static readonly ListMultiOrderProduct: MultiOrderServiceListMultiOrderProduct;
  static readonly ListMultiOrderQuantityType: MultiOrderServiceListMultiOrderQuantityType;
  static readonly ListMultiOrderNames: MultiOrderServiceListMultiOrderNames;
  static readonly GetMultiOrder: MultiOrderServiceGetMultiOrder;
  static readonly ListMultiOrder: MultiOrderServiceListMultiOrder;
  static readonly SaveMultiOrder: MultiOrderServiceSaveMultiOrder;
  static readonly DoneMultiOrder: MultiOrderServiceDoneMultiOrder;
  static readonly RejectMultiOrder: MultiOrderServiceRejectMultiOrder;
  static readonly ListSupplierInfo: MultiOrderServiceListSupplierInfo;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class MultiOrderServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listMultiOrderProcess(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderProcess(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderProcessType(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderProcessType(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderDeliveryCondition(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderDeliveryCondition(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderPaymentCondition(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderPaymentCondition(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderCategory(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderCategory(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderBrand(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderBrand(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderProduct(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderProduct(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderQuantityType(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderQuantityType(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiOrderNames(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_order_pb.ListMultiOrderNamesResponse|null) => void
  ): UnaryResponse;
  listMultiOrderNames(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_order_pb.ListMultiOrderNamesResponse|null) => void
  ): UnaryResponse;
  getMultiOrder(
    requestMessage: v1_order_model_multi_order_pb.GetMultiOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_order_pb.GetMultiOrderResponse|null) => void
  ): UnaryResponse;
  getMultiOrder(
    requestMessage: v1_order_model_multi_order_pb.GetMultiOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_order_pb.GetMultiOrderResponse|null) => void
  ): UnaryResponse;
  listMultiOrder(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_order_pb.ListMultiOrderResponse|null) => void
  ): UnaryResponse;
  listMultiOrder(
    requestMessage: v1_order_model_multi_order_pb.ListMultiOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_order_pb.ListMultiOrderResponse|null) => void
  ): UnaryResponse;
  saveMultiOrder(
    requestMessage: v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderResponse|null) => void
  ): UnaryResponse;
  saveMultiOrder(
    requestMessage: v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderResponse|null) => void
  ): UnaryResponse;
  doneMultiOrder(
    requestMessage: v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderResponse|null) => void
  ): UnaryResponse;
  doneMultiOrder(
    requestMessage: v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderResponse|null) => void
  ): UnaryResponse;
  rejectMultiOrder(
    requestMessage: v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderResponse|null) => void
  ): UnaryResponse;
  rejectMultiOrder(
    requestMessage: v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderResponse|null) => void
  ): UnaryResponse;
  listSupplierInfo(
    requestMessage: v1_order_model_supplier_info_pb.ListSupplierInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_supplier_info_pb.ListSupplierInfoResponse|null) => void
  ): UnaryResponse;
  listSupplierInfo(
    requestMessage: v1_order_model_supplier_info_pb.ListSupplierInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_supplier_info_pb.ListSupplierInfoResponse|null) => void
  ): UnaryResponse;
}

