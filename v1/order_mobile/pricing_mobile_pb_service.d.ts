// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/pricing_mobile.proto

import * as v1_order_mobile_pricing_mobile_pb from "../../v1/order_mobile/pricing_mobile_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_pricing_pb from "../../v1/order/model_pricing_pb";
import {grpc} from "@improbable-eng/grpc-web";

type PricingServiceListPricingCategory = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingBrand = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingProduct = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingQuantityType = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingProductGroup = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProductGroupRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingNames = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingNamesRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListPricingNamesResponse;
};

type PricingServiceListPricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListPricingResponse;
};

type PricingServiceListPricingMerchandiser = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingMerchandiserRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListPricingMerchandiserResponse;
};

type PricingServiceListPricingByIds = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingByIdsRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListPricingByIdsResponse;
};

type PricingServiceListAvailablePricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListAvailablePricingRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListAvailablePricingResponse;
};

type PricingServiceListSupplierPricingInfo = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListSupplierPricingInfoRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListSupplierPricingInfoResponse;
};

export class PricingService {
  static readonly serviceName: string;
  static readonly ListPricingCategory: PricingServiceListPricingCategory;
  static readonly ListPricingBrand: PricingServiceListPricingBrand;
  static readonly ListPricingProduct: PricingServiceListPricingProduct;
  static readonly ListPricingQuantityType: PricingServiceListPricingQuantityType;
  static readonly ListPricingProductGroup: PricingServiceListPricingProductGroup;
  static readonly ListPricingNames: PricingServiceListPricingNames;
  static readonly ListPricing: PricingServiceListPricing;
  static readonly ListPricingMerchandiser: PricingServiceListPricingMerchandiser;
  static readonly ListPricingByIds: PricingServiceListPricingByIds;
  static readonly ListAvailablePricing: PricingServiceListAvailablePricing;
  static readonly ListSupplierPricingInfo: PricingServiceListSupplierPricingInfo;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class PricingServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listPricingCategory(
    requestMessage: v1_order_model_pricing_pb.ListPricingCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingCategory(
    requestMessage: v1_order_model_pricing_pb.ListPricingCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingBrand(
    requestMessage: v1_order_model_pricing_pb.ListPricingBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingBrand(
    requestMessage: v1_order_model_pricing_pb.ListPricingBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProduct(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProduct(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingQuantityType(
    requestMessage: v1_order_model_pricing_pb.ListPricingQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingQuantityType(
    requestMessage: v1_order_model_pricing_pb.ListPricingQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProductGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProductGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingNames(
    requestMessage: v1_order_model_pricing_pb.ListPricingNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingNamesResponse|null) => void
  ): UnaryResponse;
  listPricingNames(
    requestMessage: v1_order_model_pricing_pb.ListPricingNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingNamesResponse|null) => void
  ): UnaryResponse;
  listPricing(
    requestMessage: v1_order_model_pricing_pb.ListPricingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingResponse|null) => void
  ): UnaryResponse;
  listPricing(
    requestMessage: v1_order_model_pricing_pb.ListPricingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingResponse|null) => void
  ): UnaryResponse;
  listPricingMerchandiser(
    requestMessage: v1_order_model_pricing_pb.ListPricingMerchandiserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingMerchandiserResponse|null) => void
  ): UnaryResponse;
  listPricingMerchandiser(
    requestMessage: v1_order_model_pricing_pb.ListPricingMerchandiserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingMerchandiserResponse|null) => void
  ): UnaryResponse;
  listPricingByIds(
    requestMessage: v1_order_model_pricing_pb.ListPricingByIdsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingByIdsResponse|null) => void
  ): UnaryResponse;
  listPricingByIds(
    requestMessage: v1_order_model_pricing_pb.ListPricingByIdsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingByIdsResponse|null) => void
  ): UnaryResponse;
  listAvailablePricing(
    requestMessage: v1_order_model_pricing_pb.ListAvailablePricingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListAvailablePricingResponse|null) => void
  ): UnaryResponse;
  listAvailablePricing(
    requestMessage: v1_order_model_pricing_pb.ListAvailablePricingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListAvailablePricingResponse|null) => void
  ): UnaryResponse;
  listSupplierPricingInfo(
    requestMessage: v1_order_model_pricing_pb.ListSupplierPricingInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListSupplierPricingInfoResponse|null) => void
  ): UnaryResponse;
  listSupplierPricingInfo(
    requestMessage: v1_order_model_pricing_pb.ListSupplierPricingInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListSupplierPricingInfoResponse|null) => void
  ): UnaryResponse;
}

