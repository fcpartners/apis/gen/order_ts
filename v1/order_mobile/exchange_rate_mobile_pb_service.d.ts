// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/exchange_rate_mobile.proto

import * as v1_order_mobile_exchange_rate_mobile_pb from "../../v1/order_mobile/exchange_rate_mobile_pb";
import * as v1_order_model_exchange_rate_pb from "../../v1/order/model_exchange_rate_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ExchangeRateServiceGetExchangeRateByCurrency = {
  readonly methodName: string;
  readonly service: typeof ExchangeRateService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyRequest;
  readonly responseType: typeof v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyResponse;
};

export class ExchangeRateService {
  static readonly serviceName: string;
  static readonly GetExchangeRateByCurrency: ExchangeRateServiceGetExchangeRateByCurrency;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ExchangeRateServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getExchangeRateByCurrency(
    requestMessage: v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyResponse|null) => void
  ): UnaryResponse;
  getExchangeRateByCurrency(
    requestMessage: v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyResponse|null) => void
  ): UnaryResponse;
}

