// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/multi_order_mobile.proto

var v1_order_mobile_multi_order_mobile_pb = require("../../v1/order_mobile/multi_order_mobile_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_multi_order_pb = require("../../v1/order/model_multi_order_pb");
var v1_order_model_supplier_info_pb = require("../../v1/order/model_supplier_info_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MultiOrderService = (function () {
  function MultiOrderService() {}
  MultiOrderService.serviceName = "fcp.order.v1.order_mobile.MultiOrderService";
  return MultiOrderService;
}());

MultiOrderService.ListMultiOrderProcess = {
  methodName: "ListMultiOrderProcess",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderProcessType = {
  methodName: "ListMultiOrderProcessType",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderDeliveryCondition = {
  methodName: "ListMultiOrderDeliveryCondition",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderPaymentCondition = {
  methodName: "ListMultiOrderPaymentCondition",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderCategory = {
  methodName: "ListMultiOrderCategory",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderBrand = {
  methodName: "ListMultiOrderBrand",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderProduct = {
  methodName: "ListMultiOrderProduct",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderQuantityType = {
  methodName: "ListMultiOrderQuantityType",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderNames = {
  methodName: "ListMultiOrderNames",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderNamesRequest,
  responseType: v1_order_model_multi_order_pb.ListMultiOrderNamesResponse
};

MultiOrderService.GetMultiOrder = {
  methodName: "GetMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.GetMultiOrderRequest,
  responseType: v1_order_model_multi_order_pb.GetMultiOrderResponse
};

MultiOrderService.ListMultiOrder = {
  methodName: "ListMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderRequest,
  responseType: v1_order_model_multi_order_pb.ListMultiOrderResponse
};

MultiOrderService.SaveMultiOrder = {
  methodName: "SaveMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderRequest,
  responseType: v1_order_mobile_multi_order_mobile_pb.SaveMultiOrderResponse
};

MultiOrderService.DoneMultiOrder = {
  methodName: "DoneMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderRequest,
  responseType: v1_order_mobile_multi_order_mobile_pb.DoneMultiOrderResponse
};

MultiOrderService.RejectMultiOrder = {
  methodName: "RejectMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderRequest,
  responseType: v1_order_mobile_multi_order_mobile_pb.RejectMultiOrderResponse
};

MultiOrderService.ListSupplierInfo = {
  methodName: "ListSupplierInfo",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_supplier_info_pb.ListSupplierInfoRequest,
  responseType: v1_order_model_supplier_info_pb.ListSupplierInfoResponse
};

exports.MultiOrderService = MultiOrderService;

function MultiOrderServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MultiOrderServiceClient.prototype.listMultiOrderProcess = function listMultiOrderProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderProcessType = function listMultiOrderProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderDeliveryCondition = function listMultiOrderDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderPaymentCondition = function listMultiOrderPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderCategory = function listMultiOrderCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderBrand = function listMultiOrderBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderProduct = function listMultiOrderProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderQuantityType = function listMultiOrderQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderNames = function listMultiOrderNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.getMultiOrder = function getMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.GetMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrder = function listMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.saveMultiOrder = function saveMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.SaveMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.doneMultiOrder = function doneMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.DoneMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.rejectMultiOrder = function rejectMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.RejectMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listSupplierInfo = function listSupplierInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListSupplierInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MultiOrderServiceClient = MultiOrderServiceClient;

