// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/multi_deal_mobile.proto

import * as jspb from "google-protobuf";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_multi_deal_pb from "../../v1/order/model_multi_deal_pb";
import * as v1_order_model_user_rating_pb from "../../v1/order/model_user_rating_pb";

export class DoneMultiDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  hasRating(): boolean;
  clearRating(): void;
  getRating(): v1_order_model_user_rating_pb.UserRating | undefined;
  setRating(value?: v1_order_model_user_rating_pb.UserRating): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiDealRequest): DoneMultiDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiDealRequest;
  static deserializeBinaryFromReader(message: DoneMultiDealRequest, reader: jspb.BinaryReader): DoneMultiDealRequest;
}

export namespace DoneMultiDealRequest {
  export type AsObject = {
    id: number,
    rating?: v1_order_model_user_rating_pb.UserRating.AsObject,
  }
}

export class DoneMultiDealResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiDealResponse): DoneMultiDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiDealResponse;
  static deserializeBinaryFromReader(message: DoneMultiDealResponse, reader: jspb.BinaryReader): DoneMultiDealResponse;
}

export namespace DoneMultiDealResponse {
  export type AsObject = {
  }
}

