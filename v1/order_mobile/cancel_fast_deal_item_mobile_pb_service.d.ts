// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/cancel_fast_deal_item_mobile.proto

import * as v1_order_mobile_cancel_fast_deal_item_mobile_pb from "../../v1/order_mobile/cancel_fast_deal_item_mobile_pb";
import * as v1_order_model_cancel_fast_deal_item_pb from "../../v1/order/model_cancel_fast_deal_item_pb";
import {grpc} from "@improbable-eng/grpc-web";

type CancelFastDealItemServiceGetCancelFastDealItem = {
  readonly methodName: string;
  readonly service: typeof CancelFastDealItemService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemRequest;
  readonly responseType: typeof v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemResponse;
};

type CancelFastDealItemServiceListCancelFastDealItem = {
  readonly methodName: string;
  readonly service: typeof CancelFastDealItemService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemRequest;
  readonly responseType: typeof v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemResponse;
};

type CancelFastDealItemServiceSaveCancelFastDealItem = {
  readonly methodName: string;
  readonly service: typeof CancelFastDealItemService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemRequest;
  readonly responseType: typeof v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemResponse;
};

export class CancelFastDealItemService {
  static readonly serviceName: string;
  static readonly GetCancelFastDealItem: CancelFastDealItemServiceGetCancelFastDealItem;
  static readonly ListCancelFastDealItem: CancelFastDealItemServiceListCancelFastDealItem;
  static readonly SaveCancelFastDealItem: CancelFastDealItemServiceSaveCancelFastDealItem;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class CancelFastDealItemServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getCancelFastDealItem(
    requestMessage: v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemResponse|null) => void
  ): UnaryResponse;
  getCancelFastDealItem(
    requestMessage: v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemResponse|null) => void
  ): UnaryResponse;
  listCancelFastDealItem(
    requestMessage: v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemResponse|null) => void
  ): UnaryResponse;
  listCancelFastDealItem(
    requestMessage: v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemResponse|null) => void
  ): UnaryResponse;
  saveCancelFastDealItem(
    requestMessage: v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemResponse|null) => void
  ): UnaryResponse;
  saveCancelFastDealItem(
    requestMessage: v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemResponse|null) => void
  ): UnaryResponse;
}

