// source: v1/order_mobile/search_mobile.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_model_search_pb = require('../../v1/order/model_search_pb.js');
goog.object.extend(proto, v1_order_model_search_pb);
