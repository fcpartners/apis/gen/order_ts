// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/multi_order_mobile.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";
import * as v1_order_model_multi_order_pb from "../../v1/order/model_multi_order_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";
import * as v1_order_model_supplier_info_pb from "../../v1/order/model_supplier_info_pb";

export class SaveMultiOrderRequest extends jspb.Message {
  hasMultiOrder(): boolean;
  clearMultiOrder(): void;
  getMultiOrder(): v1_order_model_multi_order_pb.MultiOrder | undefined;
  setMultiOrder(value?: v1_order_model_multi_order_pb.MultiOrder): void;

  clearOrdersList(): void;
  getOrdersList(): Array<v1_order_model_order_pb.Order>;
  setOrdersList(value: Array<v1_order_model_order_pb.Order>): void;
  addOrders(value?: v1_order_model_order_pb.Order, index?: number): v1_order_model_order_pb.Order;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveMultiOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveMultiOrderRequest): SaveMultiOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveMultiOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveMultiOrderRequest;
  static deserializeBinaryFromReader(message: SaveMultiOrderRequest, reader: jspb.BinaryReader): SaveMultiOrderRequest;
}

export namespace SaveMultiOrderRequest {
  export type AsObject = {
    multiOrder?: v1_order_model_multi_order_pb.MultiOrder.AsObject,
    ordersList: Array<v1_order_model_order_pb.Order.AsObject>,
  }
}

export class SaveMultiOrderResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveMultiOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveMultiOrderResponse): SaveMultiOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveMultiOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveMultiOrderResponse;
  static deserializeBinaryFromReader(message: SaveMultiOrderResponse, reader: jspb.BinaryReader): SaveMultiOrderResponse;
}

export namespace SaveMultiOrderResponse {
  export type AsObject = {
    id: number,
  }
}

export class RejectMultiOrderRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiOrderRequest): RejectMultiOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiOrderRequest;
  static deserializeBinaryFromReader(message: RejectMultiOrderRequest, reader: jspb.BinaryReader): RejectMultiOrderRequest;
}

export namespace RejectMultiOrderRequest {
  export type AsObject = {
    id: number,
  }
}

export class RejectMultiOrderResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiOrderResponse): RejectMultiOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiOrderResponse;
  static deserializeBinaryFromReader(message: RejectMultiOrderResponse, reader: jspb.BinaryReader): RejectMultiOrderResponse;
}

export namespace RejectMultiOrderResponse {
  export type AsObject = {
  }
}

export class DoneMultiOrderRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiOrderRequest): DoneMultiOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiOrderRequest;
  static deserializeBinaryFromReader(message: DoneMultiOrderRequest, reader: jspb.BinaryReader): DoneMultiOrderRequest;
}

export namespace DoneMultiOrderRequest {
  export type AsObject = {
    id: number,
  }
}

export class DoneMultiOrderResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneMultiOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DoneMultiOrderResponse): DoneMultiOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneMultiOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneMultiOrderResponse;
  static deserializeBinaryFromReader(message: DoneMultiOrderResponse, reader: jspb.BinaryReader): DoneMultiOrderResponse;
}

export namespace DoneMultiOrderResponse {
  export type AsObject = {
  }
}

