// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/pricing_mobile.proto

var v1_order_mobile_pricing_mobile_pb = require("../../v1/order_mobile/pricing_mobile_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_pricing_pb = require("../../v1/order/model_pricing_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var PricingService = (function () {
  function PricingService() {}
  PricingService.serviceName = "fcp.order.v1.order_mobile.PricingService";
  return PricingService;
}());

PricingService.ListPricingCategory = {
  methodName: "ListPricingCategory",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingBrand = {
  methodName: "ListPricingBrand",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingProduct = {
  methodName: "ListPricingProduct",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingQuantityType = {
  methodName: "ListPricingQuantityType",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingProductGroup = {
  methodName: "ListPricingProductGroup",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProductGroupRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingNames = {
  methodName: "ListPricingNames",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingNamesRequest,
  responseType: v1_order_model_pricing_pb.ListPricingNamesResponse
};

PricingService.ListPricing = {
  methodName: "ListPricing",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingRequest,
  responseType: v1_order_model_pricing_pb.ListPricingResponse
};

PricingService.ListPricingMerchandiser = {
  methodName: "ListPricingMerchandiser",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingMerchandiserRequest,
  responseType: v1_order_model_pricing_pb.ListPricingMerchandiserResponse
};

PricingService.ListPricingByIds = {
  methodName: "ListPricingByIds",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingByIdsRequest,
  responseType: v1_order_model_pricing_pb.ListPricingByIdsResponse
};

PricingService.ListAvailablePricing = {
  methodName: "ListAvailablePricing",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListAvailablePricingRequest,
  responseType: v1_order_model_pricing_pb.ListAvailablePricingResponse
};

PricingService.ListSupplierPricingInfo = {
  methodName: "ListSupplierPricingInfo",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListSupplierPricingInfoRequest,
  responseType: v1_order_model_pricing_pb.ListSupplierPricingInfoResponse
};

exports.PricingService = PricingService;

function PricingServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

PricingServiceClient.prototype.listPricingCategory = function listPricingCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingBrand = function listPricingBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingProduct = function listPricingProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingQuantityType = function listPricingQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingProductGroup = function listPricingProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingNames = function listPricingNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricing = function listPricing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingMerchandiser = function listPricingMerchandiser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingMerchandiser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingByIds = function listPricingByIds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingByIds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listAvailablePricing = function listAvailablePricing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListAvailablePricing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listSupplierPricingInfo = function listSupplierPricingInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListSupplierPricingInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.PricingServiceClient = PricingServiceClient;

