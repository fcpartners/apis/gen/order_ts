// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/cancel_fast_deal_item_mobile.proto

var v1_order_mobile_cancel_fast_deal_item_mobile_pb = require("../../v1/order_mobile/cancel_fast_deal_item_mobile_pb");
var v1_order_model_cancel_fast_deal_item_pb = require("../../v1/order/model_cancel_fast_deal_item_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var CancelFastDealItemService = (function () {
  function CancelFastDealItemService() {}
  CancelFastDealItemService.serviceName = "fcp.order.v1.order_mobile.CancelFastDealItemService";
  return CancelFastDealItemService;
}());

CancelFastDealItemService.GetCancelFastDealItem = {
  methodName: "GetCancelFastDealItem",
  service: CancelFastDealItemService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemRequest,
  responseType: v1_order_model_cancel_fast_deal_item_pb.GetCancelFastDealItemResponse
};

CancelFastDealItemService.ListCancelFastDealItem = {
  methodName: "ListCancelFastDealItem",
  service: CancelFastDealItemService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemRequest,
  responseType: v1_order_model_cancel_fast_deal_item_pb.ListCancelFastDealItemResponse
};

CancelFastDealItemService.SaveCancelFastDealItem = {
  methodName: "SaveCancelFastDealItem",
  service: CancelFastDealItemService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemRequest,
  responseType: v1_order_model_cancel_fast_deal_item_pb.SaveCancelFastDealItemResponse
};

exports.CancelFastDealItemService = CancelFastDealItemService;

function CancelFastDealItemServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

CancelFastDealItemServiceClient.prototype.getCancelFastDealItem = function getCancelFastDealItem(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelFastDealItemService.GetCancelFastDealItem, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CancelFastDealItemServiceClient.prototype.listCancelFastDealItem = function listCancelFastDealItem(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelFastDealItemService.ListCancelFastDealItem, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CancelFastDealItemServiceClient.prototype.saveCancelFastDealItem = function saveCancelFastDealItem(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CancelFastDealItemService.SaveCancelFastDealItem, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.CancelFastDealItemServiceClient = CancelFastDealItemServiceClient;

