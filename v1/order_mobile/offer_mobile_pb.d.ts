// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/offer_mobile.proto

import * as jspb from "google-protobuf";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";

export class AcceptOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  clearIdsList(): void;
  getIdsList(): Array<number>;
  setIdsList(value: Array<number>): void;
  addIds(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AcceptOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AcceptOfferRequest): AcceptOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AcceptOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AcceptOfferRequest;
  static deserializeBinaryFromReader(message: AcceptOfferRequest, reader: jspb.BinaryReader): AcceptOfferRequest;
}

export namespace AcceptOfferRequest {
  export type AsObject = {
    id: number,
    idsList: Array<number>,
  }
}

export class AcceptOfferResponse extends jspb.Message {
  clearErrorsList(): void;
  getErrorsList(): Array<ResponseMessage>;
  setErrorsList(value: Array<ResponseMessage>): void;
  addErrors(value?: ResponseMessage, index?: number): ResponseMessage;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AcceptOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AcceptOfferResponse): AcceptOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AcceptOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AcceptOfferResponse;
  static deserializeBinaryFromReader(message: AcceptOfferResponse, reader: jspb.BinaryReader): AcceptOfferResponse;
}

export namespace AcceptOfferResponse {
  export type AsObject = {
    errorsList: Array<ResponseMessage.AsObject>,
  }
}

export class ResponseMessage extends jspb.Message {
  getOfferId(): number;
  setOfferId(value: number): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResponseMessage.AsObject;
  static toObject(includeInstance: boolean, msg: ResponseMessage): ResponseMessage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResponseMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResponseMessage;
  static deserializeBinaryFromReader(message: ResponseMessage, reader: jspb.BinaryReader): ResponseMessage;
}

export namespace ResponseMessage {
  export type AsObject = {
    offerId: number,
    message: string,
  }
}

export class ListOfferMinEffectivePriceRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): v1_order_model_offer_pb.OfferFilter | undefined;
  setFilter(value?: v1_order_model_offer_pb.OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferMinEffectivePriceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferMinEffectivePriceRequest): ListOfferMinEffectivePriceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferMinEffectivePriceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferMinEffectivePriceRequest;
  static deserializeBinaryFromReader(message: ListOfferMinEffectivePriceRequest, reader: jspb.BinaryReader): ListOfferMinEffectivePriceRequest;
}

export namespace ListOfferMinEffectivePriceRequest {
  export type AsObject = {
    filter?: v1_order_model_offer_pb.OfferFilter.AsObject,
  }
}

export class ListOfferMinEffectivePriceResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_offer_pb.Offer>;
  setItemsList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addItems(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferMinEffectivePriceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferMinEffectivePriceResponse): ListOfferMinEffectivePriceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferMinEffectivePriceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferMinEffectivePriceResponse;
  static deserializeBinaryFromReader(message: ListOfferMinEffectivePriceResponse, reader: jspb.BinaryReader): ListOfferMinEffectivePriceResponse;
}

export namespace ListOfferMinEffectivePriceResponse {
  export type AsObject = {
    itemsList: Array<v1_order_model_offer_pb.Offer.AsObject>,
  }
}

export class ListOfferMaxEffectivePriceRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): v1_order_model_offer_pb.OfferFilter | undefined;
  setFilter(value?: v1_order_model_offer_pb.OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferMaxEffectivePriceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferMaxEffectivePriceRequest): ListOfferMaxEffectivePriceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferMaxEffectivePriceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferMaxEffectivePriceRequest;
  static deserializeBinaryFromReader(message: ListOfferMaxEffectivePriceRequest, reader: jspb.BinaryReader): ListOfferMaxEffectivePriceRequest;
}

export namespace ListOfferMaxEffectivePriceRequest {
  export type AsObject = {
    filter?: v1_order_model_offer_pb.OfferFilter.AsObject,
  }
}

export class ListOfferMaxEffectivePriceResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_offer_pb.Offer>;
  setItemsList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addItems(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferMaxEffectivePriceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferMaxEffectivePriceResponse): ListOfferMaxEffectivePriceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferMaxEffectivePriceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferMaxEffectivePriceResponse;
  static deserializeBinaryFromReader(message: ListOfferMaxEffectivePriceResponse, reader: jspb.BinaryReader): ListOfferMaxEffectivePriceResponse;
}

export namespace ListOfferMaxEffectivePriceResponse {
  export type AsObject = {
    itemsList: Array<v1_order_model_offer_pb.Offer.AsObject>,
  }
}

