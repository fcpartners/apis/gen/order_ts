// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/pushup_token_mobile.proto

import * as v1_order_mobile_pushup_token_mobile_pb from "../../v1/order_mobile/pushup_token_mobile_pb";
import * as v1_order_model_pushup_notification_pb from "../../v1/order/model_pushup_notification_pb";
import {grpc} from "@improbable-eng/grpc-web";

type PushUpServiceSendToken = {
  readonly methodName: string;
  readonly service: typeof PushUpService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pushup_notification_pb.SendTokenRequest;
  readonly responseType: typeof v1_order_model_pushup_notification_pb.SendTokenResponse;
};

type PushUpServiceRemoveToken = {
  readonly methodName: string;
  readonly service: typeof PushUpService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pushup_notification_pb.RemoveTokenRequest;
  readonly responseType: typeof v1_order_model_pushup_notification_pb.RemoveTokenResponse;
};

export class PushUpService {
  static readonly serviceName: string;
  static readonly SendToken: PushUpServiceSendToken;
  static readonly RemoveToken: PushUpServiceRemoveToken;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class PushUpServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  sendToken(
    requestMessage: v1_order_model_pushup_notification_pb.SendTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pushup_notification_pb.SendTokenResponse|null) => void
  ): UnaryResponse;
  sendToken(
    requestMessage: v1_order_model_pushup_notification_pb.SendTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pushup_notification_pb.SendTokenResponse|null) => void
  ): UnaryResponse;
  removeToken(
    requestMessage: v1_order_model_pushup_notification_pb.RemoveTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pushup_notification_pb.RemoveTokenResponse|null) => void
  ): UnaryResponse;
  removeToken(
    requestMessage: v1_order_model_pushup_notification_pb.RemoveTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pushup_notification_pb.RemoveTokenResponse|null) => void
  ): UnaryResponse;
}

