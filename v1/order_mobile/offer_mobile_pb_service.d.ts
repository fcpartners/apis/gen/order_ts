// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/offer_mobile.proto

import * as v1_order_mobile_offer_mobile_pb from "../../v1/order_mobile/offer_mobile_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";
import {grpc} from "@improbable-eng/grpc-web";

type OfferServiceListOfferProcess = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferProcessType = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferCurrency = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferCurrencyRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferCategory = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferBrand = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferProduct = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferQuantityType = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferNames = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferNamesRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferNamesResponse;
};

type OfferServiceGetOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.GetOfferRequest;
  readonly responseType: typeof v1_order_model_offer_pb.GetOfferResponse;
};

type OfferServiceListOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferResponse;
};

type OfferServiceAcceptOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_offer_mobile_pb.AcceptOfferRequest;
  readonly responseType: typeof v1_order_mobile_offer_mobile_pb.AcceptOfferResponse;
};

type OfferServiceListOfferMinEffectivePrice = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceRequest;
  readonly responseType: typeof v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceResponse;
};

type OfferServiceListOfferMaxEffectivePrice = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceRequest;
  readonly responseType: typeof v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceResponse;
};

type OfferServiceGetOfferFile = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.GetOfferFileRequest;
  readonly responseType: typeof v1_order_model_offer_pb.GetOfferFileResponse;
};

type OfferServiceListOfferFile = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferFileRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferFileResponse;
};

export class OfferService {
  static readonly serviceName: string;
  static readonly ListOfferProcess: OfferServiceListOfferProcess;
  static readonly ListOfferProcessType: OfferServiceListOfferProcessType;
  static readonly ListOfferDeliveryCondition: OfferServiceListOfferDeliveryCondition;
  static readonly ListOfferPaymentCondition: OfferServiceListOfferPaymentCondition;
  static readonly ListOfferCurrency: OfferServiceListOfferCurrency;
  static readonly ListOfferCategory: OfferServiceListOfferCategory;
  static readonly ListOfferBrand: OfferServiceListOfferBrand;
  static readonly ListOfferProduct: OfferServiceListOfferProduct;
  static readonly ListOfferQuantityType: OfferServiceListOfferQuantityType;
  static readonly ListOfferNames: OfferServiceListOfferNames;
  static readonly GetOffer: OfferServiceGetOffer;
  static readonly ListOffer: OfferServiceListOffer;
  static readonly AcceptOffer: OfferServiceAcceptOffer;
  static readonly ListOfferMinEffectivePrice: OfferServiceListOfferMinEffectivePrice;
  static readonly ListOfferMaxEffectivePrice: OfferServiceListOfferMaxEffectivePrice;
  static readonly GetOfferFile: OfferServiceGetOfferFile;
  static readonly ListOfferFile: OfferServiceListOfferFile;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class OfferServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listOfferProcess(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProcess(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProcessType(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProcessType(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferDeliveryCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferDeliveryCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferPaymentCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferPaymentCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferCurrency(
    requestMessage: v1_order_model_offer_pb.ListOfferCurrencyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferCurrency(
    requestMessage: v1_order_model_offer_pb.ListOfferCurrencyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferCategory(
    requestMessage: v1_order_model_offer_pb.ListOfferCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferCategory(
    requestMessage: v1_order_model_offer_pb.ListOfferCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferBrand(
    requestMessage: v1_order_model_offer_pb.ListOfferBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferBrand(
    requestMessage: v1_order_model_offer_pb.ListOfferBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProduct(
    requestMessage: v1_order_model_offer_pb.ListOfferProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProduct(
    requestMessage: v1_order_model_offer_pb.ListOfferProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferQuantityType(
    requestMessage: v1_order_model_offer_pb.ListOfferQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferQuantityType(
    requestMessage: v1_order_model_offer_pb.ListOfferQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferNames(
    requestMessage: v1_order_model_offer_pb.ListOfferNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferNamesResponse|null) => void
  ): UnaryResponse;
  listOfferNames(
    requestMessage: v1_order_model_offer_pb.ListOfferNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferNamesResponse|null) => void
  ): UnaryResponse;
  getOffer(
    requestMessage: v1_order_model_offer_pb.GetOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferResponse|null) => void
  ): UnaryResponse;
  getOffer(
    requestMessage: v1_order_model_offer_pb.GetOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferResponse|null) => void
  ): UnaryResponse;
  listOffer(
    requestMessage: v1_order_model_offer_pb.ListOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferResponse|null) => void
  ): UnaryResponse;
  listOffer(
    requestMessage: v1_order_model_offer_pb.ListOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferResponse|null) => void
  ): UnaryResponse;
  acceptOffer(
    requestMessage: v1_order_mobile_offer_mobile_pb.AcceptOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_offer_mobile_pb.AcceptOfferResponse|null) => void
  ): UnaryResponse;
  acceptOffer(
    requestMessage: v1_order_mobile_offer_mobile_pb.AcceptOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_offer_mobile_pb.AcceptOfferResponse|null) => void
  ): UnaryResponse;
  listOfferMinEffectivePrice(
    requestMessage: v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceResponse|null) => void
  ): UnaryResponse;
  listOfferMinEffectivePrice(
    requestMessage: v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceResponse|null) => void
  ): UnaryResponse;
  listOfferMaxEffectivePrice(
    requestMessage: v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceResponse|null) => void
  ): UnaryResponse;
  listOfferMaxEffectivePrice(
    requestMessage: v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceResponse|null) => void
  ): UnaryResponse;
  getOfferFile(
    requestMessage: v1_order_model_offer_pb.GetOfferFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferFileResponse|null) => void
  ): UnaryResponse;
  getOfferFile(
    requestMessage: v1_order_model_offer_pb.GetOfferFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferFileResponse|null) => void
  ): UnaryResponse;
  listOfferFile(
    requestMessage: v1_order_model_offer_pb.ListOfferFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferFileResponse|null) => void
  ): UnaryResponse;
  listOfferFile(
    requestMessage: v1_order_model_offer_pb.ListOfferFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferFileResponse|null) => void
  ): UnaryResponse;
}

