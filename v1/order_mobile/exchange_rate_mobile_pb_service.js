// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/exchange_rate_mobile.proto

var v1_order_mobile_exchange_rate_mobile_pb = require("../../v1/order_mobile/exchange_rate_mobile_pb");
var v1_order_model_exchange_rate_pb = require("../../v1/order/model_exchange_rate_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var ExchangeRateService = (function () {
  function ExchangeRateService() {}
  ExchangeRateService.serviceName = "fcp.order.v1.order_mobile.ExchangeRateService";
  return ExchangeRateService;
}());

ExchangeRateService.GetExchangeRateByCurrency = {
  methodName: "GetExchangeRateByCurrency",
  service: ExchangeRateService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyRequest,
  responseType: v1_order_model_exchange_rate_pb.GetExchangeRateByCurrencyResponse
};

exports.ExchangeRateService = ExchangeRateService;

function ExchangeRateServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

ExchangeRateServiceClient.prototype.getExchangeRateByCurrency = function getExchangeRateByCurrency(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(ExchangeRateService.GetExchangeRateByCurrency, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.ExchangeRateServiceClient = ExchangeRateServiceClient;

