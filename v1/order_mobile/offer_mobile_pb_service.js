// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/offer_mobile.proto

var v1_order_mobile_offer_mobile_pb = require("../../v1/order_mobile/offer_mobile_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_offer_pb = require("../../v1/order/model_offer_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var OfferService = (function () {
  function OfferService() {}
  OfferService.serviceName = "fcp.order.v1.order_mobile.OfferService";
  return OfferService;
}());

OfferService.ListOfferProcess = {
  methodName: "ListOfferProcess",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferProcessType = {
  methodName: "ListOfferProcessType",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferDeliveryCondition = {
  methodName: "ListOfferDeliveryCondition",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferPaymentCondition = {
  methodName: "ListOfferPaymentCondition",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferCurrency = {
  methodName: "ListOfferCurrency",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferCurrencyRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferCategory = {
  methodName: "ListOfferCategory",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferBrand = {
  methodName: "ListOfferBrand",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferProduct = {
  methodName: "ListOfferProduct",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferQuantityType = {
  methodName: "ListOfferQuantityType",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferNames = {
  methodName: "ListOfferNames",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferNamesRequest,
  responseType: v1_order_model_offer_pb.ListOfferNamesResponse
};

OfferService.GetOffer = {
  methodName: "GetOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.GetOfferRequest,
  responseType: v1_order_model_offer_pb.GetOfferResponse
};

OfferService.ListOffer = {
  methodName: "ListOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferRequest,
  responseType: v1_order_model_offer_pb.ListOfferResponse
};

OfferService.AcceptOffer = {
  methodName: "AcceptOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_mobile_offer_mobile_pb.AcceptOfferRequest,
  responseType: v1_order_mobile_offer_mobile_pb.AcceptOfferResponse
};

OfferService.ListOfferMinEffectivePrice = {
  methodName: "ListOfferMinEffectivePrice",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceRequest,
  responseType: v1_order_mobile_offer_mobile_pb.ListOfferMinEffectivePriceResponse
};

OfferService.ListOfferMaxEffectivePrice = {
  methodName: "ListOfferMaxEffectivePrice",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceRequest,
  responseType: v1_order_mobile_offer_mobile_pb.ListOfferMaxEffectivePriceResponse
};

OfferService.GetOfferFile = {
  methodName: "GetOfferFile",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.GetOfferFileRequest,
  responseType: v1_order_model_offer_pb.GetOfferFileResponse
};

OfferService.ListOfferFile = {
  methodName: "ListOfferFile",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferFileRequest,
  responseType: v1_order_model_offer_pb.ListOfferFileResponse
};

exports.OfferService = OfferService;

function OfferServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

OfferServiceClient.prototype.listOfferProcess = function listOfferProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferProcessType = function listOfferProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferDeliveryCondition = function listOfferDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferPaymentCondition = function listOfferPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferCurrency = function listOfferCurrency(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferCurrency, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferCategory = function listOfferCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferBrand = function listOfferBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferProduct = function listOfferProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferQuantityType = function listOfferQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferNames = function listOfferNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.getOffer = function getOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.GetOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOffer = function listOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.acceptOffer = function acceptOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.AcceptOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferMinEffectivePrice = function listOfferMinEffectivePrice(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferMinEffectivePrice, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferMaxEffectivePrice = function listOfferMaxEffectivePrice(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferMaxEffectivePrice, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.getOfferFile = function getOfferFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.GetOfferFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferFile = function listOfferFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.OfferServiceClient = OfferServiceClient;

