// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/order_mobile.proto

import * as v1_order_mobile_order_mobile_pb from "../../v1/order_mobile/order_mobile_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";
import {grpc} from "@improbable-eng/grpc-web";

type OrderServiceListOrderProcess = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderProcessType = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderCategory = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderBrand = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderProduct = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderQuantityType = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OrderServiceListOrderNames = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderNamesRequest;
  readonly responseType: typeof v1_order_model_order_pb.ListOrderNamesResponse;
};

type OrderServiceGetOrder = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.GetOrderRequest;
  readonly responseType: typeof v1_order_model_order_pb.GetOrderResponse;
};

type OrderServiceListOrder = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_order_pb.ListOrderRequest;
  readonly responseType: typeof v1_order_model_order_pb.ListOrderResponse;
};

type OrderServiceSaveOrder = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_order_mobile_pb.SaveOrderRequest;
  readonly responseType: typeof v1_order_mobile_order_mobile_pb.SaveOrderResponse;
};

type OrderServiceRejectOrder = {
  readonly methodName: string;
  readonly service: typeof OrderService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_mobile_order_mobile_pb.RejectOrderRequest;
  readonly responseType: typeof v1_order_mobile_order_mobile_pb.RejectOrderResponse;
};

export class OrderService {
  static readonly serviceName: string;
  static readonly ListOrderProcess: OrderServiceListOrderProcess;
  static readonly ListOrderProcessType: OrderServiceListOrderProcessType;
  static readonly ListOrderDeliveryCondition: OrderServiceListOrderDeliveryCondition;
  static readonly ListOrderPaymentCondition: OrderServiceListOrderPaymentCondition;
  static readonly ListOrderCategory: OrderServiceListOrderCategory;
  static readonly ListOrderBrand: OrderServiceListOrderBrand;
  static readonly ListOrderProduct: OrderServiceListOrderProduct;
  static readonly ListOrderQuantityType: OrderServiceListOrderQuantityType;
  static readonly ListOrderNames: OrderServiceListOrderNames;
  static readonly GetOrder: OrderServiceGetOrder;
  static readonly ListOrder: OrderServiceListOrder;
  static readonly SaveOrder: OrderServiceSaveOrder;
  static readonly RejectOrder: OrderServiceRejectOrder;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class OrderServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listOrderProcess(
    requestMessage: v1_order_model_order_pb.ListOrderProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderProcess(
    requestMessage: v1_order_model_order_pb.ListOrderProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderProcessType(
    requestMessage: v1_order_model_order_pb.ListOrderProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderProcessType(
    requestMessage: v1_order_model_order_pb.ListOrderProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderDeliveryCondition(
    requestMessage: v1_order_model_order_pb.ListOrderDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderDeliveryCondition(
    requestMessage: v1_order_model_order_pb.ListOrderDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderPaymentCondition(
    requestMessage: v1_order_model_order_pb.ListOrderPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderPaymentCondition(
    requestMessage: v1_order_model_order_pb.ListOrderPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderCategory(
    requestMessage: v1_order_model_order_pb.ListOrderCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderCategory(
    requestMessage: v1_order_model_order_pb.ListOrderCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderBrand(
    requestMessage: v1_order_model_order_pb.ListOrderBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderBrand(
    requestMessage: v1_order_model_order_pb.ListOrderBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderProduct(
    requestMessage: v1_order_model_order_pb.ListOrderProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderProduct(
    requestMessage: v1_order_model_order_pb.ListOrderProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderQuantityType(
    requestMessage: v1_order_model_order_pb.ListOrderQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderQuantityType(
    requestMessage: v1_order_model_order_pb.ListOrderQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOrderNames(
    requestMessage: v1_order_model_order_pb.ListOrderNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_order_pb.ListOrderNamesResponse|null) => void
  ): UnaryResponse;
  listOrderNames(
    requestMessage: v1_order_model_order_pb.ListOrderNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_order_pb.ListOrderNamesResponse|null) => void
  ): UnaryResponse;
  getOrder(
    requestMessage: v1_order_model_order_pb.GetOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_order_pb.GetOrderResponse|null) => void
  ): UnaryResponse;
  getOrder(
    requestMessage: v1_order_model_order_pb.GetOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_order_pb.GetOrderResponse|null) => void
  ): UnaryResponse;
  listOrder(
    requestMessage: v1_order_model_order_pb.ListOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_order_pb.ListOrderResponse|null) => void
  ): UnaryResponse;
  listOrder(
    requestMessage: v1_order_model_order_pb.ListOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_order_pb.ListOrderResponse|null) => void
  ): UnaryResponse;
  saveOrder(
    requestMessage: v1_order_mobile_order_mobile_pb.SaveOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_order_mobile_pb.SaveOrderResponse|null) => void
  ): UnaryResponse;
  saveOrder(
    requestMessage: v1_order_mobile_order_mobile_pb.SaveOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_order_mobile_pb.SaveOrderResponse|null) => void
  ): UnaryResponse;
  rejectOrder(
    requestMessage: v1_order_mobile_order_mobile_pb.RejectOrderRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_order_mobile_pb.RejectOrderResponse|null) => void
  ): UnaryResponse;
  rejectOrder(
    requestMessage: v1_order_mobile_order_mobile_pb.RejectOrderRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_mobile_order_mobile_pb.RejectOrderResponse|null) => void
  ): UnaryResponse;
}

