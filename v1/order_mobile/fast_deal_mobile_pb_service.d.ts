// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/fast_deal_mobile.proto

import * as v1_order_mobile_fast_deal_mobile_pb from "../../v1/order_mobile/fast_deal_mobile_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_fast_deal_pb from "../../v1/order/model_fast_deal_pb";
import {grpc} from "@improbable-eng/grpc-web";

type FastDealServiceListFastDealProcessType = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealBrand = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealProduct = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealCurrency = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealCurrencyRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealNames = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealNamesRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealNamesResponse;
};

type FastDealServiceGetFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.GetFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.GetFastDealResponse;
};

type FastDealServiceListFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealResponse;
};

type FastDealServiceCreateFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.CreateFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.CreateFastDealResponse;
};

type FastDealServiceRejectFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.RejectFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.RejectFastDealResponse;
};

type FastDealServiceDoneFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.DoneFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.DoneFastDealResponse;
};

type FastDealServiceGetFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.GetFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.GetFastDealFileResponse;
};

type FastDealServiceListFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealFileResponse;
};

type FastDealServiceAddFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.AddFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.AddFastDealFileResponse;
};

type FastDealServiceDeleteFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.DeleteFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.DeleteFastDealFileResponse;
};

export class FastDealService {
  static readonly serviceName: string;
  static readonly ListFastDealProcessType: FastDealServiceListFastDealProcessType;
  static readonly ListFastDealBrand: FastDealServiceListFastDealBrand;
  static readonly ListFastDealProduct: FastDealServiceListFastDealProduct;
  static readonly ListFastDealCurrency: FastDealServiceListFastDealCurrency;
  static readonly ListFastDealPaymentCondition: FastDealServiceListFastDealPaymentCondition;
  static readonly ListFastDealDeliveryCondition: FastDealServiceListFastDealDeliveryCondition;
  static readonly ListFastDealNames: FastDealServiceListFastDealNames;
  static readonly GetFastDeal: FastDealServiceGetFastDeal;
  static readonly ListFastDeal: FastDealServiceListFastDeal;
  static readonly CreateFastDeal: FastDealServiceCreateFastDeal;
  static readonly RejectFastDeal: FastDealServiceRejectFastDeal;
  static readonly DoneFastDeal: FastDealServiceDoneFastDeal;
  static readonly GetFastDealFile: FastDealServiceGetFastDealFile;
  static readonly ListFastDealFile: FastDealServiceListFastDealFile;
  static readonly AddFastDealFile: FastDealServiceAddFastDealFile;
  static readonly DeleteFastDealFile: FastDealServiceDeleteFastDealFile;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class FastDealServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listFastDealProcessType(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProcessType(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealBrand(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealBrand(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProduct(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProduct(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealCurrency(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealCurrencyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealCurrency(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealCurrencyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealPaymentCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealPaymentCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealDeliveryCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealDeliveryCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealNames(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealNamesResponse|null) => void
  ): UnaryResponse;
  listFastDealNames(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealNamesResponse|null) => void
  ): UnaryResponse;
  getFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealResponse|null) => void
  ): UnaryResponse;
  getFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealResponse|null) => void
  ): UnaryResponse;
  listFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealResponse|null) => void
  ): UnaryResponse;
  listFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealResponse|null) => void
  ): UnaryResponse;
  createFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.CreateFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.CreateFastDealResponse|null) => void
  ): UnaryResponse;
  createFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.CreateFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.CreateFastDealResponse|null) => void
  ): UnaryResponse;
  rejectFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.RejectFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.RejectFastDealResponse|null) => void
  ): UnaryResponse;
  rejectFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.RejectFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.RejectFastDealResponse|null) => void
  ): UnaryResponse;
  doneFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.DoneFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DoneFastDealResponse|null) => void
  ): UnaryResponse;
  doneFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.DoneFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DoneFastDealResponse|null) => void
  ): UnaryResponse;
  getFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealFileResponse|null) => void
  ): UnaryResponse;
  getFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealFileResponse|null) => void
  ): UnaryResponse;
  listFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealFileResponse|null) => void
  ): UnaryResponse;
  listFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealFileResponse|null) => void
  ): UnaryResponse;
  addFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.AddFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.AddFastDealFileResponse|null) => void
  ): UnaryResponse;
  addFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.AddFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.AddFastDealFileResponse|null) => void
  ): UnaryResponse;
  deleteFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileResponse|null) => void
  ): UnaryResponse;
  deleteFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileResponse|null) => void
  ): UnaryResponse;
}

