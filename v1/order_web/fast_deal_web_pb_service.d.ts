// package: fcp.order.v1.order_web
// file: v1/order_web/fast_deal_web.proto

import * as v1_order_web_fast_deal_web_pb from "../../v1/order_web/fast_deal_web_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_fast_deal_pb from "../../v1/order/model_fast_deal_pb";
import {grpc} from "@improbable-eng/grpc-web";

type FastDealServiceListFastDealProcess = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealProcessType = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealProductGroup = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealProductGroupRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealBrand = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealProduct = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealLogisticsOperator = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealLogisticsOperatorRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealState = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealStateRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealCity = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealCityRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type FastDealServiceListFastDealNames = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealNamesRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealNamesResponse;
};

type FastDealServiceGetFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.GetFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.GetFastDealResponse;
};

type FastDealServiceListFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealResponse;
};

type FastDealServiceListFastDealItemAgent = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealItemAgentRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealItemAgentResponse;
};

type FastDealServiceRejectFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.RejectFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.RejectFastDealResponse;
};

type FastDealServiceApproveFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ApproveFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ApproveFastDealResponse;
};

type FastDealServiceDoneFastDeal = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.DoneFastDealRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.DoneFastDealResponse;
};

type FastDealServiceUpdateStatusFastDealItem = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.UpdateFastDealItemStatusRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.UpdateFastDealItemStatusResponse;
};

type FastDealServiceGetFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.GetFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.GetFastDealFileResponse;
};

type FastDealServiceListFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.ListFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.ListFastDealFileResponse;
};

type FastDealServiceAddFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.AddFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.AddFastDealFileResponse;
};

type FastDealServiceDeleteFastDealFile = {
  readonly methodName: string;
  readonly service: typeof FastDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_fast_deal_pb.DeleteFastDealFileRequest;
  readonly responseType: typeof v1_order_model_fast_deal_pb.DeleteFastDealFileResponse;
};

export class FastDealService {
  static readonly serviceName: string;
  static readonly ListFastDealProcess: FastDealServiceListFastDealProcess;
  static readonly ListFastDealProcessType: FastDealServiceListFastDealProcessType;
  static readonly ListFastDealProductGroup: FastDealServiceListFastDealProductGroup;
  static readonly ListFastDealBrand: FastDealServiceListFastDealBrand;
  static readonly ListFastDealProduct: FastDealServiceListFastDealProduct;
  static readonly ListFastDealDeliveryCondition: FastDealServiceListFastDealDeliveryCondition;
  static readonly ListFastDealPaymentCondition: FastDealServiceListFastDealPaymentCondition;
  static readonly ListFastDealLogisticsOperator: FastDealServiceListFastDealLogisticsOperator;
  static readonly ListFastDealState: FastDealServiceListFastDealState;
  static readonly ListFastDealCity: FastDealServiceListFastDealCity;
  static readonly ListFastDealNames: FastDealServiceListFastDealNames;
  static readonly GetFastDeal: FastDealServiceGetFastDeal;
  static readonly ListFastDeal: FastDealServiceListFastDeal;
  static readonly ListFastDealItemAgent: FastDealServiceListFastDealItemAgent;
  static readonly RejectFastDeal: FastDealServiceRejectFastDeal;
  static readonly ApproveFastDeal: FastDealServiceApproveFastDeal;
  static readonly DoneFastDeal: FastDealServiceDoneFastDeal;
  static readonly UpdateStatusFastDealItem: FastDealServiceUpdateStatusFastDealItem;
  static readonly GetFastDealFile: FastDealServiceGetFastDealFile;
  static readonly ListFastDealFile: FastDealServiceListFastDealFile;
  static readonly AddFastDealFile: FastDealServiceAddFastDealFile;
  static readonly DeleteFastDealFile: FastDealServiceDeleteFastDealFile;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class FastDealServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listFastDealProcess(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProcess(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProcessType(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProcessType(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProductGroup(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProductGroup(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealBrand(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealBrand(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProduct(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealProduct(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealDeliveryCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealDeliveryCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealPaymentCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealPaymentCondition(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealLogisticsOperator(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealLogisticsOperatorRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealLogisticsOperator(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealLogisticsOperatorRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealState(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealStateRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealState(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealStateRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealCity(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealCityRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealCity(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealCityRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listFastDealNames(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealNamesResponse|null) => void
  ): UnaryResponse;
  listFastDealNames(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealNamesResponse|null) => void
  ): UnaryResponse;
  getFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealResponse|null) => void
  ): UnaryResponse;
  getFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealResponse|null) => void
  ): UnaryResponse;
  listFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealResponse|null) => void
  ): UnaryResponse;
  listFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealResponse|null) => void
  ): UnaryResponse;
  listFastDealItemAgent(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealItemAgentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealItemAgentResponse|null) => void
  ): UnaryResponse;
  listFastDealItemAgent(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealItemAgentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealItemAgentResponse|null) => void
  ): UnaryResponse;
  rejectFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.RejectFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.RejectFastDealResponse|null) => void
  ): UnaryResponse;
  rejectFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.RejectFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.RejectFastDealResponse|null) => void
  ): UnaryResponse;
  approveFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.ApproveFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ApproveFastDealResponse|null) => void
  ): UnaryResponse;
  approveFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.ApproveFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ApproveFastDealResponse|null) => void
  ): UnaryResponse;
  doneFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.DoneFastDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DoneFastDealResponse|null) => void
  ): UnaryResponse;
  doneFastDeal(
    requestMessage: v1_order_model_fast_deal_pb.DoneFastDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DoneFastDealResponse|null) => void
  ): UnaryResponse;
  updateStatusFastDealItem(
    requestMessage: v1_order_model_fast_deal_pb.UpdateFastDealItemStatusRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.UpdateFastDealItemStatusResponse|null) => void
  ): UnaryResponse;
  updateStatusFastDealItem(
    requestMessage: v1_order_model_fast_deal_pb.UpdateFastDealItemStatusRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.UpdateFastDealItemStatusResponse|null) => void
  ): UnaryResponse;
  getFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealFileResponse|null) => void
  ): UnaryResponse;
  getFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.GetFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.GetFastDealFileResponse|null) => void
  ): UnaryResponse;
  listFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealFileResponse|null) => void
  ): UnaryResponse;
  listFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.ListFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.ListFastDealFileResponse|null) => void
  ): UnaryResponse;
  addFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.AddFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.AddFastDealFileResponse|null) => void
  ): UnaryResponse;
  addFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.AddFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.AddFastDealFileResponse|null) => void
  ): UnaryResponse;
  deleteFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileResponse|null) => void
  ): UnaryResponse;
  deleteFastDealFile(
    requestMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_fast_deal_pb.DeleteFastDealFileResponse|null) => void
  ): UnaryResponse;
}

