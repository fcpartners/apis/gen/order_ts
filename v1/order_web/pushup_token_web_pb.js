// source: v1/order_web/pushup_token_web.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_model_pushup_notification_pb = require('../../v1/order/model_pushup_notification_pb.js');
goog.object.extend(proto, v1_order_model_pushup_notification_pb);
