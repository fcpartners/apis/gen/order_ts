// package: fcp.order.v1.order_web
// file: v1/order_web/offer_web.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";

export class SaveOfferRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_offer_pb.Offer | undefined;
  setItem(value?: v1_order_model_offer_pb.Offer): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOfferRequest): SaveOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOfferRequest;
  static deserializeBinaryFromReader(message: SaveOfferRequest, reader: jspb.BinaryReader): SaveOfferRequest;
}

export namespace SaveOfferRequest {
  export type AsObject = {
    item?: v1_order_model_offer_pb.Offer.AsObject,
  }
}

export class SaveOfferResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOfferResponse): SaveOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOfferResponse;
  static deserializeBinaryFromReader(message: SaveOfferResponse, reader: jspb.BinaryReader): SaveOfferResponse;
}

export namespace SaveOfferResponse {
  export type AsObject = {
    id: number,
  }
}

export class ActivateOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivateOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ActivateOfferRequest): ActivateOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActivateOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivateOfferRequest;
  static deserializeBinaryFromReader(message: ActivateOfferRequest, reader: jspb.BinaryReader): ActivateOfferRequest;
}

export namespace ActivateOfferRequest {
  export type AsObject = {
    id: number,
  }
}

export class ActivateOfferResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivateOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ActivateOfferResponse): ActivateOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActivateOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivateOfferResponse;
  static deserializeBinaryFromReader(message: ActivateOfferResponse, reader: jspb.BinaryReader): ActivateOfferResponse;
}

export namespace ActivateOfferResponse {
  export type AsObject = {
  }
}

export class RejectOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectOfferRequest): RejectOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectOfferRequest;
  static deserializeBinaryFromReader(message: RejectOfferRequest, reader: jspb.BinaryReader): RejectOfferRequest;
}

export namespace RejectOfferRequest {
  export type AsObject = {
    id: number,
  }
}

export class RejectOfferResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectOfferResponse): RejectOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectOfferResponse;
  static deserializeBinaryFromReader(message: RejectOfferResponse, reader: jspb.BinaryReader): RejectOfferResponse;
}

export namespace RejectOfferResponse {
  export type AsObject = {
  }
}

export class SaveMultiOfferRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_offer_pb.Offer>;
  setItemsList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addItems(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveMultiOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveMultiOfferRequest): SaveMultiOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveMultiOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveMultiOfferRequest;
  static deserializeBinaryFromReader(message: SaveMultiOfferRequest, reader: jspb.BinaryReader): SaveMultiOfferRequest;
}

export namespace SaveMultiOfferRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_offer_pb.Offer.AsObject>,
  }
}

export class SaveMultiOfferResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveMultiOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveMultiOfferResponse): SaveMultiOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveMultiOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveMultiOfferResponse;
  static deserializeBinaryFromReader(message: SaveMultiOfferResponse, reader: jspb.BinaryReader): SaveMultiOfferResponse;
}

export namespace SaveMultiOfferResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

export class ActivateMultiOfferRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_offer_pb.Offer>;
  setItemsList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addItems(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivateMultiOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ActivateMultiOfferRequest): ActivateMultiOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActivateMultiOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivateMultiOfferRequest;
  static deserializeBinaryFromReader(message: ActivateMultiOfferRequest, reader: jspb.BinaryReader): ActivateMultiOfferRequest;
}

export namespace ActivateMultiOfferRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_offer_pb.Offer.AsObject>,
  }
}

export class ActivateMultiOfferResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivateMultiOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ActivateMultiOfferResponse): ActivateMultiOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActivateMultiOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivateMultiOfferResponse;
  static deserializeBinaryFromReader(message: ActivateMultiOfferResponse, reader: jspb.BinaryReader): ActivateMultiOfferResponse;
}

export namespace ActivateMultiOfferResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

export class DeleteMultiOfferRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_offer_pb.Offer>;
  setItemsList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addItems(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteMultiOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteMultiOfferRequest): DeleteMultiOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteMultiOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteMultiOfferRequest;
  static deserializeBinaryFromReader(message: DeleteMultiOfferRequest, reader: jspb.BinaryReader): DeleteMultiOfferRequest;
}

export namespace DeleteMultiOfferRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_offer_pb.Offer.AsObject>,
  }
}

export class DeleteMultiOfferResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteMultiOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteMultiOfferResponse): DeleteMultiOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteMultiOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteMultiOfferResponse;
  static deserializeBinaryFromReader(message: DeleteMultiOfferResponse, reader: jspb.BinaryReader): DeleteMultiOfferResponse;
}

export namespace DeleteMultiOfferResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

export class RejectMultiOfferRequest extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_offer_pb.Offer>;
  setItemsList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addItems(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiOfferRequest): RejectMultiOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiOfferRequest;
  static deserializeBinaryFromReader(message: RejectMultiOfferRequest, reader: jspb.BinaryReader): RejectMultiOfferRequest;
}

export namespace RejectMultiOfferRequest {
  export type AsObject = {
    itemsList: Array<v1_order_model_offer_pb.Offer.AsObject>,
  }
}

export class RejectMultiOfferResponse extends jspb.Message {
  getIsOk(): boolean;
  setIsOk(value: boolean): void;

  clearErrsList(): void;
  getErrsList(): Array<v1_order_common_pb.ObjectError>;
  setErrsList(value: Array<v1_order_common_pb.ObjectError>): void;
  addErrs(value?: v1_order_common_pb.ObjectError, index?: number): v1_order_common_pb.ObjectError;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiOfferResponse): RejectMultiOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiOfferResponse;
  static deserializeBinaryFromReader(message: RejectMultiOfferResponse, reader: jspb.BinaryReader): RejectMultiOfferResponse;
}

export namespace RejectMultiOfferResponse {
  export type AsObject = {
    isOk: boolean,
    errsList: Array<v1_order_common_pb.ObjectError.AsObject>,
  }
}

