// package: fcp.order.v1.order_web
// file: v1/order_web/pricing_web.proto

import * as v1_order_web_pricing_web_pb from "../../v1/order_web/pricing_web_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_pricing_pb from "../../v1/order/model_pricing_pb";
import {grpc} from "@improbable-eng/grpc-web";

type PricingServiceListPricingProcess = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingProcessType = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingCategory = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingBrand = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingProduct = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingQuantityType = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingProductGroup = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProductGroupRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingWarehouse = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingWarehouseLocationRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingNames = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingNamesRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListPricingNamesResponse;
};

type PricingServiceListPricingSubGroup = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingProductSubGroupRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingSortType = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingSortTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingPlantType = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingPlantTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingSpecies = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingSpeciesRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingMaturityGroup = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingMaturityGroupRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingFruitForm = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingFruitFormRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceListPricingPollinationType = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingPollinationTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type PricingServiceGetPricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.GetPricingRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.GetPricingResponse;
};

type PricingServiceListPricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_pricing_pb.ListPricingRequest;
  readonly responseType: typeof v1_order_model_pricing_pb.ListPricingResponse;
};

type PricingServiceSavePricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_pricing_web_pb.SavePricingRequest;
  readonly responseType: typeof v1_order_web_pricing_web_pb.SavePricingResponse;
};

type PricingServiceDeletePricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_pricing_web_pb.DeletePricingRequest;
  readonly responseType: typeof v1_order_web_pricing_web_pb.DeletePricingResponse;
};

type PricingServiceDeletePriceList = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_pricing_web_pb.DeletePriceListRequest;
  readonly responseType: typeof v1_order_web_pricing_web_pb.DeletePriceListResponse;
};

type PricingServiceUpdatePriceList = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_pricing_web_pb.UpdatePriceListRequest;
  readonly responseType: typeof v1_order_web_pricing_web_pb.UpdatePriceListResponse;
};

type PricingServiceUploadPricing = {
  readonly methodName: string;
  readonly service: typeof PricingService;
  readonly requestStream: true;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_pricing_web_pb.UploadPricingRequest;
  readonly responseType: typeof v1_order_web_pricing_web_pb.UploadPricingResponse;
};

export class PricingService {
  static readonly serviceName: string;
  static readonly ListPricingProcess: PricingServiceListPricingProcess;
  static readonly ListPricingProcessType: PricingServiceListPricingProcessType;
  static readonly ListPricingCategory: PricingServiceListPricingCategory;
  static readonly ListPricingBrand: PricingServiceListPricingBrand;
  static readonly ListPricingProduct: PricingServiceListPricingProduct;
  static readonly ListPricingQuantityType: PricingServiceListPricingQuantityType;
  static readonly ListPricingProductGroup: PricingServiceListPricingProductGroup;
  static readonly ListPricingWarehouse: PricingServiceListPricingWarehouse;
  static readonly ListPricingDeliveryCondition: PricingServiceListPricingDeliveryCondition;
  static readonly ListPricingNames: PricingServiceListPricingNames;
  static readonly ListPricingSubGroup: PricingServiceListPricingSubGroup;
  static readonly ListPricingSortType: PricingServiceListPricingSortType;
  static readonly ListPricingPlantType: PricingServiceListPricingPlantType;
  static readonly ListPricingSpecies: PricingServiceListPricingSpecies;
  static readonly ListPricingMaturityGroup: PricingServiceListPricingMaturityGroup;
  static readonly ListPricingFruitForm: PricingServiceListPricingFruitForm;
  static readonly ListPricingPollinationType: PricingServiceListPricingPollinationType;
  static readonly GetPricing: PricingServiceGetPricing;
  static readonly ListPricing: PricingServiceListPricing;
  static readonly SavePricing: PricingServiceSavePricing;
  static readonly DeletePricing: PricingServiceDeletePricing;
  static readonly DeletePriceList: PricingServiceDeletePriceList;
  static readonly UpdatePriceList: PricingServiceUpdatePriceList;
  static readonly UploadPricing: PricingServiceUploadPricing;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class PricingServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listPricingProcess(
    requestMessage: v1_order_model_pricing_pb.ListPricingProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProcess(
    requestMessage: v1_order_model_pricing_pb.ListPricingProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProcessType(
    requestMessage: v1_order_model_pricing_pb.ListPricingProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProcessType(
    requestMessage: v1_order_model_pricing_pb.ListPricingProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingCategory(
    requestMessage: v1_order_model_pricing_pb.ListPricingCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingCategory(
    requestMessage: v1_order_model_pricing_pb.ListPricingCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingBrand(
    requestMessage: v1_order_model_pricing_pb.ListPricingBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingBrand(
    requestMessage: v1_order_model_pricing_pb.ListPricingBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProduct(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProduct(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingQuantityType(
    requestMessage: v1_order_model_pricing_pb.ListPricingQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingQuantityType(
    requestMessage: v1_order_model_pricing_pb.ListPricingQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProductGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingProductGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingWarehouse(
    requestMessage: v1_order_model_pricing_pb.ListPricingWarehouseLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingWarehouse(
    requestMessage: v1_order_model_pricing_pb.ListPricingWarehouseLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingDeliveryCondition(
    requestMessage: v1_order_model_pricing_pb.ListPricingDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingDeliveryCondition(
    requestMessage: v1_order_model_pricing_pb.ListPricingDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingNames(
    requestMessage: v1_order_model_pricing_pb.ListPricingNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingNamesResponse|null) => void
  ): UnaryResponse;
  listPricingNames(
    requestMessage: v1_order_model_pricing_pb.ListPricingNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingNamesResponse|null) => void
  ): UnaryResponse;
  listPricingSubGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingSubGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingProductSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingSortType(
    requestMessage: v1_order_model_pricing_pb.ListPricingSortTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingSortType(
    requestMessage: v1_order_model_pricing_pb.ListPricingSortTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingPlantType(
    requestMessage: v1_order_model_pricing_pb.ListPricingPlantTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingPlantType(
    requestMessage: v1_order_model_pricing_pb.ListPricingPlantTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingSpecies(
    requestMessage: v1_order_model_pricing_pb.ListPricingSpeciesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingSpecies(
    requestMessage: v1_order_model_pricing_pb.ListPricingSpeciesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingMaturityGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingMaturityGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingMaturityGroup(
    requestMessage: v1_order_model_pricing_pb.ListPricingMaturityGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingFruitForm(
    requestMessage: v1_order_model_pricing_pb.ListPricingFruitFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingFruitForm(
    requestMessage: v1_order_model_pricing_pb.ListPricingFruitFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingPollinationType(
    requestMessage: v1_order_model_pricing_pb.ListPricingPollinationTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listPricingPollinationType(
    requestMessage: v1_order_model_pricing_pb.ListPricingPollinationTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  getPricing(
    requestMessage: v1_order_model_pricing_pb.GetPricingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.GetPricingResponse|null) => void
  ): UnaryResponse;
  getPricing(
    requestMessage: v1_order_model_pricing_pb.GetPricingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.GetPricingResponse|null) => void
  ): UnaryResponse;
  listPricing(
    requestMessage: v1_order_model_pricing_pb.ListPricingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingResponse|null) => void
  ): UnaryResponse;
  listPricing(
    requestMessage: v1_order_model_pricing_pb.ListPricingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_pricing_pb.ListPricingResponse|null) => void
  ): UnaryResponse;
  savePricing(
    requestMessage: v1_order_web_pricing_web_pb.SavePricingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.SavePricingResponse|null) => void
  ): UnaryResponse;
  savePricing(
    requestMessage: v1_order_web_pricing_web_pb.SavePricingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.SavePricingResponse|null) => void
  ): UnaryResponse;
  deletePricing(
    requestMessage: v1_order_web_pricing_web_pb.DeletePricingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.DeletePricingResponse|null) => void
  ): UnaryResponse;
  deletePricing(
    requestMessage: v1_order_web_pricing_web_pb.DeletePricingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.DeletePricingResponse|null) => void
  ): UnaryResponse;
  deletePriceList(
    requestMessage: v1_order_web_pricing_web_pb.DeletePriceListRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.DeletePriceListResponse|null) => void
  ): UnaryResponse;
  deletePriceList(
    requestMessage: v1_order_web_pricing_web_pb.DeletePriceListRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.DeletePriceListResponse|null) => void
  ): UnaryResponse;
  updatePriceList(
    requestMessage: v1_order_web_pricing_web_pb.UpdatePriceListRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.UpdatePriceListResponse|null) => void
  ): UnaryResponse;
  updatePriceList(
    requestMessage: v1_order_web_pricing_web_pb.UpdatePriceListRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_pricing_web_pb.UpdatePriceListResponse|null) => void
  ): UnaryResponse;
  uploadPricing(metadata?: grpc.Metadata): RequestStream<v1_order_web_pricing_web_pb.UploadPricingRequest>;
}

