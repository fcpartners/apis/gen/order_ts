// package: fcp.order.v1.order_web
// file: v1/order_web/offer_web.proto

var v1_order_web_offer_web_pb = require("../../v1/order_web/offer_web_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_offer_pb = require("../../v1/order/model_offer_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var OfferService = (function () {
  function OfferService() {}
  OfferService.serviceName = "fcp.order.v1.order_web.OfferService";
  return OfferService;
}());

OfferService.ListOfferProcess = {
  methodName: "ListOfferProcess",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferProcessType = {
  methodName: "ListOfferProcessType",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferDeliveryCondition = {
  methodName: "ListOfferDeliveryCondition",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferPaymentCondition = {
  methodName: "ListOfferPaymentCondition",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferCategory = {
  methodName: "ListOfferCategory",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferBrand = {
  methodName: "ListOfferBrand",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferProduct = {
  methodName: "ListOfferProduct",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferQuantityType = {
  methodName: "ListOfferQuantityType",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

OfferService.ListOfferNames = {
  methodName: "ListOfferNames",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferNamesRequest,
  responseType: v1_order_model_offer_pb.ListOfferNamesResponse
};

OfferService.GetOffer = {
  methodName: "GetOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.GetOfferRequest,
  responseType: v1_order_model_offer_pb.GetOfferResponse
};

OfferService.ListOffer = {
  methodName: "ListOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferRequest,
  responseType: v1_order_model_offer_pb.ListOfferResponse
};

OfferService.ListOfferWrapped = {
  methodName: "ListOfferWrapped",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferWrappedRequest,
  responseType: v1_order_model_offer_pb.ListOfferWrappedResponse
};

OfferService.SaveOffer = {
  methodName: "SaveOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.SaveOfferRequest,
  responseType: v1_order_web_offer_web_pb.SaveOfferResponse
};

OfferService.ActivateOffer = {
  methodName: "ActivateOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.ActivateOfferRequest,
  responseType: v1_order_web_offer_web_pb.ActivateOfferResponse
};

OfferService.RejectOffer = {
  methodName: "RejectOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.RejectOfferRequest,
  responseType: v1_order_web_offer_web_pb.RejectOfferResponse
};

OfferService.SaveMultiOffer = {
  methodName: "SaveMultiOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.SaveMultiOfferRequest,
  responseType: v1_order_web_offer_web_pb.SaveMultiOfferResponse
};

OfferService.ActivateMultiOffer = {
  methodName: "ActivateMultiOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.ActivateMultiOfferRequest,
  responseType: v1_order_web_offer_web_pb.ActivateMultiOfferResponse
};

OfferService.DeleteMultiOffer = {
  methodName: "DeleteMultiOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.DeleteMultiOfferRequest,
  responseType: v1_order_web_offer_web_pb.DeleteMultiOfferResponse
};

OfferService.RejectMultiOffer = {
  methodName: "RejectMultiOffer",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_offer_web_pb.RejectMultiOfferRequest,
  responseType: v1_order_web_offer_web_pb.RejectMultiOfferResponse
};

OfferService.GetOfferFile = {
  methodName: "GetOfferFile",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.GetOfferFileRequest,
  responseType: v1_order_model_offer_pb.GetOfferFileResponse
};

OfferService.ListOfferFile = {
  methodName: "ListOfferFile",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.ListOfferFileRequest,
  responseType: v1_order_model_offer_pb.ListOfferFileResponse
};

OfferService.AddOfferFile = {
  methodName: "AddOfferFile",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.AddOfferFileRequest,
  responseType: v1_order_model_offer_pb.AddOfferFileResponse
};

OfferService.DeleteOfferFile = {
  methodName: "DeleteOfferFile",
  service: OfferService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_offer_pb.DeleteOfferFileRequest,
  responseType: v1_order_model_offer_pb.DeleteOfferFileResponse
};

exports.OfferService = OfferService;

function OfferServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

OfferServiceClient.prototype.listOfferProcess = function listOfferProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferProcessType = function listOfferProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferDeliveryCondition = function listOfferDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferPaymentCondition = function listOfferPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferCategory = function listOfferCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferBrand = function listOfferBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferProduct = function listOfferProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferQuantityType = function listOfferQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferNames = function listOfferNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.getOffer = function getOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.GetOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOffer = function listOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferWrapped = function listOfferWrapped(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferWrapped, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.saveOffer = function saveOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.SaveOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.activateOffer = function activateOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ActivateOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.rejectOffer = function rejectOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.RejectOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.saveMultiOffer = function saveMultiOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.SaveMultiOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.activateMultiOffer = function activateMultiOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ActivateMultiOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.deleteMultiOffer = function deleteMultiOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.DeleteMultiOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.rejectMultiOffer = function rejectMultiOffer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.RejectMultiOffer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.getOfferFile = function getOfferFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.GetOfferFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.listOfferFile = function listOfferFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.ListOfferFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.addOfferFile = function addOfferFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.AddOfferFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

OfferServiceClient.prototype.deleteOfferFile = function deleteOfferFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(OfferService.DeleteOfferFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.OfferServiceClient = OfferServiceClient;

