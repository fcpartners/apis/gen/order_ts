// package: fcp.order.v1.order_web
// file: v1/order_web/offer_web.proto

import * as v1_order_web_offer_web_pb from "../../v1/order_web/offer_web_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";
import {grpc} from "@improbable-eng/grpc-web";

type OfferServiceListOfferProcess = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferProcessRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferProcessType = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferDeliveryCondition = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferDeliveryConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferPaymentCondition = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferPaymentConditionRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferCategory = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferCategoryRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferBrand = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferBrandRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferProduct = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferProductRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferQuantityType = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferQuantityTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type OfferServiceListOfferNames = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferNamesRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferNamesResponse;
};

type OfferServiceGetOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.GetOfferRequest;
  readonly responseType: typeof v1_order_model_offer_pb.GetOfferResponse;
};

type OfferServiceListOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferResponse;
};

type OfferServiceListOfferWrapped = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferWrappedRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferWrappedResponse;
};

type OfferServiceSaveOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.SaveOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.SaveOfferResponse;
};

type OfferServiceActivateOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.ActivateOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.ActivateOfferResponse;
};

type OfferServiceRejectOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.RejectOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.RejectOfferResponse;
};

type OfferServiceSaveMultiOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.SaveMultiOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.SaveMultiOfferResponse;
};

type OfferServiceActivateMultiOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.ActivateMultiOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.ActivateMultiOfferResponse;
};

type OfferServiceDeleteMultiOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.DeleteMultiOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.DeleteMultiOfferResponse;
};

type OfferServiceRejectMultiOffer = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_web_offer_web_pb.RejectMultiOfferRequest;
  readonly responseType: typeof v1_order_web_offer_web_pb.RejectMultiOfferResponse;
};

type OfferServiceGetOfferFile = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.GetOfferFileRequest;
  readonly responseType: typeof v1_order_model_offer_pb.GetOfferFileResponse;
};

type OfferServiceListOfferFile = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.ListOfferFileRequest;
  readonly responseType: typeof v1_order_model_offer_pb.ListOfferFileResponse;
};

type OfferServiceAddOfferFile = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.AddOfferFileRequest;
  readonly responseType: typeof v1_order_model_offer_pb.AddOfferFileResponse;
};

type OfferServiceDeleteOfferFile = {
  readonly methodName: string;
  readonly service: typeof OfferService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_offer_pb.DeleteOfferFileRequest;
  readonly responseType: typeof v1_order_model_offer_pb.DeleteOfferFileResponse;
};

export class OfferService {
  static readonly serviceName: string;
  static readonly ListOfferProcess: OfferServiceListOfferProcess;
  static readonly ListOfferProcessType: OfferServiceListOfferProcessType;
  static readonly ListOfferDeliveryCondition: OfferServiceListOfferDeliveryCondition;
  static readonly ListOfferPaymentCondition: OfferServiceListOfferPaymentCondition;
  static readonly ListOfferCategory: OfferServiceListOfferCategory;
  static readonly ListOfferBrand: OfferServiceListOfferBrand;
  static readonly ListOfferProduct: OfferServiceListOfferProduct;
  static readonly ListOfferQuantityType: OfferServiceListOfferQuantityType;
  static readonly ListOfferNames: OfferServiceListOfferNames;
  static readonly GetOffer: OfferServiceGetOffer;
  static readonly ListOffer: OfferServiceListOffer;
  static readonly ListOfferWrapped: OfferServiceListOfferWrapped;
  static readonly SaveOffer: OfferServiceSaveOffer;
  static readonly ActivateOffer: OfferServiceActivateOffer;
  static readonly RejectOffer: OfferServiceRejectOffer;
  static readonly SaveMultiOffer: OfferServiceSaveMultiOffer;
  static readonly ActivateMultiOffer: OfferServiceActivateMultiOffer;
  static readonly DeleteMultiOffer: OfferServiceDeleteMultiOffer;
  static readonly RejectMultiOffer: OfferServiceRejectMultiOffer;
  static readonly GetOfferFile: OfferServiceGetOfferFile;
  static readonly ListOfferFile: OfferServiceListOfferFile;
  static readonly AddOfferFile: OfferServiceAddOfferFile;
  static readonly DeleteOfferFile: OfferServiceDeleteOfferFile;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class OfferServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listOfferProcess(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProcess(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProcessType(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProcessType(
    requestMessage: v1_order_model_offer_pb.ListOfferProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferDeliveryCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferDeliveryConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferDeliveryCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferDeliveryConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferPaymentCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferPaymentConditionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferPaymentCondition(
    requestMessage: v1_order_model_offer_pb.ListOfferPaymentConditionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferCategory(
    requestMessage: v1_order_model_offer_pb.ListOfferCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferCategory(
    requestMessage: v1_order_model_offer_pb.ListOfferCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferBrand(
    requestMessage: v1_order_model_offer_pb.ListOfferBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferBrand(
    requestMessage: v1_order_model_offer_pb.ListOfferBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProduct(
    requestMessage: v1_order_model_offer_pb.ListOfferProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferProduct(
    requestMessage: v1_order_model_offer_pb.ListOfferProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferQuantityType(
    requestMessage: v1_order_model_offer_pb.ListOfferQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferQuantityType(
    requestMessage: v1_order_model_offer_pb.ListOfferQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listOfferNames(
    requestMessage: v1_order_model_offer_pb.ListOfferNamesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferNamesResponse|null) => void
  ): UnaryResponse;
  listOfferNames(
    requestMessage: v1_order_model_offer_pb.ListOfferNamesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferNamesResponse|null) => void
  ): UnaryResponse;
  getOffer(
    requestMessage: v1_order_model_offer_pb.GetOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferResponse|null) => void
  ): UnaryResponse;
  getOffer(
    requestMessage: v1_order_model_offer_pb.GetOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferResponse|null) => void
  ): UnaryResponse;
  listOffer(
    requestMessage: v1_order_model_offer_pb.ListOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferResponse|null) => void
  ): UnaryResponse;
  listOffer(
    requestMessage: v1_order_model_offer_pb.ListOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferResponse|null) => void
  ): UnaryResponse;
  listOfferWrapped(
    requestMessage: v1_order_model_offer_pb.ListOfferWrappedRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferWrappedResponse|null) => void
  ): UnaryResponse;
  listOfferWrapped(
    requestMessage: v1_order_model_offer_pb.ListOfferWrappedRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferWrappedResponse|null) => void
  ): UnaryResponse;
  saveOffer(
    requestMessage: v1_order_web_offer_web_pb.SaveOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.SaveOfferResponse|null) => void
  ): UnaryResponse;
  saveOffer(
    requestMessage: v1_order_web_offer_web_pb.SaveOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.SaveOfferResponse|null) => void
  ): UnaryResponse;
  activateOffer(
    requestMessage: v1_order_web_offer_web_pb.ActivateOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.ActivateOfferResponse|null) => void
  ): UnaryResponse;
  activateOffer(
    requestMessage: v1_order_web_offer_web_pb.ActivateOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.ActivateOfferResponse|null) => void
  ): UnaryResponse;
  rejectOffer(
    requestMessage: v1_order_web_offer_web_pb.RejectOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.RejectOfferResponse|null) => void
  ): UnaryResponse;
  rejectOffer(
    requestMessage: v1_order_web_offer_web_pb.RejectOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.RejectOfferResponse|null) => void
  ): UnaryResponse;
  saveMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.SaveMultiOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.SaveMultiOfferResponse|null) => void
  ): UnaryResponse;
  saveMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.SaveMultiOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.SaveMultiOfferResponse|null) => void
  ): UnaryResponse;
  activateMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.ActivateMultiOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.ActivateMultiOfferResponse|null) => void
  ): UnaryResponse;
  activateMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.ActivateMultiOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.ActivateMultiOfferResponse|null) => void
  ): UnaryResponse;
  deleteMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.DeleteMultiOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.DeleteMultiOfferResponse|null) => void
  ): UnaryResponse;
  deleteMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.DeleteMultiOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.DeleteMultiOfferResponse|null) => void
  ): UnaryResponse;
  rejectMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.RejectMultiOfferRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.RejectMultiOfferResponse|null) => void
  ): UnaryResponse;
  rejectMultiOffer(
    requestMessage: v1_order_web_offer_web_pb.RejectMultiOfferRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_web_offer_web_pb.RejectMultiOfferResponse|null) => void
  ): UnaryResponse;
  getOfferFile(
    requestMessage: v1_order_model_offer_pb.GetOfferFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferFileResponse|null) => void
  ): UnaryResponse;
  getOfferFile(
    requestMessage: v1_order_model_offer_pb.GetOfferFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.GetOfferFileResponse|null) => void
  ): UnaryResponse;
  listOfferFile(
    requestMessage: v1_order_model_offer_pb.ListOfferFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferFileResponse|null) => void
  ): UnaryResponse;
  listOfferFile(
    requestMessage: v1_order_model_offer_pb.ListOfferFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.ListOfferFileResponse|null) => void
  ): UnaryResponse;
  addOfferFile(
    requestMessage: v1_order_model_offer_pb.AddOfferFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.AddOfferFileResponse|null) => void
  ): UnaryResponse;
  addOfferFile(
    requestMessage: v1_order_model_offer_pb.AddOfferFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.AddOfferFileResponse|null) => void
  ): UnaryResponse;
  deleteOfferFile(
    requestMessage: v1_order_model_offer_pb.DeleteOfferFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.DeleteOfferFileResponse|null) => void
  ): UnaryResponse;
  deleteOfferFile(
    requestMessage: v1_order_model_offer_pb.DeleteOfferFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_offer_pb.DeleteOfferFileResponse|null) => void
  ): UnaryResponse;
}

