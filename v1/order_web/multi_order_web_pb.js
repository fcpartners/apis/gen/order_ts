// source: v1/order_web/multi_order_web.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_common_pb = require('../../v1/order/common_pb.js');
goog.object.extend(proto, v1_order_common_pb);
var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
var v1_order_model_order_pb = require('../../v1/order/model_order_pb.js');
goog.object.extend(proto, v1_order_model_order_pb);
var v1_order_model_multi_order_pb = require('../../v1/order/model_multi_order_pb.js');
goog.object.extend(proto, v1_order_model_multi_order_pb);
var v1_order_model_product_pb = require('../../v1/order/model_product_pb.js');
goog.object.extend(proto, v1_order_model_product_pb);
