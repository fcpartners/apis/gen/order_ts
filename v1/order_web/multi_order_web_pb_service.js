// package: fcp.order.v1.order_web
// file: v1/order_web/multi_order_web.proto

var v1_order_web_multi_order_web_pb = require("../../v1/order_web/multi_order_web_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_multi_order_pb = require("../../v1/order/model_multi_order_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MultiOrderService = (function () {
  function MultiOrderService() {}
  MultiOrderService.serviceName = "fcp.order.v1.order_web.MultiOrderService";
  return MultiOrderService;
}());

MultiOrderService.ListMultiOrderProcess = {
  methodName: "ListMultiOrderProcess",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderProcessType = {
  methodName: "ListMultiOrderProcessType",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderDeliveryCondition = {
  methodName: "ListMultiOrderDeliveryCondition",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderPaymentCondition = {
  methodName: "ListMultiOrderPaymentCondition",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderPaymentConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderCategory = {
  methodName: "ListMultiOrderCategory",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderBrand = {
  methodName: "ListMultiOrderBrand",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderProduct = {
  methodName: "ListMultiOrderProduct",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderQuantityType = {
  methodName: "ListMultiOrderQuantityType",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

MultiOrderService.ListMultiOrderNames = {
  methodName: "ListMultiOrderNames",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderNamesRequest,
  responseType: v1_order_model_multi_order_pb.ListMultiOrderNamesResponse
};

MultiOrderService.GetMultiOrder = {
  methodName: "GetMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.GetMultiOrderRequest,
  responseType: v1_order_model_multi_order_pb.GetMultiOrderResponse
};

MultiOrderService.ListMultiOrder = {
  methodName: "ListMultiOrder",
  service: MultiOrderService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_multi_order_pb.ListMultiOrderRequest,
  responseType: v1_order_model_multi_order_pb.ListMultiOrderResponse
};

exports.MultiOrderService = MultiOrderService;

function MultiOrderServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MultiOrderServiceClient.prototype.listMultiOrderProcess = function listMultiOrderProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderProcessType = function listMultiOrderProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderDeliveryCondition = function listMultiOrderDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderPaymentCondition = function listMultiOrderPaymentCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderPaymentCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderCategory = function listMultiOrderCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderBrand = function listMultiOrderBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderProduct = function listMultiOrderProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderQuantityType = function listMultiOrderQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrderNames = function listMultiOrderNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrderNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.getMultiOrder = function getMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.GetMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MultiOrderServiceClient.prototype.listMultiOrder = function listMultiOrder(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MultiOrderService.ListMultiOrder, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MultiOrderServiceClient = MultiOrderServiceClient;

