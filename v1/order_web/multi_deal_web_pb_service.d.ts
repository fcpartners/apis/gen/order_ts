// package: fcp.order.v1.order_web
// file: v1/order_web/multi_deal_web.proto

import * as v1_order_web_multi_deal_web_pb from "../../v1/order_web/multi_deal_web_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_multi_deal_pb from "../../v1/order/model_multi_deal_pb";
import {grpc} from "@improbable-eng/grpc-web";

type MultiDealServiceListMultiDealProcessType = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.ListMultiDealProcessTypeRequest;
  readonly responseType: typeof v1_order_enum_pb.DictResponse;
};

type MultiDealServiceGetMultiDeal = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.GetMultiDealRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.GetMultiDealResponse;
};

type MultiDealServiceListMultiDeal = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.ListMultiDealRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.ListMultiDealResponse;
};

type MultiDealServiceGetMultiDealFile = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.GetMultiDealFileRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.GetMultiDealFileResponse;
};

type MultiDealServiceListMultiDealFile = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.ListMultiDealFileRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.ListMultiDealFileResponse;
};

type MultiDealServiceAddMultiDealFile = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.AddMultiDealFileRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.AddMultiDealFileResponse;
};

type MultiDealServiceDeleteMultiDealFile = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.DeleteMultiDealFileRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.DeleteMultiDealFileResponse;
};

type MultiDealServiceGetMultiDealContact = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.GetMultiDealContactRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.GetMultiDealContactResponse;
};

type MultiDealServiceListMultiDealContact = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.ListMultiDealContactRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.ListMultiDealContactResponse;
};

type MultiDealServiceRejectMultiDeal = {
  readonly methodName: string;
  readonly service: typeof MultiDealService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_order_model_multi_deal_pb.RejectMultiDealRequest;
  readonly responseType: typeof v1_order_model_multi_deal_pb.RejectMultiDealResponse;
};

export class MultiDealService {
  static readonly serviceName: string;
  static readonly ListMultiDealProcessType: MultiDealServiceListMultiDealProcessType;
  static readonly GetMultiDeal: MultiDealServiceGetMultiDeal;
  static readonly ListMultiDeal: MultiDealServiceListMultiDeal;
  static readonly GetMultiDealFile: MultiDealServiceGetMultiDealFile;
  static readonly ListMultiDealFile: MultiDealServiceListMultiDealFile;
  static readonly AddMultiDealFile: MultiDealServiceAddMultiDealFile;
  static readonly DeleteMultiDealFile: MultiDealServiceDeleteMultiDealFile;
  static readonly GetMultiDealContact: MultiDealServiceGetMultiDealContact;
  static readonly ListMultiDealContact: MultiDealServiceListMultiDealContact;
  static readonly RejectMultiDeal: MultiDealServiceRejectMultiDeal;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class MultiDealServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listMultiDealProcessType(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealProcessTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  listMultiDealProcessType(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealProcessTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_enum_pb.DictResponse|null) => void
  ): UnaryResponse;
  getMultiDeal(
    requestMessage: v1_order_model_multi_deal_pb.GetMultiDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.GetMultiDealResponse|null) => void
  ): UnaryResponse;
  getMultiDeal(
    requestMessage: v1_order_model_multi_deal_pb.GetMultiDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.GetMultiDealResponse|null) => void
  ): UnaryResponse;
  listMultiDeal(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.ListMultiDealResponse|null) => void
  ): UnaryResponse;
  listMultiDeal(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.ListMultiDealResponse|null) => void
  ): UnaryResponse;
  getMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.GetMultiDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.GetMultiDealFileResponse|null) => void
  ): UnaryResponse;
  getMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.GetMultiDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.GetMultiDealFileResponse|null) => void
  ): UnaryResponse;
  listMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.ListMultiDealFileResponse|null) => void
  ): UnaryResponse;
  listMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.ListMultiDealFileResponse|null) => void
  ): UnaryResponse;
  addMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.AddMultiDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.AddMultiDealFileResponse|null) => void
  ): UnaryResponse;
  addMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.AddMultiDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.AddMultiDealFileResponse|null) => void
  ): UnaryResponse;
  deleteMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.DeleteMultiDealFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.DeleteMultiDealFileResponse|null) => void
  ): UnaryResponse;
  deleteMultiDealFile(
    requestMessage: v1_order_model_multi_deal_pb.DeleteMultiDealFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.DeleteMultiDealFileResponse|null) => void
  ): UnaryResponse;
  getMultiDealContact(
    requestMessage: v1_order_model_multi_deal_pb.GetMultiDealContactRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.GetMultiDealContactResponse|null) => void
  ): UnaryResponse;
  getMultiDealContact(
    requestMessage: v1_order_model_multi_deal_pb.GetMultiDealContactRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.GetMultiDealContactResponse|null) => void
  ): UnaryResponse;
  listMultiDealContact(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealContactRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.ListMultiDealContactResponse|null) => void
  ): UnaryResponse;
  listMultiDealContact(
    requestMessage: v1_order_model_multi_deal_pb.ListMultiDealContactRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.ListMultiDealContactResponse|null) => void
  ): UnaryResponse;
  rejectMultiDeal(
    requestMessage: v1_order_model_multi_deal_pb.RejectMultiDealRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.RejectMultiDealResponse|null) => void
  ): UnaryResponse;
  rejectMultiDeal(
    requestMessage: v1_order_model_multi_deal_pb.RejectMultiDealRequest,
    callback: (error: ServiceError|null, responseMessage: v1_order_model_multi_deal_pb.RejectMultiDealResponse|null) => void
  ): UnaryResponse;
}

