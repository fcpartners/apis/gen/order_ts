// package: fcp.order.v1.order_web
// file: v1/order_web/pricing_web.proto

var v1_order_web_pricing_web_pb = require("../../v1/order_web/pricing_web_pb");
var v1_order_enum_pb = require("../../v1/order/enum_pb");
var v1_order_model_pricing_pb = require("../../v1/order/model_pricing_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var PricingService = (function () {
  function PricingService() {}
  PricingService.serviceName = "fcp.order.v1.order_web.PricingService";
  return PricingService;
}());

PricingService.ListPricingProcess = {
  methodName: "ListPricingProcess",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProcessRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingProcessType = {
  methodName: "ListPricingProcessType",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProcessTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingCategory = {
  methodName: "ListPricingCategory",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingCategoryRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingBrand = {
  methodName: "ListPricingBrand",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingBrandRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingProduct = {
  methodName: "ListPricingProduct",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProductRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingQuantityType = {
  methodName: "ListPricingQuantityType",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingQuantityTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingProductGroup = {
  methodName: "ListPricingProductGroup",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProductGroupRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingWarehouse = {
  methodName: "ListPricingWarehouse",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingWarehouseLocationRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingDeliveryCondition = {
  methodName: "ListPricingDeliveryCondition",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingDeliveryConditionRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingNames = {
  methodName: "ListPricingNames",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingNamesRequest,
  responseType: v1_order_model_pricing_pb.ListPricingNamesResponse
};

PricingService.ListPricingSubGroup = {
  methodName: "ListPricingSubGroup",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingProductSubGroupRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingSortType = {
  methodName: "ListPricingSortType",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingSortTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingPlantType = {
  methodName: "ListPricingPlantType",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingPlantTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingSpecies = {
  methodName: "ListPricingSpecies",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingSpeciesRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingMaturityGroup = {
  methodName: "ListPricingMaturityGroup",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingMaturityGroupRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingFruitForm = {
  methodName: "ListPricingFruitForm",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingFruitFormRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.ListPricingPollinationType = {
  methodName: "ListPricingPollinationType",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingPollinationTypeRequest,
  responseType: v1_order_enum_pb.DictResponse
};

PricingService.GetPricing = {
  methodName: "GetPricing",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.GetPricingRequest,
  responseType: v1_order_model_pricing_pb.GetPricingResponse
};

PricingService.ListPricing = {
  methodName: "ListPricing",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pricing_pb.ListPricingRequest,
  responseType: v1_order_model_pricing_pb.ListPricingResponse
};

PricingService.SavePricing = {
  methodName: "SavePricing",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_pricing_web_pb.SavePricingRequest,
  responseType: v1_order_web_pricing_web_pb.SavePricingResponse
};

PricingService.DeletePricing = {
  methodName: "DeletePricing",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_pricing_web_pb.DeletePricingRequest,
  responseType: v1_order_web_pricing_web_pb.DeletePricingResponse
};

PricingService.DeletePriceList = {
  methodName: "DeletePriceList",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_pricing_web_pb.DeletePriceListRequest,
  responseType: v1_order_web_pricing_web_pb.DeletePriceListResponse
};

PricingService.UpdatePriceList = {
  methodName: "UpdatePriceList",
  service: PricingService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_web_pricing_web_pb.UpdatePriceListRequest,
  responseType: v1_order_web_pricing_web_pb.UpdatePriceListResponse
};

PricingService.UploadPricing = {
  methodName: "UploadPricing",
  service: PricingService,
  requestStream: true,
  responseStream: false,
  requestType: v1_order_web_pricing_web_pb.UploadPricingRequest,
  responseType: v1_order_web_pricing_web_pb.UploadPricingResponse
};

exports.PricingService = PricingService;

function PricingServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

PricingServiceClient.prototype.listPricingProcess = function listPricingProcess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingProcess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingProcessType = function listPricingProcessType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingProcessType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingCategory = function listPricingCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingBrand = function listPricingBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingProduct = function listPricingProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingQuantityType = function listPricingQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingProductGroup = function listPricingProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingWarehouse = function listPricingWarehouse(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingWarehouse, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingDeliveryCondition = function listPricingDeliveryCondition(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingDeliveryCondition, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingNames = function listPricingNames(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingNames, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingSubGroup = function listPricingSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingSortType = function listPricingSortType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingSortType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingPlantType = function listPricingPlantType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingPlantType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingSpecies = function listPricingSpecies(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingSpecies, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingMaturityGroup = function listPricingMaturityGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingMaturityGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingFruitForm = function listPricingFruitForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingFruitForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricingPollinationType = function listPricingPollinationType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricingPollinationType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.getPricing = function getPricing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.GetPricing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.listPricing = function listPricing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.ListPricing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.savePricing = function savePricing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.SavePricing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.deletePricing = function deletePricing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.DeletePricing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.deletePriceList = function deletePriceList(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.DeletePriceList, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.updatePriceList = function updatePriceList(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PricingService.UpdatePriceList, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PricingServiceClient.prototype.uploadPricing = function uploadPricing(metadata) {
  var listeners = {
    end: [],
    status: []
  };
  var client = grpc.client(PricingService.UploadPricing, {
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport
  });
  client.onEnd(function (status, statusMessage, trailers) {
    listeners.status.forEach(function (handler) {
      handler({ code: status, details: statusMessage, metadata: trailers });
    });
    listeners.end.forEach(function (handler) {
      handler({ code: status, details: statusMessage, metadata: trailers });
    });
    listeners = null;
  });
  return {
    on: function (type, handler) {
      listeners[type].push(handler);
      return this;
    },
    write: function (requestMessage) {
      if (!client.started) {
        client.start(metadata);
      }
      client.send(requestMessage);
      return this;
    },
    end: function () {
      client.finishSend();
    },
    cancel: function () {
      listeners = null;
      client.close();
    }
  };
};

exports.PricingServiceClient = PricingServiceClient;

