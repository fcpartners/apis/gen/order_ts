// package: fcp.order.v1.order_web
// file: v1/order_web/pricing_web.proto

import * as jspb from "google-protobuf";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_pricing_pb from "../../v1/order/model_pricing_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";

export class SavePricingRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_pricing_pb.Pricing | undefined;
  setItem(value?: v1_order_model_pricing_pb.Pricing): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePricingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SavePricingRequest): SavePricingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePricingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePricingRequest;
  static deserializeBinaryFromReader(message: SavePricingRequest, reader: jspb.BinaryReader): SavePricingRequest;
}

export namespace SavePricingRequest {
  export type AsObject = {
    item?: v1_order_model_pricing_pb.Pricing.AsObject,
  }
}

export class SavePricingResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePricingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SavePricingResponse): SavePricingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePricingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePricingResponse;
  static deserializeBinaryFromReader(message: SavePricingResponse, reader: jspb.BinaryReader): SavePricingResponse;
}

export namespace SavePricingResponse {
  export type AsObject = {
    id: number,
  }
}

export class DeletePricingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePricingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePricingRequest): DeletePricingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePricingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePricingRequest;
  static deserializeBinaryFromReader(message: DeletePricingRequest, reader: jspb.BinaryReader): DeletePricingRequest;
}

export namespace DeletePricingRequest {
  export type AsObject = {
    id: number,
  }
}

export class DeletePricingResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePricingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePricingResponse): DeletePricingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePricingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePricingResponse;
  static deserializeBinaryFromReader(message: DeletePricingResponse, reader: jspb.BinaryReader): DeletePricingResponse;
}

export namespace DeletePricingResponse {
  export type AsObject = {
  }
}

export class UpdatePriceListRequest extends jspb.Message {
  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getStatus(): v1_order_model_pricing_pb.Pricing.StatusMap[keyof v1_order_model_pricing_pb.Pricing.StatusMap];
  setStatus(value: v1_order_model_pricing_pb.Pricing.StatusMap[keyof v1_order_model_pricing_pb.Pricing.StatusMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdatePriceListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdatePriceListRequest): UpdatePriceListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdatePriceListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdatePriceListRequest;
  static deserializeBinaryFromReader(message: UpdatePriceListRequest, reader: jspb.BinaryReader): UpdatePriceListRequest;
}

export namespace UpdatePriceListRequest {
  export type AsObject = {
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    status: v1_order_model_pricing_pb.Pricing.StatusMap[keyof v1_order_model_pricing_pb.Pricing.StatusMap],
  }
}

export class UpdatePriceListResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdatePriceListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdatePriceListResponse): UpdatePriceListResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdatePriceListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdatePriceListResponse;
  static deserializeBinaryFromReader(message: UpdatePriceListResponse, reader: jspb.BinaryReader): UpdatePriceListResponse;
}

export namespace UpdatePriceListResponse {
  export type AsObject = {
  }
}

export class DeletePriceListRequest extends jspb.Message {
  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePriceListRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePriceListRequest): DeletePriceListRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePriceListRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePriceListRequest;
  static deserializeBinaryFromReader(message: DeletePriceListRequest, reader: jspb.BinaryReader): DeletePriceListRequest;
}

export namespace DeletePriceListRequest {
  export type AsObject = {
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
  }
}

export class DeletePriceListResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeletePriceListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeletePriceListResponse): DeletePriceListResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeletePriceListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeletePriceListResponse;
  static deserializeBinaryFromReader(message: DeletePriceListResponse, reader: jspb.BinaryReader): DeletePriceListResponse;
}

export namespace DeletePriceListResponse {
  export type AsObject = {
  }
}

export class UploadPricingRequest extends jspb.Message {
  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadPricingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UploadPricingRequest): UploadPricingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UploadPricingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadPricingRequest;
  static deserializeBinaryFromReader(message: UploadPricingRequest, reader: jspb.BinaryReader): UploadPricingRequest;
}

export namespace UploadPricingRequest {
  export type AsObject = {
    content: Uint8Array | string,
  }
}

export class UploadPricingResponse extends jspb.Message {
  getStatus(): UploadPricingResponse.UploadStatusMap[keyof UploadPricingResponse.UploadStatusMap];
  setStatus(value: UploadPricingResponse.UploadStatusMap[keyof UploadPricingResponse.UploadStatusMap]): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadPricingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UploadPricingResponse): UploadPricingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UploadPricingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadPricingResponse;
  static deserializeBinaryFromReader(message: UploadPricingResponse, reader: jspb.BinaryReader): UploadPricingResponse;
}

export namespace UploadPricingResponse {
  export type AsObject = {
    status: UploadPricingResponse.UploadStatusMap[keyof UploadPricingResponse.UploadStatusMap],
    message: string,
  }

  export interface UploadStatusMap {
    UNKNOWN: 0;
    OK: 1;
    FAILED: 2;
  }

  export const UploadStatus: UploadStatusMap;
}

