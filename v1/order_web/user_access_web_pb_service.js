// package: fcp.order.v1.order_web
// file: v1/order_web/user_access_web.proto

var v1_order_web_user_access_web_pb = require("../../v1/order_web/user_access_web_pb");
var v1_order_model_user_access_pb = require("../../v1/order/model_user_access_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var UserAccessService = (function () {
  function UserAccessService() {}
  UserAccessService.serviceName = "fcp.order.v1.order_web.UserAccessService";
  return UserAccessService;
}());

UserAccessService.GetUserAccess = {
  methodName: "GetUserAccess",
  service: UserAccessService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_user_access_pb.GetUserAccessRequest,
  responseType: v1_order_model_user_access_pb.GetUserAccessResponse
};

exports.UserAccessService = UserAccessService;

function UserAccessServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

UserAccessServiceClient.prototype.getUserAccess = function getUserAccess(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserAccessService.GetUserAccess, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.UserAccessServiceClient = UserAccessServiceClient;

