// package: fcp.order.v1.order_web
// file: v1/order_web/pushup_token_web.proto

var v1_order_web_pushup_token_web_pb = require("../../v1/order_web/pushup_token_web_pb");
var v1_order_model_pushup_notification_pb = require("../../v1/order/model_pushup_notification_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var PushUpService = (function () {
  function PushUpService() {}
  PushUpService.serviceName = "fcp.order.v1.order_web.PushUpService";
  return PushUpService;
}());

PushUpService.SendToken = {
  methodName: "SendToken",
  service: PushUpService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pushup_notification_pb.SendTokenRequest,
  responseType: v1_order_model_pushup_notification_pb.SendTokenResponse
};

PushUpService.RemoveToken = {
  methodName: "RemoveToken",
  service: PushUpService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_pushup_notification_pb.RemoveTokenRequest,
  responseType: v1_order_model_pushup_notification_pb.RemoveTokenResponse
};

exports.PushUpService = PushUpService;

function PushUpServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

PushUpServiceClient.prototype.sendToken = function sendToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PushUpService.SendToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

PushUpServiceClient.prototype.removeToken = function removeToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(PushUpService.RemoveToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.PushUpServiceClient = PushUpServiceClient;

