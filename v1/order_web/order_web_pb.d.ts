// package: fcp.order.v1.order_web
// file: v1/order_web/order_web.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";

