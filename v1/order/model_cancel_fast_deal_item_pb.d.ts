// package: fcp.order.v1.order
// file: v1/order/model_cancel_fast_deal_item.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_deal_pb from "../../v1/order/model_deal_pb";

export class CancelFastDealItem extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  getStatus(): CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap];
  setStatus(value: CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap]): void;

  getFastDealItemId(): number;
  setFastDealItemId(value: number): void;

  getFastDealItemExternalId(): string;
  setFastDealItemExternalId(value: string): void;

  getReason(): v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap];
  setReason(value: v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap]): void;

  getReasonComment(): string;
  setReasonComment(value: string): void;

  getAdminId(): string;
  setAdminId(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CancelFastDealItem.AsObject;
  static toObject(includeInstance: boolean, msg: CancelFastDealItem): CancelFastDealItem.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CancelFastDealItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CancelFastDealItem;
  static deserializeBinaryFromReader(message: CancelFastDealItem, reader: jspb.BinaryReader): CancelFastDealItem;
}

export namespace CancelFastDealItem {
  export type AsObject = {
    id: number,
    externalId: string,
    status: CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap],
    fastDealItemId: number,
    fastDealItemExternalId: string,
    reason: v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap],
    reasonComment: string,
    adminId: string,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class StatusValue extends jspb.Message {
    getValue(): CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap];
    setValue(value: CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
  }

  export const Status: StatusMap;
}

export class CancelFastDealItemFilter extends jspb.Message {
  clearStatusesList(): void;
  getStatusesList(): Array<CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap]>;
  setStatusesList(value: Array<CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap]>): void;
  addStatuses(value: CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap], index?: number): CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap];

  hasReason(): boolean;
  clearReason(): void;
  getReason(): v1_order_enum_pb.CancelClaimReasonValue | undefined;
  setReason(value?: v1_order_enum_pb.CancelClaimReasonValue): void;

  hasReasonComment(): boolean;
  clearReasonComment(): void;
  getReasonComment(): google_protobuf_wrappers_pb.StringValue | undefined;
  setReasonComment(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFastDealItemId(): boolean;
  clearFastDealItemId(): void;
  getFastDealItemId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFastDealItemId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_order_common_pb.StateValue | undefined;
  setState(value?: v1_order_common_pb.StateValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CancelFastDealItemFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CancelFastDealItemFilter): CancelFastDealItemFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CancelFastDealItemFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CancelFastDealItemFilter;
  static deserializeBinaryFromReader(message: CancelFastDealItemFilter, reader: jspb.BinaryReader): CancelFastDealItemFilter;
}

export namespace CancelFastDealItemFilter {
  export type AsObject = {
    statusesList: Array<CancelFastDealItem.StatusMap[keyof CancelFastDealItem.StatusMap]>,
    reason?: v1_order_enum_pb.CancelClaimReasonValue.AsObject,
    reasonComment?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fastDealItemId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    state?: v1_order_common_pb.StateValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListCancelFastDealItemRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CancelFastDealItemFilter | undefined;
  setFilter(value?: CancelFastDealItemFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCancelFastDealItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCancelFastDealItemRequest): ListCancelFastDealItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCancelFastDealItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCancelFastDealItemRequest;
  static deserializeBinaryFromReader(message: ListCancelFastDealItemRequest, reader: jspb.BinaryReader): ListCancelFastDealItemRequest;
}

export namespace ListCancelFastDealItemRequest {
  export type AsObject = {
    filter?: CancelFastDealItemFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCancelFastDealItemResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<CancelFastDealItem>;
  setItemsList(value: Array<CancelFastDealItem>): void;
  addItems(value?: CancelFastDealItem, index?: number): CancelFastDealItem;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCancelFastDealItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCancelFastDealItemResponse): ListCancelFastDealItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCancelFastDealItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCancelFastDealItemResponse;
  static deserializeBinaryFromReader(message: ListCancelFastDealItemResponse, reader: jspb.BinaryReader): ListCancelFastDealItemResponse;
}

export namespace ListCancelFastDealItemResponse {
  export type AsObject = {
    itemsList: Array<CancelFastDealItem.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetCancelFastDealItemRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCancelFastDealItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCancelFastDealItemRequest): GetCancelFastDealItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCancelFastDealItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCancelFastDealItemRequest;
  static deserializeBinaryFromReader(message: GetCancelFastDealItemRequest, reader: jspb.BinaryReader): GetCancelFastDealItemRequest;
}

export namespace GetCancelFastDealItemRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCancelFastDealItemResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CancelFastDealItem | undefined;
  setItem(value?: CancelFastDealItem): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCancelFastDealItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCancelFastDealItemResponse): GetCancelFastDealItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCancelFastDealItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCancelFastDealItemResponse;
  static deserializeBinaryFromReader(message: GetCancelFastDealItemResponse, reader: jspb.BinaryReader): GetCancelFastDealItemResponse;
}

export namespace GetCancelFastDealItemResponse {
  export type AsObject = {
    item?: CancelFastDealItem.AsObject,
  }
}

export class SaveCancelFastDealItemRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CancelFastDealItem | undefined;
  setItem(value?: CancelFastDealItem): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCancelFastDealItemRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCancelFastDealItemRequest): SaveCancelFastDealItemRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCancelFastDealItemRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCancelFastDealItemRequest;
  static deserializeBinaryFromReader(message: SaveCancelFastDealItemRequest, reader: jspb.BinaryReader): SaveCancelFastDealItemRequest;
}

export namespace SaveCancelFastDealItemRequest {
  export type AsObject = {
    item?: CancelFastDealItem.AsObject,
  }
}

export class SaveCancelFastDealItemResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCancelFastDealItemResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCancelFastDealItemResponse): SaveCancelFastDealItemResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCancelFastDealItemResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCancelFastDealItemResponse;
  static deserializeBinaryFromReader(message: SaveCancelFastDealItemResponse, reader: jspb.BinaryReader): SaveCancelFastDealItemResponse;
}

export namespace SaveCancelFastDealItemResponse {
  export type AsObject = {
    id: number,
  }
}

