// package: fcp.order.v1.order
// file: v1/order/model_deal.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_file_pb from "../../v1/order/file_pb";
import * as v1_order_finance_pb from "../../v1/order/finance_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";
import * as v1_order_model_multi_deal_pb from "../../v1/order/model_multi_deal_pb";

export class DealResult extends jspb.Message {
  hasContract(): boolean;
  clearContract(): void;
  getContract(): v1_order_model_base_pb.OperationIdent | undefined;
  setContract(value?: v1_order_model_base_pb.OperationIdent): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DealResult.AsObject;
  static toObject(includeInstance: boolean, msg: DealResult): DealResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DealResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DealResult;
  static deserializeBinaryFromReader(message: DealResult, reader: jspb.BinaryReader): DealResult;
}

export namespace DealResult {
  export type AsObject = {
    contract?: v1_order_model_base_pb.OperationIdent.AsObject,
  }
}

export class Deal extends jspb.Message {
  getMultiDealId(): number;
  setMultiDealId(value: number): void;

  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getStatus(): Deal.StatusMap[keyof Deal.StatusMap];
  setStatus(value: Deal.StatusMap[keyof Deal.StatusMap]): void;

  hasOrder(): boolean;
  clearOrder(): void;
  getOrder(): v1_order_model_order_pb.Order | undefined;
  setOrder(value?: v1_order_model_order_pb.Order): void;

  hasOffer(): boolean;
  clearOffer(): void;
  getOffer(): v1_order_model_offer_pb.Offer | undefined;
  setOffer(value?: v1_order_model_offer_pb.Offer): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getDiscount(): number;
  setDiscount(value: number): void;

  getAmount(): number;
  setAmount(value: number): void;

  getAmountWithVat(): number;
  setAmountWithVat(value: number): void;

  getVatAmount(): number;
  setVatAmount(value: number): void;

  getBasicAmount(): number;
  setBasicAmount(value: number): void;

  getBasicAmountWithVat(): number;
  setBasicAmountWithVat(value: number): void;

  getVatBasicAmount(): number;
  setVatBasicAmount(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  hasResult(): boolean;
  clearResult(): void;
  getResult(): DealResult | undefined;
  setResult(value?: DealResult): void;

  getChatId(): number;
  setChatId(value: number): void;

  getChatIdent(): string;
  setChatIdent(value: string): void;

  hasOrderUser(): boolean;
  clearOrderUser(): void;
  getOrderUser(): v1_order_model_base_pb.UserInfo | undefined;
  setOrderUser(value?: v1_order_model_base_pb.UserInfo): void;

  hasOfferUser(): boolean;
  clearOfferUser(): void;
  getOfferUser(): v1_order_model_base_pb.UserInfo | undefined;
  setOfferUser(value?: v1_order_model_base_pb.UserInfo): void;

  getCancelInProgress(): boolean;
  setCancelInProgress(value: boolean): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  getDeliveryPriceApplied(): boolean;
  setDeliveryPriceApplied(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Deal.AsObject;
  static toObject(includeInstance: boolean, msg: Deal): Deal.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Deal, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Deal;
  static deserializeBinaryFromReader(message: Deal, reader: jspb.BinaryReader): Deal;
}

export namespace Deal {
  export type AsObject = {
    multiDealId: number,
    id: number,
    name: string,
    status: Deal.StatusMap[keyof Deal.StatusMap],
    order?: v1_order_model_order_pb.Order.AsObject,
    offer?: v1_order_model_offer_pb.Offer.AsObject,
    currency: string,
    discount: number,
    amount: number,
    amountWithVat: number,
    vatAmount: number,
    basicAmount: number,
    basicAmountWithVat: number,
    vatBasicAmount: number,
    externalId: string,
    result?: DealResult.AsObject,
    chatId: number,
    chatIdent: string,
    orderUser?: v1_order_model_base_pb.UserInfo.AsObject,
    offerUser?: v1_order_model_base_pb.UserInfo.AsObject,
    cancelInProgress: boolean,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
    deliveryPriceApplied: boolean,
  }

  export class StatusValue extends jspb.Message {
    getValue(): Deal.StatusMap[keyof Deal.StatusMap];
    setValue(value: Deal.StatusMap[keyof Deal.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: Deal.StatusMap[keyof Deal.StatusMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
  }

  export const Status: StatusMap;
}

export class DealWrapped extends jspb.Message {
  hasMultiDeal(): boolean;
  clearMultiDeal(): void;
  getMultiDeal(): v1_order_model_multi_deal_pb.MultiDeal | undefined;
  setMultiDeal(value?: v1_order_model_multi_deal_pb.MultiDeal): void;

  clearDealsList(): void;
  getDealsList(): Array<Deal>;
  setDealsList(value: Array<Deal>): void;
  addDeals(value?: Deal, index?: number): Deal;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DealWrapped.AsObject;
  static toObject(includeInstance: boolean, msg: DealWrapped): DealWrapped.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DealWrapped, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DealWrapped;
  static deserializeBinaryFromReader(message: DealWrapped, reader: jspb.BinaryReader): DealWrapped;
}

export namespace DealWrapped {
  export type AsObject = {
    multiDeal?: v1_order_model_multi_deal_pb.MultiDeal.AsObject,
    dealsList: Array<Deal.AsObject>,
  }
}

export class DealAgent extends jspb.Message {
  hasDeal(): boolean;
  clearDeal(): void;
  getDeal(): Deal | undefined;
  setDeal(value?: Deal): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DealAgent.AsObject;
  static toObject(includeInstance: boolean, msg: DealAgent): DealAgent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DealAgent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DealAgent;
  static deserializeBinaryFromReader(message: DealAgent, reader: jspb.BinaryReader): DealAgent;
}

export namespace DealAgent {
  export type AsObject = {
    deal?: Deal.AsObject,
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
  }
}

export class DealFilter extends jspb.Message {
  hasMultiDealId(): boolean;
  clearMultiDealId(): void;
  getMultiDealId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMultiDealId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasMultiOrderId(): boolean;
  clearMultiOrderId(): void;
  getMultiOrderId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMultiOrderId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasOfferId(): boolean;
  clearOfferId(): void;
  getOfferId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setOfferId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): Deal.StatusValue | undefined;
  setStatus(value?: Deal.StatusValue): void;

  clearStatusesList(): void;
  getStatusesList(): Array<Deal.StatusValue>;
  setStatusesList(value: Array<Deal.StatusValue>): void;
  addStatuses(value?: Deal.StatusValue, index?: number): Deal.StatusValue;

  hasProductFilter(): boolean;
  clearProductFilter(): void;
  getProductFilter(): v1_order_model_product_pb.ProductFilter | undefined;
  setProductFilter(value?: v1_order_model_product_pb.ProductFilter): void;

  hasConditionFilter(): boolean;
  clearConditionFilter(): void;
  getConditionFilter(): v1_order_model_offer_pb.OfferConditionFilter | undefined;
  setConditionFilter(value?: v1_order_model_offer_pb.OfferConditionFilter): void;

  clearCurrenciesList(): void;
  getCurrenciesList(): Array<string>;
  setCurrenciesList(value: Array<string>): void;
  addCurrencies(value: string, index?: number): string;

  hasCurrency(): boolean;
  clearCurrency(): void;
  getCurrency(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCurrency(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDiscount(): boolean;
  clearDiscount(): void;
  getDiscount(): v1_order_finance_pb.AmountRange | undefined;
  setDiscount(value?: v1_order_finance_pb.AmountRange): void;

  hasAmount(): boolean;
  clearAmount(): void;
  getAmount(): v1_order_finance_pb.TotalAmountRange | undefined;
  setAmount(value?: v1_order_finance_pb.TotalAmountRange): void;

  hasResult(): boolean;
  clearResult(): void;
  getResult(): DealResult | undefined;
  setResult(value?: DealResult): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasSupplierId(): boolean;
  clearSupplierId(): void;
  getSupplierId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSupplierId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasHasSupplier(): boolean;
  clearHasSupplier(): void;
  getHasSupplier(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setHasSupplier(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasAgentId(): boolean;
  clearAgentId(): void;
  getAgentId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setAgentId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOrderBusinessName(): boolean;
  clearOrderBusinessName(): void;
  getOrderBusinessName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOrderBusinessName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOfferBusinessName(): boolean;
  clearOfferBusinessName(): void;
  getOfferBusinessName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOfferBusinessName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOrderCreator(): boolean;
  clearOrderCreator(): void;
  getOrderCreator(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOrderCreator(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOfferCreator(): boolean;
  clearOfferCreator(): void;
  getOfferCreator(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOfferCreator(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DealFilter.AsObject;
  static toObject(includeInstance: boolean, msg: DealFilter): DealFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DealFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DealFilter;
  static deserializeBinaryFromReader(message: DealFilter, reader: jspb.BinaryReader): DealFilter;
}

export namespace DealFilter {
  export type AsObject = {
    multiDealId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    multiOrderId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    offerId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    status?: Deal.StatusValue.AsObject,
    statusesList: Array<Deal.StatusValue.AsObject>,
    productFilter?: v1_order_model_product_pb.ProductFilter.AsObject,
    conditionFilter?: v1_order_model_offer_pb.OfferConditionFilter.AsObject,
    currenciesList: Array<string>,
    currency?: google_protobuf_wrappers_pb.StringValue.AsObject,
    discount?: v1_order_finance_pb.AmountRange.AsObject,
    amount?: v1_order_finance_pb.TotalAmountRange.AsObject,
    result?: DealResult.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    supplierId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    hasSupplier?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    agentId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    orderBusinessName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    offerBusinessName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    orderCreator?: google_protobuf_wrappers_pb.StringValue.AsObject,
    offerCreator?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
  }
}

export class ListDealRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealRequest): ListDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealRequest;
  static deserializeBinaryFromReader(message: ListDealRequest, reader: jspb.BinaryReader): ListDealRequest;
}

export namespace ListDealRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListDealResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Deal>;
  setItemsList(value: Array<Deal>): void;
  addItems(value?: Deal, index?: number): Deal;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealResponse): ListDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealResponse;
  static deserializeBinaryFromReader(message: ListDealResponse, reader: jspb.BinaryReader): ListDealResponse;
}

export namespace ListDealResponse {
  export type AsObject = {
    itemsList: Array<Deal.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListDealWrappedRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealWrappedRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealWrappedRequest): ListDealWrappedRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealWrappedRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealWrappedRequest;
  static deserializeBinaryFromReader(message: ListDealWrappedRequest, reader: jspb.BinaryReader): ListDealWrappedRequest;
}

export namespace ListDealWrappedRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListDealWrappedResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<DealWrapped>;
  setItemsList(value: Array<DealWrapped>): void;
  addItems(value?: DealWrapped, index?: number): DealWrapped;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealWrappedResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealWrappedResponse): ListDealWrappedResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealWrappedResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealWrappedResponse;
  static deserializeBinaryFromReader(message: ListDealWrappedResponse, reader: jspb.BinaryReader): ListDealWrappedResponse;
}

export namespace ListDealWrappedResponse {
  export type AsObject = {
    itemsList: Array<DealWrapped.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListDealAgentRequest extends jspb.Message {
  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  getAgentId(): string;
  setAgentId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealAgentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealAgentRequest): ListDealAgentRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealAgentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealAgentRequest;
  static deserializeBinaryFromReader(message: ListDealAgentRequest, reader: jspb.BinaryReader): ListDealAgentRequest;
}

export namespace ListDealAgentRequest {
  export type AsObject = {
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
    agentId: string,
  }
}

export class ListDealAgentResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<DealAgent>;
  setItemsList(value: Array<DealAgent>): void;
  addItems(value?: DealAgent, index?: number): DealAgent;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealAgentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealAgentResponse): ListDealAgentResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealAgentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealAgentResponse;
  static deserializeBinaryFromReader(message: ListDealAgentResponse, reader: jspb.BinaryReader): ListDealAgentResponse;
}

export namespace ListDealAgentResponse {
  export type AsObject = {
    itemsList: Array<DealAgent.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDealRequest): GetDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDealRequest;
  static deserializeBinaryFromReader(message: GetDealRequest, reader: jspb.BinaryReader): GetDealRequest;
}

export namespace GetDealRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetDealResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Deal | undefined;
  setItem(value?: Deal): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDealResponse): GetDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDealResponse;
  static deserializeBinaryFromReader(message: GetDealResponse, reader: jspb.BinaryReader): GetDealResponse;
}

export namespace GetDealResponse {
  export type AsObject = {
    item?: Deal.AsObject,
  }
}

export class GetDealFileRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDealFileRequest): GetDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDealFileRequest;
  static deserializeBinaryFromReader(message: GetDealFileRequest, reader: jspb.BinaryReader): GetDealFileRequest;
}

export namespace GetDealFileRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetDealFileResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_file_pb.File | undefined;
  setItem(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDealFileResponse): GetDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDealFileResponse;
  static deserializeBinaryFromReader(message: GetDealFileResponse, reader: jspb.BinaryReader): GetDealFileResponse;
}

export namespace GetDealFileResponse {
  export type AsObject = {
    item?: v1_order_file_pb.File.AsObject,
  }
}

export class ListDealFileRequest extends jspb.Message {
  getDealId(): number;
  setDealId(value: number): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealFileRequest): ListDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealFileRequest;
  static deserializeBinaryFromReader(message: ListDealFileRequest, reader: jspb.BinaryReader): ListDealFileRequest;
}

export namespace ListDealFileRequest {
  export type AsObject = {
    dealId: number,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListDealFileResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_file_pb.File>;
  setItemsList(value: Array<v1_order_file_pb.File>): void;
  addItems(value?: v1_order_file_pb.File, index?: number): v1_order_file_pb.File;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealFileResponse): ListDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealFileResponse;
  static deserializeBinaryFromReader(message: ListDealFileResponse, reader: jspb.BinaryReader): ListDealFileResponse;
}

export namespace ListDealFileResponse {
  export type AsObject = {
    itemsList: Array<v1_order_file_pb.File.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class AddDealFileRequest extends jspb.Message {
  getDealId(): number;
  setDealId(value: number): void;

  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddDealFileRequest): AddDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddDealFileRequest;
  static deserializeBinaryFromReader(message: AddDealFileRequest, reader: jspb.BinaryReader): AddDealFileRequest;
}

export namespace AddDealFileRequest {
  export type AsObject = {
    dealId: number,
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class AddDealFileResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AddDealFileResponse): AddDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddDealFileResponse;
  static deserializeBinaryFromReader(message: AddDealFileResponse, reader: jspb.BinaryReader): AddDealFileResponse;
}

export namespace AddDealFileResponse {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteDealFileRequest extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDealFileRequest): DeleteDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDealFileRequest;
  static deserializeBinaryFromReader(message: DeleteDealFileRequest, reader: jspb.BinaryReader): DeleteDealFileRequest;
}

export namespace DeleteDealFileRequest {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteDealFileResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDealFileResponse): DeleteDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDealFileResponse;
  static deserializeBinaryFromReader(message: DeleteDealFileResponse, reader: jspb.BinaryReader): DeleteDealFileResponse;
}

export namespace DeleteDealFileResponse {
  export type AsObject = {
  }
}

export class GetDealContactRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDealContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDealContactRequest): GetDealContactRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDealContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDealContactRequest;
  static deserializeBinaryFromReader(message: GetDealContactRequest, reader: jspb.BinaryReader): GetDealContactRequest;
}

export namespace GetDealContactRequest {
  export type AsObject = {
    id: string,
  }
}

export class GetDealContactResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_base_pb.UserInfo | undefined;
  setItem(value?: v1_order_model_base_pb.UserInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDealContactResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDealContactResponse): GetDealContactResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDealContactResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDealContactResponse;
  static deserializeBinaryFromReader(message: GetDealContactResponse, reader: jspb.BinaryReader): GetDealContactResponse;
}

export namespace GetDealContactResponse {
  export type AsObject = {
    item?: v1_order_model_base_pb.UserInfo.AsObject,
  }
}

export class ListDealContactRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ListDealContactRequest.Filter | undefined;
  setFilter(value?: ListDealContactRequest.Filter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealContactRequest): ListDealContactRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealContactRequest;
  static deserializeBinaryFromReader(message: ListDealContactRequest, reader: jspb.BinaryReader): ListDealContactRequest;
}

export namespace ListDealContactRequest {
  export type AsObject = {
    filter?: ListDealContactRequest.Filter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }

  export class Filter extends jspb.Message {
    hasSearchString(): boolean;
    clearSearchString(): void;
    getSearchString(): google_protobuf_wrappers_pb.StringValue | undefined;
    setSearchString(value?: google_protobuf_wrappers_pb.StringValue): void;

    clearStatusList(): void;
    getStatusList(): Array<number>;
    setStatusList(value: Array<number>): void;
    addStatus(value: number, index?: number): number;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Filter.AsObject;
    static toObject(includeInstance: boolean, msg: Filter): Filter.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Filter, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Filter;
    static deserializeBinaryFromReader(message: Filter, reader: jspb.BinaryReader): Filter;
  }

  export namespace Filter {
    export type AsObject = {
      searchString?: google_protobuf_wrappers_pb.StringValue.AsObject,
      statusList: Array<number>,
    }
  }
}

export class ListDealContactResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_base_pb.UserInfo>;
  setItemsList(value: Array<v1_order_model_base_pb.UserInfo>): void;
  addItems(value?: v1_order_model_base_pb.UserInfo, index?: number): v1_order_model_base_pb.UserInfo;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealContactResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealContactResponse): ListDealContactResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealContactResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealContactResponse;
  static deserializeBinaryFromReader(message: ListDealContactResponse, reader: jspb.BinaryReader): ListDealContactResponse;
}

export namespace ListDealContactResponse {
  export type AsObject = {
    itemsList: Array<v1_order_model_base_pb.UserInfo.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListDealCurrencyRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealCurrencyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealCurrencyRequest): ListDealCurrencyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealCurrencyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealCurrencyRequest;
  static deserializeBinaryFromReader(message: ListDealCurrencyRequest, reader: jspb.BinaryReader): ListDealCurrencyRequest;
}

export namespace ListDealCurrencyRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealProcessRequest): ListDealProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealProcessRequest;
  static deserializeBinaryFromReader(message: ListDealProcessRequest, reader: jspb.BinaryReader): ListDealProcessRequest;
}

export namespace ListDealProcessRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealProcessTypeRequest): ListDealProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListDealProcessTypeRequest, reader: jspb.BinaryReader): ListDealProcessTypeRequest;
}

export namespace ListDealProcessTypeRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealDeliveryConditionRequest): ListDealDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListDealDeliveryConditionRequest, reader: jspb.BinaryReader): ListDealDeliveryConditionRequest;
}

export namespace ListDealDeliveryConditionRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealPaymentConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealPaymentConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealPaymentConditionRequest): ListDealPaymentConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealPaymentConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealPaymentConditionRequest;
  static deserializeBinaryFromReader(message: ListDealPaymentConditionRequest, reader: jspb.BinaryReader): ListDealPaymentConditionRequest;
}

export namespace ListDealPaymentConditionRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealCategoryRequest): ListDealCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealCategoryRequest;
  static deserializeBinaryFromReader(message: ListDealCategoryRequest, reader: jspb.BinaryReader): ListDealCategoryRequest;
}

export namespace ListDealCategoryRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealBrandRequest): ListDealBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealBrandRequest;
  static deserializeBinaryFromReader(message: ListDealBrandRequest, reader: jspb.BinaryReader): ListDealBrandRequest;
}

export namespace ListDealBrandRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealProductRequest): ListDealProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealProductRequest;
  static deserializeBinaryFromReader(message: ListDealProductRequest, reader: jspb.BinaryReader): ListDealProductRequest;
}

export namespace ListDealProductRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DealFilter | undefined;
  setFilter(value?: DealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealQuantityTypeRequest): ListDealQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListDealQuantityTypeRequest, reader: jspb.BinaryReader): ListDealQuantityTypeRequest;
}

export namespace ListDealQuantityTypeRequest {
  export type AsObject = {
    filter?: DealFilter.AsObject,
  }
}

export class ListDealNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPaymentConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCurrenciesList(): void;
  getCurrenciesList(): Array<v1_order_enum_pb.NameFilter>;
  setCurrenciesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCurrencies(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealNamesRequest): ListDealNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealNamesRequest;
  static deserializeBinaryFromReader(message: ListDealNamesRequest, reader: jspb.BinaryReader): ListDealNamesRequest;
}

export namespace ListDealNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    currenciesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListDealNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPaymentConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCurrenciesList(): void;
  getCurrenciesList(): Array<v1_order_enum_pb.DictItem>;
  setCurrenciesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCurrencies(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDealNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDealNamesResponse): ListDealNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDealNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDealNamesResponse;
  static deserializeBinaryFromReader(message: ListDealNamesResponse, reader: jspb.BinaryReader): ListDealNamesResponse;
}

export namespace ListDealNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    currenciesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

