// package: fcp.order.v1.order
// file: v1/order/model_multi_deal.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_file_pb from "../../v1/order/file_pb";
import * as v1_order_finance_pb from "../../v1/order/finance_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";

export class MultiDealResult extends jspb.Message {
  hasContract(): boolean;
  clearContract(): void;
  getContract(): v1_order_model_base_pb.OperationIdent | undefined;
  setContract(value?: v1_order_model_base_pb.OperationIdent): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiDealResult.AsObject;
  static toObject(includeInstance: boolean, msg: MultiDealResult): MultiDealResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiDealResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiDealResult;
  static deserializeBinaryFromReader(message: MultiDealResult, reader: jspb.BinaryReader): MultiDealResult;
}

export namespace MultiDealResult {
  export type AsObject = {
    contract?: v1_order_model_base_pb.OperationIdent.AsObject,
  }
}

export class MultiDeal extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getStatus(): MultiDeal.StatusMap[keyof MultiDeal.StatusMap];
  setStatus(value: MultiDeal.StatusMap[keyof MultiDeal.StatusMap]): void;

  hasFarmer(): boolean;
  clearFarmer(): void;
  getFarmer(): v1_order_model_base_pb.UserInfo | undefined;
  setFarmer(value?: v1_order_model_base_pb.UserInfo): void;

  hasSupplier(): boolean;
  clearSupplier(): void;
  getSupplier(): v1_order_model_base_pb.UserInfo | undefined;
  setSupplier(value?: v1_order_model_base_pb.UserInfo): void;

  getChatId(): number;
  setChatId(value: number): void;

  getChatIdent(): string;
  setChatIdent(value: string): void;

  hasResult(): boolean;
  clearResult(): void;
  getResult(): MultiDealResult | undefined;
  setResult(value?: MultiDealResult): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getTotalAmount(): number;
  setTotalAmount(value: number): void;

  getTotalVatAmount(): number;
  setTotalVatAmount(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getCategoryNum(): number;
  setCategoryNum(value: number): void;

  getProductNum(): number;
  setProductNum(value: number): void;

  getCancelInProgress(): boolean;
  setCancelInProgress(value: boolean): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  getClientName(): string;
  setClientName(value: string): void;

  getFarmerEdrpou(): string;
  setFarmerEdrpou(value: string): void;

  getCreatedName(): string;
  setCreatedName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiDeal.AsObject;
  static toObject(includeInstance: boolean, msg: MultiDeal): MultiDeal.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiDeal, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiDeal;
  static deserializeBinaryFromReader(message: MultiDeal, reader: jspb.BinaryReader): MultiDeal;
}

export namespace MultiDeal {
  export type AsObject = {
    id: number,
    name: string,
    status: MultiDeal.StatusMap[keyof MultiDeal.StatusMap],
    farmer?: v1_order_model_base_pb.UserInfo.AsObject,
    supplier?: v1_order_model_base_pb.UserInfo.AsObject,
    chatId: number,
    chatIdent: string,
    result?: MultiDealResult.AsObject,
    currency: string,
    totalAmount: number,
    totalVatAmount: number,
    externalId: string,
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    categoryNum: number,
    productNum: number,
    cancelInProgress: boolean,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
    clientName: string,
    farmerEdrpou: string,
    createdName: string,
  }

  export class StatusValue extends jspb.Message {
    getValue(): MultiDeal.StatusMap[keyof MultiDeal.StatusMap];
    setValue(value: MultiDeal.StatusMap[keyof MultiDeal.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: MultiDeal.StatusMap[keyof MultiDeal.StatusMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
  }

  export const Status: StatusMap;
}

export class MultiDealFilter extends jspb.Message {
  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): MultiDeal.StatusValue | undefined;
  setStatus(value?: MultiDeal.StatusValue): void;

  clearStatusesList(): void;
  getStatusesList(): Array<MultiDeal.StatusValue>;
  setStatusesList(value: Array<MultiDeal.StatusValue>): void;
  addStatuses(value?: MultiDeal.StatusValue, index?: number): MultiDeal.StatusValue;

  clearTypeList(): void;
  getTypeList(): Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>;
  setTypeList(value: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>): void;
  addType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap], index?: number): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCurrency(): boolean;
  clearCurrency(): void;
  getCurrency(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCurrency(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasTotalAmount(): boolean;
  clearTotalAmount(): void;
  getTotalAmount(): v1_order_finance_pb.TotalAmountRange | undefined;
  setTotalAmount(value?: v1_order_finance_pb.TotalAmountRange): void;

  hasOrderBusinessName(): boolean;
  clearOrderBusinessName(): void;
  getOrderBusinessName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOrderBusinessName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOfferBusinessName(): boolean;
  clearOfferBusinessName(): void;
  getOfferBusinessName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOfferBusinessName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasOrderCreator(): boolean;
  clearOrderCreator(): void;
  getOrderCreator(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOrderCreator(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOfferCreator(): boolean;
  clearOfferCreator(): void;
  getOfferCreator(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOfferCreator(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiDealFilter.AsObject;
  static toObject(includeInstance: boolean, msg: MultiDealFilter): MultiDealFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiDealFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiDealFilter;
  static deserializeBinaryFromReader(message: MultiDealFilter, reader: jspb.BinaryReader): MultiDealFilter;
}

export namespace MultiDealFilter {
  export type AsObject = {
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    status?: MultiDeal.StatusValue.AsObject,
    statusesList: Array<MultiDeal.StatusValue.AsObject>,
    typeList: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>,
    externalId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    currency?: google_protobuf_wrappers_pb.StringValue.AsObject,
    totalAmount?: v1_order_finance_pb.TotalAmountRange.AsObject,
    orderBusinessName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    offerBusinessName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    orderCreator?: google_protobuf_wrappers_pb.StringValue.AsObject,
    offerCreator?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListMultiDealRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealRequest): ListMultiDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealRequest;
  static deserializeBinaryFromReader(message: ListMultiDealRequest, reader: jspb.BinaryReader): ListMultiDealRequest;
}

export namespace ListMultiDealRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListMultiDealResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<MultiDeal>;
  setItemsList(value: Array<MultiDeal>): void;
  addItems(value?: MultiDeal, index?: number): MultiDeal;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealResponse): ListMultiDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealResponse;
  static deserializeBinaryFromReader(message: ListMultiDealResponse, reader: jspb.BinaryReader): ListMultiDealResponse;
}

export namespace ListMultiDealResponse {
  export type AsObject = {
    itemsList: Array<MultiDeal.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetMultiDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiDealRequest): GetMultiDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiDealRequest;
  static deserializeBinaryFromReader(message: GetMultiDealRequest, reader: jspb.BinaryReader): GetMultiDealRequest;
}

export namespace GetMultiDealRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetMultiDealResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): MultiDeal | undefined;
  setItem(value?: MultiDeal): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiDealResponse): GetMultiDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiDealResponse;
  static deserializeBinaryFromReader(message: GetMultiDealResponse, reader: jspb.BinaryReader): GetMultiDealResponse;
}

export namespace GetMultiDealResponse {
  export type AsObject = {
    item?: MultiDeal.AsObject,
  }
}

export class GetMultiDealContactRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiDealContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiDealContactRequest): GetMultiDealContactRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiDealContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiDealContactRequest;
  static deserializeBinaryFromReader(message: GetMultiDealContactRequest, reader: jspb.BinaryReader): GetMultiDealContactRequest;
}

export namespace GetMultiDealContactRequest {
  export type AsObject = {
    id: string,
  }
}

export class GetMultiDealContactResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_base_pb.UserInfo | undefined;
  setItem(value?: v1_order_model_base_pb.UserInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiDealContactResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiDealContactResponse): GetMultiDealContactResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiDealContactResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiDealContactResponse;
  static deserializeBinaryFromReader(message: GetMultiDealContactResponse, reader: jspb.BinaryReader): GetMultiDealContactResponse;
}

export namespace GetMultiDealContactResponse {
  export type AsObject = {
    item?: v1_order_model_base_pb.UserInfo.AsObject,
  }
}

export class ListMultiDealContactRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ListMultiDealContactRequest.Filter | undefined;
  setFilter(value?: ListMultiDealContactRequest.Filter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealContactRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealContactRequest): ListMultiDealContactRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealContactRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealContactRequest;
  static deserializeBinaryFromReader(message: ListMultiDealContactRequest, reader: jspb.BinaryReader): ListMultiDealContactRequest;
}

export namespace ListMultiDealContactRequest {
  export type AsObject = {
    filter?: ListMultiDealContactRequest.Filter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }

  export class Filter extends jspb.Message {
    hasSearchString(): boolean;
    clearSearchString(): void;
    getSearchString(): google_protobuf_wrappers_pb.StringValue | undefined;
    setSearchString(value?: google_protobuf_wrappers_pb.StringValue): void;

    clearStatusList(): void;
    getStatusList(): Array<number>;
    setStatusList(value: Array<number>): void;
    addStatus(value: number, index?: number): number;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Filter.AsObject;
    static toObject(includeInstance: boolean, msg: Filter): Filter.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Filter, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Filter;
    static deserializeBinaryFromReader(message: Filter, reader: jspb.BinaryReader): Filter;
  }

  export namespace Filter {
    export type AsObject = {
      searchString?: google_protobuf_wrappers_pb.StringValue.AsObject,
      statusList: Array<number>,
    }
  }
}

export class ListMultiDealContactResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_model_base_pb.UserInfo>;
  setItemsList(value: Array<v1_order_model_base_pb.UserInfo>): void;
  addItems(value?: v1_order_model_base_pb.UserInfo, index?: number): v1_order_model_base_pb.UserInfo;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealContactResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealContactResponse): ListMultiDealContactResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealContactResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealContactResponse;
  static deserializeBinaryFromReader(message: ListMultiDealContactResponse, reader: jspb.BinaryReader): ListMultiDealContactResponse;
}

export namespace ListMultiDealContactResponse {
  export type AsObject = {
    itemsList: Array<v1_order_model_base_pb.UserInfo.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListMultiDealProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealProcessRequest): ListMultiDealProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealProcessRequest;
  static deserializeBinaryFromReader(message: ListMultiDealProcessRequest, reader: jspb.BinaryReader): ListMultiDealProcessRequest;
}

export namespace ListMultiDealProcessRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealProcessTypeRequest): ListMultiDealProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListMultiDealProcessTypeRequest, reader: jspb.BinaryReader): ListMultiDealProcessTypeRequest;
}

export namespace ListMultiDealProcessTypeRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealDeliveryConditionRequest): ListMultiDealDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListMultiDealDeliveryConditionRequest, reader: jspb.BinaryReader): ListMultiDealDeliveryConditionRequest;
}

export namespace ListMultiDealDeliveryConditionRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealPaymentConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealPaymentConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealPaymentConditionRequest): ListMultiDealPaymentConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealPaymentConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealPaymentConditionRequest;
  static deserializeBinaryFromReader(message: ListMultiDealPaymentConditionRequest, reader: jspb.BinaryReader): ListMultiDealPaymentConditionRequest;
}

export namespace ListMultiDealPaymentConditionRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealCategoryRequest): ListMultiDealCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealCategoryRequest;
  static deserializeBinaryFromReader(message: ListMultiDealCategoryRequest, reader: jspb.BinaryReader): ListMultiDealCategoryRequest;
}

export namespace ListMultiDealCategoryRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealBrandRequest): ListMultiDealBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealBrandRequest;
  static deserializeBinaryFromReader(message: ListMultiDealBrandRequest, reader: jspb.BinaryReader): ListMultiDealBrandRequest;
}

export namespace ListMultiDealBrandRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealProductRequest): ListMultiDealProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealProductRequest;
  static deserializeBinaryFromReader(message: ListMultiDealProductRequest, reader: jspb.BinaryReader): ListMultiDealProductRequest;
}

export namespace ListMultiDealProductRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiDealFilter | undefined;
  setFilter(value?: MultiDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealQuantityTypeRequest): ListMultiDealQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListMultiDealQuantityTypeRequest, reader: jspb.BinaryReader): ListMultiDealQuantityTypeRequest;
}

export namespace ListMultiDealQuantityTypeRequest {
  export type AsObject = {
    filter?: MultiDealFilter.AsObject,
  }
}

export class ListMultiDealNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPaymentConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealNamesRequest): ListMultiDealNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealNamesRequest;
  static deserializeBinaryFromReader(message: ListMultiDealNamesRequest, reader: jspb.BinaryReader): ListMultiDealNamesRequest;
}

export namespace ListMultiDealNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListMultiDealNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPaymentConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealNamesResponse): ListMultiDealNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealNamesResponse;
  static deserializeBinaryFromReader(message: ListMultiDealNamesResponse, reader: jspb.BinaryReader): ListMultiDealNamesResponse;
}

export namespace ListMultiDealNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

export class GetMultiDealFileRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiDealFileRequest): GetMultiDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiDealFileRequest;
  static deserializeBinaryFromReader(message: GetMultiDealFileRequest, reader: jspb.BinaryReader): GetMultiDealFileRequest;
}

export namespace GetMultiDealFileRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetMultiDealFileResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_file_pb.File | undefined;
  setItem(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiDealFileResponse): GetMultiDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiDealFileResponse;
  static deserializeBinaryFromReader(message: GetMultiDealFileResponse, reader: jspb.BinaryReader): GetMultiDealFileResponse;
}

export namespace GetMultiDealFileResponse {
  export type AsObject = {
    item?: v1_order_file_pb.File.AsObject,
  }
}

export class ListMultiDealFileRequest extends jspb.Message {
  getMultiDealId(): number;
  setMultiDealId(value: number): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealFileRequest): ListMultiDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealFileRequest;
  static deserializeBinaryFromReader(message: ListMultiDealFileRequest, reader: jspb.BinaryReader): ListMultiDealFileRequest;
}

export namespace ListMultiDealFileRequest {
  export type AsObject = {
    multiDealId: number,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListMultiDealFileResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_file_pb.File>;
  setItemsList(value: Array<v1_order_file_pb.File>): void;
  addItems(value?: v1_order_file_pb.File, index?: number): v1_order_file_pb.File;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiDealFileResponse): ListMultiDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiDealFileResponse;
  static deserializeBinaryFromReader(message: ListMultiDealFileResponse, reader: jspb.BinaryReader): ListMultiDealFileResponse;
}

export namespace ListMultiDealFileResponse {
  export type AsObject = {
    itemsList: Array<v1_order_file_pb.File.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class AddMultiDealFileRequest extends jspb.Message {
  getMultiDealId(): number;
  setMultiDealId(value: number): void;

  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddMultiDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddMultiDealFileRequest): AddMultiDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddMultiDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddMultiDealFileRequest;
  static deserializeBinaryFromReader(message: AddMultiDealFileRequest, reader: jspb.BinaryReader): AddMultiDealFileRequest;
}

export namespace AddMultiDealFileRequest {
  export type AsObject = {
    multiDealId: number,
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class AddMultiDealFileResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddMultiDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AddMultiDealFileResponse): AddMultiDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddMultiDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddMultiDealFileResponse;
  static deserializeBinaryFromReader(message: AddMultiDealFileResponse, reader: jspb.BinaryReader): AddMultiDealFileResponse;
}

export namespace AddMultiDealFileResponse {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteMultiDealFileRequest extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteMultiDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteMultiDealFileRequest): DeleteMultiDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteMultiDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteMultiDealFileRequest;
  static deserializeBinaryFromReader(message: DeleteMultiDealFileRequest, reader: jspb.BinaryReader): DeleteMultiDealFileRequest;
}

export namespace DeleteMultiDealFileRequest {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteMultiDealFileResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteMultiDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteMultiDealFileResponse): DeleteMultiDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteMultiDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteMultiDealFileResponse;
  static deserializeBinaryFromReader(message: DeleteMultiDealFileResponse, reader: jspb.BinaryReader): DeleteMultiDealFileResponse;
}

export namespace DeleteMultiDealFileResponse {
  export type AsObject = {
  }
}

export class RejectMultiDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getRating(): v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap];
  setRating(value: v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap]): void;

  getDescription(): string;
  setDescription(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiDealRequest): RejectMultiDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiDealRequest;
  static deserializeBinaryFromReader(message: RejectMultiDealRequest, reader: jspb.BinaryReader): RejectMultiDealRequest;
}

export namespace RejectMultiDealRequest {
  export type AsObject = {
    id: number,
    rating: v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap],
    description: string,
  }
}

export class RejectMultiDealResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectMultiDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectMultiDealResponse): RejectMultiDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectMultiDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectMultiDealResponse;
  static deserializeBinaryFromReader(message: RejectMultiDealResponse, reader: jspb.BinaryReader): RejectMultiDealResponse;
}

export namespace RejectMultiDealResponse {
  export type AsObject = {
  }
}

