// source: v1/order/model_fast_deal.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_wrappers_pb = require('google-protobuf/google/protobuf/wrappers_pb.js');
goog.object.extend(proto, google_protobuf_wrappers_pb);
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var v1_order_common_pb = require('../../v1/order/common_pb.js');
goog.object.extend(proto, v1_order_common_pb);
var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
var v1_order_file_pb = require('../../v1/order/file_pb.js');
goog.object.extend(proto, v1_order_file_pb);
var v1_order_model_base_pb = require('../../v1/order/model_base_pb.js');
goog.object.extend(proto, v1_order_model_base_pb);
var v1_order_model_pricing_pb = require('../../v1/order/model_pricing_pb.js');
goog.object.extend(proto, v1_order_model_pricing_pb);
var v1_order_model_product_pb = require('../../v1/order/model_product_pb.js');
goog.object.extend(proto, v1_order_model_product_pb);
var v1_order_model_user_rating_pb = require('../../v1/order/model_user_rating_pb.js');
goog.object.extend(proto, v1_order_model_user_rating_pb);
goog.exportSymbol('proto.fcp.order.v1.order.AddFastDealFileRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.AddFastDealFileResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ApproveFastDealRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ApproveFastDealResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.CreateFastDealRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.CreateFastDealResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.DeleteFastDealFileRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.DeleteFastDealFileResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.DoneFastDealRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.DoneFastDealResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.DeliveryAddress', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.DeliveryCondition', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.LogisticsOperator', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.PaymentCondition', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.PaymentConditionValue', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.PaymentInfo', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.Status', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDeal.StatusValue', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDealFilter', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDealItem', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDealItem.PriceType', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDealItem.Status', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDealItemSnapshot', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.FastDealSnapshot', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.GetFastDealFileRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.GetFastDealFileResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.GetFastDealRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.GetFastDealResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealBrandRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealCityRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealCurrencyRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealFileRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealFileResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealItemAgentRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealItemAgentResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealNamesRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealNamesResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealProcessRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealProcessTypeRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealProductGroupRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealProductRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.ListFastDealStateRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.RejectFastDealRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.RejectFastDealResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.FastDeal.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.displayName = 'proto.fcp.order.v1.order.FastDeal';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal.StatusValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal.StatusValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.StatusValue.displayName = 'proto.fcp.order.v1.order.FastDeal.StatusValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.displayName = 'proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.displayName = 'proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal.DeliveryAddress, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.DeliveryAddress.displayName = 'proto.fcp.order.v1.order.FastDeal.DeliveryAddress';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal.PaymentConditionValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.displayName = 'proto.fcp.order.v1.order.FastDeal.PaymentConditionValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDeal.PaymentInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDeal.PaymentInfo.displayName = 'proto.fcp.order.v1.order.FastDeal.PaymentInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDealSnapshot = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDealSnapshot, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDealSnapshot.displayName = 'proto.fcp.order.v1.order.FastDealSnapshot';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDealItem = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDealItem, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDealItem.displayName = 'proto.fcp.order.v1.order.FastDealItem';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDealItemSnapshot = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDealItemSnapshot, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDealItemSnapshot.displayName = 'proto.fcp.order.v1.order.FastDealItemSnapshot';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.FastDealFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.FastDealFilter.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.FastDealFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.FastDealFilter.displayName = 'proto.fcp.order.v1.order.FastDealFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealResponse.displayName = 'proto.fcp.order.v1.order.ListFastDealResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealItemAgentRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealItemAgentRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealItemAgentRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealItemAgentRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealItemAgentResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealItemAgentResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealItemAgentResponse.displayName = 'proto.fcp.order.v1.order.ListFastDealItemAgentResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealProcessRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealProcessRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealProcessRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealProcessTypeRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealProcessTypeRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealProductGroupRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealProductGroupRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealProductGroupRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealBrandRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealBrandRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealBrandRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealProductRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealProductRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealProductRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealProductRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealCurrencyRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealCurrencyRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealCurrencyRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealStateRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealStateRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealStateRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealStateRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealCityRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealCityRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealCityRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealCityRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealNamesRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealNamesRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealNamesRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealNamesRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealNamesResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealNamesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealNamesResponse.displayName = 'proto.fcp.order.v1.order.ListFastDealNamesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.GetFastDealRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.GetFastDealRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.GetFastDealRequest.displayName = 'proto.fcp.order.v1.order.GetFastDealRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.GetFastDealResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.GetFastDealResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.GetFastDealResponse.displayName = 'proto.fcp.order.v1.order.GetFastDealResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.CreateFastDealRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.CreateFastDealRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.CreateFastDealRequest.displayName = 'proto.fcp.order.v1.order.CreateFastDealRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.CreateFastDealResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.CreateFastDealResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.CreateFastDealResponse.displayName = 'proto.fcp.order.v1.order.CreateFastDealResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.RejectFastDealRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.RejectFastDealRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.RejectFastDealRequest.displayName = 'proto.fcp.order.v1.order.RejectFastDealRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.RejectFastDealResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.RejectFastDealResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.RejectFastDealResponse.displayName = 'proto.fcp.order.v1.order.RejectFastDealResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ApproveFastDealRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ApproveFastDealRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ApproveFastDealRequest.displayName = 'proto.fcp.order.v1.order.ApproveFastDealRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ApproveFastDealResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.ApproveFastDealResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ApproveFastDealResponse.displayName = 'proto.fcp.order.v1.order.ApproveFastDealResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.DoneFastDealRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.DoneFastDealRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.DoneFastDealRequest.displayName = 'proto.fcp.order.v1.order.DoneFastDealRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.DoneFastDealResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.DoneFastDealResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.DoneFastDealResponse.displayName = 'proto.fcp.order.v1.order.DoneFastDealResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.displayName = 'proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.displayName = 'proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.GetFastDealFileRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.GetFastDealFileRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.GetFastDealFileRequest.displayName = 'proto.fcp.order.v1.order.GetFastDealFileRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.GetFastDealFileResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.GetFastDealFileResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.GetFastDealFileResponse.displayName = 'proto.fcp.order.v1.order.GetFastDealFileResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealFileRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealFileRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealFileRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealFileRequest.displayName = 'proto.fcp.order.v1.order.ListFastDealFileRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.ListFastDealFileResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.ListFastDealFileResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.ListFastDealFileResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.ListFastDealFileResponse.displayName = 'proto.fcp.order.v1.order.ListFastDealFileResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.AddFastDealFileRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.AddFastDealFileRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.AddFastDealFileRequest.displayName = 'proto.fcp.order.v1.order.AddFastDealFileRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.AddFastDealFileResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.AddFastDealFileResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.AddFastDealFileResponse.displayName = 'proto.fcp.order.v1.order.AddFastDealFileResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.DeleteFastDealFileRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.DeleteFastDealFileRequest.displayName = 'proto.fcp.order.v1.order.DeleteFastDealFileRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.DeleteFastDealFileResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.DeleteFastDealFileResponse.displayName = 'proto.fcp.order.v1.order.DeleteFastDealFileResponse';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.FastDeal.repeatedFields_ = [25,21];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    externalId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    status: jspb.Message.getFieldWithDefault(msg, 3, 0),
    deliveryCondition: jspb.Message.getFieldWithDefault(msg, 4, 0),
    logisticsOperator: jspb.Message.getFieldWithDefault(msg, 5, 0),
    deliveryAddress: (f = msg.getDeliveryAddress()) && proto.fcp.order.v1.order.FastDeal.DeliveryAddress.toObject(includeInstance, f),
    deliveryDate: (f = msg.getDeliveryDate()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    paymentCondition: jspb.Message.getFieldWithDefault(msg, 8, 0),
    paymentInfo: (f = msg.getPaymentInfo()) && proto.fcp.order.v1.order.FastDeal.PaymentInfo.toObject(includeInstance, f),
    customer: (f = msg.getCustomer()) && v1_order_model_base_pb.UserInfo.toObject(includeInstance, f),
    supplier: (f = msg.getSupplier()) && v1_order_model_base_pb.UserInfo.toObject(includeInstance, f),
    customerNotes: jspb.Message.getFieldWithDefault(msg, 20, ""),
    chatId: jspb.Message.getFieldWithDefault(msg, 23, 0),
    chatIdent: jspb.Message.getFieldWithDefault(msg, 24, ""),
    snapshot: (f = msg.getSnapshot()) && proto.fcp.order.v1.order.FastDealSnapshot.toObject(includeInstance, f),
    snapshotItemsList: jspb.Message.toObjectList(msg.getSnapshotItemsList(),
    proto.fcp.order.v1.order.FastDealItemSnapshot.toObject, includeInstance),
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.order.v1.order.FastDealItem.toObject, includeInstance),
    audit: (f = msg.getAudit()) && v1_order_common_pb.Audit.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal}
 */
proto.fcp.order.v1.order.FastDeal.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal;
  return proto.fcp.order.v1.order.FastDeal.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal}
 */
proto.fcp.order.v1.order.FastDeal.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setExternalId(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.Status} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 4:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} */ (reader.readEnum());
      msg.setDeliveryCondition(value);
      break;
    case 5:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} */ (reader.readEnum());
      msg.setLogisticsOperator(value);
      break;
    case 6:
      var value = new proto.fcp.order.v1.order.FastDeal.DeliveryAddress;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.DeliveryAddress.deserializeBinaryFromReader);
      msg.setDeliveryAddress(value);
      break;
    case 7:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setDeliveryDate(value);
      break;
    case 8:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} */ (reader.readEnum());
      msg.setPaymentCondition(value);
      break;
    case 9:
      var value = new proto.fcp.order.v1.order.FastDeal.PaymentInfo;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.PaymentInfo.deserializeBinaryFromReader);
      msg.setPaymentInfo(value);
      break;
    case 18:
      var value = new v1_order_model_base_pb.UserInfo;
      reader.readMessage(value,v1_order_model_base_pb.UserInfo.deserializeBinaryFromReader);
      msg.setCustomer(value);
      break;
    case 19:
      var value = new v1_order_model_base_pb.UserInfo;
      reader.readMessage(value,v1_order_model_base_pb.UserInfo.deserializeBinaryFromReader);
      msg.setSupplier(value);
      break;
    case 20:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerNotes(value);
      break;
    case 23:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setChatId(value);
      break;
    case 24:
      var value = /** @type {string} */ (reader.readString());
      msg.setChatIdent(value);
      break;
    case 26:
      var value = new proto.fcp.order.v1.order.FastDealSnapshot;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealSnapshot.deserializeBinaryFromReader);
      msg.setSnapshot(value);
      break;
    case 25:
      var value = new proto.fcp.order.v1.order.FastDealItemSnapshot;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealItemSnapshot.deserializeBinaryFromReader);
      msg.addSnapshotItems(value);
      break;
    case 21:
      var value = new proto.fcp.order.v1.order.FastDealItem;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealItem.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    case 22:
      var value = new v1_order_common_pb.Audit;
      reader.readMessage(value,v1_order_common_pb.Audit.deserializeBinaryFromReader);
      msg.setAudit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getExternalId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getDeliveryCondition();
  if (f !== 0.0) {
    writer.writeEnum(
      4,
      f
    );
  }
  f = message.getLogisticsOperator();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getDeliveryAddress();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.fcp.order.v1.order.FastDeal.DeliveryAddress.serializeBinaryToWriter
    );
  }
  f = message.getDeliveryDate();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getPaymentCondition();
  if (f !== 0.0) {
    writer.writeEnum(
      8,
      f
    );
  }
  f = message.getPaymentInfo();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      proto.fcp.order.v1.order.FastDeal.PaymentInfo.serializeBinaryToWriter
    );
  }
  f = message.getCustomer();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      v1_order_model_base_pb.UserInfo.serializeBinaryToWriter
    );
  }
  f = message.getSupplier();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      v1_order_model_base_pb.UserInfo.serializeBinaryToWriter
    );
  }
  f = message.getCustomerNotes();
  if (f.length > 0) {
    writer.writeString(
      20,
      f
    );
  }
  f = message.getChatId();
  if (f !== 0) {
    writer.writeInt64(
      23,
      f
    );
  }
  f = message.getChatIdent();
  if (f.length > 0) {
    writer.writeString(
      24,
      f
    );
  }
  f = message.getSnapshot();
  if (f != null) {
    writer.writeMessage(
      26,
      f,
      proto.fcp.order.v1.order.FastDealSnapshot.serializeBinaryToWriter
    );
  }
  f = message.getSnapshotItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      25,
      f,
      proto.fcp.order.v1.order.FastDealItemSnapshot.serializeBinaryToWriter
    );
  }
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      21,
      f,
      proto.fcp.order.v1.order.FastDealItem.serializeBinaryToWriter
    );
  }
  f = message.getAudit();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      v1_order_common_pb.Audit.serializeBinaryToWriter
    );
  }
};


/**
 * @enum {number}
 */
proto.fcp.order.v1.order.FastDeal.Status = {
  ACTIVE: 0,
  REJECT: 1,
  DONE: 2,
  EXECUTE: 3
};

/**
 * @enum {number}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryCondition = {
  PICKUP: 0,
  POST_OFFICE: 1,
  COURIER: 2
};

/**
 * @enum {number}
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperator = {
  SUPPLIER: 0,
  NOVA_POST: 1,
  UKR_POST: 2
};

/**
 * @enum {number}
 */
proto.fcp.order.v1.order.FastDeal.PaymentCondition = {
  PAY_NOW: 0,
  PAY_AT_DELIVERY: 1,
  BANK_TRANSFER_COMPANY: 2,
  BANK_TRANSFER_PERSONAL: 3
};




if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.StatusValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal.StatusValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal.StatusValue}
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal.StatusValue;
  return proto.fcp.order.v1.order.FastDeal.StatusValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal.StatusValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal.StatusValue}
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.Status} */ (reader.readEnum());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.StatusValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal.StatusValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * optional Status value = 1;
 * @return {!proto.fcp.order.v1.order.FastDeal.Status}
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.prototype.getValue = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.Status} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.Status} value
 * @return {!proto.fcp.order.v1.order.FastDeal.StatusValue} returns this
 */
proto.fcp.order.v1.order.FastDeal.StatusValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue;
  return proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} */ (reader.readEnum());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * optional DeliveryCondition value = 1;
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.prototype.getValue = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryConditionValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue}
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue;
  return proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue}
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} */ (reader.readEnum());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * optional LogisticsOperator value = 1;
 * @return {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator}
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.prototype.getValue = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} value
 * @return {!proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue} returns this
 */
proto.fcp.order.v1.order.FastDeal.LogisticsOperatorValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.DeliveryAddress.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.toObject = function(includeInstance, msg) {
  var f, obj = {
    customerFirstName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    customerMiddleName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    customerLastName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    customerPhone: jspb.Message.getFieldWithDefault(msg, 4, ""),
    customerEmail: jspb.Message.getFieldWithDefault(msg, 5, ""),
    zip: jspb.Message.getFieldWithDefault(msg, 6, ""),
    state: jspb.Message.getFieldWithDefault(msg, 7, ""),
    city: jspb.Message.getFieldWithDefault(msg, 8, ""),
    street: jspb.Message.getFieldWithDefault(msg, 9, ""),
    building: jspb.Message.getFieldWithDefault(msg, 10, ""),
    apartment: jspb.Message.getFieldWithDefault(msg, 11, ""),
    note: jspb.Message.getFieldWithDefault(msg, 12, ""),
    receiverFirstName: jspb.Message.getFieldWithDefault(msg, 13, ""),
    receiverMiddleName: jspb.Message.getFieldWithDefault(msg, 14, ""),
    receiverLastName: jspb.Message.getFieldWithDefault(msg, 15, ""),
    receiverPhone: jspb.Message.getFieldWithDefault(msg, 16, ""),
    receiverEmail: jspb.Message.getFieldWithDefault(msg, 17, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal.DeliveryAddress;
  return proto.fcp.order.v1.order.FastDeal.DeliveryAddress.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerFirstName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerMiddleName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerLastName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerPhone(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerEmail(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setZip(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setState(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setCity(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setStreet(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuilding(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setApartment(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setNote(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setReceiverFirstName(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setReceiverMiddleName(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setReceiverLastName(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setReceiverPhone(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setReceiverEmail(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.DeliveryAddress.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCustomerFirstName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCustomerMiddleName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getCustomerLastName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getCustomerPhone();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getCustomerEmail();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getZip();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getState();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getCity();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getStreet();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getBuilding();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getApartment();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getNote();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getReceiverFirstName();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getReceiverMiddleName();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getReceiverLastName();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getReceiverPhone();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getReceiverEmail();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
};


/**
 * optional string customer_first_name = 1;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getCustomerFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setCustomerFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string customer_middle_name = 2;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getCustomerMiddleName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setCustomerMiddleName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string customer_last_name = 3;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getCustomerLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setCustomerLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string customer_phone = 4;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getCustomerPhone = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setCustomerPhone = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string customer_email = 5;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getCustomerEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setCustomerEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string zip = 6;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getZip = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setZip = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string state = 7;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getState = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setState = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string city = 8;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getCity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setCity = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string street = 9;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getStreet = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setStreet = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string building = 10;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getBuilding = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setBuilding = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string apartment = 11;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getApartment = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setApartment = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string note = 12;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getNote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setNote = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string receiver_first_name = 13;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getReceiverFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setReceiverFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string receiver_middle_name = 14;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getReceiverMiddleName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setReceiverMiddleName = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional string receiver_last_name = 15;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getReceiverLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setReceiverLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional string receiver_phone = 16;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getReceiverPhone = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setReceiverPhone = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional string receiver_email = 17;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.getReceiverEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryAddress} returns this
 */
proto.fcp.order.v1.order.FastDeal.DeliveryAddress.prototype.setReceiverEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentConditionValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentConditionValue}
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal.PaymentConditionValue;
  return proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentConditionValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentConditionValue}
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} */ (reader.readEnum());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentConditionValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * optional PaymentCondition value = 1;
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentCondition}
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.prototype.getValue = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} value
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentConditionValue} returns this
 */
proto.fcp.order.v1.order.FastDeal.PaymentConditionValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDeal.PaymentInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    currency: jspb.Message.getFieldWithDefault(msg, 1, ""),
    totalAmount: jspb.Message.getFieldWithDefault(msg, 2, 0),
    paymentDate: (f = msg.getPaymentDate()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentInfo}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDeal.PaymentInfo;
  return proto.fcp.order.v1.order.FastDeal.PaymentInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentInfo}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setCurrency(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setTotalAmount(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setPaymentDate(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDeal.PaymentInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCurrency();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getTotalAmount();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getPaymentDate();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
};


/**
 * optional string currency = 1;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.getCurrency = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} returns this
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.setCurrency = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 total_amount = 2;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.getTotalAmount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} returns this
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.setTotalAmount = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional google.protobuf.Timestamp payment_date = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.getPaymentDate = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} returns this
*/
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.setPaymentDate = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentInfo} returns this
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.clearPaymentDate = function() {
  return this.setPaymentDate(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.PaymentInfo.prototype.hasPaymentDate = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string external_id = 2;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getExternalId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setExternalId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional Status status = 3;
 * @return {!proto.fcp.order.v1.order.FastDeal.Status}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getStatus = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.Status} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.Status} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional DeliveryCondition delivery_condition = 4;
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getDeliveryCondition = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setDeliveryCondition = function(value) {
  return jspb.Message.setProto3EnumField(this, 4, value);
};


/**
 * optional LogisticsOperator logistics_operator = 5;
 * @return {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getLogisticsOperator = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setLogisticsOperator = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * optional DeliveryAddress delivery_address = 6;
 * @return {?proto.fcp.order.v1.order.FastDeal.DeliveryAddress}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getDeliveryAddress = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDeal.DeliveryAddress} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDeal.DeliveryAddress, 6));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDeal.DeliveryAddress|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setDeliveryAddress = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearDeliveryAddress = function() {
  return this.setDeliveryAddress(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasDeliveryAddress = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional google.protobuf.Timestamp delivery_date = 7;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getDeliveryDate = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 7));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setDeliveryDate = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearDeliveryDate = function() {
  return this.setDeliveryDate(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasDeliveryDate = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional PaymentCondition payment_condition = 8;
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentCondition}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getPaymentCondition = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setPaymentCondition = function(value) {
  return jspb.Message.setProto3EnumField(this, 8, value);
};


/**
 * optional PaymentInfo payment_info = 9;
 * @return {?proto.fcp.order.v1.order.FastDeal.PaymentInfo}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getPaymentInfo = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDeal.PaymentInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDeal.PaymentInfo, 9));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDeal.PaymentInfo|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setPaymentInfo = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearPaymentInfo = function() {
  return this.setPaymentInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasPaymentInfo = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional UserInfo customer = 18;
 * @return {?proto.fcp.order.v1.order.UserInfo}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getCustomer = function() {
  return /** @type{?proto.fcp.order.v1.order.UserInfo} */ (
    jspb.Message.getWrapperField(this, v1_order_model_base_pb.UserInfo, 18));
};


/**
 * @param {?proto.fcp.order.v1.order.UserInfo|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setCustomer = function(value) {
  return jspb.Message.setWrapperField(this, 18, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearCustomer = function() {
  return this.setCustomer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasCustomer = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional UserInfo supplier = 19;
 * @return {?proto.fcp.order.v1.order.UserInfo}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getSupplier = function() {
  return /** @type{?proto.fcp.order.v1.order.UserInfo} */ (
    jspb.Message.getWrapperField(this, v1_order_model_base_pb.UserInfo, 19));
};


/**
 * @param {?proto.fcp.order.v1.order.UserInfo|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setSupplier = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearSupplier = function() {
  return this.setSupplier(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasSupplier = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional string customer_notes = 20;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getCustomerNotes = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 20, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setCustomerNotes = function(value) {
  return jspb.Message.setProto3StringField(this, 20, value);
};


/**
 * optional int64 chat_id = 23;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getChatId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 23, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setChatId = function(value) {
  return jspb.Message.setProto3IntField(this, 23, value);
};


/**
 * optional string chat_ident = 24;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getChatIdent = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 24, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.setChatIdent = function(value) {
  return jspb.Message.setProto3StringField(this, 24, value);
};


/**
 * optional FastDealSnapshot snapshot = 26;
 * @return {?proto.fcp.order.v1.order.FastDealSnapshot}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getSnapshot = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealSnapshot} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealSnapshot, 26));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealSnapshot|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setSnapshot = function(value) {
  return jspb.Message.setWrapperField(this, 26, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearSnapshot = function() {
  return this.setSnapshot(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasSnapshot = function() {
  return jspb.Message.getField(this, 26) != null;
};


/**
 * repeated FastDealItemSnapshot snapshot_items = 25;
 * @return {!Array<!proto.fcp.order.v1.order.FastDealItemSnapshot>}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getSnapshotItemsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.FastDealItemSnapshot>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.order.v1.order.FastDealItemSnapshot, 25));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDealItemSnapshot>} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setSnapshotItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 25, value);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItemSnapshot=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot}
 */
proto.fcp.order.v1.order.FastDeal.prototype.addSnapshotItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 25, opt_value, proto.fcp.order.v1.order.FastDealItemSnapshot, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearSnapshotItemsList = function() {
  return this.setSnapshotItemsList([]);
};


/**
 * repeated FastDealItem items = 21;
 * @return {!Array<!proto.fcp.order.v1.order.FastDealItem>}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.FastDealItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.order.v1.order.FastDealItem, 21));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDealItem>} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 21, value);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealItem}
 */
proto.fcp.order.v1.order.FastDeal.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 21, opt_value, proto.fcp.order.v1.order.FastDealItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};


/**
 * optional Audit audit = 22;
 * @return {?proto.fcp.order.v1.order.Audit}
 */
proto.fcp.order.v1.order.FastDeal.prototype.getAudit = function() {
  return /** @type{?proto.fcp.order.v1.order.Audit} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.Audit, 22));
};


/**
 * @param {?proto.fcp.order.v1.order.Audit|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
*/
proto.fcp.order.v1.order.FastDeal.prototype.setAudit = function(value) {
  return jspb.Message.setWrapperField(this, 22, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDeal} returns this
 */
proto.fcp.order.v1.order.FastDeal.prototype.clearAudit = function() {
  return this.setAudit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDeal.prototype.hasAudit = function() {
  return jspb.Message.getField(this, 22) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDealSnapshot.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDealSnapshot} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealSnapshot.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    deliveryCondition: jspb.Message.getFieldWithDefault(msg, 2, 0),
    logisticsOperator: jspb.Message.getFieldWithDefault(msg, 3, 0),
    deliveryAddress: (f = msg.getDeliveryAddress()) && proto.fcp.order.v1.order.FastDeal.DeliveryAddress.toObject(includeInstance, f),
    deliveryDate: (f = msg.getDeliveryDate()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    paymentCondition: jspb.Message.getFieldWithDefault(msg, 6, 0),
    paymentInfo: (f = msg.getPaymentInfo()) && proto.fcp.order.v1.order.FastDeal.PaymentInfo.toObject(includeInstance, f),
    customer: (f = msg.getCustomer()) && v1_order_model_base_pb.UserInfo.toObject(includeInstance, f),
    supplier: (f = msg.getSupplier()) && v1_order_model_base_pb.UserInfo.toObject(includeInstance, f),
    customerNotes: jspb.Message.getFieldWithDefault(msg, 10, ""),
    chatId: jspb.Message.getFieldWithDefault(msg, 11, 0),
    chatIdent: jspb.Message.getFieldWithDefault(msg, 12, ""),
    audit: (f = msg.getAudit()) && v1_order_common_pb.Audit.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot}
 */
proto.fcp.order.v1.order.FastDealSnapshot.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDealSnapshot;
  return proto.fcp.order.v1.order.FastDealSnapshot.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDealSnapshot} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot}
 */
proto.fcp.order.v1.order.FastDealSnapshot.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} */ (reader.readEnum());
      msg.setDeliveryCondition(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} */ (reader.readEnum());
      msg.setLogisticsOperator(value);
      break;
    case 4:
      var value = new proto.fcp.order.v1.order.FastDeal.DeliveryAddress;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.DeliveryAddress.deserializeBinaryFromReader);
      msg.setDeliveryAddress(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setDeliveryDate(value);
      break;
    case 6:
      var value = /** @type {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} */ (reader.readEnum());
      msg.setPaymentCondition(value);
      break;
    case 7:
      var value = new proto.fcp.order.v1.order.FastDeal.PaymentInfo;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.PaymentInfo.deserializeBinaryFromReader);
      msg.setPaymentInfo(value);
      break;
    case 8:
      var value = new v1_order_model_base_pb.UserInfo;
      reader.readMessage(value,v1_order_model_base_pb.UserInfo.deserializeBinaryFromReader);
      msg.setCustomer(value);
      break;
    case 9:
      var value = new v1_order_model_base_pb.UserInfo;
      reader.readMessage(value,v1_order_model_base_pb.UserInfo.deserializeBinaryFromReader);
      msg.setSupplier(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setCustomerNotes(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setChatId(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setChatIdent(value);
      break;
    case 21:
      var value = new v1_order_common_pb.Audit;
      reader.readMessage(value,v1_order_common_pb.Audit.deserializeBinaryFromReader);
      msg.setAudit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDealSnapshot.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDealSnapshot} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealSnapshot.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getDeliveryCondition();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getLogisticsOperator();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getDeliveryAddress();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.fcp.order.v1.order.FastDeal.DeliveryAddress.serializeBinaryToWriter
    );
  }
  f = message.getDeliveryDate();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getPaymentCondition();
  if (f !== 0.0) {
    writer.writeEnum(
      6,
      f
    );
  }
  f = message.getPaymentInfo();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.fcp.order.v1.order.FastDeal.PaymentInfo.serializeBinaryToWriter
    );
  }
  f = message.getCustomer();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      v1_order_model_base_pb.UserInfo.serializeBinaryToWriter
    );
  }
  f = message.getSupplier();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      v1_order_model_base_pb.UserInfo.serializeBinaryToWriter
    );
  }
  f = message.getCustomerNotes();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getChatId();
  if (f !== 0) {
    writer.writeInt64(
      11,
      f
    );
  }
  f = message.getChatIdent();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getAudit();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      v1_order_common_pb.Audit.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional FastDeal.DeliveryCondition delivery_condition = 2;
 * @return {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getDeliveryCondition = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setDeliveryCondition = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional FastDeal.LogisticsOperator logistics_operator = 3;
 * @return {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getLogisticsOperator = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setLogisticsOperator = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional FastDeal.DeliveryAddress delivery_address = 4;
 * @return {?proto.fcp.order.v1.order.FastDeal.DeliveryAddress}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getDeliveryAddress = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDeal.DeliveryAddress} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDeal.DeliveryAddress, 4));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDeal.DeliveryAddress|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setDeliveryAddress = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.clearDeliveryAddress = function() {
  return this.setDeliveryAddress(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.hasDeliveryAddress = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional google.protobuf.Timestamp delivery_date = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getDeliveryDate = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setDeliveryDate = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.clearDeliveryDate = function() {
  return this.setDeliveryDate(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.hasDeliveryDate = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional FastDeal.PaymentCondition payment_condition = 6;
 * @return {!proto.fcp.order.v1.order.FastDeal.PaymentCondition}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getPaymentCondition = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setPaymentCondition = function(value) {
  return jspb.Message.setProto3EnumField(this, 6, value);
};


/**
 * optional FastDeal.PaymentInfo payment_info = 7;
 * @return {?proto.fcp.order.v1.order.FastDeal.PaymentInfo}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getPaymentInfo = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDeal.PaymentInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDeal.PaymentInfo, 7));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDeal.PaymentInfo|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setPaymentInfo = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.clearPaymentInfo = function() {
  return this.setPaymentInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.hasPaymentInfo = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional UserInfo customer = 8;
 * @return {?proto.fcp.order.v1.order.UserInfo}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getCustomer = function() {
  return /** @type{?proto.fcp.order.v1.order.UserInfo} */ (
    jspb.Message.getWrapperField(this, v1_order_model_base_pb.UserInfo, 8));
};


/**
 * @param {?proto.fcp.order.v1.order.UserInfo|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setCustomer = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.clearCustomer = function() {
  return this.setCustomer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.hasCustomer = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional UserInfo supplier = 9;
 * @return {?proto.fcp.order.v1.order.UserInfo}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getSupplier = function() {
  return /** @type{?proto.fcp.order.v1.order.UserInfo} */ (
    jspb.Message.getWrapperField(this, v1_order_model_base_pb.UserInfo, 9));
};


/**
 * @param {?proto.fcp.order.v1.order.UserInfo|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setSupplier = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.clearSupplier = function() {
  return this.setSupplier(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.hasSupplier = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string customer_notes = 10;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getCustomerNotes = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setCustomerNotes = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional int64 chat_id = 11;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getChatId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setChatId = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * optional string chat_ident = 12;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getChatIdent = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setChatIdent = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional Audit audit = 21;
 * @return {?proto.fcp.order.v1.order.Audit}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.getAudit = function() {
  return /** @type{?proto.fcp.order.v1.order.Audit} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.Audit, 21));
};


/**
 * @param {?proto.fcp.order.v1.order.Audit|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealSnapshot.prototype.setAudit = function(value) {
  return jspb.Message.setWrapperField(this, 21, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.clearAudit = function() {
  return this.setAudit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealSnapshot.prototype.hasAudit = function() {
  return jspb.Message.getField(this, 21) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDealItem.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDealItem} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealItem.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    externalId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    pricing: (f = msg.getPricing()) && v1_order_model_pricing_pb.Pricing.toObject(includeInstance, f),
    priceType: jspb.Message.getFieldWithDefault(msg, 11, 0),
    pricePerPackageWithVat: jspb.Message.getFieldWithDefault(msg, 12, 0),
    pricePerItemWithVat: jspb.Message.getFieldWithDefault(msg, 13, 0),
    status: jspb.Message.getFieldWithDefault(msg, 4, 0),
    packageQuantity: jspb.Message.getFieldWithDefault(msg, 5, 0),
    currency: jspb.Message.getFieldWithDefault(msg, 6, ""),
    amount: jspb.Message.getFieldWithDefault(msg, 7, 0),
    amountWithVat: jspb.Message.getFieldWithDefault(msg, 10, 0),
    vatAmount: jspb.Message.getFieldWithDefault(msg, 9, 0),
    cancelInProgress: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    audit: (f = msg.getAudit()) && v1_order_common_pb.Audit.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDealItem}
 */
proto.fcp.order.v1.order.FastDealItem.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDealItem;
  return proto.fcp.order.v1.order.FastDealItem.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDealItem} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDealItem}
 */
proto.fcp.order.v1.order.FastDealItem.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setExternalId(value);
      break;
    case 3:
      var value = new v1_order_model_pricing_pb.Pricing;
      reader.readMessage(value,v1_order_model_pricing_pb.Pricing.deserializeBinaryFromReader);
      msg.setPricing(value);
      break;
    case 11:
      var value = /** @type {!proto.fcp.order.v1.order.FastDealItem.PriceType} */ (reader.readEnum());
      msg.setPriceType(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPricePerPackageWithVat(value);
      break;
    case 13:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPricePerItemWithVat(value);
      break;
    case 4:
      var value = /** @type {!proto.fcp.order.v1.order.FastDealItem.Status} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPackageQuantity(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setCurrency(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setAmount(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setAmountWithVat(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setVatAmount(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCancelInProgress(value);
      break;
    case 22:
      var value = new v1_order_common_pb.Audit;
      reader.readMessage(value,v1_order_common_pb.Audit.deserializeBinaryFromReader);
      msg.setAudit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDealItem.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDealItem} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealItem.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getExternalId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPricing();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_order_model_pricing_pb.Pricing.serializeBinaryToWriter
    );
  }
  f = message.getPriceType();
  if (f !== 0.0) {
    writer.writeEnum(
      11,
      f
    );
  }
  f = message.getPricePerPackageWithVat();
  if (f !== 0) {
    writer.writeInt64(
      12,
      f
    );
  }
  f = message.getPricePerItemWithVat();
  if (f !== 0) {
    writer.writeInt64(
      13,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      4,
      f
    );
  }
  f = message.getPackageQuantity();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getCurrency();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getAmount();
  if (f !== 0) {
    writer.writeInt64(
      7,
      f
    );
  }
  f = message.getAmountWithVat();
  if (f !== 0) {
    writer.writeInt64(
      10,
      f
    );
  }
  f = message.getVatAmount();
  if (f !== 0) {
    writer.writeInt64(
      9,
      f
    );
  }
  f = message.getCancelInProgress();
  if (f) {
    writer.writeBool(
      8,
      f
    );
  }
  f = message.getAudit();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      v1_order_common_pb.Audit.serializeBinaryToWriter
    );
  }
};


/**
 * @enum {number}
 */
proto.fcp.order.v1.order.FastDealItem.PriceType = {
  RETAIL: 0,
  WHOLESALE: 1,
  MERCHANDISER: 2
};

/**
 * @enum {number}
 */
proto.fcp.order.v1.order.FastDealItem.Status = {
  ACTIVE: 0,
  REJECT: 1,
  DONE: 2,
  EXECUTE: 3
};

/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string external_id = 2;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getExternalId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setExternalId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional Pricing pricing = 3;
 * @return {?proto.fcp.order.v1.order.Pricing}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getPricing = function() {
  return /** @type{?proto.fcp.order.v1.order.Pricing} */ (
    jspb.Message.getWrapperField(this, v1_order_model_pricing_pb.Pricing, 3));
};


/**
 * @param {?proto.fcp.order.v1.order.Pricing|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
*/
proto.fcp.order.v1.order.FastDealItem.prototype.setPricing = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.clearPricing = function() {
  return this.setPricing(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.hasPricing = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional PriceType price_type = 11;
 * @return {!proto.fcp.order.v1.order.FastDealItem.PriceType}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getPriceType = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDealItem.PriceType} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItem.PriceType} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setPriceType = function(value) {
  return jspb.Message.setProto3EnumField(this, 11, value);
};


/**
 * optional int64 price_per_package_with_VAT = 12;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getPricePerPackageWithVat = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 12, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setPricePerPackageWithVat = function(value) {
  return jspb.Message.setProto3IntField(this, 12, value);
};


/**
 * optional int64 price_per_item_with_VAT = 13;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getPricePerItemWithVat = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 13, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setPricePerItemWithVat = function(value) {
  return jspb.Message.setProto3IntField(this, 13, value);
};


/**
 * optional Status status = 4;
 * @return {!proto.fcp.order.v1.order.FastDealItem.Status}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getStatus = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDealItem.Status} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItem.Status} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 4, value);
};


/**
 * optional int64 package_quantity = 5;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getPackageQuantity = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setPackageQuantity = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional string currency = 6;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getCurrency = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setCurrency = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional int64 amount = 7;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getAmount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setAmount = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int64 amount_with_VAT = 10;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getAmountWithVat = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 10, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setAmountWithVat = function(value) {
  return jspb.Message.setProto3IntField(this, 10, value);
};


/**
 * optional int64 VAT_amount = 9;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getVatAmount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setVatAmount = function(value) {
  return jspb.Message.setProto3IntField(this, 9, value);
};


/**
 * optional bool cancel_in_progress = 8;
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getCancelInProgress = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.setCancelInProgress = function(value) {
  return jspb.Message.setProto3BooleanField(this, 8, value);
};


/**
 * optional Audit audit = 22;
 * @return {?proto.fcp.order.v1.order.Audit}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.getAudit = function() {
  return /** @type{?proto.fcp.order.v1.order.Audit} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.Audit, 22));
};


/**
 * @param {?proto.fcp.order.v1.order.Audit|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
*/
proto.fcp.order.v1.order.FastDealItem.prototype.setAudit = function(value) {
  return jspb.Message.setWrapperField(this, 22, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealItem} returns this
 */
proto.fcp.order.v1.order.FastDealItem.prototype.clearAudit = function() {
  return this.setAudit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealItem.prototype.hasAudit = function() {
  return jspb.Message.getField(this, 22) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDealItemSnapshot.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDealItemSnapshot} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    externalId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    product: (f = msg.getProduct()) && v1_order_model_product_pb.Product.toObject(includeInstance, f),
    packageId: jspb.Message.getFieldWithDefault(msg, 4, 0),
    packageAmount: jspb.Message.getFieldWithDefault(msg, 5, 0),
    pb_package: jspb.Message.getFieldWithDefault(msg, 6, ""),
    currency: jspb.Message.getFieldWithDefault(msg, 7, ""),
    priceType: jspb.Message.getFieldWithDefault(msg, 9, 0),
    pricePerPackageWithVat: jspb.Message.getFieldWithDefault(msg, 10, 0),
    pricePerItemWithVat: jspb.Message.getFieldWithDefault(msg, 11, 0),
    vatRate: jspb.Message.getFieldWithDefault(msg, 12, 0),
    amount: jspb.Message.getFieldWithDefault(msg, 8, 0),
    audit: (f = msg.getAudit()) && v1_order_common_pb.Audit.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDealItemSnapshot;
  return proto.fcp.order.v1.order.FastDealItemSnapshot.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDealItemSnapshot} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setExternalId(value);
      break;
    case 3:
      var value = new v1_order_model_product_pb.Product;
      reader.readMessage(value,v1_order_model_product_pb.Product.deserializeBinaryFromReader);
      msg.setProduct(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPackageId(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPackageAmount(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setPackage(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setCurrency(value);
      break;
    case 9:
      var value = /** @type {!proto.fcp.order.v1.order.FastDealItem.PriceType} */ (reader.readEnum());
      msg.setPriceType(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPricePerPackageWithVat(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPricePerItemWithVat(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setVatRate(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setAmount(value);
      break;
    case 22:
      var value = new v1_order_common_pb.Audit;
      reader.readMessage(value,v1_order_common_pb.Audit.deserializeBinaryFromReader);
      msg.setAudit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDealItemSnapshot.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDealItemSnapshot} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getExternalId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getProduct();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_order_model_product_pb.Product.serializeBinaryToWriter
    );
  }
  f = message.getPackageId();
  if (f !== 0) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = message.getPackageAmount();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
  f = message.getPackage();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getCurrency();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getPriceType();
  if (f !== 0.0) {
    writer.writeEnum(
      9,
      f
    );
  }
  f = message.getPricePerPackageWithVat();
  if (f !== 0) {
    writer.writeInt64(
      10,
      f
    );
  }
  f = message.getPricePerItemWithVat();
  if (f !== 0) {
    writer.writeInt64(
      11,
      f
    );
  }
  f = message.getVatRate();
  if (f !== 0) {
    writer.writeInt64(
      12,
      f
    );
  }
  f = message.getAmount();
  if (f !== 0) {
    writer.writeInt64(
      8,
      f
    );
  }
  f = message.getAudit();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      v1_order_common_pb.Audit.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string external_id = 2;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getExternalId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setExternalId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional Product product = 3;
 * @return {?proto.fcp.order.v1.order.Product}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getProduct = function() {
  return /** @type{?proto.fcp.order.v1.order.Product} */ (
    jspb.Message.getWrapperField(this, v1_order_model_product_pb.Product, 3));
};


/**
 * @param {?proto.fcp.order.v1.order.Product|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setProduct = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.clearProduct = function() {
  return this.setProduct(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.hasProduct = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 package_id = 4;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getPackageId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setPackageId = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int64 package_amount = 5;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getPackageAmount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setPackageAmount = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional string package = 6;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getPackage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setPackage = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string currency = 7;
 * @return {string}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getCurrency = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setCurrency = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional FastDealItem.PriceType price_type = 9;
 * @return {!proto.fcp.order.v1.order.FastDealItem.PriceType}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getPriceType = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDealItem.PriceType} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItem.PriceType} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setPriceType = function(value) {
  return jspb.Message.setProto3EnumField(this, 9, value);
};


/**
 * optional int64 price_per_package_with_VAT = 10;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getPricePerPackageWithVat = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 10, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setPricePerPackageWithVat = function(value) {
  return jspb.Message.setProto3IntField(this, 10, value);
};


/**
 * optional int64 price_per_item_with_VAT = 11;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getPricePerItemWithVat = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setPricePerItemWithVat = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * optional int64 VAT_rate = 12;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getVatRate = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 12, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setVatRate = function(value) {
  return jspb.Message.setProto3IntField(this, 12, value);
};


/**
 * optional int64 amount = 8;
 * @return {number}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getAmount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setAmount = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional Audit audit = 22;
 * @return {?proto.fcp.order.v1.order.Audit}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.getAudit = function() {
  return /** @type{?proto.fcp.order.v1.order.Audit} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.Audit, 22));
};


/**
 * @param {?proto.fcp.order.v1.order.Audit|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
*/
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.setAudit = function(value) {
  return jspb.Message.setWrapperField(this, 22, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot} returns this
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.clearAudit = function() {
  return this.setAudit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealItemSnapshot.prototype.hasAudit = function() {
  return jspb.Message.getField(this, 22) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.FastDealFilter.repeatedFields_ = [1,3,4,5,7,9,10,11];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.FastDealFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.FastDealFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    idsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    externalId: (f = msg.getExternalId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    statusesList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    stateList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    cityList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f,
    productFilter: (f = msg.getProductFilter()) && v1_order_model_product_pb.ProductFilter.toObject(includeInstance, f),
    currencyList: (f = jspb.Message.getRepeatedField(msg, 7)) == null ? undefined : f,
    logisticOperatorList: (f = jspb.Message.getRepeatedField(msg, 9)) == null ? undefined : f,
    deliveryConditionList: (f = jspb.Message.getRepeatedField(msg, 10)) == null ? undefined : f,
    paymentConditionList: (f = jspb.Message.getRepeatedField(msg, 11)) == null ? undefined : f,
    createdAt: (f = msg.getCreatedAt()) && v1_order_common_pb.TimeRange.toObject(includeInstance, f),
    updatedAt: (f = msg.getUpdatedAt()) && v1_order_common_pb.TimeRange.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.FastDealFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.FastDealFilter;
  return proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.FastDealFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setIdsList(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setExternalId(value);
      break;
    case 3:
      var value = /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.Status>} */ (reader.readPackedEnum());
      msg.setStatusesList(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.addState(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.addCity(value);
      break;
    case 6:
      var value = new v1_order_model_product_pb.ProductFilter;
      reader.readMessage(value,v1_order_model_product_pb.ProductFilter.deserializeBinaryFromReader);
      msg.setProductFilter(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.addCurrency(value);
      break;
    case 9:
      var value = /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.LogisticsOperator>} */ (reader.readPackedEnum());
      msg.setLogisticOperatorList(value);
      break;
    case 10:
      var value = /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.DeliveryCondition>} */ (reader.readPackedEnum());
      msg.setDeliveryConditionList(value);
      break;
    case 11:
      var value = /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.PaymentCondition>} */ (reader.readPackedEnum());
      msg.setPaymentConditionList(value);
      break;
    case 22:
      var value = new v1_order_common_pb.TimeRange;
      reader.readMessage(value,v1_order_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setCreatedAt(value);
      break;
    case 23:
      var value = new v1_order_common_pb.TimeRange;
      reader.readMessage(value,v1_order_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setUpdatedAt(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.FastDealFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdsList();
  if (f.length > 0) {
    writer.writePackedInt64(
      1,
      f
    );
  }
  f = message.getExternalId();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getStatusesList();
  if (f.length > 0) {
    writer.writePackedEnum(
      3,
      f
    );
  }
  f = message.getStateList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      4,
      f
    );
  }
  f = message.getCityList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      5,
      f
    );
  }
  f = message.getProductFilter();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      v1_order_model_product_pb.ProductFilter.serializeBinaryToWriter
    );
  }
  f = message.getCurrencyList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      7,
      f
    );
  }
  f = message.getLogisticOperatorList();
  if (f.length > 0) {
    writer.writePackedEnum(
      9,
      f
    );
  }
  f = message.getDeliveryConditionList();
  if (f.length > 0) {
    writer.writePackedEnum(
      10,
      f
    );
  }
  f = message.getPaymentConditionList();
  if (f.length > 0) {
    writer.writePackedEnum(
      11,
      f
    );
  }
  f = message.getCreatedAt();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      v1_order_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedAt();
  if (f != null) {
    writer.writeMessage(
      23,
      f,
      v1_order_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
};


/**
 * repeated int64 ids = 1;
 * @return {!Array<number>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getIdsList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setIdsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addIds = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearIdsList = function() {
  return this.setIdsList([]);
};


/**
 * optional google.protobuf.StringValue external_id = 2;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getExternalId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 2));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
*/
proto.fcp.order.v1.order.FastDealFilter.prototype.setExternalId = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearExternalId = function() {
  return this.setExternalId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.hasExternalId = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * repeated FastDeal.Status statuses = 3;
 * @return {!Array<!proto.fcp.order.v1.order.FastDeal.Status>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getStatusesList = function() {
  return /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.Status>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDeal.Status>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setStatusesList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.Status} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addStatuses = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearStatusesList = function() {
  return this.setStatusesList([]);
};


/**
 * repeated string state = 4;
 * @return {!Array<string>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getStateList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setStateList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addState = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearStateList = function() {
  return this.setStateList([]);
};


/**
 * repeated string city = 5;
 * @return {!Array<string>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getCityList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setCityList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addCity = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearCityList = function() {
  return this.setCityList([]);
};


/**
 * optional ProductFilter product_filter = 6;
 * @return {?proto.fcp.order.v1.order.ProductFilter}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getProductFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.ProductFilter} */ (
    jspb.Message.getWrapperField(this, v1_order_model_product_pb.ProductFilter, 6));
};


/**
 * @param {?proto.fcp.order.v1.order.ProductFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
*/
proto.fcp.order.v1.order.FastDealFilter.prototype.setProductFilter = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearProductFilter = function() {
  return this.setProductFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.hasProductFilter = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * repeated string currency = 7;
 * @return {!Array<string>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getCurrencyList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 7));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setCurrencyList = function(value) {
  return jspb.Message.setField(this, 7, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addCurrency = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 7, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearCurrencyList = function() {
  return this.setCurrencyList([]);
};


/**
 * repeated FastDeal.LogisticsOperator logistic_operator = 9;
 * @return {!Array<!proto.fcp.order.v1.order.FastDeal.LogisticsOperator>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getLogisticOperatorList = function() {
  return /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.LogisticsOperator>} */ (jspb.Message.getRepeatedField(this, 9));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDeal.LogisticsOperator>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setLogisticOperatorList = function(value) {
  return jspb.Message.setField(this, 9, value || []);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.LogisticsOperator} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addLogisticOperator = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 9, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearLogisticOperatorList = function() {
  return this.setLogisticOperatorList([]);
};


/**
 * repeated FastDeal.DeliveryCondition delivery_condition = 10;
 * @return {!Array<!proto.fcp.order.v1.order.FastDeal.DeliveryCondition>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getDeliveryConditionList = function() {
  return /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.DeliveryCondition>} */ (jspb.Message.getRepeatedField(this, 10));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDeal.DeliveryCondition>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setDeliveryConditionList = function(value) {
  return jspb.Message.setField(this, 10, value || []);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.DeliveryCondition} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addDeliveryCondition = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 10, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearDeliveryConditionList = function() {
  return this.setDeliveryConditionList([]);
};


/**
 * repeated FastDeal.PaymentCondition payment_condition = 11;
 * @return {!Array<!proto.fcp.order.v1.order.FastDeal.PaymentCondition>}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getPaymentConditionList = function() {
  return /** @type {!Array<!proto.fcp.order.v1.order.FastDeal.PaymentCondition>} */ (jspb.Message.getRepeatedField(this, 11));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDeal.PaymentCondition>} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.setPaymentConditionList = function(value) {
  return jspb.Message.setField(this, 11, value || []);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal.PaymentCondition} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.addPaymentCondition = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 11, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearPaymentConditionList = function() {
  return this.setPaymentConditionList([]);
};


/**
 * optional TimeRange created_at = 22;
 * @return {?proto.fcp.order.v1.order.TimeRange}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getCreatedAt = function() {
  return /** @type{?proto.fcp.order.v1.order.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.TimeRange, 22));
};


/**
 * @param {?proto.fcp.order.v1.order.TimeRange|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
*/
proto.fcp.order.v1.order.FastDealFilter.prototype.setCreatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 22, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearCreatedAt = function() {
  return this.setCreatedAt(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.hasCreatedAt = function() {
  return jspb.Message.getField(this, 22) != null;
};


/**
 * optional TimeRange updated_at = 23;
 * @return {?proto.fcp.order.v1.order.TimeRange}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.getUpdatedAt = function() {
  return /** @type{?proto.fcp.order.v1.order.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.TimeRange, 23));
};


/**
 * @param {?proto.fcp.order.v1.order.TimeRange|undefined} value
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
*/
proto.fcp.order.v1.order.FastDealFilter.prototype.setUpdatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 23, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.FastDealFilter} returns this
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.clearUpdatedAt = function() {
  return this.setUpdatedAt(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.FastDealFilter.prototype.hasUpdatedAt = function() {
  return jspb.Message.getField(this, 23) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealRequest.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f),
    sortingList: jspb.Message.toObjectList(msg.getSortingList(),
    v1_order_common_pb.Sorting.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_order_common_pb.PaginationRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest}
 */
proto.fcp.order.v1.order.ListFastDealRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealRequest;
  return proto.fcp.order.v1.order.ListFastDealRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest}
 */
proto.fcp.order.v1.order.ListFastDealRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 2:
      var value = new v1_order_common_pb.Sorting;
      reader.readMessage(value,v1_order_common_pb.Sorting.deserializeBinaryFromReader);
      msg.addSorting(value);
      break;
    case 3:
      var value = new v1_order_common_pb.PaginationRequest;
      reader.readMessage(value,v1_order_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
  f = message.getSortingList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_order_common_pb.Sorting.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_order_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated Sorting sorting = 2;
 * @return {!Array<!proto.fcp.order.v1.order.Sorting>}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.getSortingList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.Sorting>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_common_pb.Sorting, 2));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.Sorting>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealRequest.prototype.setSortingList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.order.v1.order.Sorting=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.Sorting}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.addSorting = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.order.v1.order.Sorting, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.clearSortingList = function() {
  return this.setSortingList([]);
};


/**
 * optional PaginationRequest pagination = 3;
 * @return {?proto.fcp.order.v1.order.PaginationRequest}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.order.v1.order.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.PaginationRequest, 3));
};


/**
 * @param {?proto.fcp.order.v1.order.PaginationRequest|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.order.v1.order.FastDeal.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_order_common_pb.PaginationResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealResponse}
 */
proto.fcp.order.v1.order.ListFastDealResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealResponse;
  return proto.fcp.order.v1.order.ListFastDealResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealResponse}
 */
proto.fcp.order.v1.order.ListFastDealResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDeal;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    case 2:
      var value = new v1_order_common_pb.PaginationResponse;
      reader.readMessage(value,v1_order_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDeal.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_order_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated FastDeal items = 1;
 * @return {!Array<!proto.fcp.order.v1.order.FastDeal>}
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.FastDeal>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.order.v1.order.FastDeal, 1));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDeal>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDeal=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDeal}
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.order.v1.order.FastDeal, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};


/**
 * optional PaginationResponse pagination = 2;
 * @return {?proto.fcp.order.v1.order.PaginationResponse}
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.order.v1.order.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.PaginationResponse, 2));
};


/**
 * @param {?proto.fcp.order.v1.order.PaginationResponse|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealItemAgentRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    sortingList: jspb.Message.toObjectList(msg.getSortingList(),
    v1_order_common_pb.Sorting.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_order_common_pb.PaginationRequest.toObject(includeInstance, f),
    agentId: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealItemAgentRequest;
  return proto.fcp.order.v1.order.ListFastDealItemAgentRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_common_pb.Sorting;
      reader.readMessage(value,v1_order_common_pb.Sorting.deserializeBinaryFromReader);
      msg.addSorting(value);
      break;
    case 2:
      var value = new v1_order_common_pb.PaginationRequest;
      reader.readMessage(value,v1_order_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setAgentId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealItemAgentRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSortingList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      v1_order_common_pb.Sorting.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_order_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
  f = message.getAgentId();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * repeated Sorting sorting = 1;
 * @return {!Array<!proto.fcp.order.v1.order.Sorting>}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.getSortingList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.Sorting>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_common_pb.Sorting, 1));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.Sorting>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.setSortingList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.order.v1.order.Sorting=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.Sorting}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.addSorting = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.order.v1.order.Sorting, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.clearSortingList = function() {
  return this.setSortingList([]);
};


/**
 * optional PaginationRequest pagination = 2;
 * @return {?proto.fcp.order.v1.order.PaginationRequest}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.order.v1.order.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.PaginationRequest, 2));
};


/**
 * @param {?proto.fcp.order.v1.order.PaginationRequest|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string agent_id = 4;
 * @return {string}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.getAgentId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealItemAgentRequest.prototype.setAgentId = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealItemAgentResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.order.v1.order.FastDealItemSnapshot.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_order_common_pb.PaginationResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealItemAgentResponse;
  return proto.fcp.order.v1.order.ListFastDealItemAgentResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 3:
      var value = new proto.fcp.order.v1.order.FastDealItemSnapshot;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealItemSnapshot.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    case 2:
      var value = new v1_order_common_pb.PaginationResponse;
      reader.readMessage(value,v1_order_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealItemAgentResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.fcp.order.v1.order.FastDealItemSnapshot.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_order_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated FastDealItemSnapshot items = 3;
 * @return {!Array<!proto.fcp.order.v1.order.FastDealItemSnapshot>}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.FastDealItemSnapshot>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.order.v1.order.FastDealItemSnapshot, 3));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.FastDealItemSnapshot>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItemSnapshot=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.FastDealItemSnapshot}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.order.v1.order.FastDealItemSnapshot, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};


/**
 * optional PaginationResponse pagination = 2;
 * @return {?proto.fcp.order.v1.order.PaginationResponse}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.order.v1.order.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.PaginationResponse, 2));
};


/**
 * @param {?proto.fcp.order.v1.order.PaginationResponse|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealItemAgentResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealItemAgentResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealProcessRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealProcessRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessRequest}
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealProcessRequest;
  return proto.fcp.order.v1.order.ListFastDealProcessRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealProcessRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessRequest}
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealProcessRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealProcessRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealProcessRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealProcessRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest}
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealProcessTypeRequest;
  return proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest}
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealProcessTypeRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealProcessTypeRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealProductGroupRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest}
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealProductGroupRequest;
  return proto.fcp.order.v1.order.ListFastDealProductGroupRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest}
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealProductGroupRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealProductGroupRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealProductGroupRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealBrandRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealBrandRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealBrandRequest}
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealBrandRequest;
  return proto.fcp.order.v1.order.ListFastDealBrandRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealBrandRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealBrandRequest}
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealBrandRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealBrandRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealBrandRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealBrandRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealBrandRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealBrandRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealProductRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealProductRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealProductRequest}
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealProductRequest;
  return proto.fcp.order.v1.order.ListFastDealProductRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealProductRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealProductRequest}
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealProductRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealProductRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealProductRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealProductRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealProductRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealProductRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealCurrencyRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest}
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealCurrencyRequest;
  return proto.fcp.order.v1.order.ListFastDealCurrencyRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest}
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealCurrencyRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealCurrencyRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealCurrencyRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest}
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest;
  return proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest}
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealDeliveryConditionRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest}
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest;
  return proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest}
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealPaymentConditionRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest}
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest;
  return proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest}
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealLogisticsOperatorRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealStateRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealStateRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealStateRequest}
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealStateRequest;
  return proto.fcp.order.v1.order.ListFastDealStateRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealStateRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealStateRequest}
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealStateRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealStateRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealStateRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealStateRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealStateRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealStateRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealCityRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealCityRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.order.v1.order.FastDealFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealCityRequest}
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealCityRequest;
  return proto.fcp.order.v1.order.ListFastDealCityRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealCityRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealCityRequest}
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDealFilter;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDealFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealCityRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealCityRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDealFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDealFilter filter = 1;
 * @return {?proto.fcp.order.v1.order.FastDealFilter}
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDealFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDealFilter, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDealFilter|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealCityRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealCityRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealCityRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealCityRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.repeatedFields_ = [1,2,3,4,13,5,6,7,8,9,10,11,12];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealNamesRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealNamesRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    processesList: jspb.Message.toObjectList(msg.getProcessesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    typesList: jspb.Message.toObjectList(msg.getTypesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    deliveryConditionsList: jspb.Message.toObjectList(msg.getDeliveryConditionsList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    paymentConditionsList: jspb.Message.toObjectList(msg.getPaymentConditionsList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    currenciesList: jspb.Message.toObjectList(msg.getCurrenciesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    productGroupsList: jspb.Message.toObjectList(msg.getProductGroupsList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    categoriesList: jspb.Message.toObjectList(msg.getCategoriesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    brandsList: jspb.Message.toObjectList(msg.getBrandsList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    productsList: jspb.Message.toObjectList(msg.getProductsList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    quantityTypesList: jspb.Message.toObjectList(msg.getQuantityTypesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    logisticsOperatorsList: jspb.Message.toObjectList(msg.getLogisticsOperatorsList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    statesList: jspb.Message.toObjectList(msg.getStatesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance),
    citiesList: jspb.Message.toObjectList(msg.getCitiesList(),
    v1_order_enum_pb.NameFilter.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealNamesRequest;
  return proto.fcp.order.v1.order.ListFastDealNamesRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealNamesRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addProcesses(value);
      break;
    case 2:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addTypes(value);
      break;
    case 3:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addDeliveryConditions(value);
      break;
    case 4:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addPaymentConditions(value);
      break;
    case 13:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addCurrencies(value);
      break;
    case 5:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addProductGroups(value);
      break;
    case 6:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addCategories(value);
      break;
    case 7:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addBrands(value);
      break;
    case 8:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addProducts(value);
      break;
    case 9:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addQuantityTypes(value);
      break;
    case 10:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addLogisticsOperators(value);
      break;
    case 11:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addStates(value);
      break;
    case 12:
      var value = new v1_order_enum_pb.NameFilter;
      reader.readMessage(value,v1_order_enum_pb.NameFilter.deserializeBinaryFromReader);
      msg.addCities(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealNamesRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealNamesRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getProcessesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getTypesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getDeliveryConditionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getPaymentConditionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getCurrenciesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      13,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getProductGroupsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getCategoriesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getBrandsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getProductsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getQuantityTypesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getLogisticsOperatorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getStatesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
  f = message.getCitiesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      12,
      f,
      v1_order_enum_pb.NameFilter.serializeBinaryToWriter
    );
  }
};


/**
 * repeated NameFilter processes = 1;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getProcessesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 1));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setProcessesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addProcesses = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearProcessesList = function() {
  return this.setProcessesList([]);
};


/**
 * repeated NameFilter types = 2;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getTypesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 2));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setTypesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addTypes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearTypesList = function() {
  return this.setTypesList([]);
};


/**
 * repeated NameFilter delivery_conditions = 3;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getDeliveryConditionsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 3));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setDeliveryConditionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addDeliveryConditions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearDeliveryConditionsList = function() {
  return this.setDeliveryConditionsList([]);
};


/**
 * repeated NameFilter payment_conditions = 4;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getPaymentConditionsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 4));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setPaymentConditionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addPaymentConditions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearPaymentConditionsList = function() {
  return this.setPaymentConditionsList([]);
};


/**
 * repeated NameFilter currencies = 13;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getCurrenciesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 13));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setCurrenciesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 13, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addCurrencies = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 13, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearCurrenciesList = function() {
  return this.setCurrenciesList([]);
};


/**
 * repeated NameFilter product_groups = 5;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getProductGroupsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 5));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setProductGroupsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addProductGroups = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearProductGroupsList = function() {
  return this.setProductGroupsList([]);
};


/**
 * repeated NameFilter categories = 6;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getCategoriesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 6));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setCategoriesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addCategories = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearCategoriesList = function() {
  return this.setCategoriesList([]);
};


/**
 * repeated NameFilter brands = 7;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getBrandsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 7));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setBrandsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addBrands = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearBrandsList = function() {
  return this.setBrandsList([]);
};


/**
 * repeated NameFilter products = 8;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getProductsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 8));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setProductsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addProducts = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearProductsList = function() {
  return this.setProductsList([]);
};


/**
 * repeated NameFilter quantity_types = 9;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getQuantityTypesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 9));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setQuantityTypesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addQuantityTypes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearQuantityTypesList = function() {
  return this.setQuantityTypesList([]);
};


/**
 * repeated NameFilter logistics_operators = 10;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getLogisticsOperatorsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 10));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setLogisticsOperatorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addLogisticsOperators = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearLogisticsOperatorsList = function() {
  return this.setLogisticsOperatorsList([]);
};


/**
 * repeated NameFilter states = 11;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getStatesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 11));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setStatesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addStates = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearStatesList = function() {
  return this.setStatesList([]);
};


/**
 * repeated NameFilter cities = 12;
 * @return {!Array<!proto.fcp.order.v1.order.NameFilter>}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.getCitiesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.NameFilter>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.NameFilter, 12));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.NameFilter>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.setCitiesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 12, value);
};


/**
 * @param {!proto.fcp.order.v1.order.NameFilter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.NameFilter}
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.addCities = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 12, opt_value, proto.fcp.order.v1.order.NameFilter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesRequest.prototype.clearCitiesList = function() {
  return this.setCitiesList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.repeatedFields_ = [1,2,3,4,13,5,6,7,8,9,10,11,12];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealNamesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealNamesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    processesList: jspb.Message.toObjectList(msg.getProcessesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    typesList: jspb.Message.toObjectList(msg.getTypesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    deliveryConditionsList: jspb.Message.toObjectList(msg.getDeliveryConditionsList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    paymentConditionsList: jspb.Message.toObjectList(msg.getPaymentConditionsList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    currenciesList: jspb.Message.toObjectList(msg.getCurrenciesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    productGroupsList: jspb.Message.toObjectList(msg.getProductGroupsList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    categoriesList: jspb.Message.toObjectList(msg.getCategoriesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    brandsList: jspb.Message.toObjectList(msg.getBrandsList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    productsList: jspb.Message.toObjectList(msg.getProductsList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    quantityTypesList: jspb.Message.toObjectList(msg.getQuantityTypesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    logisticsOperatorsList: jspb.Message.toObjectList(msg.getLogisticsOperatorsList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    statesList: jspb.Message.toObjectList(msg.getStatesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance),
    citiesList: jspb.Message.toObjectList(msg.getCitiesList(),
    v1_order_enum_pb.DictItem.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealNamesResponse;
  return proto.fcp.order.v1.order.ListFastDealNamesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealNamesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addProcesses(value);
      break;
    case 2:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addTypes(value);
      break;
    case 3:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addDeliveryConditions(value);
      break;
    case 4:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addPaymentConditions(value);
      break;
    case 13:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addCurrencies(value);
      break;
    case 5:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addProductGroups(value);
      break;
    case 6:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addCategories(value);
      break;
    case 7:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addBrands(value);
      break;
    case 8:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addProducts(value);
      break;
    case 9:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addQuantityTypes(value);
      break;
    case 10:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addLogisticsOperators(value);
      break;
    case 11:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addStates(value);
      break;
    case 12:
      var value = new v1_order_enum_pb.DictItem;
      reader.readMessage(value,v1_order_enum_pb.DictItem.deserializeBinaryFromReader);
      msg.addCities(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealNamesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealNamesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getProcessesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getTypesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getDeliveryConditionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getPaymentConditionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getCurrenciesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      13,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getProductGroupsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getCategoriesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getBrandsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getProductsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getQuantityTypesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getLogisticsOperatorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getStatesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
  f = message.getCitiesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      12,
      f,
      v1_order_enum_pb.DictItem.serializeBinaryToWriter
    );
  }
};


/**
 * repeated DictItem processes = 1;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getProcessesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 1));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setProcessesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addProcesses = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearProcessesList = function() {
  return this.setProcessesList([]);
};


/**
 * repeated DictItem types = 2;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getTypesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 2));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setTypesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addTypes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearTypesList = function() {
  return this.setTypesList([]);
};


/**
 * repeated DictItem delivery_conditions = 3;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getDeliveryConditionsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 3));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setDeliveryConditionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addDeliveryConditions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearDeliveryConditionsList = function() {
  return this.setDeliveryConditionsList([]);
};


/**
 * repeated DictItem payment_conditions = 4;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getPaymentConditionsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 4));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setPaymentConditionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addPaymentConditions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearPaymentConditionsList = function() {
  return this.setPaymentConditionsList([]);
};


/**
 * repeated DictItem currencies = 13;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getCurrenciesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 13));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setCurrenciesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 13, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addCurrencies = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 13, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearCurrenciesList = function() {
  return this.setCurrenciesList([]);
};


/**
 * repeated DictItem product_groups = 5;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getProductGroupsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 5));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setProductGroupsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addProductGroups = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearProductGroupsList = function() {
  return this.setProductGroupsList([]);
};


/**
 * repeated DictItem categories = 6;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getCategoriesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 6));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setCategoriesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addCategories = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearCategoriesList = function() {
  return this.setCategoriesList([]);
};


/**
 * repeated DictItem brands = 7;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getBrandsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 7));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setBrandsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addBrands = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearBrandsList = function() {
  return this.setBrandsList([]);
};


/**
 * repeated DictItem products = 8;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getProductsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 8));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setProductsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addProducts = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearProductsList = function() {
  return this.setProductsList([]);
};


/**
 * repeated DictItem quantity_types = 9;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getQuantityTypesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 9));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setQuantityTypesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addQuantityTypes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearQuantityTypesList = function() {
  return this.setQuantityTypesList([]);
};


/**
 * repeated DictItem logistics_operators = 10;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getLogisticsOperatorsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 10));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setLogisticsOperatorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addLogisticsOperators = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearLogisticsOperatorsList = function() {
  return this.setLogisticsOperatorsList([]);
};


/**
 * repeated DictItem states = 11;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getStatesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 11));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setStatesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addStates = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearStatesList = function() {
  return this.setStatesList([]);
};


/**
 * repeated DictItem cities = 12;
 * @return {!Array<!proto.fcp.order.v1.order.DictItem>}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.getCitiesList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.DictItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_enum_pb.DictItem, 12));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.DictItem>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.setCitiesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 12, value);
};


/**
 * @param {!proto.fcp.order.v1.order.DictItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.DictItem}
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.addCities = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 12, opt_value, proto.fcp.order.v1.order.DictItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealNamesResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealNamesResponse.prototype.clearCitiesList = function() {
  return this.setCitiesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.GetFastDealRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.GetFastDealRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.GetFastDealRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.GetFastDealRequest}
 */
proto.fcp.order.v1.order.GetFastDealRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.GetFastDealRequest;
  return proto.fcp.order.v1.order.GetFastDealRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.GetFastDealRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.GetFastDealRequest}
 */
proto.fcp.order.v1.order.GetFastDealRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.GetFastDealRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.GetFastDealRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.GetFastDealRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.GetFastDealRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.GetFastDealRequest} returns this
 */
proto.fcp.order.v1.order.GetFastDealRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.GetFastDealResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.GetFastDealResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.GetFastDealResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.order.v1.order.FastDeal.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.GetFastDealResponse}
 */
proto.fcp.order.v1.order.GetFastDealResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.GetFastDealResponse;
  return proto.fcp.order.v1.order.GetFastDealResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.GetFastDealResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.GetFastDealResponse}
 */
proto.fcp.order.v1.order.GetFastDealResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDeal;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.GetFastDealResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.GetFastDealResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.GetFastDealResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDeal.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDeal item = 1;
 * @return {?proto.fcp.order.v1.order.FastDeal}
 */
proto.fcp.order.v1.order.GetFastDealResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDeal} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDeal, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDeal|undefined} value
 * @return {!proto.fcp.order.v1.order.GetFastDealResponse} returns this
*/
proto.fcp.order.v1.order.GetFastDealResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.GetFastDealResponse} returns this
 */
proto.fcp.order.v1.order.GetFastDealResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.GetFastDealResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.CreateFastDealRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.CreateFastDealRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.CreateFastDealRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.CreateFastDealRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.order.v1.order.FastDeal.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.CreateFastDealRequest}
 */
proto.fcp.order.v1.order.CreateFastDealRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.CreateFastDealRequest;
  return proto.fcp.order.v1.order.CreateFastDealRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.CreateFastDealRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.CreateFastDealRequest}
 */
proto.fcp.order.v1.order.CreateFastDealRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.FastDeal;
      reader.readMessage(value,proto.fcp.order.v1.order.FastDeal.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.CreateFastDealRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.CreateFastDealRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.CreateFastDealRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.CreateFastDealRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.FastDeal.serializeBinaryToWriter
    );
  }
};


/**
 * optional FastDeal item = 1;
 * @return {?proto.fcp.order.v1.order.FastDeal}
 */
proto.fcp.order.v1.order.CreateFastDealRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.order.v1.order.FastDeal} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.FastDeal, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.FastDeal|undefined} value
 * @return {!proto.fcp.order.v1.order.CreateFastDealRequest} returns this
*/
proto.fcp.order.v1.order.CreateFastDealRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.CreateFastDealRequest} returns this
 */
proto.fcp.order.v1.order.CreateFastDealRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.CreateFastDealRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.CreateFastDealResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.CreateFastDealResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.CreateFastDealResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.CreateFastDealResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.CreateFastDealResponse}
 */
proto.fcp.order.v1.order.CreateFastDealResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.CreateFastDealResponse;
  return proto.fcp.order.v1.order.CreateFastDealResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.CreateFastDealResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.CreateFastDealResponse}
 */
proto.fcp.order.v1.order.CreateFastDealResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.CreateFastDealResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.CreateFastDealResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.CreateFastDealResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.CreateFastDealResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.CreateFastDealResponse.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.CreateFastDealResponse} returns this
 */
proto.fcp.order.v1.order.CreateFastDealResponse.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.RejectFastDealRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.RejectFastDealRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.RejectFastDealRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.RejectFastDealRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.RejectFastDealRequest}
 */
proto.fcp.order.v1.order.RejectFastDealRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.RejectFastDealRequest;
  return proto.fcp.order.v1.order.RejectFastDealRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.RejectFastDealRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.RejectFastDealRequest}
 */
proto.fcp.order.v1.order.RejectFastDealRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.RejectFastDealRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.RejectFastDealRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.RejectFastDealRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.RejectFastDealRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.RejectFastDealRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.RejectFastDealRequest} returns this
 */
proto.fcp.order.v1.order.RejectFastDealRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.RejectFastDealResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.RejectFastDealResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.RejectFastDealResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.RejectFastDealResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.RejectFastDealResponse}
 */
proto.fcp.order.v1.order.RejectFastDealResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.RejectFastDealResponse;
  return proto.fcp.order.v1.order.RejectFastDealResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.RejectFastDealResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.RejectFastDealResponse}
 */
proto.fcp.order.v1.order.RejectFastDealResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.RejectFastDealResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.RejectFastDealResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.RejectFastDealResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.RejectFastDealResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ApproveFastDealRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ApproveFastDealRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ApproveFastDealRequest}
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ApproveFastDealRequest;
  return proto.fcp.order.v1.order.ApproveFastDealRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ApproveFastDealRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ApproveFastDealRequest}
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ApproveFastDealRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ApproveFastDealRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.ApproveFastDealRequest} returns this
 */
proto.fcp.order.v1.order.ApproveFastDealRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ApproveFastDealResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ApproveFastDealResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ApproveFastDealResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ApproveFastDealResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ApproveFastDealResponse}
 */
proto.fcp.order.v1.order.ApproveFastDealResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ApproveFastDealResponse;
  return proto.fcp.order.v1.order.ApproveFastDealResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ApproveFastDealResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ApproveFastDealResponse}
 */
proto.fcp.order.v1.order.ApproveFastDealResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ApproveFastDealResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ApproveFastDealResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ApproveFastDealResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ApproveFastDealResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.DoneFastDealRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.DoneFastDealRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DoneFastDealRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    rating: (f = msg.getRating()) && v1_order_model_user_rating_pb.UserRating.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.DoneFastDealRequest}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.DoneFastDealRequest;
  return proto.fcp.order.v1.order.DoneFastDealRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.DoneFastDealRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.DoneFastDealRequest}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = new v1_order_model_user_rating_pb.UserRating;
      reader.readMessage(value,v1_order_model_user_rating_pb.UserRating.deserializeBinaryFromReader);
      msg.setRating(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.DoneFastDealRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.DoneFastDealRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DoneFastDealRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getRating();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_order_model_user_rating_pb.UserRating.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.DoneFastDealRequest} returns this
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional UserRating rating = 2;
 * @return {?proto.fcp.order.v1.order.UserRating}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.getRating = function() {
  return /** @type{?proto.fcp.order.v1.order.UserRating} */ (
    jspb.Message.getWrapperField(this, v1_order_model_user_rating_pb.UserRating, 2));
};


/**
 * @param {?proto.fcp.order.v1.order.UserRating|undefined} value
 * @return {!proto.fcp.order.v1.order.DoneFastDealRequest} returns this
*/
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.setRating = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.DoneFastDealRequest} returns this
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.clearRating = function() {
  return this.setRating(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.DoneFastDealRequest.prototype.hasRating = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.DoneFastDealResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.DoneFastDealResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.DoneFastDealResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DoneFastDealResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.DoneFastDealResponse}
 */
proto.fcp.order.v1.order.DoneFastDealResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.DoneFastDealResponse;
  return proto.fcp.order.v1.order.DoneFastDealResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.DoneFastDealResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.DoneFastDealResponse}
 */
proto.fcp.order.v1.order.DoneFastDealResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.DoneFastDealResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.DoneFastDealResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.DoneFastDealResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DoneFastDealResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    status: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest;
  return proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {!proto.fcp.order.v1.order.FastDealItem.Status} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest} returns this
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional FastDealItem.Status status = 2;
 * @return {!proto.fcp.order.v1.order.FastDealItem.Status}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.prototype.getStatus = function() {
  return /** @type {!proto.fcp.order.v1.order.FastDealItem.Status} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.FastDealItem.Status} value
 * @return {!proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest} returns this
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusRequest.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse;
  return proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.UpdateFastDealItemStatusResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.GetFastDealFileRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.GetFastDealFileRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.GetFastDealFileRequest}
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.GetFastDealFileRequest;
  return proto.fcp.order.v1.order.GetFastDealFileRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.GetFastDealFileRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.GetFastDealFileRequest}
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.GetFastDealFileRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.GetFastDealFileRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.GetFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.GetFastDealFileRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.GetFastDealFileResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.GetFastDealFileResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && v1_order_file_pb.File.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.GetFastDealFileResponse}
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.GetFastDealFileResponse;
  return proto.fcp.order.v1.order.GetFastDealFileResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.GetFastDealFileResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.GetFastDealFileResponse}
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_file_pb.File;
      reader.readMessage(value,v1_order_file_pb.File.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.GetFastDealFileResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.GetFastDealFileResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_order_file_pb.File.serializeBinaryToWriter
    );
  }
};


/**
 * optional File item = 1;
 * @return {?proto.fcp.order.v1.order.File}
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.order.v1.order.File} */ (
    jspb.Message.getWrapperField(this, v1_order_file_pb.File, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.File|undefined} value
 * @return {!proto.fcp.order.v1.order.GetFastDealFileResponse} returns this
*/
proto.fcp.order.v1.order.GetFastDealFileResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.GetFastDealFileResponse} returns this
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.GetFastDealFileResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealFileRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealFileRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    fastDealId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    sortingList: jspb.Message.toObjectList(msg.getSortingList(),
    v1_order_common_pb.Sorting.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_order_common_pb.PaginationRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealFileRequest;
  return proto.fcp.order.v1.order.ListFastDealFileRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealFileRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFastDealId(value);
      break;
    case 2:
      var value = new v1_order_common_pb.Sorting;
      reader.readMessage(value,v1_order_common_pb.Sorting.deserializeBinaryFromReader);
      msg.addSorting(value);
      break;
    case 3:
      var value = new v1_order_common_pb.PaginationRequest;
      reader.readMessage(value,v1_order_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealFileRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealFileRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFastDealId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getSortingList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_order_common_pb.Sorting.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_order_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 fast_deal_id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.getFastDealId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.setFastDealId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * repeated Sorting sorting = 2;
 * @return {!Array<!proto.fcp.order.v1.order.Sorting>}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.getSortingList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.Sorting>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_common_pb.Sorting, 2));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.Sorting>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.setSortingList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.order.v1.order.Sorting=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.Sorting}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.addSorting = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.order.v1.order.Sorting, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.clearSortingList = function() {
  return this.setSortingList([]);
};


/**
 * optional PaginationRequest pagination = 3;
 * @return {?proto.fcp.order.v1.order.PaginationRequest}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.order.v1.order.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.PaginationRequest, 3));
};


/**
 * @param {?proto.fcp.order.v1.order.PaginationRequest|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest} returns this
*/
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealFileRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.ListFastDealFileResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.ListFastDealFileResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    v1_order_file_pb.File.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_order_common_pb.PaginationResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileResponse}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.ListFastDealFileResponse;
  return proto.fcp.order.v1.order.ListFastDealFileResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.ListFastDealFileResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileResponse}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_file_pb.File;
      reader.readMessage(value,v1_order_file_pb.File.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    case 2:
      var value = new v1_order_common_pb.PaginationResponse;
      reader.readMessage(value,v1_order_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.ListFastDealFileResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.ListFastDealFileResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      v1_order_file_pb.File.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_order_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated File items = 1;
 * @return {!Array<!proto.fcp.order.v1.order.File>}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.order.v1.order.File>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_order_file_pb.File, 1));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.File>} value
 * @return {!proto.fcp.order.v1.order.ListFastDealFileResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.order.v1.order.File=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.File}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.order.v1.order.File, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};


/**
 * optional PaginationResponse pagination = 2;
 * @return {?proto.fcp.order.v1.order.PaginationResponse}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.order.v1.order.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.PaginationResponse, 2));
};


/**
 * @param {?proto.fcp.order.v1.order.PaginationResponse|undefined} value
 * @return {!proto.fcp.order.v1.order.ListFastDealFileResponse} returns this
*/
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.ListFastDealFileResponse} returns this
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.ListFastDealFileResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.AddFastDealFileRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.AddFastDealFileRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    fastDealId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    file: (f = msg.getFile()) && v1_order_file_pb.File.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.AddFastDealFileRequest}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.AddFastDealFileRequest;
  return proto.fcp.order.v1.order.AddFastDealFileRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.AddFastDealFileRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.AddFastDealFileRequest}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setFastDealId(value);
      break;
    case 2:
      var value = new v1_order_file_pb.File;
      reader.readMessage(value,v1_order_file_pb.File.deserializeBinaryFromReader);
      msg.setFile(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.AddFastDealFileRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.AddFastDealFileRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFastDealId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getFile();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_order_file_pb.File.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 fast_deal_id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.getFastDealId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order.AddFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.setFastDealId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional File file = 2;
 * @return {?proto.fcp.order.v1.order.File}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.getFile = function() {
  return /** @type{?proto.fcp.order.v1.order.File} */ (
    jspb.Message.getWrapperField(this, v1_order_file_pb.File, 2));
};


/**
 * @param {?proto.fcp.order.v1.order.File|undefined} value
 * @return {!proto.fcp.order.v1.order.AddFastDealFileRequest} returns this
*/
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.setFile = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.AddFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.clearFile = function() {
  return this.setFile(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.AddFastDealFileRequest.prototype.hasFile = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.AddFastDealFileResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.AddFastDealFileResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    file: (f = msg.getFile()) && v1_order_file_pb.File.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.AddFastDealFileResponse}
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.AddFastDealFileResponse;
  return proto.fcp.order.v1.order.AddFastDealFileResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.AddFastDealFileResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.AddFastDealFileResponse}
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_file_pb.File;
      reader.readMessage(value,v1_order_file_pb.File.deserializeBinaryFromReader);
      msg.setFile(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.AddFastDealFileResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.AddFastDealFileResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFile();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_order_file_pb.File.serializeBinaryToWriter
    );
  }
};


/**
 * optional File file = 1;
 * @return {?proto.fcp.order.v1.order.File}
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.prototype.getFile = function() {
  return /** @type{?proto.fcp.order.v1.order.File} */ (
    jspb.Message.getWrapperField(this, v1_order_file_pb.File, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.File|undefined} value
 * @return {!proto.fcp.order.v1.order.AddFastDealFileResponse} returns this
*/
proto.fcp.order.v1.order.AddFastDealFileResponse.prototype.setFile = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.AddFastDealFileResponse} returns this
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.prototype.clearFile = function() {
  return this.setFile(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.AddFastDealFileResponse.prototype.hasFile = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.DeleteFastDealFileRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.DeleteFastDealFileRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    file: (f = msg.getFile()) && v1_order_file_pb.File.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.DeleteFastDealFileRequest}
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.DeleteFastDealFileRequest;
  return proto.fcp.order.v1.order.DeleteFastDealFileRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.DeleteFastDealFileRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.DeleteFastDealFileRequest}
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_file_pb.File;
      reader.readMessage(value,v1_order_file_pb.File.deserializeBinaryFromReader);
      msg.setFile(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.DeleteFastDealFileRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.DeleteFastDealFileRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFile();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_order_file_pb.File.serializeBinaryToWriter
    );
  }
};


/**
 * optional File file = 1;
 * @return {?proto.fcp.order.v1.order.File}
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.prototype.getFile = function() {
  return /** @type{?proto.fcp.order.v1.order.File} */ (
    jspb.Message.getWrapperField(this, v1_order_file_pb.File, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.File|undefined} value
 * @return {!proto.fcp.order.v1.order.DeleteFastDealFileRequest} returns this
*/
proto.fcp.order.v1.order.DeleteFastDealFileRequest.prototype.setFile = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.DeleteFastDealFileRequest} returns this
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.prototype.clearFile = function() {
  return this.setFile(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.DeleteFastDealFileRequest.prototype.hasFile = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.DeleteFastDealFileResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.DeleteFastDealFileResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.DeleteFastDealFileResponse}
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.DeleteFastDealFileResponse;
  return proto.fcp.order.v1.order.DeleteFastDealFileResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.DeleteFastDealFileResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.DeleteFastDealFileResponse}
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.DeleteFastDealFileResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.DeleteFastDealFileResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.DeleteFastDealFileResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


goog.object.extend(exports, proto.fcp.order.v1.order);
