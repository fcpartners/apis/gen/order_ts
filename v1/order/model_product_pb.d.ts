// package: fcp.order.v1.order
// file: v1/order/model_product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";

export class Product extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getCategoryId(): number;
  setCategoryId(value: number): void;

  getCategoryName(): string;
  setCategoryName(value: string): void;

  getBrandId(): number;
  setBrandId(value: number): void;

  getBrandName(): string;
  setBrandName(value: string): void;

  getQuantityTypeId(): number;
  setQuantityTypeId(value: number): void;

  getQuantityTypeName(): string;
  setQuantityTypeName(value: string): void;

  getQuantity(): number;
  setQuantity(value: number): void;

  getProductGroupId(): number;
  setProductGroupId(value: number): void;

  getProductGroupName(): string;
  setProductGroupName(value: string): void;

  getProductSubgroupId(): number;
  setProductSubgroupId(value: number): void;

  getProductSubgroupName(): string;
  setProductSubgroupName(value: string): void;

  hasProductSeedsCondition(): boolean;
  clearProductSeedsCondition(): void;
  getProductSeedsCondition(): ProductSeedsCondition | undefined;
  setProductSeedsCondition(value?: ProductSeedsCondition): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Product.AsObject;
  static toObject(includeInstance: boolean, msg: Product): Product.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Product, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Product;
  static deserializeBinaryFromReader(message: Product, reader: jspb.BinaryReader): Product;
}

export namespace Product {
  export type AsObject = {
    id: number,
    name: string,
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    categoryId: number,
    categoryName: string,
    brandId: number,
    brandName: string,
    quantityTypeId: number,
    quantityTypeName: string,
    quantity: number,
    productGroupId: number,
    productGroupName: string,
    productSubgroupId: number,
    productSubgroupName: string,
    productSeedsCondition?: ProductSeedsCondition.AsObject,
  }
}

export class ProductSeedsCondition extends jspb.Message {
  getSortTypeId(): number;
  setSortTypeId(value: number): void;

  getSortTypeName(): string;
  setSortTypeName(value: string): void;

  getPlantTypeId(): number;
  setPlantTypeId(value: number): void;

  getPlantTypeName(): string;
  setPlantTypeName(value: string): void;

  getSpeciesId(): number;
  setSpeciesId(value: number): void;

  getSpeciesName(): string;
  setSpeciesName(value: string): void;

  getFruitFormId(): number;
  setFruitFormId(value: number): void;

  getFruitFormName(): string;
  setFruitFormName(value: string): void;

  getMaturityGroupId(): number;
  setMaturityGroupId(value: number): void;

  getMaturityGroupName(): string;
  setMaturityGroupName(value: string): void;

  getPollinationTypeId(): number;
  setPollinationTypeId(value: number): void;

  getPollinationTypeName(): string;
  setPollinationTypeName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductSeedsCondition.AsObject;
  static toObject(includeInstance: boolean, msg: ProductSeedsCondition): ProductSeedsCondition.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductSeedsCondition, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductSeedsCondition;
  static deserializeBinaryFromReader(message: ProductSeedsCondition, reader: jspb.BinaryReader): ProductSeedsCondition;
}

export namespace ProductSeedsCondition {
  export type AsObject = {
    sortTypeId: number,
    sortTypeName: string,
    plantTypeId: number,
    plantTypeName: string,
    speciesId: number,
    speciesName: string,
    fruitFormId: number,
    fruitFormName: string,
    maturityGroupId: number,
    maturityGroupName: string,
    pollinationTypeId: number,
    pollinationTypeName: string,
  }
}

export class ProductFile extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getUrl(): string;
  setUrl(value: string): void;

  getType(): ProductFileTypeMap[keyof ProductFileTypeMap];
  setType(value: ProductFileTypeMap[keyof ProductFileTypeMap]): void;

  getQuality(): ImageQualityMap[keyof ImageQualityMap];
  setQuality(value: ImageQualityMap[keyof ImageQualityMap]): void;

  getOrder(): number;
  setOrder(value: number): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFile.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFile): ProductFile.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFile;
  static deserializeBinaryFromReader(message: ProductFile, reader: jspb.BinaryReader): ProductFile;
}

export namespace ProductFile {
  export type AsObject = {
    id: number,
    name: string,
    url: string,
    type: ProductFileTypeMap[keyof ProductFileTypeMap],
    quality: ImageQualityMap[keyof ImageQualityMap],
    order: number,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class ProductFileFilter extends jspb.Message {
  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasQuality(): boolean;
  clearQuality(): void;
  getQuality(): ImageQualityValue | undefined;
  setQuality(value?: ImageQualityValue): void;

  getProductPackageId(): number;
  setProductPackageId(value: number): void;

  clearFileTypeList(): void;
  getFileTypeList(): Array<ProductFileTypeMap[keyof ProductFileTypeMap]>;
  setFileTypeList(value: Array<ProductFileTypeMap[keyof ProductFileTypeMap]>): void;
  addFileType(value: ProductFileTypeMap[keyof ProductFileTypeMap], index?: number): ProductFileTypeMap[keyof ProductFileTypeMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFileFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFileFilter): ProductFileFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFileFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFileFilter;
  static deserializeBinaryFromReader(message: ProductFileFilter, reader: jspb.BinaryReader): ProductFileFilter;
}

export namespace ProductFileFilter {
  export type AsObject = {
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    quality?: ImageQualityValue.AsObject,
    productPackageId: number,
    fileTypeList: Array<ProductFileTypeMap[keyof ProductFileTypeMap]>,
  }
}

export class ListProductFileRequest extends jspb.Message {
  clearFileTypeList(): void;
  getFileTypeList(): Array<ProductFileTypeMap[keyof ProductFileTypeMap]>;
  setFileTypeList(value: Array<ProductFileTypeMap[keyof ProductFileTypeMap]>): void;
  addFileType(value: ProductFileTypeMap[keyof ProductFileTypeMap], index?: number): ProductFileTypeMap[keyof ProductFileTypeMap];

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductFileRequest): ListProductFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductFileRequest;
  static deserializeBinaryFromReader(message: ListProductFileRequest, reader: jspb.BinaryReader): ListProductFileRequest;
}

export namespace ListProductFileRequest {
  export type AsObject = {
    fileTypeList: Array<ProductFileTypeMap[keyof ProductFileTypeMap]>,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
  }
}

export class ImageQualityValue extends jspb.Message {
  getValue(): ImageQualityMap[keyof ImageQualityMap];
  setValue(value: ImageQualityMap[keyof ImageQualityMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageQualityValue.AsObject;
  static toObject(includeInstance: boolean, msg: ImageQualityValue): ImageQualityValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ImageQualityValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageQualityValue;
  static deserializeBinaryFromReader(message: ImageQualityValue, reader: jspb.BinaryReader): ImageQualityValue;
}

export namespace ImageQualityValue {
  export type AsObject = {
    value: ImageQualityMap[keyof ImageQualityMap],
  }
}

export class ProductFilter extends jspb.Message {
  clearIdList(): void;
  getIdList(): Array<number>;
  setIdList(value: Array<number>): void;
  addId(value: number, index?: number): number;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearProcessList(): void;
  getProcessList(): Array<v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]>;
  setProcessList(value: Array<v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]>): void;
  addProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap], index?: number): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];

  clearTypeList(): void;
  getTypeList(): Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>;
  setTypeList(value: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>): void;
  addType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap], index?: number): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];

  clearCategoryIdList(): void;
  getCategoryIdList(): Array<number>;
  setCategoryIdList(value: Array<number>): void;
  addCategoryId(value: number, index?: number): number;

  hasCategoryName(): boolean;
  clearCategoryName(): void;
  getCategoryName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCategoryName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearBrandIdList(): void;
  getBrandIdList(): Array<number>;
  setBrandIdList(value: Array<number>): void;
  addBrandId(value: number, index?: number): number;

  hasBrandName(): boolean;
  clearBrandName(): void;
  getBrandName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setBrandName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearQuantityTypeIdList(): void;
  getQuantityTypeIdList(): Array<number>;
  setQuantityTypeIdList(value: Array<number>): void;
  addQuantityTypeId(value: number, index?: number): number;

  hasQuantityTypeName(): boolean;
  clearQuantityTypeName(): void;
  getQuantityTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setQuantityTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearProductGroupIdList(): void;
  getProductGroupIdList(): Array<number>;
  setProductGroupIdList(value: Array<number>): void;
  addProductGroupId(value: number, index?: number): number;

  hasProductGroupName(): boolean;
  clearProductGroupName(): void;
  getProductGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearProductSubgroupIdList(): void;
  getProductSubgroupIdList(): Array<number>;
  setProductSubgroupIdList(value: Array<number>): void;
  addProductSubgroupId(value: number, index?: number): number;

  hasProductSubgroupName(): boolean;
  clearProductSubgroupName(): void;
  getProductSubgroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductSubgroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearCategoryGroupIdList(): void;
  getCategoryGroupIdList(): Array<number>;
  setCategoryGroupIdList(value: Array<number>): void;
  addCategoryGroupId(value: number, index?: number): number;

  hasCategoryGroupName(): boolean;
  clearCategoryGroupName(): void;
  getCategoryGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCategoryGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearColorIdList(): void;
  getColorIdList(): Array<number>;
  setColorIdList(value: Array<number>): void;
  addColorId(value: number, index?: number): number;

  hasColorName(): boolean;
  clearColorName(): void;
  getColorName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setColorName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearPackageIdList(): void;
  getPackageIdList(): Array<number>;
  setPackageIdList(value: Array<number>): void;
  addPackageId(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFilter): ProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFilter;
  static deserializeBinaryFromReader(message: ProductFilter, reader: jspb.BinaryReader): ProductFilter;
}

export namespace ProductFilter {
  export type AsObject = {
    idList: Array<number>,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    processList: Array<v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]>,
    typeList: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>,
    categoryIdList: Array<number>,
    categoryName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    brandIdList: Array<number>,
    brandName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    quantityTypeIdList: Array<number>,
    quantityTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productGroupIdList: Array<number>,
    productGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productSubgroupIdList: Array<number>,
    productSubgroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    categoryGroupIdList: Array<number>,
    categoryGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    colorIdList: Array<number>,
    colorName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    packageIdList: Array<number>,
  }
}

export class ProductChemicalsFilter extends jspb.Message {
  clearToxicityClassIdList(): void;
  getToxicityClassIdList(): Array<number>;
  setToxicityClassIdList(value: Array<number>): void;
  addToxicityClassId(value: number, index?: number): number;

  clearPreparativeFormIdList(): void;
  getPreparativeFormIdList(): Array<number>;
  setPreparativeFormIdList(value: Array<number>): void;
  addPreparativeFormId(value: number, index?: number): number;

  clearProductActiveSubstanceIdList(): void;
  getProductActiveSubstanceIdList(): Array<number>;
  setProductActiveSubstanceIdList(value: Array<number>): void;
  addProductActiveSubstanceId(value: number, index?: number): number;

  clearChemicalClassGroupIdList(): void;
  getChemicalClassGroupIdList(): Array<number>;
  setChemicalClassGroupIdList(value: Array<number>): void;
  addChemicalClassGroupId(value: number, index?: number): number;

  clearDistributionTypeChemicalIdList(): void;
  getDistributionTypeChemicalIdList(): Array<number>;
  setDistributionTypeChemicalIdList(value: Array<number>): void;
  addDistributionTypeChemicalId(value: number, index?: number): number;

  clearApplicationMethodIdList(): void;
  getApplicationMethodIdList(): Array<number>;
  setApplicationMethodIdList(value: Array<number>): void;
  addApplicationMethodId(value: number, index?: number): number;

  clearSpectrumActionChemicalIdList(): void;
  getSpectrumActionChemicalIdList(): Array<number>;
  setSpectrumActionChemicalIdList(value: Array<number>): void;
  addSpectrumActionChemicalId(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductChemicalsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductChemicalsFilter): ProductChemicalsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductChemicalsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductChemicalsFilter;
  static deserializeBinaryFromReader(message: ProductChemicalsFilter, reader: jspb.BinaryReader): ProductChemicalsFilter;
}

export namespace ProductChemicalsFilter {
  export type AsObject = {
    toxicityClassIdList: Array<number>,
    preparativeFormIdList: Array<number>,
    productActiveSubstanceIdList: Array<number>,
    chemicalClassGroupIdList: Array<number>,
    distributionTypeChemicalIdList: Array<number>,
    applicationMethodIdList: Array<number>,
    spectrumActionChemicalIdList: Array<number>,
  }
}

export class ProductSeedsFilter extends jspb.Message {
  clearFlowerTypeList(): void;
  getFlowerTypeList(): Array<string>;
  setFlowerTypeList(value: Array<string>): void;
  addFlowerType(value: string, index?: number): string;

  hasIsOrganic(): boolean;
  clearIsOrganic(): void;
  getIsOrganic(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsOrganic(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasIsPollinator(): boolean;
  clearIsPollinator(): void;
  getIsPollinator(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsPollinator(value?: google_protobuf_wrappers_pb.BoolValue): void;

  clearHarvestYearList(): void;
  getHarvestYearList(): Array<number>;
  setHarvestYearList(value: Array<number>): void;
  addHarvestYear(value: number, index?: number): number;

  clearSortTypeIdList(): void;
  getSortTypeIdList(): Array<number>;
  setSortTypeIdList(value: Array<number>): void;
  addSortTypeId(value: number, index?: number): number;

  hasSortTypeName(): boolean;
  clearSortTypeName(): void;
  getSortTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSortTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearPlantTypeIdList(): void;
  getPlantTypeIdList(): Array<number>;
  setPlantTypeIdList(value: Array<number>): void;
  addPlantTypeId(value: number, index?: number): number;

  hasPlantTypeName(): boolean;
  clearPlantTypeName(): void;
  getPlantTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPlantTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearSpeciesIdList(): void;
  getSpeciesIdList(): Array<number>;
  setSpeciesIdList(value: Array<number>): void;
  addSpeciesId(value: number, index?: number): number;

  hasSpeciesName(): boolean;
  clearSpeciesName(): void;
  getSpeciesName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSpeciesName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearMaturityGroupIdList(): void;
  getMaturityGroupIdList(): Array<number>;
  setMaturityGroupIdList(value: Array<number>): void;
  addMaturityGroupId(value: number, index?: number): number;

  hasMaturityGroupName(): boolean;
  clearMaturityGroupName(): void;
  getMaturityGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setMaturityGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearFruitFormIdList(): void;
  getFruitFormIdList(): Array<number>;
  setFruitFormIdList(value: Array<number>): void;
  addFruitFormId(value: number, index?: number): number;

  hasFruitFormName(): boolean;
  clearFruitFormName(): void;
  getFruitFormName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFruitFormName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearPollinationTypeIdList(): void;
  getPollinationTypeIdList(): Array<number>;
  setPollinationTypeIdList(value: Array<number>): void;
  addPollinationTypeId(value: number, index?: number): number;

  hasPollinationTypeName(): boolean;
  clearPollinationTypeName(): void;
  getPollinationTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPollinationTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearFruitAverageWeightIdList(): void;
  getFruitAverageWeightIdList(): Array<number>;
  setFruitAverageWeightIdList(value: Array<number>): void;
  addFruitAverageWeightId(value: number, index?: number): number;

  hasFruitAverageWeightName(): boolean;
  clearFruitAverageWeightName(): void;
  getFruitAverageWeightName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFruitAverageWeightName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearOriginCountryIdList(): void;
  getOriginCountryIdList(): Array<number>;
  setOriginCountryIdList(value: Array<number>): void;
  addOriginCountryId(value: number, index?: number): number;

  hasOriginCountryName(): boolean;
  clearOriginCountryName(): void;
  getOriginCountryName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOriginCountryName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearTreatmentChemicalsIdList(): void;
  getTreatmentChemicalsIdList(): Array<number>;
  setTreatmentChemicalsIdList(value: Array<number>): void;
  addTreatmentChemicalsId(value: number, index?: number): number;

  hasTreatmentChemicalsName(): boolean;
  clearTreatmentChemicalsName(): void;
  getTreatmentChemicalsName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setTreatmentChemicalsName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearTechnologyTypeIdList(): void;
  getTechnologyTypeIdList(): Array<number>;
  setTechnologyTypeIdList(value: Array<number>): void;
  addTechnologyTypeId(value: number, index?: number): number;

  hasTechnologyTypeName(): boolean;
  clearTechnologyTypeName(): void;
  getTechnologyTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setTechnologyTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearGrowTypeIdList(): void;
  getGrowTypeIdList(): Array<number>;
  setGrowTypeIdList(value: Array<number>): void;
  addGrowTypeId(value: number, index?: number): number;

  hasGrowTypeName(): boolean;
  clearGrowTypeName(): void;
  getGrowTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGrowTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearGrowSeasonIdList(): void;
  getGrowSeasonIdList(): Array<number>;
  setGrowSeasonIdList(value: Array<number>): void;
  addGrowSeasonId(value: number, index?: number): number;

  hasGrowSeasonName(): boolean;
  clearGrowSeasonName(): void;
  getGrowSeasonName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGrowSeasonName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearPurposeIdList(): void;
  getPurposeIdList(): Array<number>;
  setPurposeIdList(value: Array<number>): void;
  addPurposeId(value: number, index?: number): number;

  hasPurposeName(): boolean;
  clearPurposeName(): void;
  getPurposeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPurposeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearReproductionIdList(): void;
  getReproductionIdList(): Array<number>;
  setReproductionIdList(value: Array<number>): void;
  addReproductionId(value: number, index?: number): number;

  hasReproductionName(): boolean;
  clearReproductionName(): void;
  getReproductionName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setReproductionName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearFruitTechColorNameList(): void;
  getFruitTechColorNameList(): Array<string>;
  setFruitTechColorNameList(value: Array<string>): void;
  addFruitTechColorName(value: string, index?: number): string;

  clearFruitBioColorNameList(): void;
  getFruitBioColorNameList(): Array<string>;
  setFruitBioColorNameList(value: Array<string>): void;
  addFruitBioColorName(value: string, index?: number): string;

  clearFruitPulpColorNameList(): void;
  getFruitPulpColorNameList(): Array<string>;
  setFruitPulpColorNameList(value: Array<string>): void;
  addFruitPulpColorName(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductSeedsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductSeedsFilter): ProductSeedsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductSeedsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductSeedsFilter;
  static deserializeBinaryFromReader(message: ProductSeedsFilter, reader: jspb.BinaryReader): ProductSeedsFilter;
}

export namespace ProductSeedsFilter {
  export type AsObject = {
    flowerTypeList: Array<string>,
    isOrganic?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    isPollinator?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    harvestYearList: Array<number>,
    sortTypeIdList: Array<number>,
    sortTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    plantTypeIdList: Array<number>,
    plantTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    speciesIdList: Array<number>,
    speciesName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    maturityGroupIdList: Array<number>,
    maturityGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fruitFormIdList: Array<number>,
    fruitFormName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    pollinationTypeIdList: Array<number>,
    pollinationTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fruitAverageWeightIdList: Array<number>,
    fruitAverageWeightName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    originCountryIdList: Array<number>,
    originCountryName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    treatmentChemicalsIdList: Array<number>,
    treatmentChemicalsName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    technologyTypeIdList: Array<number>,
    technologyTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    growTypeIdList: Array<number>,
    growTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    growSeasonIdList: Array<number>,
    growSeasonName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    purposeIdList: Array<number>,
    purposeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    reproductionIdList: Array<number>,
    reproductionName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fruitTechColorNameList: Array<string>,
    fruitBioColorNameList: Array<string>,
    fruitPulpColorNameList: Array<string>,
  }
}

export class ResourcesSeeds extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesSeeds.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesSeeds): ResourcesSeeds.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesSeeds, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesSeeds;
  static deserializeBinaryFromReader(message: ResourcesSeeds, reader: jspb.BinaryReader): ResourcesSeeds;
}

export namespace ResourcesSeeds {
  export type AsObject = {
  }
}

export class ResourcesFertilize extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesFertilize.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesFertilize): ResourcesFertilize.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesFertilize, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesFertilize;
  static deserializeBinaryFromReader(message: ResourcesFertilize, reader: jspb.BinaryReader): ResourcesFertilize;
}

export namespace ResourcesFertilize {
  export type AsObject = {
  }
}

export class ResourcesMicroFertilizers extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesMicroFertilizers.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesMicroFertilizers): ResourcesMicroFertilizers.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesMicroFertilizers, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesMicroFertilizers;
  static deserializeBinaryFromReader(message: ResourcesMicroFertilizers, reader: jspb.BinaryReader): ResourcesMicroFertilizers;
}

export namespace ResourcesMicroFertilizers {
  export type AsObject = {
  }
}

export class ResourcesChemistry extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesChemistry.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesChemistry): ResourcesChemistry.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesChemistry, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesChemistry;
  static deserializeBinaryFromReader(message: ResourcesChemistry, reader: jspb.BinaryReader): ResourcesChemistry;
}

export namespace ResourcesChemistry {
  export type AsObject = {
  }
}

export interface ImageQualityMap {
  IQ_UNSPECIFIED: 0;
  IQ_DETAILED: 1;
  IQ_COMPRESSED: 2;
}

export const ImageQuality: ImageQualityMap;

export interface ProductFileTypeMap {
  PFT_UNSPECIFIED: 0;
  PFT_IMAGE: 1;
  PFT_VIDEO: 2;
  PFT_DOCUMENT: 3;
}

export const ProductFileType: ProductFileTypeMap;

