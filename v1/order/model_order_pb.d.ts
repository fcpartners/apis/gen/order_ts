// package: fcp.order.v1.order
// file: v1/order/model_order.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";

export class OrderCondition extends jspb.Message {
  getDeliveryCondition(): v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap];
  setDeliveryCondition(value: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]): void;

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDeliveryDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getDeliveryPlace(): string;
  setDeliveryPlace(value: string): void;

  getPaymentCondition(): v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap];
  setPaymentCondition(value: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]): void;

  hasPaymentDate(): boolean;
  clearPaymentDate(): void;
  getPaymentDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setPaymentDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasExpiredDate(): boolean;
  clearExpiredDate(): void;
  getExpiredDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setExpiredDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getAlternatives(): boolean;
  setAlternatives(value: boolean): void;

  getConsulting(): boolean;
  setConsulting(value: boolean): void;

  getConditionAlternatives(): boolean;
  setConditionAlternatives(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrderCondition.AsObject;
  static toObject(includeInstance: boolean, msg: OrderCondition): OrderCondition.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OrderCondition, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrderCondition;
  static deserializeBinaryFromReader(message: OrderCondition, reader: jspb.BinaryReader): OrderCondition;
}

export namespace OrderCondition {
  export type AsObject = {
    deliveryCondition: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap],
    deliveryDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    deliveryPlace: string,
    paymentCondition: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap],
    paymentDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    expiredDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    alternatives: boolean,
    consulting: boolean,
    conditionAlternatives: boolean,
  }
}

export class OrderConditionFilter extends jspb.Message {
  clearDeliveryConditionList(): void;
  getDeliveryConditionList(): Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>;
  setDeliveryConditionList(value: Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>): void;
  addDeliveryCondition(value: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap], index?: number): v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap];

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): v1_order_common_pb.TimeRange | undefined;
  setDeliveryDate(value?: v1_order_common_pb.TimeRange): void;

  clearPaymentConditionList(): void;
  getPaymentConditionList(): Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>;
  setPaymentConditionList(value: Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>): void;
  addPaymentCondition(value: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap], index?: number): v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap];

  hasPaymentDate(): boolean;
  clearPaymentDate(): void;
  getPaymentDate(): v1_order_common_pb.TimeRange | undefined;
  setPaymentDate(value?: v1_order_common_pb.TimeRange): void;

  hasExpiredDate(): boolean;
  clearExpiredDate(): void;
  getExpiredDate(): v1_order_common_pb.TimeRange | undefined;
  setExpiredDate(value?: v1_order_common_pb.TimeRange): void;

  hasAlternatives(): boolean;
  clearAlternatives(): void;
  getAlternatives(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setAlternatives(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasConsulting(): boolean;
  clearConsulting(): void;
  getConsulting(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setConsulting(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasConditionAlternatives(): boolean;
  clearConditionAlternatives(): void;
  getConditionAlternatives(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setConditionAlternatives(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrderConditionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: OrderConditionFilter): OrderConditionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OrderConditionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrderConditionFilter;
  static deserializeBinaryFromReader(message: OrderConditionFilter, reader: jspb.BinaryReader): OrderConditionFilter;
}

export namespace OrderConditionFilter {
  export type AsObject = {
    deliveryConditionList: Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>,
    deliveryDate?: v1_order_common_pb.TimeRange.AsObject,
    paymentConditionList: Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>,
    paymentDate?: v1_order_common_pb.TimeRange.AsObject,
    expiredDate?: v1_order_common_pb.TimeRange.AsObject,
    alternatives?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    consulting?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    conditionAlternatives?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class OrderResult extends jspb.Message {
  getOfferId(): number;
  setOfferId(value: number): void;

  getDealId(): number;
  setDealId(value: number): void;

  hasOffer(): boolean;
  clearOffer(): void;
  getOffer(): v1_order_model_base_pb.OperationIdent | undefined;
  setOffer(value?: v1_order_model_base_pb.OperationIdent): void;

  hasDeal(): boolean;
  clearDeal(): void;
  getDeal(): v1_order_model_base_pb.OperationIdent | undefined;
  setDeal(value?: v1_order_model_base_pb.OperationIdent): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrderResult.AsObject;
  static toObject(includeInstance: boolean, msg: OrderResult): OrderResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OrderResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrderResult;
  static deserializeBinaryFromReader(message: OrderResult, reader: jspb.BinaryReader): OrderResult;
}

export namespace OrderResult {
  export type AsObject = {
    offerId: number,
    dealId: number,
    offer?: v1_order_model_base_pb.OperationIdent.AsObject,
    deal?: v1_order_model_base_pb.OperationIdent.AsObject,
  }
}

export class Order extends jspb.Message {
  getMultiOrderId(): number;
  setMultiOrderId(value: number): void;

  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getStatus(): Order.StatusMap[keyof Order.StatusMap];
  setStatus(value: Order.StatusMap[keyof Order.StatusMap]): void;

  hasProduct(): boolean;
  clearProduct(): void;
  getProduct(): v1_order_model_product_pb.Product | undefined;
  setProduct(value?: v1_order_model_product_pb.Product): void;

  hasCondition(): boolean;
  clearCondition(): void;
  getCondition(): OrderCondition | undefined;
  setCondition(value?: OrderCondition): void;

  hasResult(): boolean;
  clearResult(): void;
  getResult(): OrderResult | undefined;
  setResult(value?: OrderResult): void;

  getDescription(): string;
  setDescription(value: string): void;

  getOffersNum(): number;
  setOffersNum(value: number): void;

  getDealExist(): boolean;
  setDealExist(value: boolean): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getPrice(): number;
  setPrice(value: number): void;

  getVatApplied(): boolean;
  setVatApplied(value: boolean): void;

  getDeliveryPriceApplied(): boolean;
  setDeliveryPriceApplied(value: boolean): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  getOrderstage(): string;
  setOrderstage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Order.AsObject;
  static toObject(includeInstance: boolean, msg: Order): Order.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Order, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Order;
  static deserializeBinaryFromReader(message: Order, reader: jspb.BinaryReader): Order;
}

export namespace Order {
  export type AsObject = {
    multiOrderId: number,
    id: number,
    name: string,
    status: Order.StatusMap[keyof Order.StatusMap],
    product?: v1_order_model_product_pb.Product.AsObject,
    condition?: OrderCondition.AsObject,
    result?: OrderResult.AsObject,
    description: string,
    offersNum: number,
    dealExist: boolean,
    currency: string,
    price: number,
    vatApplied: boolean,
    deliveryPriceApplied: boolean,
    externalId: string,
    audit?: v1_order_common_pb.Audit.AsObject,
    orderstage: string,
  }

  export class StatusValue extends jspb.Message {
    getValue(): Order.StatusMap[keyof Order.StatusMap];
    setValue(value: Order.StatusMap[keyof Order.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: Order.StatusMap[keyof Order.StatusMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
  }

  export const Status: StatusMap;
}

export class OrderFilter extends jspb.Message {
  hasMultiOrderId(): boolean;
  clearMultiOrderId(): void;
  getMultiOrderId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMultiOrderId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): Order.StatusValue | undefined;
  setStatus(value?: Order.StatusValue): void;

  hasProductFilter(): boolean;
  clearProductFilter(): void;
  getProductFilter(): v1_order_model_product_pb.ProductFilter | undefined;
  setProductFilter(value?: v1_order_model_product_pb.ProductFilter): void;

  hasConditionFilter(): boolean;
  clearConditionFilter(): void;
  getConditionFilter(): OrderConditionFilter | undefined;
  setConditionFilter(value?: OrderConditionFilter): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasHasOffers(): boolean;
  clearHasOffers(): void;
  getHasOffers(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setHasOffers(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasVatApplied(): boolean;
  clearVatApplied(): void;
  getVatApplied(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setVatApplied(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasDeliveryPriceApplied(): boolean;
  clearDeliveryPriceApplied(): void;
  getDeliveryPriceApplied(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeliveryPriceApplied(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrderFilter.AsObject;
  static toObject(includeInstance: boolean, msg: OrderFilter): OrderFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OrderFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrderFilter;
  static deserializeBinaryFromReader(message: OrderFilter, reader: jspb.BinaryReader): OrderFilter;
}

export namespace OrderFilter {
  export type AsObject = {
    multiOrderId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    status?: Order.StatusValue.AsObject,
    productFilter?: v1_order_model_product_pb.ProductFilter.AsObject,
    conditionFilter?: OrderConditionFilter.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    hasOffers?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    vatApplied?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    deliveryPriceApplied?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    externalId?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListOrderRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderRequest): ListOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderRequest;
  static deserializeBinaryFromReader(message: ListOrderRequest, reader: jspb.BinaryReader): ListOrderRequest;
}

export namespace ListOrderRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListOrderResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Order>;
  setItemsList(value: Array<Order>): void;
  addItems(value?: Order, index?: number): Order;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderResponse): ListOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderResponse;
  static deserializeBinaryFromReader(message: ListOrderResponse, reader: jspb.BinaryReader): ListOrderResponse;
}

export namespace ListOrderResponse {
  export type AsObject = {
    itemsList: Array<Order.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetOrderRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetOrderRequest): GetOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOrderRequest;
  static deserializeBinaryFromReader(message: GetOrderRequest, reader: jspb.BinaryReader): GetOrderRequest;
}

export namespace GetOrderRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetOrderResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Order | undefined;
  setItem(value?: Order): void;

  hasResourcesSeeds(): boolean;
  clearResourcesSeeds(): void;
  getResourcesSeeds(): v1_order_model_product_pb.ResourcesSeeds | undefined;
  setResourcesSeeds(value?: v1_order_model_product_pb.ResourcesSeeds): void;

  hasResourcesFertilize(): boolean;
  clearResourcesFertilize(): void;
  getResourcesFertilize(): v1_order_model_product_pb.ResourcesFertilize | undefined;
  setResourcesFertilize(value?: v1_order_model_product_pb.ResourcesFertilize): void;

  hasResourcesMicrofertilizers(): boolean;
  clearResourcesMicrofertilizers(): void;
  getResourcesMicrofertilizers(): v1_order_model_product_pb.ResourcesFertilize | undefined;
  setResourcesMicrofertilizers(value?: v1_order_model_product_pb.ResourcesFertilize): void;

  hasResourcesChemistry(): boolean;
  clearResourcesChemistry(): void;
  getResourcesChemistry(): v1_order_model_product_pb.ResourcesChemistry | undefined;
  setResourcesChemistry(value?: v1_order_model_product_pb.ResourcesChemistry): void;

  getDetailCase(): GetOrderResponse.DetailCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetOrderResponse): GetOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOrderResponse;
  static deserializeBinaryFromReader(message: GetOrderResponse, reader: jspb.BinaryReader): GetOrderResponse;
}

export namespace GetOrderResponse {
  export type AsObject = {
    item?: Order.AsObject,
    resourcesSeeds?: v1_order_model_product_pb.ResourcesSeeds.AsObject,
    resourcesFertilize?: v1_order_model_product_pb.ResourcesFertilize.AsObject,
    resourcesMicrofertilizers?: v1_order_model_product_pb.ResourcesFertilize.AsObject,
    resourcesChemistry?: v1_order_model_product_pb.ResourcesChemistry.AsObject,
  }

  export enum DetailCase {
    DETAIL_NOT_SET = 0,
    RESOURCES_SEEDS = 100,
    RESOURCES_FERTILIZE = 101,
    RESOURCES_MICROFERTILIZERS = 102,
    RESOURCES_CHEMISTRY = 103,
  }
}

export class ListOrderProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderProcessRequest): ListOrderProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderProcessRequest;
  static deserializeBinaryFromReader(message: ListOrderProcessRequest, reader: jspb.BinaryReader): ListOrderProcessRequest;
}

export namespace ListOrderProcessRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderProcessTypeRequest): ListOrderProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListOrderProcessTypeRequest, reader: jspb.BinaryReader): ListOrderProcessTypeRequest;
}

export namespace ListOrderProcessTypeRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderDeliveryConditionRequest): ListOrderDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListOrderDeliveryConditionRequest, reader: jspb.BinaryReader): ListOrderDeliveryConditionRequest;
}

export namespace ListOrderDeliveryConditionRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderPaymentConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderPaymentConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderPaymentConditionRequest): ListOrderPaymentConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderPaymentConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderPaymentConditionRequest;
  static deserializeBinaryFromReader(message: ListOrderPaymentConditionRequest, reader: jspb.BinaryReader): ListOrderPaymentConditionRequest;
}

export namespace ListOrderPaymentConditionRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderCategoryRequest): ListOrderCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderCategoryRequest;
  static deserializeBinaryFromReader(message: ListOrderCategoryRequest, reader: jspb.BinaryReader): ListOrderCategoryRequest;
}

export namespace ListOrderCategoryRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderBrandRequest): ListOrderBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderBrandRequest;
  static deserializeBinaryFromReader(message: ListOrderBrandRequest, reader: jspb.BinaryReader): ListOrderBrandRequest;
}

export namespace ListOrderBrandRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderProductRequest): ListOrderProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderProductRequest;
  static deserializeBinaryFromReader(message: ListOrderProductRequest, reader: jspb.BinaryReader): ListOrderProductRequest;
}

export namespace ListOrderProductRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OrderFilter | undefined;
  setFilter(value?: OrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderQuantityTypeRequest): ListOrderQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListOrderQuantityTypeRequest, reader: jspb.BinaryReader): ListOrderQuantityTypeRequest;
}

export namespace ListOrderQuantityTypeRequest {
  export type AsObject = {
    filter?: OrderFilter.AsObject,
  }
}

export class ListOrderNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPaymentConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderNamesRequest): ListOrderNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderNamesRequest;
  static deserializeBinaryFromReader(message: ListOrderNamesRequest, reader: jspb.BinaryReader): ListOrderNamesRequest;
}

export namespace ListOrderNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListOrderNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPaymentConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOrderNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOrderNamesResponse): ListOrderNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOrderNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOrderNamesResponse;
  static deserializeBinaryFromReader(message: ListOrderNamesResponse, reader: jspb.BinaryReader): ListOrderNamesResponse;
}

export namespace ListOrderNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

