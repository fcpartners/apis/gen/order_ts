// package: fcp.order.v1.order
// file: v1/order/model_multi_order.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";

export class MultiOrderCondition extends jspb.Message {
  getDeliveryCondition(): v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap];
  setDeliveryCondition(value: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]): void;

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDeliveryDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getDeliveryPlace(): string;
  setDeliveryPlace(value: string): void;

  getPaymentCondition(): v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap];
  setPaymentCondition(value: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]): void;

  hasPaymentDate(): boolean;
  clearPaymentDate(): void;
  getPaymentDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setPaymentDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasExpiredDate(): boolean;
  clearExpiredDate(): void;
  getExpiredDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setExpiredDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getConditionAlternatives(): boolean;
  setConditionAlternatives(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiOrderCondition.AsObject;
  static toObject(includeInstance: boolean, msg: MultiOrderCondition): MultiOrderCondition.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiOrderCondition, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiOrderCondition;
  static deserializeBinaryFromReader(message: MultiOrderCondition, reader: jspb.BinaryReader): MultiOrderCondition;
}

export namespace MultiOrderCondition {
  export type AsObject = {
    deliveryCondition: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap],
    deliveryDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    deliveryPlace: string,
    paymentCondition: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap],
    paymentDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    expiredDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    conditionAlternatives: boolean,
  }
}

export class MultiOrderConditionFilter extends jspb.Message {
  clearDeliveryConditionList(): void;
  getDeliveryConditionList(): Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>;
  setDeliveryConditionList(value: Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>): void;
  addDeliveryCondition(value: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap], index?: number): v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap];

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): v1_order_common_pb.TimeRange | undefined;
  setDeliveryDate(value?: v1_order_common_pb.TimeRange): void;

  clearPaymentConditionList(): void;
  getPaymentConditionList(): Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>;
  setPaymentConditionList(value: Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>): void;
  addPaymentCondition(value: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap], index?: number): v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap];

  hasPaymentDate(): boolean;
  clearPaymentDate(): void;
  getPaymentDate(): v1_order_common_pb.TimeRange | undefined;
  setPaymentDate(value?: v1_order_common_pb.TimeRange): void;

  hasExpiredDate(): boolean;
  clearExpiredDate(): void;
  getExpiredDate(): v1_order_common_pb.TimeRange | undefined;
  setExpiredDate(value?: v1_order_common_pb.TimeRange): void;

  hasConditionAlternatives(): boolean;
  clearConditionAlternatives(): void;
  getConditionAlternatives(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setConditionAlternatives(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiOrderConditionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: MultiOrderConditionFilter): MultiOrderConditionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiOrderConditionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiOrderConditionFilter;
  static deserializeBinaryFromReader(message: MultiOrderConditionFilter, reader: jspb.BinaryReader): MultiOrderConditionFilter;
}

export namespace MultiOrderConditionFilter {
  export type AsObject = {
    deliveryConditionList: Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>,
    deliveryDate?: v1_order_common_pb.TimeRange.AsObject,
    paymentConditionList: Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>,
    paymentDate?: v1_order_common_pb.TimeRange.AsObject,
    expiredDate?: v1_order_common_pb.TimeRange.AsObject,
    conditionAlternatives?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class MultiOrder extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getStatus(): MultiOrder.StatusMap[keyof MultiOrder.StatusMap];
  setStatus(value: MultiOrder.StatusMap[keyof MultiOrder.StatusMap]): void;

  hasCondition(): boolean;
  clearCondition(): void;
  getCondition(): MultiOrderCondition | undefined;
  setCondition(value?: MultiOrderCondition): void;

  getDescription(): string;
  setDescription(value: string): void;

  getCategoryCount(): number;
  setCategoryCount(value: number): void;

  getProductCount(): number;
  setProductCount(value: number): void;

  clearOrdersList(): void;
  getOrdersList(): Array<v1_order_model_order_pb.Order>;
  setOrdersList(value: Array<v1_order_model_order_pb.Order>): void;
  addOrders(value?: v1_order_model_order_pb.Order, index?: number): v1_order_model_order_pb.Order;

  clearOrderIdsList(): void;
  getOrderIdsList(): Array<number>;
  setOrderIdsList(value: Array<number>): void;
  addOrderIds(value: number, index?: number): number;

  getStatusForSupplier(): v1_order_enum_pb.StatusForSupplierMap[keyof v1_order_enum_pb.StatusForSupplierMap];
  setStatusForSupplier(value: v1_order_enum_pb.StatusForSupplierMap[keyof v1_order_enum_pb.StatusForSupplierMap]): void;

  getOffersNum(): number;
  setOffersNum(value: number): void;

  getActiveOffersNum(): number;
  setActiveOffersNum(value: number): void;

  getDealsNum(): number;
  setDealsNum(value: number): void;

  getActiveDealsNum(): number;
  setActiveDealsNum(value: number): void;

  getMultiDealsNum(): number;
  setMultiDealsNum(value: number): void;

  getAlternatives(): boolean;
  setAlternatives(value: boolean): void;

  getFarmerEdrpou(): string;
  setFarmerEdrpou(value: string): void;

  getSupplierId(): string;
  setSupplierId(value: string): void;

  getIsAuction(): boolean;
  setIsAuction(value: boolean): void;

  getChatIdent(): string;
  setChatIdent(value: string): void;

  getClientName(): string;
  setClientName(value: string): void;

  getClientId(): string;
  setClientId(value: string): void;

  getMerchandiserName(): string;
  setMerchandiserName(value: string): void;

  getMultiOrderType(): MultiOrder.MultiOrderTypeMap[keyof MultiOrder.MultiOrderTypeMap];
  setMultiOrderType(value: MultiOrder.MultiOrderTypeMap[keyof MultiOrder.MultiOrderTypeMap]): void;

  clearCounterpartyIdsList(): void;
  getCounterpartyIdsList(): Array<string>;
  setCounterpartyIdsList(value: Array<string>): void;
  addCounterpartyIds(value: string, index?: number): string;

  getCreatorName(): string;
  setCreatorName(value: string): void;

  getCreatorEdrpou(): string;
  setCreatorEdrpou(value: string): void;

  clearPhonesList(): void;
  getPhonesList(): Array<string>;
  setPhonesList(value: Array<string>): void;
  addPhones(value: string, index?: number): string;

  hasCustomer(): boolean;
  clearCustomer(): void;
  getCustomer(): v1_order_model_base_pb.UserInfo | undefined;
  setCustomer(value?: v1_order_model_base_pb.UserInfo): void;

  hasSupplier(): boolean;
  clearSupplier(): void;
  getSupplier(): v1_order_model_base_pb.UserInfo | undefined;
  setSupplier(value?: v1_order_model_base_pb.UserInfo): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiOrder.AsObject;
  static toObject(includeInstance: boolean, msg: MultiOrder): MultiOrder.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiOrder, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiOrder;
  static deserializeBinaryFromReader(message: MultiOrder, reader: jspb.BinaryReader): MultiOrder;
}

export namespace MultiOrder {
  export type AsObject = {
    id: number,
    name: string,
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    status: MultiOrder.StatusMap[keyof MultiOrder.StatusMap],
    condition?: MultiOrderCondition.AsObject,
    description: string,
    categoryCount: number,
    productCount: number,
    ordersList: Array<v1_order_model_order_pb.Order.AsObject>,
    orderIdsList: Array<number>,
    statusForSupplier: v1_order_enum_pb.StatusForSupplierMap[keyof v1_order_enum_pb.StatusForSupplierMap],
    offersNum: number,
    activeOffersNum: number,
    dealsNum: number,
    activeDealsNum: number,
    multiDealsNum: number,
    alternatives: boolean,
    farmerEdrpou: string,
    supplierId: string,
    isAuction: boolean,
    chatIdent: string,
    clientName: string,
    clientId: string,
    merchandiserName: string,
    multiOrderType: MultiOrder.MultiOrderTypeMap[keyof MultiOrder.MultiOrderTypeMap],
    counterpartyIdsList: Array<string>,
    creatorName: string,
    creatorEdrpou: string,
    phonesList: Array<string>,
    customer?: v1_order_model_base_pb.UserInfo.AsObject,
    supplier?: v1_order_model_base_pb.UserInfo.AsObject,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class StatusValue extends jspb.Message {
    getValue(): MultiOrder.StatusMap[keyof MultiOrder.StatusMap];
    setValue(value: MultiOrder.StatusMap[keyof MultiOrder.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: MultiOrder.StatusMap[keyof MultiOrder.StatusMap],
    }
  }

  export class MultiOrderTypeValue extends jspb.Message {
    hasValue(): boolean;
    clearValue(): void;
    getValue(): MultiOrder | undefined;
    setValue(value?: MultiOrder): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): MultiOrderTypeValue.AsObject;
    static toObject(includeInstance: boolean, msg: MultiOrderTypeValue): MultiOrderTypeValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: MultiOrderTypeValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): MultiOrderTypeValue;
    static deserializeBinaryFromReader(message: MultiOrderTypeValue, reader: jspb.BinaryReader): MultiOrderTypeValue;
  }

  export namespace MultiOrderTypeValue {
    export type AsObject = {
      value?: MultiOrder.AsObject,
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
  }

  export const Status: StatusMap;

  export interface MultiOrderTypeMap {
    ANONYMIZED: 0;
    PUBLIC: 1;
    PERSONAL: 2;
  }

  export const MultiOrderType: MultiOrderTypeMap;
}

export class MultiOrderFilter extends jspb.Message {
  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): MultiOrder.StatusValue | undefined;
  setStatus(value?: MultiOrder.StatusValue): void;

  clearStatusesList(): void;
  getStatusesList(): Array<MultiOrder.StatusValue>;
  setStatusesList(value: Array<MultiOrder.StatusValue>): void;
  addStatuses(value?: MultiOrder.StatusValue, index?: number): MultiOrder.StatusValue;

  hasProductFilter(): boolean;
  clearProductFilter(): void;
  getProductFilter(): v1_order_model_product_pb.ProductFilter | undefined;
  setProductFilter(value?: v1_order_model_product_pb.ProductFilter): void;

  hasConditionFilter(): boolean;
  clearConditionFilter(): void;
  getConditionFilter(): MultiOrderConditionFilter | undefined;
  setConditionFilter(value?: MultiOrderConditionFilter): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasStatusForSupplier(): boolean;
  clearStatusForSupplier(): void;
  getStatusForSupplier(): v1_order_enum_pb.StatusForSupplierValue | undefined;
  setStatusForSupplier(value?: v1_order_enum_pb.StatusForSupplierValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasAlternatives(): boolean;
  clearAlternatives(): void;
  getAlternatives(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setAlternatives(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasFarmerEdrpou(): boolean;
  clearFarmerEdrpou(): void;
  getFarmerEdrpou(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFarmerEdrpou(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasSupplierId(): boolean;
  clearSupplierId(): void;
  getSupplierId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSupplierId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasHasSupplier(): boolean;
  clearHasSupplier(): void;
  getHasSupplier(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setHasSupplier(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasHasOffers(): boolean;
  clearHasOffers(): void;
  getHasOffers(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setHasOffers(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasIsAuction(): boolean;
  clearIsAuction(): void;
  getIsAuction(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsAuction(value?: google_protobuf_wrappers_pb.BoolValue): void;

  getIsLinkedToMe(): boolean;
  setIsLinkedToMe(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MultiOrderFilter.AsObject;
  static toObject(includeInstance: boolean, msg: MultiOrderFilter): MultiOrderFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MultiOrderFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MultiOrderFilter;
  static deserializeBinaryFromReader(message: MultiOrderFilter, reader: jspb.BinaryReader): MultiOrderFilter;
}

export namespace MultiOrderFilter {
  export type AsObject = {
    status?: MultiOrder.StatusValue.AsObject,
    statusesList: Array<MultiOrder.StatusValue.AsObject>,
    productFilter?: v1_order_model_product_pb.ProductFilter.AsObject,
    conditionFilter?: MultiOrderConditionFilter.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    statusForSupplier?: v1_order_enum_pb.StatusForSupplierValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    alternatives?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    farmerEdrpou?: google_protobuf_wrappers_pb.StringValue.AsObject,
    supplierId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    hasSupplier?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    hasOffers?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    isAuction?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    isLinkedToMe: boolean,
  }
}

export class ListMultiOrderRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderRequest): ListMultiOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderRequest, reader: jspb.BinaryReader): ListMultiOrderRequest;
}

export namespace ListMultiOrderRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListMultiOrderResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<MultiOrder>;
  setItemsList(value: Array<MultiOrder>): void;
  addItems(value?: MultiOrder, index?: number): MultiOrder;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderResponse): ListMultiOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderResponse;
  static deserializeBinaryFromReader(message: ListMultiOrderResponse, reader: jspb.BinaryReader): ListMultiOrderResponse;
}

export namespace ListMultiOrderResponse {
  export type AsObject = {
    itemsList: Array<MultiOrder.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetMultiOrderRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiOrderRequest): GetMultiOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiOrderRequest;
  static deserializeBinaryFromReader(message: GetMultiOrderRequest, reader: jspb.BinaryReader): GetMultiOrderRequest;
}

export namespace GetMultiOrderRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetMultiOrderResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): MultiOrder | undefined;
  setItem(value?: MultiOrder): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMultiOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMultiOrderResponse): GetMultiOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMultiOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMultiOrderResponse;
  static deserializeBinaryFromReader(message: GetMultiOrderResponse, reader: jspb.BinaryReader): GetMultiOrderResponse;
}

export namespace GetMultiOrderResponse {
  export type AsObject = {
    item?: MultiOrder.AsObject,
  }
}

export class ListMultiOrderProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderProcessRequest): ListMultiOrderProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderProcessRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderProcessRequest, reader: jspb.BinaryReader): ListMultiOrderProcessRequest;
}

export namespace ListMultiOrderProcessRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderProcessTypeRequest): ListMultiOrderProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderProcessTypeRequest, reader: jspb.BinaryReader): ListMultiOrderProcessTypeRequest;
}

export namespace ListMultiOrderProcessTypeRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderDeliveryConditionRequest): ListMultiOrderDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderDeliveryConditionRequest, reader: jspb.BinaryReader): ListMultiOrderDeliveryConditionRequest;
}

export namespace ListMultiOrderDeliveryConditionRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderPaymentConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderPaymentConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderPaymentConditionRequest): ListMultiOrderPaymentConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderPaymentConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderPaymentConditionRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderPaymentConditionRequest, reader: jspb.BinaryReader): ListMultiOrderPaymentConditionRequest;
}

export namespace ListMultiOrderPaymentConditionRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderCategoryRequest): ListMultiOrderCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderCategoryRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderCategoryRequest, reader: jspb.BinaryReader): ListMultiOrderCategoryRequest;
}

export namespace ListMultiOrderCategoryRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderBrandRequest): ListMultiOrderBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderBrandRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderBrandRequest, reader: jspb.BinaryReader): ListMultiOrderBrandRequest;
}

export namespace ListMultiOrderBrandRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderProductRequest): ListMultiOrderProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderProductRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderProductRequest, reader: jspb.BinaryReader): ListMultiOrderProductRequest;
}

export namespace ListMultiOrderProductRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MultiOrderFilter | undefined;
  setFilter(value?: MultiOrderFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderQuantityTypeRequest): ListMultiOrderQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderQuantityTypeRequest, reader: jspb.BinaryReader): ListMultiOrderQuantityTypeRequest;
}

export namespace ListMultiOrderQuantityTypeRequest {
  export type AsObject = {
    filter?: MultiOrderFilter.AsObject,
  }
}

export class ListMultiOrderNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPaymentConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderNamesRequest): ListMultiOrderNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderNamesRequest;
  static deserializeBinaryFromReader(message: ListMultiOrderNamesRequest, reader: jspb.BinaryReader): ListMultiOrderNamesRequest;
}

export namespace ListMultiOrderNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListMultiOrderNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPaymentConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMultiOrderNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMultiOrderNamesResponse): ListMultiOrderNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMultiOrderNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMultiOrderNamesResponse;
  static deserializeBinaryFromReader(message: ListMultiOrderNamesResponse, reader: jspb.BinaryReader): ListMultiOrderNamesResponse;
}

export namespace ListMultiOrderNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

