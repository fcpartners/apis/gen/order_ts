// package: fcp.order.v1.order
// file: v1/order/model_pushup_notification.proto

import * as jspb from "google-protobuf";

export class SendTokenRequest extends jspb.Message {
  getToken(): string;
  setToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendTokenRequest): SendTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SendTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendTokenRequest;
  static deserializeBinaryFromReader(message: SendTokenRequest, reader: jspb.BinaryReader): SendTokenRequest;
}

export namespace SendTokenRequest {
  export type AsObject = {
    token: string,
  }
}

export class SendTokenResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendTokenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SendTokenResponse): SendTokenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SendTokenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendTokenResponse;
  static deserializeBinaryFromReader(message: SendTokenResponse, reader: jspb.BinaryReader): SendTokenResponse;
}

export namespace SendTokenResponse {
  export type AsObject = {
  }
}

export class RemoveTokenRequest extends jspb.Message {
  getToken(): string;
  setToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoveTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RemoveTokenRequest): RemoveTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoveTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoveTokenRequest;
  static deserializeBinaryFromReader(message: RemoveTokenRequest, reader: jspb.BinaryReader): RemoveTokenRequest;
}

export namespace RemoveTokenRequest {
  export type AsObject = {
    token: string,
  }
}

export class RemoveTokenResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RemoveTokenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RemoveTokenResponse): RemoveTokenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RemoveTokenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RemoveTokenResponse;
  static deserializeBinaryFromReader(message: RemoveTokenResponse, reader: jspb.BinaryReader): RemoveTokenResponse;
}

export namespace RemoveTokenResponse {
  export type AsObject = {
  }
}

export interface ObjectTypeMap {
  OT_UNKNOWN: 0;
  OT_MULTI_ORDER: 1;
  OT_ORDER: 2;
  OT_OFFER: 3;
  OT_MULTI_DEAL: 4;
  OT_DEAL: 5;
  OT_PRICING: 6;
  OT_FAST_DEAL: 7;
  OT_FAST_DEAL_ITEM: 8;
}

export const ObjectType: ObjectTypeMap;

export interface EventTypeMap {
  ET_UNKNOWN: 0;
  ET_CREATED: 1;
  ET_UPDATED: 2;
  ET_DELETED: 3;
}

export const EventType: EventTypeMap;

