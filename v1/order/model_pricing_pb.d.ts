// package: fcp.order.v1.order
// file: v1/order/model_pricing.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_finance_pb from "../../v1/order/finance_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";

export class PricingMerchandiser extends jspb.Message {
  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getPackageId(): number;
  setPackageId(value: number): void;

  hasPricePerItemWithVat(): boolean;
  clearPricePerItemWithVat(): void;
  getPricePerItemWithVat(): PricingMerchandiser.ItemPrice | undefined;
  setPricePerItemWithVat(value?: PricingMerchandiser.ItemPrice): void;

  hasPricePerItemWithVatUah(): boolean;
  clearPricePerItemWithVatUah(): void;
  getPricePerItemWithVatUah(): PricingMerchandiser.ItemPrice | undefined;
  setPricePerItemWithVatUah(value?: PricingMerchandiser.ItemPrice): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PricingMerchandiser.AsObject;
  static toObject(includeInstance: boolean, msg: PricingMerchandiser): PricingMerchandiser.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PricingMerchandiser, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PricingMerchandiser;
  static deserializeBinaryFromReader(message: PricingMerchandiser, reader: jspb.BinaryReader): PricingMerchandiser;
}

export namespace PricingMerchandiser {
  export type AsObject = {
    productId: number,
    productName: string,
    packageId: number,
    pricePerItemWithVat?: PricingMerchandiser.ItemPrice.AsObject,
    pricePerItemWithVatUah?: PricingMerchandiser.ItemPrice.AsObject,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class ItemPrice extends jspb.Message {
    getCurrency(): string;
    setCurrency(value: string): void;

    getPricePerItemWithVat(): number;
    setPricePerItemWithVat(value: number): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ItemPrice.AsObject;
    static toObject(includeInstance: boolean, msg: ItemPrice): ItemPrice.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ItemPrice, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ItemPrice;
    static deserializeBinaryFromReader(message: ItemPrice, reader: jspb.BinaryReader): ItemPrice;
  }

  export namespace ItemPrice {
    export type AsObject = {
      currency: string,
      pricePerItemWithVat: number,
    }
  }
}

export class Pricing extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getStatus(): Pricing.StatusMap[keyof Pricing.StatusMap];
  setStatus(value: Pricing.StatusMap[keyof Pricing.StatusMap]): void;

  hasProduct(): boolean;
  clearProduct(): void;
  getProduct(): v1_order_model_product_pb.Product | undefined;
  setProduct(value?: v1_order_model_product_pb.Product): void;

  hasPricingCondition(): boolean;
  clearPricingCondition(): void;
  getPricingCondition(): PricingCondition | undefined;
  setPricingCondition(value?: PricingCondition): void;

  hasPricingConditionUah(): boolean;
  clearPricingConditionUah(): void;
  getPricingConditionUah(): PricingCondition | undefined;
  setPricingConditionUah(value?: PricingCondition): void;

  getPackageId(): number;
  setPackageId(value: number): void;

  getPackageAmount(): number;
  setPackageAmount(value: number): void;

  getPackage(): string;
  setPackage(value: string): void;

  getYear(): number;
  setYear(value: number): void;

  hasWarehouseLocation(): boolean;
  clearWarehouseLocation(): void;
  getWarehouseLocation(): Pricing.WarehouseLocation | undefined;
  setWarehouseLocation(value?: Pricing.WarehouseLocation): void;

  getDeliveryCondition(): Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap];
  setDeliveryCondition(value: Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap]): void;

  clearProductFilesList(): void;
  getProductFilesList(): Array<v1_order_model_product_pb.ProductFile>;
  setProductFilesList(value: Array<v1_order_model_product_pb.ProductFile>): void;
  addProductFiles(value?: v1_order_model_product_pb.ProductFile, index?: number): v1_order_model_product_pb.ProductFile;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Pricing.AsObject;
  static toObject(includeInstance: boolean, msg: Pricing): Pricing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Pricing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Pricing;
  static deserializeBinaryFromReader(message: Pricing, reader: jspb.BinaryReader): Pricing;
}

export namespace Pricing {
  export type AsObject = {
    id: number,
    status: Pricing.StatusMap[keyof Pricing.StatusMap],
    product?: v1_order_model_product_pb.Product.AsObject,
    pricingCondition?: PricingCondition.AsObject,
    pricingConditionUah?: PricingCondition.AsObject,
    packageId: number,
    packageAmount: number,
    pb_package: string,
    year: number,
    warehouseLocation?: Pricing.WarehouseLocation.AsObject,
    deliveryCondition: Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap],
    productFilesList: Array<v1_order_model_product_pb.ProductFile.AsObject>,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class StatusValue extends jspb.Message {
    getValue(): Pricing.StatusMap[keyof Pricing.StatusMap];
    setValue(value: Pricing.StatusMap[keyof Pricing.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: Pricing.StatusMap[keyof Pricing.StatusMap],
    }
  }

  export class WarehouseLocation extends jspb.Message {
    getId(): number;
    setId(value: number): void;

    getRegion(): string;
    setRegion(value: string): void;

    getDistrict(): string;
    setDistrict(value: string): void;

    getCity(): string;
    setCity(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): WarehouseLocation.AsObject;
    static toObject(includeInstance: boolean, msg: WarehouseLocation): WarehouseLocation.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: WarehouseLocation, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): WarehouseLocation;
    static deserializeBinaryFromReader(message: WarehouseLocation, reader: jspb.BinaryReader): WarehouseLocation;
  }

  export namespace WarehouseLocation {
    export type AsObject = {
      id: number,
      region: string,
      district: string,
      city: string,
    }
  }

  export class DeliveryConditionValue extends jspb.Message {
    getValue(): Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap];
    setValue(value: Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeliveryConditionValue.AsObject;
    static toObject(includeInstance: boolean, msg: DeliveryConditionValue): DeliveryConditionValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeliveryConditionValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeliveryConditionValue;
    static deserializeBinaryFromReader(message: DeliveryConditionValue, reader: jspb.BinaryReader): DeliveryConditionValue;
  }

  export namespace DeliveryConditionValue {
    export type AsObject = {
      value: Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    DEACTIVATED: 1;
    BANNED: 2;
  }

  export const Status: StatusMap;

  export interface DeliveryConditionMap {
    UNSPECIFIED: 0;
    EXW_EX_WORKS: 1;
    DAP_DELIVERED_AT_PLACE: 2;
  }

  export const DeliveryCondition: DeliveryConditionMap;
}

export class PricingCondition extends jspb.Message {
  getVatRate(): number;
  setVatRate(value: number): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getPricePerItem(): number;
  setPricePerItem(value: number): void;

  getPricePerPackage(): number;
  setPricePerPackage(value: number): void;

  getPricePerItemWholesale(): number;
  setPricePerItemWholesale(value: number): void;

  getPricePerPackageWholesale(): number;
  setPricePerPackageWholesale(value: number): void;

  getPricePerItemMerchandiser(): number;
  setPricePerItemMerchandiser(value: number): void;

  getPricePerPackageMerchandiser(): number;
  setPricePerPackageMerchandiser(value: number): void;

  getWholesaleValue(): number;
  setWholesaleValue(value: number): void;

  getVatAmountPerItem(): number;
  setVatAmountPerItem(value: number): void;

  getVatAmountPerPackage(): number;
  setVatAmountPerPackage(value: number): void;

  getPricePerItemWithVat(): number;
  setPricePerItemWithVat(value: number): void;

  getPricePerPackageWithVat(): number;
  setPricePerPackageWithVat(value: number): void;

  getVatAmountPerItemWholesale(): number;
  setVatAmountPerItemWholesale(value: number): void;

  getVatAmountPerPackageWholesale(): number;
  setVatAmountPerPackageWholesale(value: number): void;

  getPricePerItemWithVatWholesale(): number;
  setPricePerItemWithVatWholesale(value: number): void;

  getPricePerPackageWithVatWholesale(): number;
  setPricePerPackageWithVatWholesale(value: number): void;

  getVatAmountPerItemMerchandiser(): number;
  setVatAmountPerItemMerchandiser(value: number): void;

  getVatAmountPerPackageMerchandiser(): number;
  setVatAmountPerPackageMerchandiser(value: number): void;

  getPricePerItemWithVatMerchandiser(): number;
  setPricePerItemWithVatMerchandiser(value: number): void;

  getPricePerPackageWithVatMerchandiser(): number;
  setPricePerPackageWithVatMerchandiser(value: number): void;

  getPricePerItemDealing(): number;
  setPricePerItemDealing(value: number): void;

  getPricePerPackageDealing(): number;
  setPricePerPackageDealing(value: number): void;

  getVatAmountPerItemDealing(): number;
  setVatAmountPerItemDealing(value: number): void;

  getVatAmountPerPackageDealing(): number;
  setVatAmountPerPackageDealing(value: number): void;

  getPricePerItemWithVatDealing(): number;
  setPricePerItemWithVatDealing(value: number): void;

  getPricePerPackageWithVatDealing(): number;
  setPricePerPackageWithVatDealing(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PricingCondition.AsObject;
  static toObject(includeInstance: boolean, msg: PricingCondition): PricingCondition.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PricingCondition, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PricingCondition;
  static deserializeBinaryFromReader(message: PricingCondition, reader: jspb.BinaryReader): PricingCondition;
}

export namespace PricingCondition {
  export type AsObject = {
    vatRate: number,
    currency: string,
    pricePerItem: number,
    pricePerPackage: number,
    pricePerItemWholesale: number,
    pricePerPackageWholesale: number,
    pricePerItemMerchandiser: number,
    pricePerPackageMerchandiser: number,
    wholesaleValue: number,
    vatAmountPerItem: number,
    vatAmountPerPackage: number,
    pricePerItemWithVat: number,
    pricePerPackageWithVat: number,
    vatAmountPerItemWholesale: number,
    vatAmountPerPackageWholesale: number,
    pricePerItemWithVatWholesale: number,
    pricePerPackageWithVatWholesale: number,
    vatAmountPerItemMerchandiser: number,
    vatAmountPerPackageMerchandiser: number,
    pricePerItemWithVatMerchandiser: number,
    pricePerPackageWithVatMerchandiser: number,
    pricePerItemDealing: number,
    pricePerPackageDealing: number,
    vatAmountPerItemDealing: number,
    vatAmountPerPackageDealing: number,
    pricePerItemWithVatDealing: number,
    pricePerPackageWithVatDealing: number,
  }
}

export class DeliveryPricingConditionFilter extends jspb.Message {
  clearWarehouseLocationIdsList(): void;
  getWarehouseLocationIdsList(): Array<number>;
  setWarehouseLocationIdsList(value: Array<number>): void;
  addWarehouseLocationIds(value: number, index?: number): number;

  clearDeliveryConditionList(): void;
  getDeliveryConditionList(): Array<Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap]>;
  setDeliveryConditionList(value: Array<Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap]>): void;
  addDeliveryCondition(value: Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap], index?: number): Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeliveryPricingConditionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: DeliveryPricingConditionFilter): DeliveryPricingConditionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeliveryPricingConditionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeliveryPricingConditionFilter;
  static deserializeBinaryFromReader(message: DeliveryPricingConditionFilter, reader: jspb.BinaryReader): DeliveryPricingConditionFilter;
}

export namespace DeliveryPricingConditionFilter {
  export type AsObject = {
    warehouseLocationIdsList: Array<number>,
    deliveryConditionList: Array<Pricing.DeliveryConditionMap[keyof Pricing.DeliveryConditionMap]>,
  }
}

export class PricingFilter extends jspb.Message {
  hasProductFilter(): boolean;
  clearProductFilter(): void;
  getProductFilter(): v1_order_model_product_pb.ProductFilter | undefined;
  setProductFilter(value?: v1_order_model_product_pb.ProductFilter): void;

  hasProductSeedsFilter(): boolean;
  clearProductSeedsFilter(): void;
  getProductSeedsFilter(): v1_order_model_product_pb.ProductSeedsFilter | undefined;
  setProductSeedsFilter(value?: v1_order_model_product_pb.ProductSeedsFilter): void;

  hasProductChemicalsFilter(): boolean;
  clearProductChemicalsFilter(): void;
  getProductChemicalsFilter(): v1_order_model_product_pb.ProductChemicalsFilter | undefined;
  setProductChemicalsFilter(value?: v1_order_model_product_pb.ProductChemicalsFilter): void;

  clearStatusesList(): void;
  getStatusesList(): Array<Pricing.StatusValue>;
  setStatusesList(value: Array<Pricing.StatusValue>): void;
  addStatuses(value?: Pricing.StatusValue, index?: number): Pricing.StatusValue;

  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): Pricing.StatusValue | undefined;
  setStatus(value?: Pricing.StatusValue): void;

  hasCurrency(): boolean;
  clearCurrency(): void;
  getCurrency(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCurrency(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPackage(): boolean;
  clearPackage(): void;
  getPackage(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPackage(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPackageAmount(): boolean;
  clearPackageAmount(): void;
  getPackageAmount(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPackageAmount(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPricePerItemRange(): boolean;
  clearPricePerItemRange(): void;
  getPricePerItemRange(): v1_order_finance_pb.AmountRange | undefined;
  setPricePerItemRange(value?: v1_order_finance_pb.AmountRange): void;

  hasPricePerPackageRange(): boolean;
  clearPricePerPackageRange(): void;
  getPricePerPackageRange(): v1_order_finance_pb.AmountRange | undefined;
  setPricePerPackageRange(value?: v1_order_finance_pb.AmountRange): void;

  getAlternatives(): boolean;
  setAlternatives(value: boolean): void;

  hasDeliveryConditionFilter(): boolean;
  clearDeliveryConditionFilter(): void;
  getDeliveryConditionFilter(): DeliveryPricingConditionFilter | undefined;
  setDeliveryConditionFilter(value?: DeliveryPricingConditionFilter): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_order_common_pb.StateValue | undefined;
  setState(value?: v1_order_common_pb.StateValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PricingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PricingFilter): PricingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PricingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PricingFilter;
  static deserializeBinaryFromReader(message: PricingFilter, reader: jspb.BinaryReader): PricingFilter;
}

export namespace PricingFilter {
  export type AsObject = {
    productFilter?: v1_order_model_product_pb.ProductFilter.AsObject,
    productSeedsFilter?: v1_order_model_product_pb.ProductSeedsFilter.AsObject,
    productChemicalsFilter?: v1_order_model_product_pb.ProductChemicalsFilter.AsObject,
    statusesList: Array<Pricing.StatusValue.AsObject>,
    status?: Pricing.StatusValue.AsObject,
    currency?: google_protobuf_wrappers_pb.StringValue.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    pb_package?: google_protobuf_wrappers_pb.StringValue.AsObject,
    packageAmount?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    pricePerItemRange?: v1_order_finance_pb.AmountRange.AsObject,
    pricePerPackageRange?: v1_order_finance_pb.AmountRange.AsObject,
    alternatives: boolean,
    deliveryConditionFilter?: DeliveryPricingConditionFilter.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    state?: v1_order_common_pb.StateValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListPricingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  hasFiles(): boolean;
  clearFiles(): void;
  getFiles(): v1_order_model_product_pb.ListProductFileRequest | undefined;
  setFiles(value?: v1_order_model_product_pb.ListProductFileRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingRequest): ListPricingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingRequest;
  static deserializeBinaryFromReader(message: ListPricingRequest, reader: jspb.BinaryReader): ListPricingRequest;
}

export namespace ListPricingRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
    files?: v1_order_model_product_pb.ListProductFileRequest.AsObject,
  }
}

export class ListPricingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Pricing>;
  setItemsList(value: Array<Pricing>): void;
  addItems(value?: Pricing, index?: number): Pricing;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingResponse): ListPricingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingResponse;
  static deserializeBinaryFromReader(message: ListPricingResponse, reader: jspb.BinaryReader): ListPricingResponse;
}

export namespace ListPricingResponse {
  export type AsObject = {
    itemsList: Array<Pricing.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListPricingMerchandiserRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingMerchandiserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingMerchandiserRequest): ListPricingMerchandiserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingMerchandiserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingMerchandiserRequest;
  static deserializeBinaryFromReader(message: ListPricingMerchandiserRequest, reader: jspb.BinaryReader): ListPricingMerchandiserRequest;
}

export namespace ListPricingMerchandiserRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPricingMerchandiserResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PricingMerchandiser>;
  setItemsList(value: Array<PricingMerchandiser>): void;
  addItems(value?: PricingMerchandiser, index?: number): PricingMerchandiser;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingMerchandiserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingMerchandiserResponse): ListPricingMerchandiserResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingMerchandiserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingMerchandiserResponse;
  static deserializeBinaryFromReader(message: ListPricingMerchandiserResponse, reader: jspb.BinaryReader): ListPricingMerchandiserResponse;
}

export namespace ListPricingMerchandiserResponse {
  export type AsObject = {
    itemsList: Array<PricingMerchandiser.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetPricingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPricingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPricingRequest): GetPricingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPricingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPricingRequest;
  static deserializeBinaryFromReader(message: GetPricingRequest, reader: jspb.BinaryReader): GetPricingRequest;
}

export namespace GetPricingRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPricingResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Pricing | undefined;
  setItem(value?: Pricing): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPricingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPricingResponse): GetPricingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPricingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPricingResponse;
  static deserializeBinaryFromReader(message: GetPricingResponse, reader: jspb.BinaryReader): GetPricingResponse;
}

export namespace GetPricingResponse {
  export type AsObject = {
    item?: Pricing.AsObject,
  }
}

export class ListPricingProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingProcessRequest): ListPricingProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingProcessRequest;
  static deserializeBinaryFromReader(message: ListPricingProcessRequest, reader: jspb.BinaryReader): ListPricingProcessRequest;
}

export namespace ListPricingProcessRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingProcessTypeRequest): ListPricingProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListPricingProcessTypeRequest, reader: jspb.BinaryReader): ListPricingProcessTypeRequest;
}

export namespace ListPricingProcessTypeRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingDeliveryConditionRequest): ListPricingDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListPricingDeliveryConditionRequest, reader: jspb.BinaryReader): ListPricingDeliveryConditionRequest;
}

export namespace ListPricingDeliveryConditionRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingWarehouseLocationRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingWarehouseLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingWarehouseLocationRequest): ListPricingWarehouseLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingWarehouseLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingWarehouseLocationRequest;
  static deserializeBinaryFromReader(message: ListPricingWarehouseLocationRequest, reader: jspb.BinaryReader): ListPricingWarehouseLocationRequest;
}

export namespace ListPricingWarehouseLocationRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingProductGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingProductGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingProductGroupRequest): ListPricingProductGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingProductGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingProductGroupRequest;
  static deserializeBinaryFromReader(message: ListPricingProductGroupRequest, reader: jspb.BinaryReader): ListPricingProductGroupRequest;
}

export namespace ListPricingProductGroupRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingProductSubGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingProductSubGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingProductSubGroupRequest): ListPricingProductSubGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingProductSubGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingProductSubGroupRequest;
  static deserializeBinaryFromReader(message: ListPricingProductSubGroupRequest, reader: jspb.BinaryReader): ListPricingProductSubGroupRequest;
}

export namespace ListPricingProductSubGroupRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingCategoryRequest): ListPricingCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingCategoryRequest;
  static deserializeBinaryFromReader(message: ListPricingCategoryRequest, reader: jspb.BinaryReader): ListPricingCategoryRequest;
}

export namespace ListPricingCategoryRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingBrandRequest): ListPricingBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingBrandRequest;
  static deserializeBinaryFromReader(message: ListPricingBrandRequest, reader: jspb.BinaryReader): ListPricingBrandRequest;
}

export namespace ListPricingBrandRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingProductRequest): ListPricingProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingProductRequest;
  static deserializeBinaryFromReader(message: ListPricingProductRequest, reader: jspb.BinaryReader): ListPricingProductRequest;
}

export namespace ListPricingProductRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingQuantityTypeRequest): ListPricingQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListPricingQuantityTypeRequest, reader: jspb.BinaryReader): ListPricingQuantityTypeRequest;
}

export namespace ListPricingQuantityTypeRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingSortTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingSortTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingSortTypeRequest): ListPricingSortTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingSortTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingSortTypeRequest;
  static deserializeBinaryFromReader(message: ListPricingSortTypeRequest, reader: jspb.BinaryReader): ListPricingSortTypeRequest;
}

export namespace ListPricingSortTypeRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingPlantTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingPlantTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingPlantTypeRequest): ListPricingPlantTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingPlantTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingPlantTypeRequest;
  static deserializeBinaryFromReader(message: ListPricingPlantTypeRequest, reader: jspb.BinaryReader): ListPricingPlantTypeRequest;
}

export namespace ListPricingPlantTypeRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingSpeciesRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingSpeciesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingSpeciesRequest): ListPricingSpeciesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingSpeciesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingSpeciesRequest;
  static deserializeBinaryFromReader(message: ListPricingSpeciesRequest, reader: jspb.BinaryReader): ListPricingSpeciesRequest;
}

export namespace ListPricingSpeciesRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingMaturityGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingMaturityGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingMaturityGroupRequest): ListPricingMaturityGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingMaturityGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingMaturityGroupRequest;
  static deserializeBinaryFromReader(message: ListPricingMaturityGroupRequest, reader: jspb.BinaryReader): ListPricingMaturityGroupRequest;
}

export namespace ListPricingMaturityGroupRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingFruitFormRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingFruitFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingFruitFormRequest): ListPricingFruitFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingFruitFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingFruitFormRequest;
  static deserializeBinaryFromReader(message: ListPricingFruitFormRequest, reader: jspb.BinaryReader): ListPricingFruitFormRequest;
}

export namespace ListPricingFruitFormRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingPollinationTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingPollinationTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingPollinationTypeRequest): ListPricingPollinationTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingPollinationTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingPollinationTypeRequest;
  static deserializeBinaryFromReader(message: ListPricingPollinationTypeRequest, reader: jspb.BinaryReader): ListPricingPollinationTypeRequest;
}

export namespace ListPricingPollinationTypeRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
  }
}

export class ListPricingNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductGroupsList(): void;
  getProductGroupsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductGroupsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProductGroups(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductSubgroupsList(): void;
  getProductSubgroupsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductSubgroupsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProductSubgroups(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryWarehouseLocationsList(): void;
  getDeliveryWarehouseLocationsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryWarehouseLocationsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryWarehouseLocations(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearSortTypeList(): void;
  getSortTypeList(): Array<v1_order_enum_pb.NameFilter>;
  setSortTypeList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addSortType(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPlantTypeList(): void;
  getPlantTypeList(): Array<v1_order_enum_pb.NameFilter>;
  setPlantTypeList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPlantType(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearSpeciesList(): void;
  getSpeciesList(): Array<v1_order_enum_pb.NameFilter>;
  setSpeciesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addSpecies(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearMaturityGroupList(): void;
  getMaturityGroupList(): Array<v1_order_enum_pb.NameFilter>;
  setMaturityGroupList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addMaturityGroup(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearFruitFormList(): void;
  getFruitFormList(): Array<v1_order_enum_pb.NameFilter>;
  setFruitFormList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addFruitForm(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPollinationTypeList(): void;
  getPollinationTypeList(): Array<v1_order_enum_pb.NameFilter>;
  setPollinationTypeList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPollinationType(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingNamesRequest): ListPricingNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingNamesRequest;
  static deserializeBinaryFromReader(message: ListPricingNamesRequest, reader: jspb.BinaryReader): ListPricingNamesRequest;
}

export namespace ListPricingNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productGroupsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productSubgroupsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryWarehouseLocationsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    sortTypeList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    plantTypeList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    speciesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    maturityGroupList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    fruitFormList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    pollinationTypeList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListPricingNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductGroupsList(): void;
  getProductGroupsList(): Array<v1_order_enum_pb.DictItem>;
  setProductGroupsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProductGroups(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductSubgroupsList(): void;
  getProductSubgroupsList(): Array<v1_order_enum_pb.DictItem>;
  setProductSubgroupsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProductSubgroups(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryWarehouseLocationsList(): void;
  getDeliveryWarehouseLocationsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryWarehouseLocationsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryWarehouseLocations(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearSortTypeList(): void;
  getSortTypeList(): Array<v1_order_enum_pb.DictItem>;
  setSortTypeList(value: Array<v1_order_enum_pb.DictItem>): void;
  addSortType(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPlantTypeList(): void;
  getPlantTypeList(): Array<v1_order_enum_pb.DictItem>;
  setPlantTypeList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPlantType(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearSpeciesList(): void;
  getSpeciesList(): Array<v1_order_enum_pb.DictItem>;
  setSpeciesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addSpecies(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearMaturityGroupList(): void;
  getMaturityGroupList(): Array<v1_order_enum_pb.DictItem>;
  setMaturityGroupList(value: Array<v1_order_enum_pb.DictItem>): void;
  addMaturityGroup(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearFruitFormList(): void;
  getFruitFormList(): Array<v1_order_enum_pb.DictItem>;
  setFruitFormList(value: Array<v1_order_enum_pb.DictItem>): void;
  addFruitForm(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPollinationTypeList(): void;
  getPollinationTypeList(): Array<v1_order_enum_pb.DictItem>;
  setPollinationTypeList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPollinationType(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingNamesResponse): ListPricingNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingNamesResponse;
  static deserializeBinaryFromReader(message: ListPricingNamesResponse, reader: jspb.BinaryReader): ListPricingNamesResponse;
}

export namespace ListPricingNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productGroupsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productSubgroupsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryWarehouseLocationsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    sortTypeList: Array<v1_order_enum_pb.DictItem.AsObject>,
    plantTypeList: Array<v1_order_enum_pb.DictItem.AsObject>,
    speciesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    maturityGroupList: Array<v1_order_enum_pb.DictItem.AsObject>,
    fruitFormList: Array<v1_order_enum_pb.DictItem.AsObject>,
    pollinationTypeList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

export class ListPricingByIdsRequest extends jspb.Message {
  clearIdsList(): void;
  getIdsList(): Array<number>;
  setIdsList(value: Array<number>): void;
  addIds(value: number, index?: number): number;

  hasFiles(): boolean;
  clearFiles(): void;
  getFiles(): v1_order_model_product_pb.ListProductFileRequest | undefined;
  setFiles(value?: v1_order_model_product_pb.ListProductFileRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingByIdsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingByIdsRequest): ListPricingByIdsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingByIdsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingByIdsRequest;
  static deserializeBinaryFromReader(message: ListPricingByIdsRequest, reader: jspb.BinaryReader): ListPricingByIdsRequest;
}

export namespace ListPricingByIdsRequest {
  export type AsObject = {
    idsList: Array<number>,
    files?: v1_order_model_product_pb.ListProductFileRequest.AsObject,
  }
}

export class ListPricingByIdsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Pricing>;
  setItemsList(value: Array<Pricing>): void;
  addItems(value?: Pricing, index?: number): Pricing;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPricingByIdsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPricingByIdsResponse): ListPricingByIdsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPricingByIdsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPricingByIdsResponse;
  static deserializeBinaryFromReader(message: ListPricingByIdsResponse, reader: jspb.BinaryReader): ListPricingByIdsResponse;
}

export namespace ListPricingByIdsResponse {
  export type AsObject = {
    itemsList: Array<Pricing.AsObject>,
  }
}

export class AvailablePricingFilter extends jspb.Message {
  getProductId(): number;
  setProductId(value: number): void;

  getQuantityTypeId(): number;
  setQuantityTypeId(value: number): void;

  getAlternatives(): boolean;
  setAlternatives(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AvailablePricingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: AvailablePricingFilter): AvailablePricingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AvailablePricingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AvailablePricingFilter;
  static deserializeBinaryFromReader(message: AvailablePricingFilter, reader: jspb.BinaryReader): AvailablePricingFilter;
}

export namespace AvailablePricingFilter {
  export type AsObject = {
    productId: number,
    quantityTypeId: number,
    alternatives: boolean,
  }
}

export class ListAvailablePricingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): AvailablePricingFilter | undefined;
  setFilter(value?: AvailablePricingFilter): void;

  hasPricingFilter(): boolean;
  clearPricingFilter(): void;
  getPricingFilter(): PricingFilter | undefined;
  setPricingFilter(value?: PricingFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListAvailablePricingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListAvailablePricingRequest): ListAvailablePricingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListAvailablePricingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListAvailablePricingRequest;
  static deserializeBinaryFromReader(message: ListAvailablePricingRequest, reader: jspb.BinaryReader): ListAvailablePricingRequest;
}

export namespace ListAvailablePricingRequest {
  export type AsObject = {
    filter?: AvailablePricingFilter.AsObject,
    pricingFilter?: PricingFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListAvailablePricingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Pricing>;
  setItemsList(value: Array<Pricing>): void;
  addItems(value?: Pricing, index?: number): Pricing;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListAvailablePricingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListAvailablePricingResponse): ListAvailablePricingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListAvailablePricingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListAvailablePricingResponse;
  static deserializeBinaryFromReader(message: ListAvailablePricingResponse, reader: jspb.BinaryReader): ListAvailablePricingResponse;
}

export namespace ListAvailablePricingResponse {
  export type AsObject = {
    itemsList: Array<Pricing.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class PricingBySupplier extends jspb.Message {
  getSupplierId(): string;
  setSupplierId(value: string): void;

  clearPriceListList(): void;
  getPriceListList(): Array<Pricing>;
  setPriceListList(value: Array<Pricing>): void;
  addPriceList(value?: Pricing, index?: number): Pricing;

  getPricingCount(): number;
  setPricingCount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PricingBySupplier.AsObject;
  static toObject(includeInstance: boolean, msg: PricingBySupplier): PricingBySupplier.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PricingBySupplier, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PricingBySupplier;
  static deserializeBinaryFromReader(message: PricingBySupplier, reader: jspb.BinaryReader): PricingBySupplier;
}

export namespace PricingBySupplier {
  export type AsObject = {
    supplierId: string,
    priceListList: Array<Pricing.AsObject>,
    pricingCount: number,
  }
}

export class SupplierPricingInfo extends jspb.Message {
  clearPriceListBySupplierList(): void;
  getPriceListBySupplierList(): Array<PricingBySupplier>;
  setPriceListBySupplierList(value: Array<PricingBySupplier>): void;
  addPriceListBySupplier(value?: PricingBySupplier, index?: number): PricingBySupplier;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SupplierPricingInfo.AsObject;
  static toObject(includeInstance: boolean, msg: SupplierPricingInfo): SupplierPricingInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SupplierPricingInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SupplierPricingInfo;
  static deserializeBinaryFromReader(message: SupplierPricingInfo, reader: jspb.BinaryReader): SupplierPricingInfo;
}

export namespace SupplierPricingInfo {
  export type AsObject = {
    priceListBySupplierList: Array<PricingBySupplier.AsObject>,
  }
}

export class ListSupplierPricingInfoRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PricingFilter | undefined;
  setFilter(value?: PricingFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSupplierPricingInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSupplierPricingInfoRequest): ListSupplierPricingInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSupplierPricingInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSupplierPricingInfoRequest;
  static deserializeBinaryFromReader(message: ListSupplierPricingInfoRequest, reader: jspb.BinaryReader): ListSupplierPricingInfoRequest;
}

export namespace ListSupplierPricingInfoRequest {
  export type AsObject = {
    filter?: PricingFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSupplierPricingInfoResponse extends jspb.Message {
  hasItems(): boolean;
  clearItems(): void;
  getItems(): SupplierPricingInfo | undefined;
  setItems(value?: SupplierPricingInfo): void;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSupplierPricingInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSupplierPricingInfoResponse): ListSupplierPricingInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSupplierPricingInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSupplierPricingInfoResponse;
  static deserializeBinaryFromReader(message: ListSupplierPricingInfoResponse, reader: jspb.BinaryReader): ListSupplierPricingInfoResponse;
}

export namespace ListSupplierPricingInfoResponse {
  export type AsObject = {
    items?: SupplierPricingInfo.AsObject,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

