// package: fcp.order.v1.order
// file: v1/order/finance.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";

export class DiscountTypeValue extends jspb.Message {
  getValue(): DiscountTypeMap[keyof DiscountTypeMap];
  setValue(value: DiscountTypeMap[keyof DiscountTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DiscountTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: DiscountTypeValue): DiscountTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DiscountTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DiscountTypeValue;
  static deserializeBinaryFromReader(message: DiscountTypeValue, reader: jspb.BinaryReader): DiscountTypeValue;
}

export namespace DiscountTypeValue {
  export type AsObject = {
    value: DiscountTypeMap[keyof DiscountTypeMap],
  }
}

export class Discount extends jspb.Message {
  getType(): DiscountTypeMap[keyof DiscountTypeMap];
  setType(value: DiscountTypeMap[keyof DiscountTypeMap]): void;

  getValue(): number;
  setValue(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Discount.AsObject;
  static toObject(includeInstance: boolean, msg: Discount): Discount.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Discount, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Discount;
  static deserializeBinaryFromReader(message: Discount, reader: jspb.BinaryReader): Discount;
}

export namespace Discount {
  export type AsObject = {
    type: DiscountTypeMap[keyof DiscountTypeMap],
    value: number,
  }
}

export class Amount extends jspb.Message {
  getCurrency(): string;
  setCurrency(value: string): void;

  getAmount(): number;
  setAmount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Amount.AsObject;
  static toObject(includeInstance: boolean, msg: Amount): Amount.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Amount, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Amount;
  static deserializeBinaryFromReader(message: Amount, reader: jspb.BinaryReader): Amount;
}

export namespace Amount {
  export type AsObject = {
    currency: string,
    amount: number,
  }
}

export class AmountRange extends jspb.Message {
  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFrom(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setTo(value?: google_protobuf_wrappers_pb.Int64Value): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AmountRange.AsObject;
  static toObject(includeInstance: boolean, msg: AmountRange): AmountRange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AmountRange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AmountRange;
  static deserializeBinaryFromReader(message: AmountRange, reader: jspb.BinaryReader): AmountRange;
}

export namespace AmountRange {
  export type AsObject = {
    from?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    to?: google_protobuf_wrappers_pb.Int64Value.AsObject,
  }
}

export class TotalAmountRange extends jspb.Message {
  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): google_protobuf_wrappers_pb.UInt64Value | undefined;
  setFrom(value?: google_protobuf_wrappers_pb.UInt64Value): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_wrappers_pb.UInt64Value | undefined;
  setTo(value?: google_protobuf_wrappers_pb.UInt64Value): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TotalAmountRange.AsObject;
  static toObject(includeInstance: boolean, msg: TotalAmountRange): TotalAmountRange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TotalAmountRange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TotalAmountRange;
  static deserializeBinaryFromReader(message: TotalAmountRange, reader: jspb.BinaryReader): TotalAmountRange;
}

export namespace TotalAmountRange {
  export type AsObject = {
    from?: google_protobuf_wrappers_pb.UInt64Value.AsObject,
    to?: google_protobuf_wrappers_pb.UInt64Value.AsObject,
  }
}

export interface DiscountTypeMap {
  DT_UNSPECIFIED: 0;
  DT_PERCENT: 1;
  DT_AMOUNT: 2;
}

export const DiscountType: DiscountTypeMap;

