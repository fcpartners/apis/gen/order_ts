// package: fcp.order.v1.order
// file: v1/order/enum.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";

export class DictItem extends jspb.Message {
  getTypeId(): ProductTypeMap[keyof ProductTypeMap];
  setTypeId(value: ProductTypeMap[keyof ProductTypeMap]): void;

  getTypeName(): string;
  setTypeName(value: string): void;

  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getCount(): number;
  setCount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DictItem.AsObject;
  static toObject(includeInstance: boolean, msg: DictItem): DictItem.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DictItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DictItem;
  static deserializeBinaryFromReader(message: DictItem, reader: jspb.BinaryReader): DictItem;
}

export namespace DictItem {
  export type AsObject = {
    typeId: ProductTypeMap[keyof ProductTypeMap],
    typeName: string,
    id: number,
    name: string,
    count: number,
  }
}

export class DictResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<DictItem>;
  setItemsList(value: Array<DictItem>): void;
  addItems(value?: DictItem, index?: number): DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DictResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DictResponse): DictResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DictResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DictResponse;
  static deserializeBinaryFromReader(message: DictResponse, reader: jspb.BinaryReader): DictResponse;
}

export namespace DictResponse {
  export type AsObject = {
    itemsList: Array<DictItem.AsObject>,
  }
}

export class NameFilter extends jspb.Message {
  getTypeId(): ProductTypeMap[keyof ProductTypeMap];
  setTypeId(value: ProductTypeMap[keyof ProductTypeMap]): void;

  getId(): number;
  setId(value: number): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NameFilter.AsObject;
  static toObject(includeInstance: boolean, msg: NameFilter): NameFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NameFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NameFilter;
  static deserializeBinaryFromReader(message: NameFilter, reader: jspb.BinaryReader): NameFilter;
}

export namespace NameFilter {
  export type AsObject = {
    typeId: ProductTypeMap[keyof ProductTypeMap],
    id: number,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ProductProcessValue extends jspb.Message {
  getValue(): ProductProcessMap[keyof ProductProcessMap];
  setValue(value: ProductProcessMap[keyof ProductProcessMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductProcessValue.AsObject;
  static toObject(includeInstance: boolean, msg: ProductProcessValue): ProductProcessValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductProcessValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductProcessValue;
  static deserializeBinaryFromReader(message: ProductProcessValue, reader: jspb.BinaryReader): ProductProcessValue;
}

export namespace ProductProcessValue {
  export type AsObject = {
    value: ProductProcessMap[keyof ProductProcessMap],
  }
}

export class ProductTypeValue extends jspb.Message {
  getValue(): ProductTypeMap[keyof ProductTypeMap];
  setValue(value: ProductTypeMap[keyof ProductTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: ProductTypeValue): ProductTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductTypeValue;
  static deserializeBinaryFromReader(message: ProductTypeValue, reader: jspb.BinaryReader): ProductTypeValue;
}

export namespace ProductTypeValue {
  export type AsObject = {
    value: ProductTypeMap[keyof ProductTypeMap],
  }
}

export class CollateralValue extends jspb.Message {
  getValue(): CollateralMap[keyof CollateralMap];
  setValue(value: CollateralMap[keyof CollateralMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CollateralValue.AsObject;
  static toObject(includeInstance: boolean, msg: CollateralValue): CollateralValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CollateralValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CollateralValue;
  static deserializeBinaryFromReader(message: CollateralValue, reader: jspb.BinaryReader): CollateralValue;
}

export namespace CollateralValue {
  export type AsObject = {
    value: CollateralMap[keyof CollateralMap],
  }
}

export class DeliveryConditionValue extends jspb.Message {
  getValue(): DeliveryConditionMap[keyof DeliveryConditionMap];
  setValue(value: DeliveryConditionMap[keyof DeliveryConditionMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeliveryConditionValue.AsObject;
  static toObject(includeInstance: boolean, msg: DeliveryConditionValue): DeliveryConditionValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeliveryConditionValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeliveryConditionValue;
  static deserializeBinaryFromReader(message: DeliveryConditionValue, reader: jspb.BinaryReader): DeliveryConditionValue;
}

export namespace DeliveryConditionValue {
  export type AsObject = {
    value: DeliveryConditionMap[keyof DeliveryConditionMap],
  }
}

export class PaymentConditionValue extends jspb.Message {
  getValue(): PaymentConditionMap[keyof PaymentConditionMap];
  setValue(value: PaymentConditionMap[keyof PaymentConditionMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaymentConditionValue.AsObject;
  static toObject(includeInstance: boolean, msg: PaymentConditionValue): PaymentConditionValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PaymentConditionValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PaymentConditionValue;
  static deserializeBinaryFromReader(message: PaymentConditionValue, reader: jspb.BinaryReader): PaymentConditionValue;
}

export namespace PaymentConditionValue {
  export type AsObject = {
    value: PaymentConditionMap[keyof PaymentConditionMap],
  }
}

export class StatusForSupplierValue extends jspb.Message {
  getValue(): StatusForSupplierMap[keyof StatusForSupplierMap];
  setValue(value: StatusForSupplierMap[keyof StatusForSupplierMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StatusForSupplierValue.AsObject;
  static toObject(includeInstance: boolean, msg: StatusForSupplierValue): StatusForSupplierValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StatusForSupplierValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StatusForSupplierValue;
  static deserializeBinaryFromReader(message: StatusForSupplierValue, reader: jspb.BinaryReader): StatusForSupplierValue;
}

export namespace StatusForSupplierValue {
  export type AsObject = {
    value: StatusForSupplierMap[keyof StatusForSupplierMap],
  }
}

export class CancelClaimReasonValue extends jspb.Message {
  getValue(): CancelClaimReasonMap[keyof CancelClaimReasonMap];
  setValue(value: CancelClaimReasonMap[keyof CancelClaimReasonMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CancelClaimReasonValue.AsObject;
  static toObject(includeInstance: boolean, msg: CancelClaimReasonValue): CancelClaimReasonValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CancelClaimReasonValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CancelClaimReasonValue;
  static deserializeBinaryFromReader(message: CancelClaimReasonValue, reader: jspb.BinaryReader): CancelClaimReasonValue;
}

export namespace CancelClaimReasonValue {
  export type AsObject = {
    value: CancelClaimReasonMap[keyof CancelClaimReasonMap],
  }
}

export interface ProductProcessMap {
  UNSPECIFIED: 0;
  RESOURCES: 1;
  SERVICES: 2;
  COMMODITIES: 3;
  FOOD: 4;
}

export const ProductProcess: ProductProcessMap;

export interface ProductTypeMap {
  PRODUCTTYPE_UNSPECIFIED: 0;
  RESOURCES_SEEDS: 100;
  RESOURCES_FERTILIZE: 101;
  RESOURCES_MICROFERTILIZE: 102;
  RESOURCES_CHEMICAL: 103;
  RESOURCES_FUEL: 104;
  SERVICES_BANKING: 200;
  SERVICES_INSURANCE: 201;
  SERVICES_LEGAL: 202;
  COMMODITIES_CROPS: 300;
  COMMODITIES_ANIMAL: 301;
  COMMODITIES_AQUA_CULTURE: 302;
  COMMODITIES_PROCESSED: 303;
  COMMODITIES_VEGETABLES_FRUIT_BERRY: 304;
  FOOD_CROPS: 400;
  FOOD_ANIMAL: 401;
  FOOD_AQUA_CULTURE: 402;
  FOOD_PROCESSED: 403;
}

export const ProductType: ProductTypeMap;

export interface CollateralMap {
  OST_NO_SUPPORT: 0;
  OST_FINANCIAL_GUARANTEE: 1;
  OST_BILL: 2;
  OST_BANK_GUARANTEE: 3;
  OST_AGRICULTURAL_RECEIPT: 4;
  OST_GOOD_PLEDGE: 5;
  OST_AGRICULTURAL_MACHINERY_PLEDGE: 6;
  OST_REALTY_PLEDGE: 7;
  OST_PROPERTY_PLEDGE: 8;
  OST_REVERSE_LEASING: 9;
}

export const Collateral: CollateralMap;

export interface DeliveryConditionMap {
  ODC_UNSPECIFIED: 0;
  ODC_EXW_EX_WORKS: 1;
  ODC_FCA_FREE_CARRIER: 2;
  ODC_FAS_FREE_ALONGSIDE_SHIP: 3;
  ODC_FOB_FREE_ON_BOARD: 4;
  ODC_CIF_COST_INSURANCE_AND_FREIGHT: 5;
  ODC_CFR_COST_AND_FREIGHT: 6;
  ODC_CPT_CARRIAGE_PAID_TO: 7;
  ODC_CIP_CARRIAGE_AND_INSURANCE_PAID_TO: 8;
  ODC_DAF_DELIVERED_AT_FRONTIER: 9;
  ODC_DES_DELIVERED_EX_SHIP: 10;
  ODC_DEQ_DELIVERED_EX_QUAY: 11;
  ODC_DDP_DELIVERED_DUTY_PAID: 12;
  ODC_DDU_DELIVERED_DUTY_UNPAID: 13;
  ODC_DAT_DELIVERED_AT_TERMINAL: 14;
  ODC_DAP_DELIVERED_AT_PLACE: 15;
}

export const DeliveryCondition: DeliveryConditionMap;

export interface PaymentConditionMap {
  PC_UNSPECIFIED: 0;
  PC_CREDIT_BY_SUPPLIER_100: 3;
  PC_PRE_PAYMENT: 4;
  PC_PAYBACK: 5;
  PC_PROMISSIONARY_NOTE: 6;
  PC_CREDIT_BY_SUPPLIER_20_80: 7;
  PC_CREDIT_BY_SUPPLIER_30_70: 8;
}

export const PaymentCondition: PaymentConditionMap;

export interface StatusForSupplierMap {
  NEW: 0;
  IN_PROGRESS: 1;
  OFFER_CREATED: 2;
  DEAL_CREATED: 3;
  REJECT: 4;
  DONE: 5;
}

export const StatusForSupplier: StatusForSupplierMap;

export interface CancelClaimReasonMap {
  NOT_SPECIFIED: 0;
  USER_CHANGED_DEAL_CONDITIONS: 1;
  USER_HAS_NOT_CONFIRM_DEAL: 2;
  CANT_COMPLETE_DEAL: 3;
  COMPLETED_DEAL_AT_ANOTHER_MARKET: 4;
  ANOTHER_REASON: 5;
}

export const CancelClaimReason: CancelClaimReasonMap;

