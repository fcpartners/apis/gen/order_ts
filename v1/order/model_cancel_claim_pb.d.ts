// package: fcp.order.v1.order
// file: v1/order/model_cancel_claim.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_deal_pb from "../../v1/order/model_deal_pb";

export class CancelClaim extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  getStatus(): CancelClaim.StatusMap[keyof CancelClaim.StatusMap];
  setStatus(value: CancelClaim.StatusMap[keyof CancelClaim.StatusMap]): void;

  getMultiDealId(): number;
  setMultiDealId(value: number): void;

  getDealId(): number;
  setDealId(value: number): void;

  getDealExternalId(): string;
  setDealExternalId(value: string): void;

  getReason(): v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap];
  setReason(value: v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap]): void;

  getReasonComment(): string;
  setReasonComment(value: string): void;

  getAdminId(): string;
  setAdminId(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CancelClaim.AsObject;
  static toObject(includeInstance: boolean, msg: CancelClaim): CancelClaim.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CancelClaim, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CancelClaim;
  static deserializeBinaryFromReader(message: CancelClaim, reader: jspb.BinaryReader): CancelClaim;
}

export namespace CancelClaim {
  export type AsObject = {
    id: number,
    externalId: string,
    status: CancelClaim.StatusMap[keyof CancelClaim.StatusMap],
    multiDealId: number,
    dealId: number,
    dealExternalId: string,
    reason: v1_order_enum_pb.CancelClaimReasonMap[keyof v1_order_enum_pb.CancelClaimReasonMap],
    reasonComment: string,
    adminId: string,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class StatusValue extends jspb.Message {
    getValue(): CancelClaim.StatusMap[keyof CancelClaim.StatusMap];
    setValue(value: CancelClaim.StatusMap[keyof CancelClaim.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: CancelClaim.StatusMap[keyof CancelClaim.StatusMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
  }

  export const Status: StatusMap;
}

export class CancelClaimFilter extends jspb.Message {
  clearStatusesList(): void;
  getStatusesList(): Array<CancelClaim.StatusMap[keyof CancelClaim.StatusMap]>;
  setStatusesList(value: Array<CancelClaim.StatusMap[keyof CancelClaim.StatusMap]>): void;
  addStatuses(value: CancelClaim.StatusMap[keyof CancelClaim.StatusMap], index?: number): CancelClaim.StatusMap[keyof CancelClaim.StatusMap];

  hasReason(): boolean;
  clearReason(): void;
  getReason(): v1_order_enum_pb.CancelClaimReasonValue | undefined;
  setReason(value?: v1_order_enum_pb.CancelClaimReasonValue): void;

  hasReasonComment(): boolean;
  clearReasonComment(): void;
  getReasonComment(): google_protobuf_wrappers_pb.StringValue | undefined;
  setReasonComment(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDealId(): boolean;
  clearDealId(): void;
  getDealId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setDealId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasMultiDealId(): boolean;
  clearMultiDealId(): void;
  getMultiDealId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMultiDealId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_order_common_pb.StateValue | undefined;
  setState(value?: v1_order_common_pb.StateValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CancelClaimFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CancelClaimFilter): CancelClaimFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CancelClaimFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CancelClaimFilter;
  static deserializeBinaryFromReader(message: CancelClaimFilter, reader: jspb.BinaryReader): CancelClaimFilter;
}

export namespace CancelClaimFilter {
  export type AsObject = {
    statusesList: Array<CancelClaim.StatusMap[keyof CancelClaim.StatusMap]>,
    reason?: v1_order_enum_pb.CancelClaimReasonValue.AsObject,
    reasonComment?: google_protobuf_wrappers_pb.StringValue.AsObject,
    externalId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    dealId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    multiDealId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    state?: v1_order_common_pb.StateValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListCancelClaimRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CancelClaimFilter | undefined;
  setFilter(value?: CancelClaimFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCancelClaimRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCancelClaimRequest): ListCancelClaimRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCancelClaimRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCancelClaimRequest;
  static deserializeBinaryFromReader(message: ListCancelClaimRequest, reader: jspb.BinaryReader): ListCancelClaimRequest;
}

export namespace ListCancelClaimRequest {
  export type AsObject = {
    filter?: CancelClaimFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCancelClaimResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<CancelClaim>;
  setItemsList(value: Array<CancelClaim>): void;
  addItems(value?: CancelClaim, index?: number): CancelClaim;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCancelClaimResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCancelClaimResponse): ListCancelClaimResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCancelClaimResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCancelClaimResponse;
  static deserializeBinaryFromReader(message: ListCancelClaimResponse, reader: jspb.BinaryReader): ListCancelClaimResponse;
}

export namespace ListCancelClaimResponse {
  export type AsObject = {
    itemsList: Array<CancelClaim.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetCancelClaimRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCancelClaimRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCancelClaimRequest): GetCancelClaimRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCancelClaimRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCancelClaimRequest;
  static deserializeBinaryFromReader(message: GetCancelClaimRequest, reader: jspb.BinaryReader): GetCancelClaimRequest;
}

export namespace GetCancelClaimRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCancelClaimResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CancelClaim | undefined;
  setItem(value?: CancelClaim): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCancelClaimResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCancelClaimResponse): GetCancelClaimResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCancelClaimResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCancelClaimResponse;
  static deserializeBinaryFromReader(message: GetCancelClaimResponse, reader: jspb.BinaryReader): GetCancelClaimResponse;
}

export namespace GetCancelClaimResponse {
  export type AsObject = {
    item?: CancelClaim.AsObject,
  }
}

export class SaveCancelClaimRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CancelClaim | undefined;
  setItem(value?: CancelClaim): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCancelClaimRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCancelClaimRequest): SaveCancelClaimRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCancelClaimRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCancelClaimRequest;
  static deserializeBinaryFromReader(message: SaveCancelClaimRequest, reader: jspb.BinaryReader): SaveCancelClaimRequest;
}

export namespace SaveCancelClaimRequest {
  export type AsObject = {
    item?: CancelClaim.AsObject,
  }
}

export class SaveCancelClaimResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCancelClaimResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCancelClaimResponse): SaveCancelClaimResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCancelClaimResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCancelClaimResponse;
  static deserializeBinaryFromReader(message: SaveCancelClaimResponse, reader: jspb.BinaryReader): SaveCancelClaimResponse;
}

export namespace SaveCancelClaimResponse {
  export type AsObject = {
    id: number,
  }
}

