// package: fcp.order.v1.order
// file: v1/order/model_user_rating.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";

export class UserRating extends jspb.Message {
  getRating(): UserRating.RatingMap[keyof UserRating.RatingMap];
  setRating(value: UserRating.RatingMap[keyof UserRating.RatingMap]): void;

  getUserId(): string;
  setUserId(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserRating.AsObject;
  static toObject(includeInstance: boolean, msg: UserRating): UserRating.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserRating, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserRating;
  static deserializeBinaryFromReader(message: UserRating, reader: jspb.BinaryReader): UserRating;
}

export namespace UserRating {
  export type AsObject = {
    rating: UserRating.RatingMap[keyof UserRating.RatingMap],
    userId: string,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class RatingValue extends jspb.Message {
    getValue(): UserRating.RatingMap[keyof UserRating.RatingMap];
    setValue(value: UserRating.RatingMap[keyof UserRating.RatingMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): RatingValue.AsObject;
    static toObject(includeInstance: boolean, msg: RatingValue): RatingValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: RatingValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): RatingValue;
    static deserializeBinaryFromReader(message: RatingValue, reader: jspb.BinaryReader): RatingValue;
  }

  export namespace RatingValue {
    export type AsObject = {
      value: UserRating.RatingMap[keyof UserRating.RatingMap],
    }
  }

  export interface RatingMap {
    NOT_SPECIFIED: 0;
    ONE: 1;
    TWO: 2;
    THREE: 3;
    FOUR: 4;
    FIVE: 5;
  }

  export const Rating: RatingMap;
}

export class UserRatingFilter extends jspb.Message {
  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasRating(): boolean;
  clearRating(): void;
  getRating(): UserRating.RatingValue | undefined;
  setRating(value?: UserRating.RatingValue): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserRatingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: UserRatingFilter): UserRatingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserRatingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserRatingFilter;
  static deserializeBinaryFromReader(message: UserRatingFilter, reader: jspb.BinaryReader): UserRatingFilter;
}

export namespace UserRatingFilter {
  export type AsObject = {
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    rating?: UserRating.RatingValue.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListUserRatingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): UserRatingFilter | undefined;
  setFilter(value?: UserRatingFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUserRatingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListUserRatingRequest): ListUserRatingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUserRatingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUserRatingRequest;
  static deserializeBinaryFromReader(message: ListUserRatingRequest, reader: jspb.BinaryReader): ListUserRatingRequest;
}

export namespace ListUserRatingRequest {
  export type AsObject = {
    filter?: UserRatingFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListUserRatingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<UserRating>;
  setItemsList(value: Array<UserRating>): void;
  addItems(value?: UserRating, index?: number): UserRating;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUserRatingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListUserRatingResponse): ListUserRatingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUserRatingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUserRatingResponse;
  static deserializeBinaryFromReader(message: ListUserRatingResponse, reader: jspb.BinaryReader): ListUserRatingResponse;
}

export namespace ListUserRatingResponse {
  export type AsObject = {
    itemsList: Array<UserRating.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetUserRatingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserRatingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserRatingRequest): GetUserRatingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserRatingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserRatingRequest;
  static deserializeBinaryFromReader(message: GetUserRatingRequest, reader: jspb.BinaryReader): GetUserRatingRequest;
}

export namespace GetUserRatingRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetUserRatingResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): UserRating | undefined;
  setItem(value?: UserRating): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserRatingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserRatingResponse): GetUserRatingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserRatingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserRatingResponse;
  static deserializeBinaryFromReader(message: GetUserRatingResponse, reader: jspb.BinaryReader): GetUserRatingResponse;
}

export namespace GetUserRatingResponse {
  export type AsObject = {
    item?: UserRating.AsObject,
  }
}

