// package: fcp.order.v1.order
// file: v1/order/model_fast_deal.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_file_pb from "../../v1/order/file_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_pricing_pb from "../../v1/order/model_pricing_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";
import * as v1_order_model_user_rating_pb from "../../v1/order/model_user_rating_pb";

export class FastDeal extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  getStatus(): FastDeal.StatusMap[keyof FastDeal.StatusMap];
  setStatus(value: FastDeal.StatusMap[keyof FastDeal.StatusMap]): void;

  getDeliveryCondition(): FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap];
  setDeliveryCondition(value: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap]): void;

  getLogisticsOperator(): FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap];
  setLogisticsOperator(value: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap]): void;

  hasDeliveryAddress(): boolean;
  clearDeliveryAddress(): void;
  getDeliveryAddress(): FastDeal.DeliveryAddress | undefined;
  setDeliveryAddress(value?: FastDeal.DeliveryAddress): void;

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDeliveryDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getPaymentCondition(): FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap];
  setPaymentCondition(value: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap]): void;

  hasPaymentInfo(): boolean;
  clearPaymentInfo(): void;
  getPaymentInfo(): FastDeal.PaymentInfo | undefined;
  setPaymentInfo(value?: FastDeal.PaymentInfo): void;

  hasCustomer(): boolean;
  clearCustomer(): void;
  getCustomer(): v1_order_model_base_pb.UserInfo | undefined;
  setCustomer(value?: v1_order_model_base_pb.UserInfo): void;

  hasSupplier(): boolean;
  clearSupplier(): void;
  getSupplier(): v1_order_model_base_pb.UserInfo | undefined;
  setSupplier(value?: v1_order_model_base_pb.UserInfo): void;

  getCustomerNotes(): string;
  setCustomerNotes(value: string): void;

  getChatId(): number;
  setChatId(value: number): void;

  getChatIdent(): string;
  setChatIdent(value: string): void;

  hasSnapshot(): boolean;
  clearSnapshot(): void;
  getSnapshot(): FastDealSnapshot | undefined;
  setSnapshot(value?: FastDealSnapshot): void;

  clearSnapshotItemsList(): void;
  getSnapshotItemsList(): Array<FastDealItemSnapshot>;
  setSnapshotItemsList(value: Array<FastDealItemSnapshot>): void;
  addSnapshotItems(value?: FastDealItemSnapshot, index?: number): FastDealItemSnapshot;

  clearItemsList(): void;
  getItemsList(): Array<FastDealItem>;
  setItemsList(value: Array<FastDealItem>): void;
  addItems(value?: FastDealItem, index?: number): FastDealItem;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FastDeal.AsObject;
  static toObject(includeInstance: boolean, msg: FastDeal): FastDeal.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FastDeal, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FastDeal;
  static deserializeBinaryFromReader(message: FastDeal, reader: jspb.BinaryReader): FastDeal;
}

export namespace FastDeal {
  export type AsObject = {
    id: number,
    externalId: string,
    status: FastDeal.StatusMap[keyof FastDeal.StatusMap],
    deliveryCondition: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap],
    logisticsOperator: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap],
    deliveryAddress?: FastDeal.DeliveryAddress.AsObject,
    deliveryDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    paymentCondition: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap],
    paymentInfo?: FastDeal.PaymentInfo.AsObject,
    customer?: v1_order_model_base_pb.UserInfo.AsObject,
    supplier?: v1_order_model_base_pb.UserInfo.AsObject,
    customerNotes: string,
    chatId: number,
    chatIdent: string,
    snapshot?: FastDealSnapshot.AsObject,
    snapshotItemsList: Array<FastDealItemSnapshot.AsObject>,
    itemsList: Array<FastDealItem.AsObject>,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class StatusValue extends jspb.Message {
    getValue(): FastDeal.StatusMap[keyof FastDeal.StatusMap];
    setValue(value: FastDeal.StatusMap[keyof FastDeal.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: FastDeal.StatusMap[keyof FastDeal.StatusMap],
    }
  }

  export class DeliveryConditionValue extends jspb.Message {
    getValue(): FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap];
    setValue(value: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeliveryConditionValue.AsObject;
    static toObject(includeInstance: boolean, msg: DeliveryConditionValue): DeliveryConditionValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeliveryConditionValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeliveryConditionValue;
    static deserializeBinaryFromReader(message: DeliveryConditionValue, reader: jspb.BinaryReader): DeliveryConditionValue;
  }

  export namespace DeliveryConditionValue {
    export type AsObject = {
      value: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap],
    }
  }

  export class LogisticsOperatorValue extends jspb.Message {
    getValue(): FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap];
    setValue(value: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): LogisticsOperatorValue.AsObject;
    static toObject(includeInstance: boolean, msg: LogisticsOperatorValue): LogisticsOperatorValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: LogisticsOperatorValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): LogisticsOperatorValue;
    static deserializeBinaryFromReader(message: LogisticsOperatorValue, reader: jspb.BinaryReader): LogisticsOperatorValue;
  }

  export namespace LogisticsOperatorValue {
    export type AsObject = {
      value: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap],
    }
  }

  export class DeliveryAddress extends jspb.Message {
    getCustomerFirstName(): string;
    setCustomerFirstName(value: string): void;

    getCustomerMiddleName(): string;
    setCustomerMiddleName(value: string): void;

    getCustomerLastName(): string;
    setCustomerLastName(value: string): void;

    getCustomerPhone(): string;
    setCustomerPhone(value: string): void;

    getCustomerEmail(): string;
    setCustomerEmail(value: string): void;

    getZip(): string;
    setZip(value: string): void;

    getState(): string;
    setState(value: string): void;

    getCity(): string;
    setCity(value: string): void;

    getStreet(): string;
    setStreet(value: string): void;

    getBuilding(): string;
    setBuilding(value: string): void;

    getApartment(): string;
    setApartment(value: string): void;

    getNote(): string;
    setNote(value: string): void;

    getReceiverFirstName(): string;
    setReceiverFirstName(value: string): void;

    getReceiverMiddleName(): string;
    setReceiverMiddleName(value: string): void;

    getReceiverLastName(): string;
    setReceiverLastName(value: string): void;

    getReceiverPhone(): string;
    setReceiverPhone(value: string): void;

    getReceiverEmail(): string;
    setReceiverEmail(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeliveryAddress.AsObject;
    static toObject(includeInstance: boolean, msg: DeliveryAddress): DeliveryAddress.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeliveryAddress, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeliveryAddress;
    static deserializeBinaryFromReader(message: DeliveryAddress, reader: jspb.BinaryReader): DeliveryAddress;
  }

  export namespace DeliveryAddress {
    export type AsObject = {
      customerFirstName: string,
      customerMiddleName: string,
      customerLastName: string,
      customerPhone: string,
      customerEmail: string,
      zip: string,
      state: string,
      city: string,
      street: string,
      building: string,
      apartment: string,
      note: string,
      receiverFirstName: string,
      receiverMiddleName: string,
      receiverLastName: string,
      receiverPhone: string,
      receiverEmail: string,
    }
  }

  export class PaymentConditionValue extends jspb.Message {
    getValue(): FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap];
    setValue(value: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PaymentConditionValue.AsObject;
    static toObject(includeInstance: boolean, msg: PaymentConditionValue): PaymentConditionValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PaymentConditionValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PaymentConditionValue;
    static deserializeBinaryFromReader(message: PaymentConditionValue, reader: jspb.BinaryReader): PaymentConditionValue;
  }

  export namespace PaymentConditionValue {
    export type AsObject = {
      value: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap],
    }
  }

  export class PaymentInfo extends jspb.Message {
    getCurrency(): string;
    setCurrency(value: string): void;

    getTotalAmount(): number;
    setTotalAmount(value: number): void;

    hasPaymentDate(): boolean;
    clearPaymentDate(): void;
    getPaymentDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
    setPaymentDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PaymentInfo.AsObject;
    static toObject(includeInstance: boolean, msg: PaymentInfo): PaymentInfo.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PaymentInfo, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PaymentInfo;
    static deserializeBinaryFromReader(message: PaymentInfo, reader: jspb.BinaryReader): PaymentInfo;
  }

  export namespace PaymentInfo {
    export type AsObject = {
      currency: string,
      totalAmount: number,
      paymentDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
    EXECUTE: 3;
  }

  export const Status: StatusMap;

  export interface DeliveryConditionMap {
    PICKUP: 0;
    POST_OFFICE: 1;
    COURIER: 2;
  }

  export const DeliveryCondition: DeliveryConditionMap;

  export interface LogisticsOperatorMap {
    SUPPLIER: 0;
    NOVA_POST: 1;
    UKR_POST: 2;
  }

  export const LogisticsOperator: LogisticsOperatorMap;

  export interface PaymentConditionMap {
    PAY_NOW: 0;
    PAY_AT_DELIVERY: 1;
    BANK_TRANSFER_COMPANY: 2;
    BANK_TRANSFER_PERSONAL: 3;
  }

  export const PaymentCondition: PaymentConditionMap;
}

export class FastDealSnapshot extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getDeliveryCondition(): FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap];
  setDeliveryCondition(value: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap]): void;

  getLogisticsOperator(): FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap];
  setLogisticsOperator(value: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap]): void;

  hasDeliveryAddress(): boolean;
  clearDeliveryAddress(): void;
  getDeliveryAddress(): FastDeal.DeliveryAddress | undefined;
  setDeliveryAddress(value?: FastDeal.DeliveryAddress): void;

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDeliveryDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getPaymentCondition(): FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap];
  setPaymentCondition(value: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap]): void;

  hasPaymentInfo(): boolean;
  clearPaymentInfo(): void;
  getPaymentInfo(): FastDeal.PaymentInfo | undefined;
  setPaymentInfo(value?: FastDeal.PaymentInfo): void;

  hasCustomer(): boolean;
  clearCustomer(): void;
  getCustomer(): v1_order_model_base_pb.UserInfo | undefined;
  setCustomer(value?: v1_order_model_base_pb.UserInfo): void;

  hasSupplier(): boolean;
  clearSupplier(): void;
  getSupplier(): v1_order_model_base_pb.UserInfo | undefined;
  setSupplier(value?: v1_order_model_base_pb.UserInfo): void;

  getCustomerNotes(): string;
  setCustomerNotes(value: string): void;

  getChatId(): number;
  setChatId(value: number): void;

  getChatIdent(): string;
  setChatIdent(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FastDealSnapshot.AsObject;
  static toObject(includeInstance: boolean, msg: FastDealSnapshot): FastDealSnapshot.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FastDealSnapshot, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FastDealSnapshot;
  static deserializeBinaryFromReader(message: FastDealSnapshot, reader: jspb.BinaryReader): FastDealSnapshot;
}

export namespace FastDealSnapshot {
  export type AsObject = {
    id: number,
    deliveryCondition: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap],
    logisticsOperator: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap],
    deliveryAddress?: FastDeal.DeliveryAddress.AsObject,
    deliveryDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    paymentCondition: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap],
    paymentInfo?: FastDeal.PaymentInfo.AsObject,
    customer?: v1_order_model_base_pb.UserInfo.AsObject,
    supplier?: v1_order_model_base_pb.UserInfo.AsObject,
    customerNotes: string,
    chatId: number,
    chatIdent: string,
    audit?: v1_order_common_pb.Audit.AsObject,
  }
}

export class FastDealItem extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  hasPricing(): boolean;
  clearPricing(): void;
  getPricing(): v1_order_model_pricing_pb.Pricing | undefined;
  setPricing(value?: v1_order_model_pricing_pb.Pricing): void;

  getPriceType(): FastDealItem.PriceTypeMap[keyof FastDealItem.PriceTypeMap];
  setPriceType(value: FastDealItem.PriceTypeMap[keyof FastDealItem.PriceTypeMap]): void;

  getPricePerPackageWithVat(): number;
  setPricePerPackageWithVat(value: number): void;

  getPricePerItemWithVat(): number;
  setPricePerItemWithVat(value: number): void;

  getStatus(): FastDealItem.StatusMap[keyof FastDealItem.StatusMap];
  setStatus(value: FastDealItem.StatusMap[keyof FastDealItem.StatusMap]): void;

  getPackageQuantity(): number;
  setPackageQuantity(value: number): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getAmount(): number;
  setAmount(value: number): void;

  getAmountWithVat(): number;
  setAmountWithVat(value: number): void;

  getVatAmount(): number;
  setVatAmount(value: number): void;

  getCancelInProgress(): boolean;
  setCancelInProgress(value: boolean): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FastDealItem.AsObject;
  static toObject(includeInstance: boolean, msg: FastDealItem): FastDealItem.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FastDealItem, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FastDealItem;
  static deserializeBinaryFromReader(message: FastDealItem, reader: jspb.BinaryReader): FastDealItem;
}

export namespace FastDealItem {
  export type AsObject = {
    id: number,
    externalId: string,
    pricing?: v1_order_model_pricing_pb.Pricing.AsObject,
    priceType: FastDealItem.PriceTypeMap[keyof FastDealItem.PriceTypeMap],
    pricePerPackageWithVat: number,
    pricePerItemWithVat: number,
    status: FastDealItem.StatusMap[keyof FastDealItem.StatusMap],
    packageQuantity: number,
    currency: string,
    amount: number,
    amountWithVat: number,
    vatAmount: number,
    cancelInProgress: boolean,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export interface PriceTypeMap {
    RETAIL: 0;
    WHOLESALE: 1;
    MERCHANDISER: 2;
  }

  export const PriceType: PriceTypeMap;

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
    EXECUTE: 3;
  }

  export const Status: StatusMap;
}

export class FastDealItemSnapshot extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): string;
  setExternalId(value: string): void;

  hasProduct(): boolean;
  clearProduct(): void;
  getProduct(): v1_order_model_product_pb.Product | undefined;
  setProduct(value?: v1_order_model_product_pb.Product): void;

  getPackageId(): number;
  setPackageId(value: number): void;

  getPackageAmount(): number;
  setPackageAmount(value: number): void;

  getPackage(): string;
  setPackage(value: string): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getPriceType(): FastDealItem.PriceTypeMap[keyof FastDealItem.PriceTypeMap];
  setPriceType(value: FastDealItem.PriceTypeMap[keyof FastDealItem.PriceTypeMap]): void;

  getPricePerPackageWithVat(): number;
  setPricePerPackageWithVat(value: number): void;

  getPricePerItemWithVat(): number;
  setPricePerItemWithVat(value: number): void;

  getVatRate(): number;
  setVatRate(value: number): void;

  getAmount(): number;
  setAmount(value: number): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FastDealItemSnapshot.AsObject;
  static toObject(includeInstance: boolean, msg: FastDealItemSnapshot): FastDealItemSnapshot.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FastDealItemSnapshot, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FastDealItemSnapshot;
  static deserializeBinaryFromReader(message: FastDealItemSnapshot, reader: jspb.BinaryReader): FastDealItemSnapshot;
}

export namespace FastDealItemSnapshot {
  export type AsObject = {
    id: number,
    externalId: string,
    product?: v1_order_model_product_pb.Product.AsObject,
    packageId: number,
    packageAmount: number,
    pb_package: string,
    currency: string,
    priceType: FastDealItem.PriceTypeMap[keyof FastDealItem.PriceTypeMap],
    pricePerPackageWithVat: number,
    pricePerItemWithVat: number,
    vatRate: number,
    amount: number,
    audit?: v1_order_common_pb.Audit.AsObject,
  }
}

export class FastDealFilter extends jspb.Message {
  clearIdsList(): void;
  getIdsList(): Array<number>;
  setIdsList(value: Array<number>): void;
  addIds(value: number, index?: number): number;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearStatusesList(): void;
  getStatusesList(): Array<FastDeal.StatusMap[keyof FastDeal.StatusMap]>;
  setStatusesList(value: Array<FastDeal.StatusMap[keyof FastDeal.StatusMap]>): void;
  addStatuses(value: FastDeal.StatusMap[keyof FastDeal.StatusMap], index?: number): FastDeal.StatusMap[keyof FastDeal.StatusMap];

  clearStateList(): void;
  getStateList(): Array<string>;
  setStateList(value: Array<string>): void;
  addState(value: string, index?: number): string;

  clearCityList(): void;
  getCityList(): Array<string>;
  setCityList(value: Array<string>): void;
  addCity(value: string, index?: number): string;

  hasProductFilter(): boolean;
  clearProductFilter(): void;
  getProductFilter(): v1_order_model_product_pb.ProductFilter | undefined;
  setProductFilter(value?: v1_order_model_product_pb.ProductFilter): void;

  clearCurrencyList(): void;
  getCurrencyList(): Array<string>;
  setCurrencyList(value: Array<string>): void;
  addCurrency(value: string, index?: number): string;

  clearLogisticOperatorList(): void;
  getLogisticOperatorList(): Array<FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap]>;
  setLogisticOperatorList(value: Array<FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap]>): void;
  addLogisticOperator(value: FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap], index?: number): FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap];

  clearDeliveryConditionList(): void;
  getDeliveryConditionList(): Array<FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap]>;
  setDeliveryConditionList(value: Array<FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap]>): void;
  addDeliveryCondition(value: FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap], index?: number): FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap];

  clearPaymentConditionList(): void;
  getPaymentConditionList(): Array<FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap]>;
  setPaymentConditionList(value: Array<FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap]>): void;
  addPaymentCondition(value: FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap], index?: number): FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap];

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_order_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_order_common_pb.TimeRange): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_order_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_order_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FastDealFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FastDealFilter): FastDealFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FastDealFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FastDealFilter;
  static deserializeBinaryFromReader(message: FastDealFilter, reader: jspb.BinaryReader): FastDealFilter;
}

export namespace FastDealFilter {
  export type AsObject = {
    idsList: Array<number>,
    externalId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    statusesList: Array<FastDeal.StatusMap[keyof FastDeal.StatusMap]>,
    stateList: Array<string>,
    cityList: Array<string>,
    productFilter?: v1_order_model_product_pb.ProductFilter.AsObject,
    currencyList: Array<string>,
    logisticOperatorList: Array<FastDeal.LogisticsOperatorMap[keyof FastDeal.LogisticsOperatorMap]>,
    deliveryConditionList: Array<FastDeal.DeliveryConditionMap[keyof FastDeal.DeliveryConditionMap]>,
    paymentConditionList: Array<FastDeal.PaymentConditionMap[keyof FastDeal.PaymentConditionMap]>,
    createdAt?: v1_order_common_pb.TimeRange.AsObject,
    updatedAt?: v1_order_common_pb.TimeRange.AsObject,
  }
}

export class ListFastDealRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealRequest): ListFastDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealRequest;
  static deserializeBinaryFromReader(message: ListFastDealRequest, reader: jspb.BinaryReader): ListFastDealRequest;
}

export namespace ListFastDealRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFastDealResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FastDeal>;
  setItemsList(value: Array<FastDeal>): void;
  addItems(value?: FastDeal, index?: number): FastDeal;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealResponse): ListFastDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealResponse;
  static deserializeBinaryFromReader(message: ListFastDealResponse, reader: jspb.BinaryReader): ListFastDealResponse;
}

export namespace ListFastDealResponse {
  export type AsObject = {
    itemsList: Array<FastDeal.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListFastDealItemAgentRequest extends jspb.Message {
  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  getAgentId(): string;
  setAgentId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealItemAgentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealItemAgentRequest): ListFastDealItemAgentRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealItemAgentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealItemAgentRequest;
  static deserializeBinaryFromReader(message: ListFastDealItemAgentRequest, reader: jspb.BinaryReader): ListFastDealItemAgentRequest;
}

export namespace ListFastDealItemAgentRequest {
  export type AsObject = {
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
    agentId: string,
  }
}

export class ListFastDealItemAgentResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FastDealItemSnapshot>;
  setItemsList(value: Array<FastDealItemSnapshot>): void;
  addItems(value?: FastDealItemSnapshot, index?: number): FastDealItemSnapshot;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealItemAgentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealItemAgentResponse): ListFastDealItemAgentResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealItemAgentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealItemAgentResponse;
  static deserializeBinaryFromReader(message: ListFastDealItemAgentResponse, reader: jspb.BinaryReader): ListFastDealItemAgentResponse;
}

export namespace ListFastDealItemAgentResponse {
  export type AsObject = {
    itemsList: Array<FastDealItemSnapshot.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class ListFastDealProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealProcessRequest): ListFastDealProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealProcessRequest;
  static deserializeBinaryFromReader(message: ListFastDealProcessRequest, reader: jspb.BinaryReader): ListFastDealProcessRequest;
}

export namespace ListFastDealProcessRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealProcessTypeRequest): ListFastDealProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListFastDealProcessTypeRequest, reader: jspb.BinaryReader): ListFastDealProcessTypeRequest;
}

export namespace ListFastDealProcessTypeRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealProductGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealProductGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealProductGroupRequest): ListFastDealProductGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealProductGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealProductGroupRequest;
  static deserializeBinaryFromReader(message: ListFastDealProductGroupRequest, reader: jspb.BinaryReader): ListFastDealProductGroupRequest;
}

export namespace ListFastDealProductGroupRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealBrandRequest): ListFastDealBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealBrandRequest;
  static deserializeBinaryFromReader(message: ListFastDealBrandRequest, reader: jspb.BinaryReader): ListFastDealBrandRequest;
}

export namespace ListFastDealBrandRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealProductRequest): ListFastDealProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealProductRequest;
  static deserializeBinaryFromReader(message: ListFastDealProductRequest, reader: jspb.BinaryReader): ListFastDealProductRequest;
}

export namespace ListFastDealProductRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealCurrencyRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealCurrencyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealCurrencyRequest): ListFastDealCurrencyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealCurrencyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealCurrencyRequest;
  static deserializeBinaryFromReader(message: ListFastDealCurrencyRequest, reader: jspb.BinaryReader): ListFastDealCurrencyRequest;
}

export namespace ListFastDealCurrencyRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealDeliveryConditionRequest): ListFastDealDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListFastDealDeliveryConditionRequest, reader: jspb.BinaryReader): ListFastDealDeliveryConditionRequest;
}

export namespace ListFastDealDeliveryConditionRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealPaymentConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealPaymentConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealPaymentConditionRequest): ListFastDealPaymentConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealPaymentConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealPaymentConditionRequest;
  static deserializeBinaryFromReader(message: ListFastDealPaymentConditionRequest, reader: jspb.BinaryReader): ListFastDealPaymentConditionRequest;
}

export namespace ListFastDealPaymentConditionRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealLogisticsOperatorRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealLogisticsOperatorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealLogisticsOperatorRequest): ListFastDealLogisticsOperatorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealLogisticsOperatorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealLogisticsOperatorRequest;
  static deserializeBinaryFromReader(message: ListFastDealLogisticsOperatorRequest, reader: jspb.BinaryReader): ListFastDealLogisticsOperatorRequest;
}

export namespace ListFastDealLogisticsOperatorRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealStateRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealStateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealStateRequest): ListFastDealStateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealStateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealStateRequest;
  static deserializeBinaryFromReader(message: ListFastDealStateRequest, reader: jspb.BinaryReader): ListFastDealStateRequest;
}

export namespace ListFastDealStateRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealCityRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FastDealFilter | undefined;
  setFilter(value?: FastDealFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealCityRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealCityRequest): ListFastDealCityRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealCityRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealCityRequest;
  static deserializeBinaryFromReader(message: ListFastDealCityRequest, reader: jspb.BinaryReader): ListFastDealCityRequest;
}

export namespace ListFastDealCityRequest {
  export type AsObject = {
    filter?: FastDealFilter.AsObject,
  }
}

export class ListFastDealNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPaymentConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCurrenciesList(): void;
  getCurrenciesList(): Array<v1_order_enum_pb.NameFilter>;
  setCurrenciesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCurrencies(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductGroupsList(): void;
  getProductGroupsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductGroupsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProductGroups(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearLogisticsOperatorsList(): void;
  getLogisticsOperatorsList(): Array<v1_order_enum_pb.NameFilter>;
  setLogisticsOperatorsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addLogisticsOperators(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearStatesList(): void;
  getStatesList(): Array<v1_order_enum_pb.NameFilter>;
  setStatesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addStates(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCitiesList(): void;
  getCitiesList(): Array<v1_order_enum_pb.NameFilter>;
  setCitiesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCities(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealNamesRequest): ListFastDealNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealNamesRequest;
  static deserializeBinaryFromReader(message: ListFastDealNamesRequest, reader: jspb.BinaryReader): ListFastDealNamesRequest;
}

export namespace ListFastDealNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    currenciesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productGroupsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    logisticsOperatorsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    statesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    citiesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListFastDealNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPaymentConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCurrenciesList(): void;
  getCurrenciesList(): Array<v1_order_enum_pb.DictItem>;
  setCurrenciesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCurrencies(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductGroupsList(): void;
  getProductGroupsList(): Array<v1_order_enum_pb.DictItem>;
  setProductGroupsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProductGroups(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearLogisticsOperatorsList(): void;
  getLogisticsOperatorsList(): Array<v1_order_enum_pb.DictItem>;
  setLogisticsOperatorsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addLogisticsOperators(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearStatesList(): void;
  getStatesList(): Array<v1_order_enum_pb.DictItem>;
  setStatesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addStates(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCitiesList(): void;
  getCitiesList(): Array<v1_order_enum_pb.DictItem>;
  setCitiesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCities(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealNamesResponse): ListFastDealNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealNamesResponse;
  static deserializeBinaryFromReader(message: ListFastDealNamesResponse, reader: jspb.BinaryReader): ListFastDealNamesResponse;
}

export namespace ListFastDealNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    currenciesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productGroupsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    logisticsOperatorsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    statesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    citiesList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

export class GetFastDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFastDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFastDealRequest): GetFastDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFastDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFastDealRequest;
  static deserializeBinaryFromReader(message: GetFastDealRequest, reader: jspb.BinaryReader): GetFastDealRequest;
}

export namespace GetFastDealRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFastDealResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FastDeal | undefined;
  setItem(value?: FastDeal): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFastDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFastDealResponse): GetFastDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFastDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFastDealResponse;
  static deserializeBinaryFromReader(message: GetFastDealResponse, reader: jspb.BinaryReader): GetFastDealResponse;
}

export namespace GetFastDealResponse {
  export type AsObject = {
    item?: FastDeal.AsObject,
  }
}

export class CreateFastDealRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FastDeal | undefined;
  setItem(value?: FastDeal): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateFastDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateFastDealRequest): CreateFastDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateFastDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateFastDealRequest;
  static deserializeBinaryFromReader(message: CreateFastDealRequest, reader: jspb.BinaryReader): CreateFastDealRequest;
}

export namespace CreateFastDealRequest {
  export type AsObject = {
    item?: FastDeal.AsObject,
  }
}

export class CreateFastDealResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateFastDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateFastDealResponse): CreateFastDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateFastDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateFastDealResponse;
  static deserializeBinaryFromReader(message: CreateFastDealResponse, reader: jspb.BinaryReader): CreateFastDealResponse;
}

export namespace CreateFastDealResponse {
  export type AsObject = {
    id: number,
  }
}

export class RejectFastDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectFastDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectFastDealRequest): RejectFastDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectFastDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectFastDealRequest;
  static deserializeBinaryFromReader(message: RejectFastDealRequest, reader: jspb.BinaryReader): RejectFastDealRequest;
}

export namespace RejectFastDealRequest {
  export type AsObject = {
    id: number,
  }
}

export class RejectFastDealResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectFastDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectFastDealResponse): RejectFastDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectFastDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectFastDealResponse;
  static deserializeBinaryFromReader(message: RejectFastDealResponse, reader: jspb.BinaryReader): RejectFastDealResponse;
}

export namespace RejectFastDealResponse {
  export type AsObject = {
  }
}

export class ApproveFastDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApproveFastDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ApproveFastDealRequest): ApproveFastDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApproveFastDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApproveFastDealRequest;
  static deserializeBinaryFromReader(message: ApproveFastDealRequest, reader: jspb.BinaryReader): ApproveFastDealRequest;
}

export namespace ApproveFastDealRequest {
  export type AsObject = {
    id: number,
  }
}

export class ApproveFastDealResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApproveFastDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ApproveFastDealResponse): ApproveFastDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApproveFastDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApproveFastDealResponse;
  static deserializeBinaryFromReader(message: ApproveFastDealResponse, reader: jspb.BinaryReader): ApproveFastDealResponse;
}

export namespace ApproveFastDealResponse {
  export type AsObject = {
  }
}

export class DoneFastDealRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  hasRating(): boolean;
  clearRating(): void;
  getRating(): v1_order_model_user_rating_pb.UserRating | undefined;
  setRating(value?: v1_order_model_user_rating_pb.UserRating): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneFastDealRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DoneFastDealRequest): DoneFastDealRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneFastDealRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneFastDealRequest;
  static deserializeBinaryFromReader(message: DoneFastDealRequest, reader: jspb.BinaryReader): DoneFastDealRequest;
}

export namespace DoneFastDealRequest {
  export type AsObject = {
    id: number,
    rating?: v1_order_model_user_rating_pb.UserRating.AsObject,
  }
}

export class DoneFastDealResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DoneFastDealResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DoneFastDealResponse): DoneFastDealResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DoneFastDealResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DoneFastDealResponse;
  static deserializeBinaryFromReader(message: DoneFastDealResponse, reader: jspb.BinaryReader): DoneFastDealResponse;
}

export namespace DoneFastDealResponse {
  export type AsObject = {
  }
}

export class UpdateFastDealItemStatusRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getStatus(): FastDealItem.StatusMap[keyof FastDealItem.StatusMap];
  setStatus(value: FastDealItem.StatusMap[keyof FastDealItem.StatusMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateFastDealItemStatusRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateFastDealItemStatusRequest): UpdateFastDealItemStatusRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateFastDealItemStatusRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateFastDealItemStatusRequest;
  static deserializeBinaryFromReader(message: UpdateFastDealItemStatusRequest, reader: jspb.BinaryReader): UpdateFastDealItemStatusRequest;
}

export namespace UpdateFastDealItemStatusRequest {
  export type AsObject = {
    id: number,
    status: FastDealItem.StatusMap[keyof FastDealItem.StatusMap],
  }
}

export class UpdateFastDealItemStatusResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateFastDealItemStatusResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateFastDealItemStatusResponse): UpdateFastDealItemStatusResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateFastDealItemStatusResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateFastDealItemStatusResponse;
  static deserializeBinaryFromReader(message: UpdateFastDealItemStatusResponse, reader: jspb.BinaryReader): UpdateFastDealItemStatusResponse;
}

export namespace UpdateFastDealItemStatusResponse {
  export type AsObject = {
  }
}

export class GetFastDealFileRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFastDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFastDealFileRequest): GetFastDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFastDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFastDealFileRequest;
  static deserializeBinaryFromReader(message: GetFastDealFileRequest, reader: jspb.BinaryReader): GetFastDealFileRequest;
}

export namespace GetFastDealFileRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFastDealFileResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_file_pb.File | undefined;
  setItem(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFastDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFastDealFileResponse): GetFastDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFastDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFastDealFileResponse;
  static deserializeBinaryFromReader(message: GetFastDealFileResponse, reader: jspb.BinaryReader): GetFastDealFileResponse;
}

export namespace GetFastDealFileResponse {
  export type AsObject = {
    item?: v1_order_file_pb.File.AsObject,
  }
}

export class ListFastDealFileRequest extends jspb.Message {
  getFastDealId(): number;
  setFastDealId(value: number): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealFileRequest): ListFastDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealFileRequest;
  static deserializeBinaryFromReader(message: ListFastDealFileRequest, reader: jspb.BinaryReader): ListFastDealFileRequest;
}

export namespace ListFastDealFileRequest {
  export type AsObject = {
    fastDealId: number,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFastDealFileResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_file_pb.File>;
  setItemsList(value: Array<v1_order_file_pb.File>): void;
  addItems(value?: v1_order_file_pb.File, index?: number): v1_order_file_pb.File;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFastDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFastDealFileResponse): ListFastDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFastDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFastDealFileResponse;
  static deserializeBinaryFromReader(message: ListFastDealFileResponse, reader: jspb.BinaryReader): ListFastDealFileResponse;
}

export namespace ListFastDealFileResponse {
  export type AsObject = {
    itemsList: Array<v1_order_file_pb.File.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class AddFastDealFileRequest extends jspb.Message {
  getFastDealId(): number;
  setFastDealId(value: number): void;

  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddFastDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddFastDealFileRequest): AddFastDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddFastDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddFastDealFileRequest;
  static deserializeBinaryFromReader(message: AddFastDealFileRequest, reader: jspb.BinaryReader): AddFastDealFileRequest;
}

export namespace AddFastDealFileRequest {
  export type AsObject = {
    fastDealId: number,
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class AddFastDealFileResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddFastDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AddFastDealFileResponse): AddFastDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddFastDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddFastDealFileResponse;
  static deserializeBinaryFromReader(message: AddFastDealFileResponse, reader: jspb.BinaryReader): AddFastDealFileResponse;
}

export namespace AddFastDealFileResponse {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteFastDealFileRequest extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteFastDealFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteFastDealFileRequest): DeleteFastDealFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteFastDealFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteFastDealFileRequest;
  static deserializeBinaryFromReader(message: DeleteFastDealFileRequest, reader: jspb.BinaryReader): DeleteFastDealFileRequest;
}

export namespace DeleteFastDealFileRequest {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteFastDealFileResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteFastDealFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteFastDealFileResponse): DeleteFastDealFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteFastDealFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteFastDealFileResponse;
  static deserializeBinaryFromReader(message: DeleteFastDealFileResponse, reader: jspb.BinaryReader): DeleteFastDealFileResponse;
}

export namespace DeleteFastDealFileResponse {
  export type AsObject = {
  }
}

