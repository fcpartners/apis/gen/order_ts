// package: fcp.order.v1.order
// file: v1/order/model_exchange_rate.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class ExchangeRate extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getBasicCurrency(): string;
  setBasicCurrency(value: string): void;

  getBasicValue(): number;
  setBasicValue(value: number): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  hasRateDate(): boolean;
  clearRateDate(): void;
  getRateDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setRateDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getBid(): number;
  setBid(value: number): void;

  getBidChange(): number;
  setBidChange(value: number): void;

  getAsk(): number;
  setAsk(value: number): void;

  getAskChange(): number;
  setAskChange(value: number): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ExchangeRate.AsObject;
  static toObject(includeInstance: boolean, msg: ExchangeRate): ExchangeRate.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ExchangeRate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ExchangeRate;
  static deserializeBinaryFromReader(message: ExchangeRate, reader: jspb.BinaryReader): ExchangeRate;
}

export namespace ExchangeRate {
  export type AsObject = {
    id: number,
    basicCurrency: string,
    basicValue: number,
    currency: string,
    rateDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    bid: number,
    bidChange: number,
    ask: number,
    askChange: number,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GetExchangeRateByCurrencyRequest extends jspb.Message {
  getBasicCurrency(): string;
  setBasicCurrency(value: string): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetExchangeRateByCurrencyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetExchangeRateByCurrencyRequest): GetExchangeRateByCurrencyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetExchangeRateByCurrencyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetExchangeRateByCurrencyRequest;
  static deserializeBinaryFromReader(message: GetExchangeRateByCurrencyRequest, reader: jspb.BinaryReader): GetExchangeRateByCurrencyRequest;
}

export namespace GetExchangeRateByCurrencyRequest {
  export type AsObject = {
    basicCurrency: string,
    currency: string,
  }
}

export class GetExchangeRateByCurrencyResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ExchangeRate | undefined;
  setItem(value?: ExchangeRate): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetExchangeRateByCurrencyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetExchangeRateByCurrencyResponse): GetExchangeRateByCurrencyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetExchangeRateByCurrencyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetExchangeRateByCurrencyResponse;
  static deserializeBinaryFromReader(message: GetExchangeRateByCurrencyResponse, reader: jspb.BinaryReader): GetExchangeRateByCurrencyResponse;
}

export namespace GetExchangeRateByCurrencyResponse {
  export type AsObject = {
    item?: ExchangeRate.AsObject,
  }
}

