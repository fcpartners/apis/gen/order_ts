// package: fcp.order.v1.order
// file: v1/order/model_supplier_info.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";

export class OffersBySupplier extends jspb.Message {
  getSupplierCode(): string;
  setSupplierCode(value: string): void;

  clearOffersList(): void;
  getOffersList(): Array<v1_order_model_offer_pb.Offer>;
  setOffersList(value: Array<v1_order_model_offer_pb.Offer>): void;
  addOffers(value?: v1_order_model_offer_pb.Offer, index?: number): v1_order_model_offer_pb.Offer;

  getOrdersHasOffersCount(): number;
  setOrdersHasOffersCount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OffersBySupplier.AsObject;
  static toObject(includeInstance: boolean, msg: OffersBySupplier): OffersBySupplier.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OffersBySupplier, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OffersBySupplier;
  static deserializeBinaryFromReader(message: OffersBySupplier, reader: jspb.BinaryReader): OffersBySupplier;
}

export namespace OffersBySupplier {
  export type AsObject = {
    supplierCode: string,
    offersList: Array<v1_order_model_offer_pb.Offer.AsObject>,
    ordersHasOffersCount: number,
  }
}

export class SupplierInfo extends jspb.Message {
  clearOffersBySupplierList(): void;
  getOffersBySupplierList(): Array<OffersBySupplier>;
  setOffersBySupplierList(value: Array<OffersBySupplier>): void;
  addOffersBySupplier(value?: OffersBySupplier, index?: number): OffersBySupplier;

  getOrderCount(): number;
  setOrderCount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SupplierInfo.AsObject;
  static toObject(includeInstance: boolean, msg: SupplierInfo): SupplierInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SupplierInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SupplierInfo;
  static deserializeBinaryFromReader(message: SupplierInfo, reader: jspb.BinaryReader): SupplierInfo;
}

export namespace SupplierInfo {
  export type AsObject = {
    offersBySupplierList: Array<OffersBySupplier.AsObject>,
    orderCount: number,
  }
}

export class ListSupplierInfoRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): v1_order_model_offer_pb.OfferFilter | undefined;
  setFilter(value?: v1_order_model_offer_pb.OfferFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSupplierInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSupplierInfoRequest): ListSupplierInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSupplierInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSupplierInfoRequest;
  static deserializeBinaryFromReader(message: ListSupplierInfoRequest, reader: jspb.BinaryReader): ListSupplierInfoRequest;
}

export namespace ListSupplierInfoRequest {
  export type AsObject = {
    filter?: v1_order_model_offer_pb.OfferFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSupplierInfoResponse extends jspb.Message {
  hasItems(): boolean;
  clearItems(): void;
  getItems(): SupplierInfo | undefined;
  setItems(value?: SupplierInfo): void;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSupplierInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSupplierInfoResponse): ListSupplierInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSupplierInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSupplierInfoResponse;
  static deserializeBinaryFromReader(message: ListSupplierInfoResponse, reader: jspb.BinaryReader): ListSupplierInfoResponse;
}

export namespace ListSupplierInfoResponse {
  export type AsObject = {
    items?: SupplierInfo.AsObject,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

